#ifndef __pcmrUtil_h

#include "pcmrTypes.h"
#include "gdcmPixelFormat.h"

BEGIN_PCMR_DECLS

PCMRCOMMON_EXPORT 
StatusType WriteBufferToPNG(const char* fileName, const gdcm::PixelFormat & pf,
                            const char *buffer, size_t resx, size_t resy,
                            double sx, double sy);

PCMRCOMMON_EXPORT 
StatusType WriteSliceToPNG(const char* fileDICOM, const char* filePNG,
                           unsigned int idx=0);

END_PCMR_DECLS;

#endif
