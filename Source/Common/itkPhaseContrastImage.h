#ifndef __itkPhaseContrastImage_h
#define __itkPhaseContrastImage_h

#include "itkImage.h"
#include <itkSymmetricSecondRankTensor.h>

namespace itk
{
  typedef unsigned char MaskPixelType;
  typedef float PCPixelType;
  typedef Vector<PCPixelType, 4>            Vector4DPixelType;
  typedef Vector<PCPixelType, 3>            Vector3DPixelType;
  typedef Vector<PCPixelType, 1>            Vector1DPixelType;
  typedef CovariantVector<PCPixelType, 3>   ConvariantVector3;
  typedef SymmetricSecondRankTensor<PCPixelType, 3> TensorPixelType;

  typedef Image<MaskPixelType, 3>
    MaskImage;
  typedef Image< PCPixelType, 4 >
    PhaseContrastImage;
  typedef Image< PCPixelType, 3 >
    PhaseContrast3DImage;
  typedef Image<Vector4DPixelType, 3>
    PhaseContrastVector4DImage;
  typedef Image<Vector3DPixelType, 3>
    PhaseContrastVector3DImage;
  typedef Image<Vector1DPixelType, 3>
    PhaseContrastVector1DImage;
  typedef Image<ConvariantVector3, 3> Covariant3DImage;
  typedef Image< TensorPixelType, 3> Tensor3DImageType;
}

#endif
