#ifndef __pcmrDefs_h
#define __pcmrDefs_h

#define BEGIN_PCMR_DECLS namespace pcmr {

#define END_PCMR_DECLS }

#define PCMR_TO_STRING0( x ) #x
#define PCMR_TO_STRING(  x )  PCMR_TO_STRING0( x )

#define PCMR_PASTE2( a, b) a##b
#define PCMR_PASTE( a, b) PCMR_PASTE2( a, b)

#endif
