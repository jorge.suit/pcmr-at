#include "pcmrTrackVTKObject.h"
#include "vtkObjectFactory.h" //for new() macro
#include <boost/foreach.hpp>
#include "vtkCommand.h"

BEGIN_PCMR_DECLS

vtkStandardNewMacro(TrackVTKObject);
 
TrackVTKObject::TrackVTKObject()
  : m_Registered(0), m_Deleted(0)
{
}
 
TrackVTKObject::~TrackVTKObject() 
{
  this->CheckPointers();
}
 
void TrackVTKObject::PrintSelf(ostream &os, vtkIndent indent)
{
}

void TrackVTKObject::OnObjectDelete(vtkObject *obj, unsigned long event, void*data)
{
  PointerMap::iterator it = this->m_PointerMap.find(obj);
  if (it == this->m_PointerMap.end())
    {
    std::cerr << "vtkObject at " << obj << " not found inside map\n";
    std::cerr.flush();
    }
  else
    {
    if (!it->second)
      {
      std::cerr << "vtkObject at " << it->first << " has being already delete\n";
      std::cerr.flush();
      }
    else
      {
      ++m_Deleted;
      }
    it->second = 0;
    }
}

void TrackVTKObject::CheckPointers()
{
  bool found = false;
  BOOST_FOREACH(PointerMap::value_type i, this->m_PointerMap)
    {
    if (i.second)
      {
      std::cout << "The vtkObject* " << i.first << " is still alive\n";
      std::cout.flush();
      found = true;
      }
    }
  if (!found)
    {
    std::cout << "From '" << this->GetName() << "', all allocated vtkObject has being released\n";
    std::cout << "**** " << this->m_Registered << " objects were registered\n";
    std::cout << "**** " << this->m_Deleted << " objects were deleted\n";
    std::cout.flush();
    }
}

void TrackVTKObject::TrackObjectPointer(vtkObject *ptr)
{
  /*
  std::cout << "TrackImageDataPointer: " 
            << ptr << "(" << ptr->GetReferenceCount() <<")" <<
            std::endl;
  */
  PointerMap::iterator it = this->m_PointerMap.find(ptr);
  if (it == this->m_PointerMap.end())
    {
    ++m_Registered;
    }
  this->m_PointerMap[ptr] = 1;
  ptr->AddObserver(vtkCommand::DeleteEvent,
                   this, &TrackVTKObject::OnObjectDelete);
}

END_PCMR_DECLS
