#include "pcmrTypes.h"
#include <string.h>

BEGIN_PCMR_DECLS

const char*GetStatusDescription( int i )
{
  static const char *_desc[] =
    {
      "undefined status",
      "ok",
      "manufacturer is no philips",
      "dicom is not 4D",
      "tag not found",
      "image type missmatch",
      "not a MR image storage",
      "not an enhanced MR image storage",
      "modality is not MR",
      "AcquisitionContrast is not FLOW_ENCODED",
      "MRAcquisitionType is not 3D",
      "PhaseContrast not equal to YES",
      "empty sequence of items",
      "length of sequence does not match expected size",
      "not enough memory",
      "unknown ComplexImageComponent",
      "invalid PCVelocity length",
      "PCVelocity is zero",
      "unable to read",
      "DICOM file is not a phase or magnitude image from a PCMR study",
      "DICOM file does not belong to multiple timesteps",
      "value missmatch",
      "CSA's item is empty",
      "Invalid DICOMDIR",
      "directory record sequence (0x0004,0x1220) not found",
      "directory record sequence (0x0004,0x1220) is empty",
      "path does not exists",
      "path is not a directory",
      "could not open file for writing",
      "fail reading image information",
      "invalid slice index",
      "invalid timestep",
      "invalid file name",
      "could not read image buffer",
      "unknown pixel type",
      "general error",
      "no flow x",
      "no flow y",
      "no flow z",
      "no mask",
      "sequence is not increasing",
      "invalid component direction"
   };
  if ( i < ST_UNDEFINED || i >= StatusTypeEnd )
    {
      i = 0;
    }
  return _desc[ i ];
}

const char*GetVelocityComponentName( int i )
{
  static const char *_name[] = { "UNKNOWN", "X", "Y", "Z" };
  if ( i < VC_UNKNOWN || i >= VelocityComponentTypeEnd )
    {
      i = 0;
    }
  return _name[ i ];
}

const char*GetPhaseDirectionName( int i )
{
  static const char *_name[] = { "UNKNOWN", "RL", "AP", "FH" };
  if ( i < PHASE_UNKNOWN || i >= PhaseDirectionTypeEnd )
    {
      i = 0;
    }
  return _name[ i ];
}

const char*GetComplexImageComponentName( int i )
{
  static const char *_name[] = { "UNKOWN", "MIXED", "MAGNITUDE", "PHASE" };
  if ( i < CIC_UNKNOWN || i >= ComplexImageComponentTypeEnd )
    {
      i = 0;
    }
  return _name[ i ];
}

ComplexImageComponentType GetComplexImageComponentId( const char *name )
{
  int i;
  for ( i = 0; i < ComplexImageComponentTypeEnd; i++ )
    {
      const char* _name = GetComplexImageComponentName( i );
      if ( !strncmp( _name, name, strlen(_name) ) )
        {
          return static_cast<ComplexImageComponentType>(i);
        }
    }
  return CIC_UNKNOWN;
}

END_PCMR_DECLS
