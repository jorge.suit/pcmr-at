#include "pcmrPhilipsScannerUnenhanced.h"
#include "pcmrPhilipsGDCMIO.h"
#include "pcmrEvents.h"
#include "itkPhaseContrastImage.h"

#include <iostream>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include "itkImageSeriesReader.h"
#include "itkGDCMImageIO.h"

using namespace boost::filesystem;

BEGIN_PHILIPS_DECLS

StatusType Scanner::Scan( const std::string &dir )
{
  path pathInputDir( dir );
  if ( !exists( pathInputDir ) )
    {
    //std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return PATH_NOEXISTS;
    }
  if ( !is_directory( pathInputDir ) )
    {
    //std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return PATH_NOTDIR;
    }
  this->Clear( );

  this->InvokeEvent(AwakeEvent());
  size_t progress = 0;
  pcmr::StatusType status = this->ScanDirectory( dir, progress );
  if ( status != pcmr::OK )
    {
    return status;
    }
  std::sort( this->m_SerieMagnitude.begin( ), this->m_SerieMagnitude.end( ) );
  std::sort( this->m_SeriePhaseRL.begin( ), this->m_SeriePhaseRL.end( ) );
  std::sort( this->m_SeriePhaseAP.begin( ), this->m_SeriePhaseAP.end( ) );
  std::sort( this->m_SeriePhaseFH.begin( ), this->m_SeriePhaseFH.end( ) );
  std::cout << "this->m_SerieMagnitude.size( ) =" << this->m_SerieMagnitude.size( )  << std::endl;
  std::cout << "this->m_SeriePhaseRL.size( ) =" << this->m_SeriePhaseRL.size( ) << std::endl;
  std::cout << "this->m_SeriePhaseAP.size( ) =" << this->m_SeriePhaseAP.size( ) << std::endl;
  std::cout << "this->m_SeriePhaseFH.size( ) =" << this->m_SeriePhaseFH.size( ) << std::endl;
  this->ComputeSpatialOrientation();
  std::cout << "Spatial Orientation = " << this->GetSpatialOrientation() << std::endl;
  return pcmr::OK;
}


typedef itk::PhaseContrast3DImage::DirectionType::InternalMatrixType DirectionMatrix;
typedef itk::ImageSeriesReader<itk::PhaseContrast3DImage> ReaderType;

static
std::string ComputeClosestRAICode(const DirectionMatrix &mat)
{
  // RAI codes for cardinal directions
  const static std::string rai_start("RAI"), rai_end("LPS");
  std::string rai_out("...");

  for(size_t i = 0; i < 3; i++)
    {
    // Get the direction of the i-th voxel coordinate
    vnl_vector<double> dir_i = mat.get_column(i);

    // Get the maximum angle with any axis
    double maxabs_i = dir_i.inf_norm();
    for(size_t off = 0; off < 3; off++)
      {
      // This trick allows us to visit (i,i) first, so that if one of the
      // direction cosines makes the same angle with two of the axes, we
      // can still assign a valid RAI code
      size_t j = (i + off) % 3;

      // Is j the best-matching direction?
      if(fabs(dir_i[j]) == maxabs_i)
        {
        rai_out[i] = dir_i[j] > 0 ? rai_start[j] : rai_end[j];
        break;
        }
      }
    }

  return rai_out;
}

void Scanner::ComputeSpatialOrientation()
{
  ReaderType::Pointer reader = ReaderType::New();
  itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();

  std::vector<std::string> oneVolume;
  this->GetMagnitudeFiles(0, oneVolume);

  reader->SetImageIO(gdcmImageIO);
  reader->SetFileNames(oneVolume);

  reader->Update();
  this->m_SpatialOrientation = ComputeClosestRAICode(reader->GetOutput()->GetDirection().GetVnlMatrix());
}

void Scanner::Clear( )
{
  this->m_NumberOfTimeSteps = 0;
  this->m_VelocityEncoding = 0.0;
  this->m_SerieMagnitude.clear( );
  this->m_SeriePhaseRL.clear( );
  this->m_SeriePhaseAP.clear( );
  this->m_SeriePhaseFH.clear( );
}

size_t Scanner::GetNumberOfFilesPerComponent( )
{
  return this->m_SerieMagnitude.size( );
}

size_t Scanner::GetNumberOfSlicesPerVolume( )
{
  return this->m_SerieMagnitude.size( ) / this->GetNumberOfPhases( );
}

size_t Scanner::GetNumberOfPhases( )
{
  return this->m_NumberOfTimeSteps;
}

void Scanner::GetTimeStepValues( std::vector<double> &timeValues )
{
  const size_t sizeVolume = this->GetNumberOfSlicesPerVolume( );
  const size_t numberOfTimeSteps = this->GetNumberOfPhases( );
  timeValues.clear( );
  for ( size_t i = 0; i < numberOfTimeSteps; i++ )
    {
    timeValues.push_back( this->m_SerieMagnitude[ i * sizeVolume ].time );
    }
}

double Scanner::GetLengthOfTimeStep()
{
  std::vector<double> timeValues;
  this->GetTimeStepValues( timeValues );
  if ( timeValues.size( ) <= 1 )
    {
    return 0.0;
    }
  double sum = 0;
  double last = timeValues[0];
  for ( int i = 1; i < timeValues.size( ); i++ )
    {
    double delta = timeValues[ i ] - last;
    sum += delta;
    last = timeValues[ i ];
    }
  return sum / ( timeValues.size( ) - 1 );
}

float Scanner::GetVelocityEncoding( )
{
  return this->m_VelocityEncoding;
}

void Scanner::GetMagnitudeFiles( size_t timeStep, std::vector<std::string> &files )
{
  const size_t sizeVolume = this->GetNumberOfSlicesPerVolume( );
  const size_t numberOfTimeSteps = this->GetNumberOfPhases( );
  const size_t indexBase = timeStep * sizeVolume;
  files.clear( );
  for ( size_t i = 0; i < sizeVolume; i++ )
    {
    files.push_back( this->m_SerieMagnitude[ indexBase + i ].path );
    }
}

std::string Scanner::GetMagnitudeCentralSlice( size_t timeStep )
{
  const size_t sizeVolume = this->GetNumberOfSlicesPerVolume( );
  const size_t indexBase = timeStep * sizeVolume;
  const size_t index = indexBase + (sizeVolume>>1);

  return this->m_SerieMagnitude[ index ].path;
}

StatusType Scanner::GetPhaseFiles( pcmr::PhaseDirectionType direction,
                                   size_t timeStep,
                                   std::vector<std::string> &files )
{
  const size_t sizeVolume = this->GetNumberOfSlicesPerVolume( );
  const size_t numberOfTimeSteps = this->GetNumberOfPhases( );
  const size_t indexBase = timeStep * sizeVolume;
  files.clear( );
  SliceContainerType *container = NULL;
  switch ( direction )
    {
    case pcmr::RL:
      container = &this->m_SeriePhaseRL;
      break;
    case pcmr::AP:
      container = &this->m_SeriePhaseAP;
      break;
    case pcmr::FH:
      container = &this->m_SeriePhaseFH;
      break;
    default:
      return pcmr::INV_DIRECTION;
    }
  for ( size_t i = 0; i < sizeVolume; i++ )
    {
    files.push_back( (*container)[ indexBase + i ].path );
    }
  return pcmr::OK;
}

std::string Scanner::GetPhaseCentralSlice( size_t timeStep,
                                           pcmr::PhaseDirectionType vc )
{
  const size_t sizeVolume = this->GetNumberOfSlicesPerVolume( );
  const size_t indexBase = timeStep * sizeVolume;
  const size_t index = indexBase + (sizeVolume>>1);
  switch ( vc )
    {
    case pcmr::RL:
      return this->m_SeriePhaseRL[ index ].path;
    case pcmr::AP:
      return this->m_SeriePhaseAP[ index ].path;
    case pcmr::FH:
      return this->m_SeriePhaseFH[ index ].path;
    default:
      return "";
    }
}

std::string Scanner::GetOneDicomFile( )
{
  return this->m_SerieMagnitude[ 0 ].path;
}

pcmr::StatusType Scanner::TryDICOMFile( const std::string &filename,
                                        SliceInfoType &sliceInfo,
                                        pcmr::ComplexImageComponentType &complexComponent,
                                        pcmr::VelocityComponentType &direction )
{

  StatusType status;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read( ) )
    {
    return pcmr::ST_NOREAD;
    }
   // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  std::string imageType;
  status = pcmr::Philips::EnhancedMRI::ReadImageType( ds, imageType );
  if ( status != pcmr::OK )
    {
    std::cerr << "Unable to read '(0008,0008) ImageType' from \"" << filename << "\"\n";
    return status;
    }

  if ( !imageType.compare( "ORIGINAL\\PRIMARY\\M_FFE\\M\\FFE" ) )
    {
    complexComponent = pcmr::MAGNITUDE;
    }
  else if ( !imageType.compare( "ORIGINAL\\PRIMARY\\VELOCITY MAP\\P\\PCA" ) )
    {
    complexComponent = pcmr::PHASE;
    }
  else
    {
    //std::cout << "'" << imageType << "'" << std::endl;
    return pcmr::UNK_COMPLEX_IC;
    }

  status = pcmr::Philips::EnhancedMRI::CheckMRAcquisitionType3D( ds );
  if ( status != pcmr::OK )
    {
    std::cout << "'" << filename << "' " << "is not 3D\n";
    if ( status == pcmr::TAG_NOTFOUND )
      {
      std::cerr << "Unable to read '(0018,0023) MRAcquisitionType' from \"" << filename << "\"\n";
      }
      return status;
    }

  float velocity[3];
  status = pcmr::Philips::EnhancedMRI::ReadPCVelocity( ds, velocity );
  if( status != pcmr::OK )
    {
    std::cerr << "While reading \"" << filename << "\" : "
              << pcmr::GetStatusDescription( status ) << std::endl;
    return status;
    }
  int idx = 0;
  int count = 0;
  for ( int i = 0; i < 3; i++ )
    {
    if ( velocity[i] != 0.0 )
      {
      count++;
      idx = i;
      }
    }
  if ( count == 0)
    {
    return pcmr::NULL_PCV;
    }
  // REVIEW: this index mapping does not correspond to the actual flow
  // component.
  // FH:     0     0    150     (this is phase Y sequence "IM_0005")
  // AP:     0     150  0       (this is phase X sequence "IM_0001")
  // RL:     150  0     0       (this is phase Z sequence "IM_0003")
  const pcmr::VelocityComponentType directionMap[] =
    { pcmr::X, pcmr::Y, pcmr::Z };
  if ( count == 1 )
    {
    direction = directionMap[ idx ];
    }
  else
    {
    return pcmr::INV_PCV_LENGTH;
    }
  sliceInfo.path = filename;
  unsigned int nphases;
  status = pcmr::Philips::EnhancedMRI::ReadNumberOfPhasesMR( ds, nphases );
  if ( status != pcmr::OK )
    {
    return status;
    }
  this->m_NumberOfTimeSteps = nphases;
  status = pcmr::Philips::EnhancedMRI::ReadSliceNumberMR( ds,
                                                          sliceInfo.sliceNumber );
  if ( status != pcmr::OK )
    {
    return status;
    }
  sliceInfo.venc = velocity[ idx ];
  status = pcmr::Philips::EnhancedMRI::ReadTriggerTime( ds, sliceInfo.time );
  if ( status == pcmr::OK )
    {
    if ( this->m_VelocityEncodingHint <= 0.0 )
      {
      this->m_VelocityEncodingHint = sliceInfo.venc;
      }
    }
  return status;
}

StatusType Scanner::ScanDirectory( const std::string &dir, size_t &progress )
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory( it->status( ) ) )
      {
      status = ScanDirectory( it->path( ).string( ), progress );
      }
    else
      {
      if ( !( ( ++progress ) % 50 ) )
        {
        this->InvokeEvent(AwakeEvent());
        }
      SliceInfoType sliceInfo;
      pcmr::ComplexImageComponentType complexComponent;
      pcmr::VelocityComponentType direction;

      status = this->TryDICOMFile( it->path().string(), sliceInfo,
                                   complexComponent, direction );
      if ( status == pcmr::OK && ( sliceInfo.venc == this->m_VelocityEncodingHint ) )
        {
        this->m_VelocityEncoding = this->m_VelocityEncodingHint;
        switch ( direction )
          {
          case pcmr::X:
            if ( complexComponent == pcmr::PHASE )
              {
              this->m_SeriePhaseRL.push_back( sliceInfo );
              }
            break;
          case pcmr::Y:
            if ( complexComponent == pcmr::PHASE )
              {
              this->m_SeriePhaseAP.push_back( sliceInfo );
              }
            else if ( complexComponent == pcmr::MAGNITUDE )
              {
              this->m_SerieMagnitude.push_back( sliceInfo );
              }
            else
              {
              }
            break;
          case pcmr::Z:
            if ( complexComponent == pcmr::PHASE )
              {
              this->m_SeriePhaseFH.push_back( sliceInfo );
              }
            break;
          }
        }
      }
    }
  return pcmr::OK;
}


END_PHILIPS_DECLS
