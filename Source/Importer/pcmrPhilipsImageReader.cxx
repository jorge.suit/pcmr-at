#include "pcmrPhilipsImageReader.h"
#include "itkExceptionObject.h"
#include "itkGDCMImageIO.h"
#include "itkImageFileReader.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkPermuteAxesImageFilter.h"

BEGIN_PHILIPS_DECLS

typedef itk::ImageFileReader< itk::PhaseContrastImage >
ReaderPhaseContrastType;

typedef itk::RegionOfInterestImageFilter< itk::PhaseContrastImage, itk::PhaseContrastImage >
PhaseContrastROIFilterType;

typedef itk::UnaryFunctorImageFilter< itk::PhaseContrastImage, itk::PhaseContrastImage, RescalerType >
PhaseRescalerFilterType;

typedef itk::PermuteAxesImageFilter<itk::PhaseContrastImage>
PermuterType;
  
itk::PhaseContrastImage::Pointer
ReadPhaseImage(const char* dicomFile,
               unsigned int numberOfPhases,
               unsigned int numberOfVolumes,
               const RescalerType& phaseRescale,
               float zSpacing,
               unsigned int minX, unsigned int maxX,
               unsigned int minY, unsigned int maxY)
{
  if (minX > maxX || minY > maxY)
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("invalid XY ROI definition");
      throw err;
    }

  itk::GDCMImageIO::Pointer gdcmIO = itk::GDCMImageIO::New();
  ReaderPhaseContrastType::Pointer reader = ReaderPhaseContrastType::New();
  reader->ReleaseDataFlagOn();
  reader->SetImageIO( gdcmIO );
  reader->SetFileName( dicomFile );
  reader->Update();
  itk::PhaseContrastImage::Pointer imgMixed = reader->GetOutput();
  imgMixed->DisconnectPipeline();
  itk::PhaseContrastImage::RegionType R0 = imgMixed->GetLargestPossibleRegion();
  itk::PhaseContrastImage::RegionType R1( R0 );

  // ITK must read/recognize only one time step
  if ( R0.GetSize( 3 ) != 1 )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Time dimension is expected to be one!");
      throw err;
    }

  // the requested phases and volumes must match the number of frames read.
  if ( R0.GetSize( 2 ) != numberOfPhases * 2 * numberOfVolumes )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Number of phases and volumes missmatch number of frames read");
      throw err;
   }

  // convert the stacked 3D big image in a real 4D
  R1.SetSize( 2, numberOfPhases );
  R1.SetSize( 3, 2 * numberOfVolumes );
  imgMixed->SetRegions( R1 );

  // fix the spacing if requested
  itk::PhaseContrastImage::SpacingType spacing0 = imgMixed->GetSpacing();
  if ( zSpacing > 0.0 )
    {
    spacing0[ 3 ] = zSpacing;
    imgMixed->SetSpacing( spacing0 );
    }
  imgMixed->Update();

  PhaseContrastROIFilterType::Pointer splitMP = PhaseContrastROIFilterType::New();
  splitMP->ReleaseDataFlagOn();
  splitMP->SetInput( imgMixed );

  itk::PhaseContrastImage::IndexType startROI;
  if (minX == maxX || minY == maxY)
    {
    startROI[0] = 0;
    startROI[1] = 0;
    }
  else 
    {
    startROI[0] = minX;
    startROI[1] = minY;
    }
  startROI[2] = 0;
  // the phase is stored at the second half of the mixed image
  startROI[3] = numberOfVolumes;
  
  itk::PhaseContrastImage::SizeType sizeROI;
  sizeROI = R1.GetSize();
  if (!(minX == maxX || minY == maxY))
    {
    sizeROI[0] = (maxX-minX-1);
    sizeROI[1] = (maxY-minY-1);
    }
  // keep the same size in dimension 2
  // the rest of the volumes
  sizeROI[3] = numberOfVolumes;
    
  itk::PhaseContrastImage::RegionType roi4D( startROI, sizeROI );
  splitMP->SetRegionOfInterest( roi4D );
  splitMP->Update();
  
  itk::PhaseContrastImage::Pointer imgPhase0 = splitMP->GetOutput();
  imgPhase0->DisconnectPipeline();

  // fix the origin
  itk::PhaseContrastImage::PointType origin4DFixed = imgPhase0->GetOrigin();
  origin4DFixed[3] = 0.0;
  imgPhase0->SetOrigin( origin4DFixed );
  
  // Philips store first the time dimension then the slice dimension,
  // so we must permute the axes.
  PermuterType::PermuteOrderArrayType order;

  PermuterType::Pointer permuter = PermuterType::New();
  permuter->ReleaseDataFlagOn();
  order[0] = 0;
  order[1] = 1;
  order[2] = 3;
  order[3] = 2;
  permuter->SetOrder( order );

  PhaseRescalerFilterType::Pointer rescaleFilter;
  if ( !phaseRescale.IsIdentity() )
    {
      //phaseRescale.Print( std::cout );
      rescaleFilter = PhaseRescalerFilterType::New();
      rescaleFilter->ReleaseDataFlagOn();
      rescaleFilter->SetFunctor( phaseRescale );
      rescaleFilter->SetInput( imgPhase0 );
      permuter->SetInput( rescaleFilter->GetOutput() );
    }
  else
    {
      //std::cout << "Rescaler is identity\n";
      permuter->SetInput( imgPhase0 );
    }
  permuter->Update();

  itk::PhaseContrastImage::Pointer imgPhase = permuter->GetOutput();
  imgPhase->DisconnectPipeline();

  return imgPhase;
}

itk::PhaseContrastImage::Pointer
ReadMagnitudeImage(const char* dicomFile,
                   unsigned int numberOfPhases,
                   unsigned int numberOfVolumes,
                   float zSpacing,
                   unsigned int minX, unsigned int maxX,
                   unsigned int minY, unsigned int maxY)
{
  if (minX > maxX || minY > maxY)
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("invalid XY ROI definition");
      throw err;
    }

  itk::GDCMImageIO::Pointer gdcmIO = itk::GDCMImageIO::New();
  ReaderPhaseContrastType::Pointer reader = ReaderPhaseContrastType::New();
  reader->ReleaseDataFlagOn();
  reader->SetImageIO( gdcmIO );
  reader->SetFileName( dicomFile );
  reader->Update();
  itk::PhaseContrastImage::Pointer imgMixed = reader->GetOutput();
  itk::PhaseContrastImage::RegionType R0 = imgMixed->GetLargestPossibleRegion();
  itk::PhaseContrastImage::RegionType R1( R0 );

  // ITK must read/recognize only one time step
  if ( R0.GetSize( 3 ) != 1 )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Time dimension is expected to be one!");
      throw err;
    }

  // the requested phases and volumes must match the number of frames read.
  if ( R0.GetSize( 2 ) != numberOfPhases * 2 * numberOfVolumes )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Number of phases and volumes missmatch number of frames read");
      throw err;
   }

  // convert the stacked 3D big image in a real 4D
  R1.SetSize( 2, numberOfPhases );
  R1.SetSize( 3, 2 * numberOfVolumes );
  imgMixed->SetRegions( R1 );
  // fix the spacing if requested
  itk::PhaseContrastImage::SpacingType spacing0 = imgMixed->GetSpacing();
  if ( zSpacing > 0.0 )
    {
    spacing0[ 3 ] = zSpacing;
    imgMixed->SetSpacing( spacing0 );
    }
  imgMixed->Update();

  PhaseContrastROIFilterType::Pointer splitMP = PhaseContrastROIFilterType::New();
  splitMP->ReleaseDataFlagOn();
  splitMP->SetInput( imgMixed );

  itk::PhaseContrastImage::IndexType startROI;
  if (minX == maxX || minY == maxY)
    {
    startROI[0] = 0;
    startROI[1] = 0;
    }
  else 
    {
    startROI[0] = minX;
    startROI[1] = minY;
    }
  startROI[2] = 0;
  // the magnitude is stored at the first half of the mixed image
  startROI[3] = 0;

  itk::PhaseContrastImage::SizeType sizeROI;
  sizeROI = R1.GetSize();
  if (!(minX == maxX || minY == maxY))
    {
    sizeROI[0] = (maxX-minX-1);
    sizeROI[1] = (maxY-minY-1);
    }
  // keep the same size in dimension 2
  sizeROI[3] = numberOfVolumes;
  
  itk::PhaseContrastImage::RegionType roi4D( startROI, sizeROI );
  splitMP->SetRegionOfInterest( roi4D );
  splitMP->Update();

  itk::PhaseContrastImage::Pointer imgMagnitude0 = splitMP->GetOutput();
  imgMagnitude0->DisconnectPipeline();

  // Philips store first the time dimension then the slice dimension,
  // so we must permute the axes.
  PermuterType::PermuteOrderArrayType order;

  PermuterType::Pointer permuter = PermuterType::New();
  permuter->ReleaseDataFlagOn();
  order[0] = 0;
  order[1] = 1;
  order[2] = 3;
  order[3] = 2;

  permuter->SetOrder( order );
  permuter->SetInput( imgMagnitude0 );
  permuter->Update();

  itk::PhaseContrastImage::Pointer imgMagnitude = permuter->GetOutput();
  imgMagnitude->DisconnectPipeline();

  return imgMagnitude;
}

PhaseContrastMixedImageType
ReadMixedImage(const char* dicomFile,
               unsigned int numberOfPhases,
               unsigned int numberOfVolumes,
               const RescalerType& phaseRescale,
               float zSpacing,
               unsigned int minX, unsigned int maxX,
               unsigned int minY, unsigned int maxY)
{
  if (minX > maxX || minY > maxY)
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("invalid XY ROI definition");
      throw err;
    }

  itk::GDCMImageIO::Pointer gdcmIO = itk::GDCMImageIO::New();
  ReaderPhaseContrastType::Pointer reader = ReaderPhaseContrastType::New();
  reader->ReleaseDataFlagOn();
  reader->SetImageIO( gdcmIO );
  reader->SetFileName( dicomFile );
  reader->Update();
  itk::PhaseContrastImage::Pointer imgMixed = reader->GetOutput();
  imgMixed->DisconnectPipeline();
  itk::PhaseContrastImage::RegionType R0 = imgMixed->GetLargestPossibleRegion();
  itk::PhaseContrastImage::RegionType R1( R0 );

  // ITK must read/recognize only one time step
  if ( R0.GetSize( 3 ) != 1 )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Time dimension is expected to be one!");
      throw err;
    }

  // the requested phases and volumes must match the number of frames read.
  if ( R0.GetSize( 2 ) != numberOfPhases * 2 * numberOfVolumes )
    {
      itk::ExceptionObject err(__FILE__, __LINE__);
      err.SetDescription("Number of phases and volumes missmatch number of frames read");
      throw err;
   }

  // convert the stacked 3D big image in a real 4D
  R1.SetSize( 2, numberOfPhases );
  R1.SetSize( 3, 2 * numberOfVolumes );
  imgMixed->SetRegions( R1 );
  // fix the spacing if requested
  itk::PhaseContrastImage::SpacingType spacing0 = imgMixed->GetSpacing();
  if ( zSpacing > 0.0 )
    {
    spacing0[ 3 ] = zSpacing;
    imgMixed->SetSpacing( spacing0 );
    }
  imgMixed->Update();

  PhaseContrastROIFilterType::Pointer splitMP = PhaseContrastROIFilterType::New();
  splitMP->ReleaseDataFlagOn();
  splitMP->SetInput( imgMixed );

  itk::PhaseContrastImage::IndexType startROI;
  startROI.Fill(0);

  itk::PhaseContrastImage::SizeType sizeROI;
  sizeROI = R1.GetSize();
  sizeROI[3] = numberOfVolumes;

  itk::PhaseContrastImage::RegionType roi4D( startROI, sizeROI );
  splitMP->SetRegionOfInterest( roi4D );
  splitMP->Update();

  itk::PhaseContrastImage::Pointer imgMagnitude0 = splitMP->GetOutput();
  imgMagnitude0->DisconnectPipeline();

  // Philips store first the time dimension then the slice dimension,
  // so we must permute the axes.
  PermuterType::PermuteOrderArrayType order;

  PermuterType::Pointer permuter = PermuterType::New();
  permuter->ReleaseDataFlagOn();
  order[0] = 0;
  order[1] = 1;
  order[2] = 3;
  order[3] = 2;
  permuter->SetOrder( order );
  permuter->SetInput( imgMagnitude0 );
  permuter->Update();

  itk::PhaseContrastImage::Pointer imgMagnitude = permuter->GetOutput();
  imgMagnitude->DisconnectPipeline();

  // now extract the phase 4D image, reuse the filters.
  
  roi4D.SetIndex( 3, numberOfVolumes );
  splitMP->SetRegionOfInterest( roi4D );
  splitMP->Update();

  itk::PhaseContrastImage::Pointer imgPhase0 = splitMP->GetOutput();
  imgPhase0->DisconnectPipeline();
  
  // fix the origin
  itk::PhaseContrastImage::PointType origin4DFixed = imgPhase0->GetOrigin();
  origin4DFixed[3] = 0.0;
  imgPhase0->SetOrigin( origin4DFixed );

  // Philips store first the time dimension then the slice dimension,
  // so we must permute the axes.

  PhaseRescalerFilterType::Pointer rescaleFilter;
  if ( !phaseRescale.IsIdentity() )
    {
      rescaleFilter = PhaseRescalerFilterType::New();
      rescaleFilter->ReleaseDataFlagOn();
      rescaleFilter->SetFunctor( phaseRescale );
      rescaleFilter->SetInput( imgPhase0 );
      rescaleFilter->Update();
      permuter->SetInput( rescaleFilter->GetOutput() );
    }
  else
    {
      permuter->SetInput( imgPhase0 );
    }
  permuter->Update();

  itk::PhaseContrastImage::Pointer imgPhase = permuter->GetOutput();
  imgPhase->DisconnectPipeline();

  return PhaseContrastMixedImageType( imgMagnitude, imgPhase );
}

END_PHILIPS_DECLS

