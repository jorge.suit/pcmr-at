#ifndef __pcmrImporterHelper_h
#define __pcmrImporterHelper_h

#include <string>
#include "itkPhaseContrastImage.h"
#include "pcmrDefs.h"
#include "pcmr4DImporter_Export.h"

BEGIN_PCMR_DECLS

void PCMR4DIMPORTER_EXPORT DumpDirections( const std::string & prompt, itk::PhaseContrast3DImage::ConstPointer image );

END_PCMR_DECLS

#endif

