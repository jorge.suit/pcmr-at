#ifndef __itkPhaseContrastTimeStdDevImageFilter_h
#define __itkPhaseContrastTimeStdDevImageFilter_h

#include "pcmrDefs.h"
#include "pcmr4DImporter_Export.h"
#include "itkImageToImageFilter.h"
#include "itkPhaseContrastImage.h"

namespace itk
{
/** \class PhaseContrastTimeStdDevImageFilter
 * \brief Implements the time-StdDev PC-MRA.
 *
 * \ingroup ImageFilters
 */

class PhaseContrastTimeStdDevImageFilter : public ImageToImageFilter< PhaseContrastImage, PhaseContrast3DImage >
{
public:
  /** Standard class typedefs. */
  typedef PhaseContrastTimeStdDevImageFilter             Self;
  typedef ImageToImageFilter< PhaseContrastImage, PhaseContrast3DImage > Superclass;
  typedef SmartPointer< Self >        Pointer;
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(PhaseContrastTimeStdDevImageFilter, ImageToImageFilter);
 
  /** The 4D magnitude image.*/
  void SetMagnitudeImage(const PhaseContrastImage* image);
 
  /** The 4D X component of the phase image.*/
  void SetPhaseXImage(const PhaseContrastImage* image);

  /** The 4D Y component of the phase image.*/
  void SetPhaseYImage(const PhaseContrastImage* image);

  /** The 4D Z component of the phase image.*/
  void SetPhaseZImage(const PhaseContrastImage* image);

  itkGetConstMacro(NoiseMaskThreshold, float);
  void SetNoiseMaskThreshold( float threshold );

  itkGetConstMacro(MaximumMagnitude, float);
  itkGetConstMacro(MaximumMagnitudeIndex, Superclass::InputImageType::IndexType)
  itkGetConstMacro(MinimumMagnitude, float);
  itkGetConstMacro(MinimumMagnitudeIndex, Superclass::InputImageType::IndexType)

  void Print( std::ostream & _out );
  
protected:
  PhaseContrastTimeStdDevImageFilter();
  ~PhaseContrastTimeStdDevImageFilter() {}

  /** This filter produce an image which has different dimension than
   * its inputs. As such, PhaseContrastTimeStdDevImageFilter needs
   * to provide an implementation for GenerateOutputInformation() in
   * order to inform the pipeline execution model.
   *
   * \sa ProcessObject::GenerateOutputInformaton()  */
  virtual void GenerateOutputInformation();

  virtual void BeforeThreadedGenerateData();
  virtual void AfterThreadedGenerateData();
 
  /** Does the real work. */
  virtual void ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);
  
private:
  PhaseContrastTimeStdDevImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &);  //purposely not implemented

  float m_NoiseMaskThreshold;
  float m_NoiseMaskThresholdValue;
  float m_MaximumMagnitude;
  float m_MinimumMagnitude;
  Superclass::InputImageType::IndexType m_MinimumMagnitudeIndex;
  Superclass::InputImageType::IndexType m_MaximumMagnitudeIndex;
};

}

#endif

