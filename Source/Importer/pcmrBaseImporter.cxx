#include "pcmrUtil.h"
#include "pcmrBaseImporter.h"

#include "itkImageFileWriter.h"

#include <boost/version.hpp>

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "gdcmReader.h"
#include "gdcmStringFilter.h"

using namespace boost::filesystem;

typedef itk::ImageFileWriter<itk::PhaseContrast3DImage> Writer3DType;

BEGIN_PCMR_DECLS

StatusType Write3DImageToStudy(itk::PhaseContrast3DImage::ConstPointer image,
                               const char *dirOutput, const char *patternName,
                               size_t t = 0 )
{
  typedef itk::ImageFileWriter< itk::PhaseContrast3DImage > Writer3DType;

  path pathOutput(dirOutput);
  pathOutput /= patternName;
  std::string outputName(pathOutput.string().c_str());
  std::stringstream ts; ts << t;
  boost::replace_all(outputName, "%t", ts.str());
  Writer3DType::Pointer writer = Writer3DType::New();
  writer->SetInput(image);
  writer->SetFileName(outputName);
  writer->Update();

  return OK;
}

#define STR_FORMAT_VERSION PCMR_TO_STRING(PCMR_PASTE(PCMR_PASTE(PCMR_FORMAT_VERSION_MAJOR, .), PCMR_FORMAT_VERSION_MINOR))

std::string BaseImporter::FormatVersion(STR_FORMAT_VERSION);

BaseImporter::BaseImporter()
  : m_Status(OK), m_SequenceDirectory("")
{
  this->m_MinimumX = this->m_MaximumX = 0;
  this->m_MinimumY = this->m_MaximumY = 0;
  this->InitExtendedDictionary();
}

BaseImporter::~BaseImporter()
{}

StatusType BaseImporter::SetOutputROI(size_t minX, size_t maxX, size_t minY, size_t maxY)
{
  this->m_MinimumX = minX;
  this->m_MaximumX = maxX;
  this->m_MinimumY = minY;
  this->m_MaximumY = maxY;

  return OK;
}

float BaseImporter::GetDefaultVelocityEncoding()
{
  return 150;
}

StatusType BaseImporter::WriteStudy(const char* studyDirectory)
{
  StatusType status;
  
  status = this->CheckOutputDirectory(studyDirectory);
  if (status != OK)
    {
    this->m_Status = status;
    return status;
    }

  this->InvokeEvent(StartPrepareWriteEvent());
  status = this->PrepareForWriting(studyDirectory);
  this->InvokeEvent(EndPrepareWriteEvent());
  if (status != OK) 
    {
    this->m_Status = status;
    return status;
    }
  
  this->InvokeEvent(StartWriteEvent());
  status = this->WriteStudyHeader(studyDirectory);
  if (status != OK) 
    {
    this->m_Status = status;
    this->InvokeEvent(EndWriteEvent());
    return status;
    }
  
  Write3DImageToStudy(this->GetPremaskImage(0), studyDirectory, "premask.vtk");
  this->InvokeEvent(AwakeEvent());

  size_t numberOfTimeStep = this->GetNumberOfTimeSteps();
  for ( size_t ts = 0; ts < numberOfTimeStep; ts++ )
    {
    Write3DImageToStudy(this->GetMagnitudeImage(ts), studyDirectory,
                        "mag_%t.vtk", ts);
    Write3DImageToStudy(this->GetPhaseImage(ts,0), studyDirectory,
                        "vct_%t_0.vtk", ts);
    Write3DImageToStudy(this->GetPhaseImage(ts,1), studyDirectory,
                        "vct_%t_1.vtk", ts);
    Write3DImageToStudy(this->GetPhaseImage(ts,2), studyDirectory,
                        "vct_%t_2.vtk", ts);    
    this->InvokeEvent(AwakeEvent());
    }
    this->InvokeEvent(EndWriteEvent());
  return OK;
}

StatusType BaseImporter::WriteRepresentativeSlice(const char* fileName)
{
  std::string nameDICOM("unknown");
  size_t indexSlice = 0;
  StatusType status = this->GetRepresentativeSlice(nameDICOM, indexSlice);
  if (status != OK)
    {
    return status;
    }
  std::cout << nameDICOM << " " << indexSlice << std::endl;
  return WriteSliceToPNG(nameDICOM.c_str(), fileName, indexSlice);
};

StatusType BaseImporter::WriteMagnitudeRepresentativeSlice(const char* fileName)
{
  std::string nameDICOM("unknown");
  size_t indexSlice = 0;
  StatusType status = this->GetMagnitudeRepresentativeSlice(nameDICOM, indexSlice);
  if (status != OK)
    {
    return status;
    }
  std::cout << nameDICOM << " " << indexSlice << std::endl;
  return WriteSliceToPNG(nameDICOM.c_str(), fileName, indexSlice);
};

StatusType BaseImporter::WritePhaseRepresentativeSlice(VelocityComponentType vc,
                                                       const char* fileName)
{
  std::string nameDICOM("unknown");
  size_t indexSlice = 0;
  StatusType status = this->GetPhaseRepresentativeSlice(vc, nameDICOM, indexSlice);
  if (status != OK)
    {
    return status;
    }
  std::cout << nameDICOM << " " << indexSlice << std::endl;
  return WriteSliceToPNG(nameDICOM.c_str(), fileName, indexSlice);
};

StatusType BaseImporter::CheckOutputDirectory(const char* studyDirectory)
{
  create_directory( studyDirectory );
  return OK;
}

StatusType BaseImporter::WriteStudyHeader(const char* studyDirectory)
{
  StatusType status1 = this->WriteStudyHeader_STH(studyDirectory);
  StatusType status2 = this->WriteStudyHeader_XML(studyDirectory);

  if (status1 != OK)
    {
    return status1;
    }
  if (status2 != OK)
    {
    return status1;
    }
  return OK;
}

StatusType BaseImporter::WriteStudyHeader_STH(const char* studyDirectory)
{
  path headerPath(studyDirectory);
  headerPath /= "header.sth";
  std::ofstream ofs(headerPath.c_str());
  
  if (ofs.good())
    {
    ofs << "time_step_count " << this->GetNumberOfTimeSteps() << std::endl
        << "venc " << this->GetVelocityEncoding() << std::endl
        << "patient_orientation " << "ASL" << std::endl
        << "image_orientation " << this->GetImageOrientationLabel() << std::endl
        << "vector_mag_min " << this->GetMinimumVelocity() << std::endl
        << "vector_mag_max " << this->GetMaximumVelocity() << std::endl
        << "length_time_step " << this->GetLengthOfTimeStep() << std::endl;
    this->WriteExtendedDictionary_STH(ofs);
    }
  ofs.close();
  return OK;
}

StatusType BaseImporter::WriteStudyHeader_XML(const char* studyDirectory)
{  
  property_tree::ptree pt;

  pt.put("Study.FormatVersion", this->GetFormatVersion());
  pt.put("Study.SourceSequence", this->GetSequenceDirectory());
  pt.put("Study.NumberOfTimeSteps", this->GetNumberOfTimeSteps());
  pt.put("Study.LengthOfTimeStep", this->GetLengthOfTimeStep());
  pt.put("Study.VelocityEncoding", this->GetVelocityEncoding());
  pt.put("Study.MaximumVelocity", this->GetMaximumVelocity());
  pt.put("Study.MinimumVelocity", this->GetMinimumVelocity());
  pt.put("Study.ImageOrientation", this->GetImageOrientationLabel());
  pt.put("Study.InvertComponentX", this->GetInvertComponentX( ) ? 1 : 0);
  pt.put("Study.InvertComponentY", this->GetInvertComponentY( ) ? 1 : 0 );
  pt.put("Study.InvertComponentZ", this->GetInvertComponentZ( ) ? 1 : 0 );

  std::vector<double> timeValues;
  if (this->GetTimeValues(timeValues) == 0)
    {
    for(size_t i = 0; i < this->GetNumberOfTimeSteps(); i++)
      {
      property_tree::ptree& node = pt.add_child("Study.TimeValues.Time", property_tree::ptree());
      node.put_value(timeValues[i]);
      }
    }
  
  this->WriteExtendedDictionary_XML(pt);
  
  path headerPath(studyDirectory);
  headerPath /= "header.xml";
  std::ofstream ofs(headerPath.c_str());
  
  if (ofs.good())
    {
#if (BOOST_VERSION >= 105600)
      property_tree::xml_writer_settings<std::string> settings('\t', 1);
#else
    property_tree::xml_writer_settings<char> settings('\t', 1);
#endif
    property_tree::xml_parser::write_xml(ofs, pt, settings);
    return OK;
    }
  else
    {
    return FAIL_OPEN_W;
    }
}

StatusType BaseImporter::WriteExtendedDictionary_STH(std::ofstream &out)
{
  out << "extended_fields {\n";
  for(std::vector<DicomField>::iterator it = this->m_ExtendedDictionary.begin();
      it != this->m_ExtendedDictionary.end(); ++it)
    {
    out << "{\"" <<it->m_Description <<  "\" \"" << it->m_Value << "\"}\n";
    }
  out << "}\n";

  return OK;
}

StatusType BaseImporter::WriteExtendedDictionary_XML(property_tree::ptree &pt)
{
  for(std::vector<DicomField>::iterator it = this->m_ExtendedDictionary.begin();
      it != this->m_ExtendedDictionary.end(); ++it)
    {
    std::string key("Study.ExtendedFields.");
    key += it->m_Key;
    std::string desc = key + ".Description";
    pt.put(desc, it->m_Description);
    std::string value = key + ".Value";
    pt.put(value, it->m_Value);
    }
  return OK;
}

void BaseImporter::InitExtendedDictionary()
{
  /*
  struct {
    gdcm::Tag tag;
    std::string description;
    std::string value;
  }*/
  DicomField table[] =
  {
    {gdcm::Tag(0x0008,0x0080), "InstitutionName", "Institution Name", ""},
    {gdcm::Tag(0x0008,0x1040), "Department", "Institutional Department Name", ""},
    {gdcm::Tag(0x0032,0x1032), "RequestPhysician", "Requesting Physician", ""},
    {gdcm::Tag(0x0008,0x0070), "Manufacturer", "Manufacturer", ""},
    {gdcm::Tag(0x0008,0x1090), "ManufacturerModel", "Manufacturer's Model Name", ""},
    {gdcm::Tag(0x0008,0x1010), "StationName", "Station Name", ""},
    {gdcm::Tag(0x0008,0x1030), "StudyDescription", "Study Description", ""},
    {gdcm::Tag(0x0008,0x103e), "SeriesDescription", "Series Description", ""},
    {gdcm::Tag(0x0008,0x0020), "StudyDate", "Study Date", ""},
    {gdcm::Tag(0x0008,0x0030), "StudyTime", "Study Time", ""},
    {gdcm::Tag(0x0018,0x1030), "ProtocolName", "Protocol Name", ""},
    {gdcm::Tag(0x0010,0x0010), "PatientName", "Patient's Name", ""},
    {gdcm::Tag(0x0010,0x0020), "PatientID", "Patient ID", ""},
    {gdcm::Tag(0x0010,0x0030), "PatientBirthDate", "Patient's Birth Date", ""},
    {gdcm::Tag(0x0010,0x0040), "PatientSex", "Patient's Sex", ""},
    {gdcm::Tag(0x0010,0x1030), "PatientWeight", "Patient's Weight", ""},
    {gdcm::Tag(0x0010,0x2000), "MedicalAlerts", "Medical Alerts", ""}
  };
  const size_t size = sizeof(table)/sizeof(table[0]);
  this->m_ExtendedDictionary.resize(size);
  std::copy(table, table+size, this->m_ExtendedDictionary.begin());
  std::cout << this->m_ExtendedDictionary[0] << std::endl;
}

StatusType BaseImporter::FillExtendedDictionary()
{
  std::string fileName = this->GetOneDicom();
  std::cout << fileName << "\n";
  gdcm::Reader reader;
  reader.SetFileName(fileName.c_str());
  if( !reader.Read() )
    {
    std::cerr << "Could not read: \"" << fileName << "\"" << std::endl;
    return ST_NOREAD;
    }
  gdcm::File &file = reader.GetFile();
  gdcm::StringFilter sf;
  sf.SetFile(file);
  //std::cout << "Attribute Value as String: " << "'" << sf.ToString( gdcm::Tag(0x0008,0x0008 ) ) << "'" << std::endl; 
  for(std::vector<DicomField>::iterator it = this->m_ExtendedDictionary.begin();
      it != this->m_ExtendedDictionary.end(); ++it)
    {
    if (file.GetDataSet().FindDataElement(it->m_Tag))
      {
      std::pair<std::string, std::string> p = sf.ToStringPair(it->m_Tag);
      std::cout << "'" << p.first << "', '" << p.second << "'" << std::endl;
      it->m_Value = p.second;
      }
    else
      {
      std::cout << "Element not found: " << *it << std::endl;
      }
    }
  return OK;
}

int BaseImporter::GetTimeValues(std::vector<double> & timeValues)
{
  timeValues.clear();
  this->GetTimeValuesInternal(timeValues);
  size_t T = timeValues.size();
  if (T && T != this->GetNumberOfTimeSteps())
    {
    std::cerr << "BaseImporter::GetTimeValues: TimeValues size missmath the number of time steps\n";
    return -1;
    }
  if (!T)
    {
    double delta = this->GetLengthOfTimeStep();
    for(size_t i = 0; i < T; ++i)
      {
      timeValues.push_back(delta*i);
      }
    }
  return 0;
}

END_PCMR_DECLS
