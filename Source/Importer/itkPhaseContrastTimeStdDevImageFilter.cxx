#include "itkPhaseContrastTimeStdDevImageFilter.h"
#include "itkObjectFactory.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageRegionConstIterator.h"
#include "itkMinimumMaximumImageCalculator.h"

namespace itk
{

PhaseContrastTimeStdDevImageFilter::PhaseContrastTimeStdDevImageFilter()
{
  this->SetNumberOfRequiredInputs(4);
  this->m_NoiseMaskThreshold = 0.1;
  this->m_NoiseMaskThresholdValue = 0.0;
}

void PhaseContrastTimeStdDevImageFilter::SetMagnitudeImage(const PhaseContrastImage* image)
{
  this->SetNthInput(0, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeStdDevImageFilter::SetPhaseXImage(const PhaseContrastImage* image)
{
  this->SetNthInput(1, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeStdDevImageFilter::SetPhaseYImage(const PhaseContrastImage* image)
{
  this->SetNthInput(2, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeStdDevImageFilter::SetPhaseZImage(const PhaseContrastImage* image)
{
  this->SetNthInput(3, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeStdDevImageFilter::SetNoiseMaskThreshold( float threshold )
{
  if ( threshold < 0.0 || threshold >= 1.0 )
    {
      threshold = 0.1;
    }
  this->m_NoiseMaskThreshold = threshold;
}

void PhaseContrastTimeStdDevImageFilter::Print( std::ostream & _out )
{
  _out << "NoiseMaskThreshold = " << this->m_NoiseMaskThreshold << "\n";
  _out << "NoiseMaskThresholdValue = " << this->m_NoiseMaskThresholdValue << "\n";
  _out << "MinimumMagnitude = " << this->m_MinimumMagnitude << "\n";
  _out << "MinimumMagnitudeIndex = " << this->m_MinimumMagnitudeIndex << "\n";
  _out << "MaximumMagnitude = " << this->m_MaximumMagnitude << "\n";
  _out << "MaximumMagnitudeIndex = " << this->m_MaximumMagnitudeIndex << "\n";
  this->GetOutput()->GetLargestPossibleRegion().Print( _out );
}

void PhaseContrastTimeStdDevImageFilter::GenerateOutputInformation()
{
  PhaseContrastImage::ConstPointer inputPtr = this->GetInput();
  PhaseContrast3DImage::Pointer outputPtr = this->GetOutput();
  
  if ( !outputPtr || !inputPtr )
    {
    return;
    }

  std::cout << "Computing output information\n";
  const Superclass::InputImageType::RegionType & inputRegion = inputPtr->GetLargestPossibleRegion();
  const Superclass::InputImageType::PointType & inputOrigin = inputPtr->GetOrigin();
  const Superclass::InputImageType::SpacingType & inputSpacing = inputPtr->GetSpacing();

  Superclass::OutputImageType::RegionType outputRegion;
  Superclass::OutputImageType::PointType outputOrigin;
  Superclass::OutputImageType::SpacingType outputSpacing;
  Superclass::OutputImageType::DirectionType outputDirection;
  
  for( unsigned int dim = 0; dim < 3; dim++ )
    {
      outputRegion.SetSize( dim, inputRegion.GetSize( dim ) );
      outputRegion.SetIndex( dim, inputRegion.GetIndex( dim ) );
      outputOrigin[ dim ] = inputOrigin[ dim ];
      outputSpacing[ dim ] = inputSpacing[ dim ];
    }
  
  outputDirection.SetIdentity();
 
  outputPtr->SetLargestPossibleRegion( outputRegion );
  outputPtr->SetOrigin( outputOrigin );
  outputPtr->SetSpacing( outputSpacing );
  outputPtr->SetDirection( outputDirection );
  outputPtr->SetNumberOfComponentsPerPixel( inputPtr->GetNumberOfComponentsPerPixel() );
} 
  
void PhaseContrastTimeStdDevImageFilter::BeforeThreadedGenerateData()
{
  typedef MinimumMaximumImageCalculator< PhaseContrastImage >  CalculatorType;
  CalculatorType::Pointer calculatorI = CalculatorType::New();
  calculatorI->SetImage( this->GetInput(0) );
  calculatorI->Compute();
  this->m_MaximumMagnitude = calculatorI->GetMaximum();
  this->m_MaximumMagnitudeIndex = calculatorI->GetIndexOfMaximum();
  this->m_MinimumMagnitude = calculatorI->GetMinimum();
  this->m_MinimumMagnitudeIndex = calculatorI->GetIndexOfMinimum();
  this->m_NoiseMaskThresholdValue = this->m_MinimumMagnitude + ( this->m_MaximumMagnitude - this->m_MinimumMagnitude ) * this->m_NoiseMaskThreshold;
  std::cout << "BeforeThreadedGenerateData: " << this->GetNumberOfThreads() << std::endl;
}

void PhaseContrastTimeStdDevImageFilter::AfterThreadedGenerateData()
{
}

void PhaseContrastTimeStdDevImageFilter::ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
  std::cout << "ThreadedGenerateData ID: " << threadId << std::endl;

  PhaseContrastImage::ConstPointer inMagnitude = this->GetInput(0);
  PhaseContrastImage::ConstPointer inPhaseX = this->GetInput(1);
  PhaseContrastImage::ConstPointer inPhaseY = this->GetInput(2);
  PhaseContrastImage::ConstPointer inPhaseZ = this->GetInput(3);
  PhaseContrast3DImage::Pointer output = this->GetOutput();

  // output iterator for the region of this thread
  ImageRegionIteratorWithIndex<PhaseContrast3DImage> itOut(output, outputRegionForThread);

  // the region to define the iterator for each pixel along time dimension
  Superclass::InputImageRegionType timeAxisRegion;
  Superclass::InputImageRegionType inputFullRegion = inMagnitude->GetLargestPossibleRegion();

  // the 4th dimension of the region must cover all time steps.
  timeAxisRegion.SetIndex( 3, inputFullRegion.GetIndex( 3 ) );
  const unsigned int N = inputFullRegion.GetSize( 3 );
  // timeAxisRegion is 1-dimensional region
  for ( unsigned int i = 0; i < 3; i ++ )
    {
      timeAxisRegion.SetSize( i, 1 );
    }
  timeAxisRegion.SetSize( 3, N );
  
  bool firstMinMax = true;
  float *rayM = new float[ N ];
  float *rayX = new float[ N ];
  float *rayY = new float[ N ];
  float *rayZ = new float[ N ];
  float *rayV = new float[ N ];
  while( !itOut.IsAtEnd() )
  {
    const Superclass::OutputImageType::IndexType &currentIndex = itOut.GetIndex();
    for ( unsigned int i = 0; i < 3; i ++ )
      {
        timeAxisRegion.SetIndex( i, currentIndex[i] );
      }
    // input iterator for for each input image
    ImageRegionConstIterator<PhaseContrastImage> itM(inMagnitude, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itX(inPhaseX, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itY(inPhaseY, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itZ(inPhaseZ, timeAxisRegion);

    float outValue = 0.0;
    float meanM = 0.0;
    float meanX = 0.0;
    float meanY = 0.0;
    float meanZ = 0.0;
    float meanV = 0.0;

    bool thresholded = false;
    size_t rayIndex = 0;
    while( !itM.IsAtEnd() )
      {
        float valueM = itM.Value();
        if ( valueM > this->m_NoiseMaskThresholdValue )
          {
          rayM[ rayIndex ] = valueM;
          float x = itX.Value();
          float y = itY.Value();
          float z = itZ.Value();
          float valueV2 = (x*x + y*y + z*z);
          float valueV = sqrt( valueV2 );
          rayX[ rayIndex ] = x;
          rayY[ rayIndex ] = y;
          rayZ[ rayIndex ] = z;
          rayV[ rayIndex ] = valueV;
          meanM += valueM;
          meanX += x;
          meanY += y;
          meanZ += z;
          meanV += valueV;
          } 
        else
          {
          thresholded = true;
          break;
          }
        ++itM;
        ++itX;
        ++itY;
        ++itZ;
        ++rayIndex;
      }
    if ( thresholded )
      {
      outValue = 0.0;
      }
    else
      {
      meanM /= N;
      meanX /= N;
      meanY /= N;
      meanZ /= N;
      meanV /= N;
      float stdDevM = 0.0;
      float stdDevX = 0.0;
      float stdDevY = 0.0;
      float stdDevZ = 0.0;
      float stdDevV = 0.0;
      float delta;
      for ( size_t i = 0; i < N; i++ )
        {
        delta = rayM[ i ] - meanM;
        stdDevM += delta*delta;
        delta = rayX[ i ] - meanX;
        stdDevX += delta*delta;
        delta = rayY[ i ] - meanY;
        stdDevY += delta*delta;
        delta = rayZ[ i ] - meanZ;
        stdDevZ= delta*delta;
        delta = rayV[ i ] - meanV;
        stdDevV += delta*delta;
        }
      outValue = stdDevM = sqrt( stdDevM / N );
      stdDevX = sqrt( stdDevX / N );
      stdDevY = sqrt( stdDevY / N );
      stdDevZ = sqrt( stdDevZ / N );
      stdDevV = sqrt( stdDevV / N );
      if ( stdDevX > outValue )
        {
        outValue = stdDevX;
        }
      if ( stdDevY > outValue )
        {
        outValue = stdDevY;
        }
      if ( stdDevZ > outValue )
        {
        outValue = stdDevZ;
        }
      if ( stdDevV > outValue )
        {
        outValue = stdDevV;
        }
      }
    itOut.Set( outValue );
    ++itOut;
  }
  delete []rayM;
  delete []rayX;
  delete []rayY;
  delete []rayZ;
  delete []rayV;
}

}
