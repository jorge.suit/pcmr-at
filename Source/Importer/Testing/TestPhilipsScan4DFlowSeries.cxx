#include <iostream>
#include <algorithm>
#include <boost/filesystem.hpp>
#include "pcmrPhilipsGDCMIO.h"
#include "gdcmIPPSorter.h"
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkPhaseContrastImage.h"
#include "itkOrientImageFilter.h"
#include "itkShiftScaleImageFilter.h"

typedef itk::GDCMImageIO ImageIOType;
typedef itk::ImageSeriesReader<itk::PhaseContrast3DImage> ReaderType;
typedef itk::ImageFileWriter<itk::PhaseContrast3DImage> WriterType;
typedef itk::OrientImageFilter<itk::PhaseContrast3DImage,
                               itk::PhaseContrast3DImage> OrienterType;
typedef itk::ShiftScaleImageFilter<itk::PhaseContrast3DImage,
                                   itk::PhaseContrast3DImage> ShiftScaleType;

using namespace boost::filesystem;

struct SliceInfoType
{
  std::string path;
  float time;
  unsigned int sliceNumber;
  SliceInfoType()
    : path( "" ), time( 0.0 ), sliceNumber( 0 ) { }

  bool operator < ( const SliceInfoType &a ) const
  {
    return ( this->time == a.time ?
             ( this->sliceNumber < a.sliceNumber ) :
             ( this->time < a.time ) );
  }

  SliceInfoType &operator = ( const SliceInfoType &a )
  {
    this->path = a.path;
    this->time = a.time;
    this->sliceNumber = a.sliceNumber;
	return *this;
  }
};

typedef std::vector< SliceInfoType > SliceContainerType;

pcmr::StatusType ReadImageType( const char *filename,
                                SliceInfoType &sliceInfo,
                                pcmr::ComplexImageComponentType &complexComponent,
                                pcmr::VelocityComponentType &direction,
                                size_t &numberOfPhases )
{
  pcmr::StatusType status;
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read( ) )
    {
    return pcmr::ST_NOREAD;
    }
   // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  status = pcmr::Philips::EnhancedMRI::CheckMRAcquisitionType3D( ds );
  if (status != pcmr::OK)
    {
    return status;
    }

  std::string imageType;

  status = pcmr::Philips::EnhancedMRI::ReadImageType( ds, imageType );
  if ( status != pcmr::OK )
    {
    std::cerr << "Unable to read 'Image Type' from \"" << filename << "\"\n";
    return status;
    }

  if ( !imageType.compare( "ORIGINAL\\PRIMARY\\M_FFE\\M\\FFE" ) )
    {
    complexComponent = pcmr::MAGNITUDE;
    }
  else if ( !imageType.compare( "ORIGINAL\\PRIMARY\\VELOCITY MAP\\P\\PCA" ) )
    {
    complexComponent = pcmr::PHASE;
    }
  else
    {
    //std::cout << "'" << imageType << "'" << std::endl;
    return pcmr::UNK_COMPLEX_IC;
    }

  float velocity[3];
  status = pcmr::Philips::EnhancedMRI::ReadPCVelocity( ds, velocity );
  if( status != pcmr::OK )
    {
    std::cerr << "While reading \"" << filename << "\" : "
              << pcmr::GetStatusDescription( status ) << std::endl;
    return status;
    }
  int idx = 0;
  int count = 0;
  for ( int i = 0; i < 3; i++ )
    {
    if ( velocity[i] != 0.0 )
      {
      count++;
      idx = i;
      }
    }
  if ( count == 0)
    {
    return pcmr::NULL_PCV;
    }
  // REVIEW: this index mapping does not correspond to the actual flow
  // component.
  // FH:     0     0    150     (this is phase Y sequence "IM_0005")
  // AP:     0     150  0       (this is phase X sequence "IM_0001")
  // RL:     150  0     0       (this is phase Z sequence "IM_0003")
  const pcmr::VelocityComponentType directionMap[] =
    //{ pcmr::Z, pcmr::X, pcmr::Y };
    { pcmr::X, pcmr::Y, pcmr::Z };
  if ( count == 1 )
    {
    direction = directionMap[ idx ];
    }
  else
    {
    return pcmr::INV_PCV_LENGTH;
    }
  sliceInfo.path = filename;
  unsigned int nphases;
  status = pcmr::Philips::EnhancedMRI::ReadNumberOfPhasesMR( ds, nphases );
  if ( status != pcmr::OK )
    {
    return status;
    }
  numberOfPhases = nphases;
  status = pcmr::Philips::EnhancedMRI::ReadSliceNumberMR( ds,
                                                          sliceInfo.sliceNumber );
  if ( status != pcmr::OK )
    {
    return status;
    }
  return pcmr::Philips::EnhancedMRI::ReadTriggerTime( ds, sliceInfo.time );
}

pcmr::StatusType ScanDirectory( const char *dir,
                                SliceContainerType &serieMagnitude,
                                SliceContainerType &serieFlowX,
                                SliceContainerType &serieFlowY,
                                SliceContainerType &serieFlowZ,
                                size_t &numberOfPhases )
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = ScanDirectory( it->path( ).string( ).c_str( ),
                              serieMagnitude,
                              serieFlowX,
                              serieFlowY,
                              serieFlowZ, numberOfPhases );
      }
    else
      {
      SliceInfoType sliceInfo;
      pcmr::ComplexImageComponentType complexComponent;
      pcmr::VelocityComponentType direction;

      status = ReadImageType( it->path().string().c_str(), sliceInfo,
                              complexComponent, direction, numberOfPhases );
      if ( status == pcmr::OK )
        {
        switch ( direction )
          {
          case pcmr::X:
            if ( complexComponent == pcmr::PHASE )
              {
              serieFlowX.push_back( sliceInfo );
              }
            break;
          case pcmr::Y:
            if ( complexComponent == pcmr::PHASE )
              {
              serieFlowY.push_back( sliceInfo );
              }
            else
              {
              }
            break;
          case pcmr::Z:
            if ( complexComponent == pcmr::PHASE )
              {
              serieFlowZ.push_back( sliceInfo );
              }
            else if ( complexComponent == pcmr::MAGNITUDE )
              {
              serieMagnitude.push_back( sliceInfo );
              }
            break;
          }
        }
      }
    }
  return pcmr::OK;
}

typedef itk::PhaseContrast3DImage::DirectionType::InternalMatrixType DirectionMatrix;
std::string ConvertDirectionMatrixToClosestRAICode(const DirectionMatrix &mat)
{
  // RAI codes for cardinal directions
  const static std::string rai_start("RAI"), rai_end("LPS");
  std::string rai_out("...");

  for(size_t i = 0; i < 3; i++)
    {
    // Get the direction of the i-th voxel coordinate
    vnl_vector<double> dir_i = mat.get_column(i);

    // Get the maximum angle with any axis
    double maxabs_i = dir_i.inf_norm();
    for(size_t off = 0; off < 3; off++)
      {
      // This trick allows us to visit (i,i) first, so that if one of the
      // direction cosines makes the same angle with two of the axes, we
      // can still assign a valid RAI code
      size_t j = (i + off) % 3;

      // Is j the best-matching direction?
      if(fabs(dir_i[j]) == maxabs_i)
        {
        rai_out[i] = dir_i[j] > 0 ? rai_start[j] : rai_end[j];
        break;
        }
      }
    }

  return rai_out;
}

int MatchLetterDirection(const char letter, const char raiCode[])
{
  for(int i = 0; i < 3; ++i)
    {
    if (raiCode[i] == letter)
      {
      return i;
      }
    }
  return -1;
}

bool MatchPhaseDirection(const char dirPhase[], const char raiCode[], int &index)
{
  index = MatchLetterDirection(dirPhase[0], raiCode);
  if(index >= 0)
    {
    return true;
    }
  index = MatchLetterDirection(dirPhase[1], raiCode);
  if(index >= 0)
    {
    return false;
    }
  index = -1;
  return false;
};

int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }
  SliceContainerType serieMagnitude;
  SliceContainerType serieFlowRL;
  SliceContainerType serieFlowAP;
  SliceContainerType serieFlowFH;
  size_t numberOfPhases = 0;
  ScanDirectory( argv[1], serieMagnitude, serieFlowRL, serieFlowAP, serieFlowFH,
                 numberOfPhases );

  std::cout << "Number Of Phases = " << numberOfPhases << std::endl;

  std::cout << "serieMagnitude.size() = " << serieMagnitude.size( ) << std::endl;
  std::cout << "serieFlowRL.size() = " << serieFlowRL.size( ) << std::endl;
  std::cout << serieFlowRL[0].path << std::endl;
  std::cout << "serieFlowAP.size() = " << serieFlowAP.size( ) << std::endl;
  std::cout << serieFlowAP[0].path << std::endl;
  std::cout << "serieFlowFH.size() = " << serieFlowFH.size( ) << std::endl;
  std::cout << serieFlowFH[0].path << std::endl;
  std::cout << '\"' << serieMagnitude[0].path << '\"' << std::endl;

  std::sort( serieFlowFH.begin( ), serieFlowFH.end( ) );
  std::vector<std::string> firstVolume;
  const size_t numberOfSlices =  serieFlowFH.size( ) / numberOfPhases;
  std::cout << "Number Of Slices = " << numberOfSlices << std::endl;
  for( int i = 0; i < numberOfSlices; i++ )
    {
    firstVolume.push_back( serieFlowFH[ numberOfSlices*3 + i ].path );
    }
  /*
  gdcm::IPPSorter ippSorter;
  if( !ippSorter.Sort( firstVolume ) )
    {
    std::cerr << "Could not sort" << std::endl;
    return -1;
    }
  */
  /*
  for (int i = 0; i < firstVolume.size( ); i++ )
    {
    std::cout << "cp \"" << firstVolume[ i ] << "\" /tmp/VV/." << std::endl;
    }
  */
  ImageIOType::Pointer gdcmIO = ImageIOType::New( );
  gdcmIO->LoadPrivateTagsOn();

  ReaderType::Pointer seriesReader = ReaderType::New( );

  seriesReader->SetImageIO( gdcmIO );
  seriesReader->SetFileNames( firstVolume );
  try
    {
    seriesReader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return -1;
    }
  std::cout << "Image Orientation Closest to: " <<
    ConvertDirectionMatrixToClosestRAICode(seriesReader->GetOutput()->GetDirection().GetVnlMatrix()) << std::endl;

  ShiftScaleType::Pointer shiftScaler = ShiftScaleType::New();

  WriterType::Pointer writer = WriterType::New( );
  writer->SetFileName( "/tmp/test_philips.vtk" );
  // OrienterType::Pointer orienter = OrienterType::New();
  // orienter->UseImageDirectionOn();
  // orienter->SetDesiredCoordinateOrientationToAxial();
  // orienter->SetInput(seriesReader->GetOutput( ));
  std::string strIntercept, strSlope;
  if(!gdcmIO->GetValueFromTag("0028|1052", strIntercept) ||
     !gdcmIO->GetValueFromTag("0028|1053", strSlope))
    {
    gdcmIO->GetValueFromTag("2005|1409", strIntercept);
    gdcmIO->GetValueFromTag("2005|140a", strSlope);
    std::cout << "using private intercept = " << strIntercept << std::endl;
    std::cout << "using private slope = " << strSlope << std::endl;
    double scale = atof(strSlope.c_str());
    double shift = atof(strIntercept.c_str()) / scale;
    shiftScaler->SetScale(scale);
    shiftScaler->SetShift(shift);
    shiftScaler->SetInput( seriesReader->GetOutput( ) );
    writer->SetInput( shiftScaler->GetOutput( ) );
    }
  else
    {
    std::cout << "The image was rescaled on READ" << std::endl;
    std::cout << "intercept used = " << strIntercept << std::endl;
    std::cout << "scale used = " << strSlope << std::endl;
    writer->SetInput( seriesReader->GetOutput( ) );
    }

  writer->Update( );


  firstVolume.clear( );
  std::sort( serieMagnitude.begin( ), serieMagnitude.end( ) );

  for( int i = 0; i < numberOfSlices; i++ )
    {
    firstVolume.push_back( serieMagnitude[ numberOfSlices*3 + i ].path );
    }
  seriesReader->SetFileNames( firstVolume );
  writer->SetFileName( "/tmp/test_philips_mag.vtk" );
  //orienter->SetInput(seriesReader->GetOutput( ));
  writer->SetInput(seriesReader->GetOutput( ) );
  writer->Update();
  return 0;
}
