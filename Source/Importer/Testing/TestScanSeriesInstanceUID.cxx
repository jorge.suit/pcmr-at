#include <iostream>
#include <map>
#include <list>
#include <boost/filesystem.hpp>
#include "pcmrGDCMIO.h"
#include <sstream>

using namespace boost::filesystem;

typedef std::list<std::string> ListOfNamesType;
typedef std::map< std::string, ListOfNamesType > MapType;

pcmr::StatusType ReadSeriesInstanceUID( const char *filename, std::string &keyUID )
{
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read( ) )
    {
    return pcmr::ST_NOREAD;
    }
   // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();


  std::string imageType;
  pcmr::StatusType status = pcmr::ReadImageType( ds, imageType );

  if (status != pcmr::OK )
    {
    std::cout << "Could not read image type " << pcmr::GetStatusDescription( status ) << std::endl;
    std::cout << "at file " << filename << std::endl;
    return status;
    }

  /*
  status = pcmr::Philips::EnhancedMRI::CheckMRAcquisitionType3D( ds );
  if ( status != pcmr::OK )
    {
    std::cout << "'" << filename << "' " << "is not 3D\n"; 
    if ( status == pcmr::TAG_NOTFOUND )
      {
      std::cerr << "Unable to read '(0018,0023) MRAcquisitionType' from \"" << filename << "\"\n";
      }
      return status; 
    }
  */

  return pcmr::ReadSeriesInstanceUID( ds, keyUID );
}

pcmr::StatusType ScanDirectory( const char *dir, MapType &mapTypes )
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = ScanDirectory( it->path().string().c_str(), mapTypes );
      }
    else
      {
      std::string keyUID;
      status = ReadSeriesInstanceUID( it->path().string().c_str(), keyUID );
      if ( status == pcmr::OK )
        {
        mapTypes[ keyUID ].push_back( it->path().string().c_str() );
        }
      }
    }
  return pcmr::OK;
}

int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }
  MapType map;

  ScanDirectory( argv[1], map );

  std::cout << map.size( ) << " series UID were found" << std::endl;

  for ( MapType::iterator it = map.begin(); it != map.end(); ++it )
    {
    std::cout << it->first << " (" << it->second.size() << " ) ...\n"
              << "  first = '" << *(it->second.begin()) << "'\n"
              << "  last  = '" << *(it->second.rbegin()) << "'\n";
    }
  return 0;
}
