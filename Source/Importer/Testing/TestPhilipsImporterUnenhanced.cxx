#include <iostream>
#include "pcmrPhilipsImporterUnenhanced.h"

int main( int argc, const char* argv[] )
{
  if ( argc != 3 )
    {
    std::cout << "Usage: " << argv[0] << " dirInput dirOutput\n";
    return -1;
    }

  pcmr::Philips::ImporterUnenhanced::Pointer importer = 
    pcmr::Philips::ImporterUnenhanced::New( );
  
  importer->SetSequenceDirectory( argv[ 1 ] );
  importer->ReadInformation( );
  importer->WriteStudy( argv[ 2 ] );
  return 0;
}

