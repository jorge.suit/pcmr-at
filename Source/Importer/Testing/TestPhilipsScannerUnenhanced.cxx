#include <iostream>
#include "pcmrImporterHelper.h"
#include "pcmrPhilipsScannerUnenhanced.h"
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkPhaseContrastImage.h"

typedef itk::GDCMImageIO ImageIOType;
typedef itk::ImageSeriesReader<itk::PhaseContrast3DImage> ReaderType;
typedef itk::ImageFileWriter<itk::PhaseContrast3DImage> WriterType;


int main( int argc, const char* argv[] )
{
  if ( argc != 2 )
    {
    std::cout << "Usage: " << argv[0] << " dir\n";
    return -1;
    }
  pcmr::Philips::Scanner::Pointer scanner = pcmr::Philips::Scanner::New( );
  pcmr::StatusType status = scanner->Scan( argv[1] );

  if ( status != pcmr::OK )
    {
    std::cerr << "Error while scanning: " << pcmr::GetStatusDescription( status ) << std::endl;
    return status;
    }

  std::cout << "Number of phases = " << scanner->GetNumberOfPhases( ) << std::endl;
  std::cout << "Number of slice per volume = " << scanner->GetNumberOfSlicesPerVolume( ) << std::endl;
  std::cout << "Number of files per component = " << scanner->GetNumberOfFilesPerComponent( ) << std::endl;
  std::cout << "Length of time-step = " << scanner->GetLengthOfTimeStep( ) << std::endl;
  std::cout << "Velocity Encoding = " << scanner->GetVelocityEncoding( ) << std::endl;
  std::cout << "Time Values = ";
  std::vector<double> timeValues;
  scanner->GetTimeStepValues( timeValues );
  for( int i = 0; i < timeValues.size(); i++ )
    {
    std::cout << ( i ? ", " : "" ) << timeValues[ i ];
    }
  std::cout << std::endl;
  std::cout << "Slice Phase RL: '" << scanner->GetPhaseCentralSlice( 3, pcmr::RL ) << "'\n";
  std::cout << "Slice Phase AP: '" << scanner->GetPhaseCentralSlice( 3, pcmr::AP ) << "'\n";
  std::cout << "Slice Phase FH: '" << scanner->GetPhaseCentralSlice( 3, pcmr::FH ) << "'\n";

  std::vector<std::string> oneVolume;
  scanner->GetPhaseFiles( pcmr::RL, 3, oneVolume );

  ImageIOType::Pointer gdcmIO = ImageIOType::New( );
  ReaderType::Pointer seriesReader = ReaderType::New( );

  seriesReader->SetImageIO( gdcmIO );
  seriesReader->SetFileNames( oneVolume );
  try
    {
    seriesReader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return -1;
    }
  //seriesReader->GetOutput( )->Print( std::cout );
  WriterType::Pointer writer = WriterType::New( );
  writer->SetFileName( "/tmp/test_philips_RL.vtk" );
  writer->SetInput( seriesReader->GetOutput( ) );
  writer->Update( );

  pcmr::DumpDirections("Direction for FlowRL:", seriesReader->GetOutput( ) );

  scanner->GetPhaseFiles( pcmr::AP, 3, oneVolume );
  seriesReader->SetFileNames( oneVolume );
  writer->SetFileName( "/tmp/test_philips_AP.vtk" );
  writer->SetInput( seriesReader->GetOutput( ) );
  writer->Update( );

  scanner->GetPhaseFiles( pcmr::FH, 3, oneVolume );
  seriesReader->SetFileNames( oneVolume );
  writer->SetFileName( "/tmp/test_philips_FH.vtk" );
  writer->SetInput( seriesReader->GetOutput( ) );
  writer->Update( );

  return 0;
}
