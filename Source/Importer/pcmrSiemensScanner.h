#ifndef __pcmrSiemensScanner_h
#define __pcmrSiemensScanner_h

#include "pcmrSiemensGDCMIO.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

BEGIN_SIEMENS_DECLS

struct PCStudyInfo
{
  ComplexImageComponentType m_ImageComponentType;
  unsigned int m_NumberOfVolumes;
  float m_TimeStep;
  float m_VelocityEncoding;
  
  PCStudyInfo()
  {
    Clear();
  }

  void Clear()
  {
    m_NumberOfVolumes = 0;
    m_TimeStep = 0.0;
    // this value must be read from DICOM, now it is fixed
    m_VelocityEncoding = 150.0;
  }

  ComplexImageComponentType GetImageComponentType() const
  {
    return m_ImageComponentType;
  }

  StatusType ReadFromDICOM(const char *pathDCM);

  StatusType ReadOtherFieldsFromCSA(gdcm::CSAHeader & csa);

  void Print() const
  {
    std::cout << "m_ImageType = " << this->m_ImageComponentType << "\n";
    std::cout << "m_NumberOfVolumes = " << this->m_NumberOfVolumes << "\n";
    std::cout << "m_TimeStep = " << this->m_TimeStep << "\n";
    std::cout << "m_VelocityEncoding = " << this->m_VelocityEncoding << "\n";
  }
};

struct PCSliceInfo
{
  std::string m_Path;
  unsigned int m_InstanceNumber;
  float m_TimeStep;

  /* The following filed may appears in the CSAHeader (0029,20,siemens csa header) */
  int m_FlowEncodingDirection;
  int m_PhaseEncodingDirectionPositive;

  bool operator<(const PCSliceInfo & a) const
  {
    if (this->m_FlowEncodingDirection == a.m_FlowEncodingDirection)
      {
        return this->m_InstanceNumber < a.m_InstanceNumber;
      }
    else if (this->m_FlowEncodingDirection == 2 || a.m_FlowEncodingDirection == 32)
      {
        return true;
      }
    else
      {
        return false;
      }
    // return m_FlowEncodingDirection this->m_InstanceNumber < a.m_InstanceNumber;
  }

  StatusType ReadFromDataSet(const gdcm::DataSet &ds,
                              ComplexImageComponentType &imageType);

  StatusType ReadOtherFieldsFromCSA(gdcm::CSAHeader &csa);
};

class PCMR4DIMPORTER_EXPORT Scanner : public itk::Object
{
 public:
  /** Standard class typedefs. */
  typedef Scanner                        Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro(Scanner, itk::Object);

  Scanner()
   : m_NumberOfSeries(0), m_NumberOfImages(0)
    {}
  
  const std::vector<PCSliceInfo>& GetSeriesMagnitude()
  {
    return this->m_SeriesMagnitude;
  }

  const std::vector<PCSliceInfo>& GetSeriesPhase()
  {
    return this->m_SeriesPhase;
  }

  size_t GetNumberOfSlicesPerVolume()
  {
    return this->m_SeriesMagnitude.size() / this->GetNumberOfTimeSteps();
  };

  size_t GetNumberOfTimeSteps()
  {
    return this->m_InfoFromMagnitude.m_NumberOfVolumes;
  }

  float GetLengthOfTimeStep()
  {
    return this->m_InfoFromMagnitude.m_TimeStep;
  }

  void GetTimeValues(std::vector<double> & timeValues);

  float GetVelocityEncoding()
  {
    return this->m_InfoFromMagnitude.m_VelocityEncoding;
  }
  
  const std::string & GetMagnitudeCentralSlice(size_t idxTimeStep);
  const std::string & GetPhaseCentralSlice(size_t idxTimeStep,
                                           VelocityComponentType vc);


  int SelectMagnitudeVolume(size_t idxTimeStep,
                            std::vector<std::string> &filenames);
  int SelectPhaseXVolume(size_t idxTimeStep,
                         std::vector<std::string> &filenames );
  int SelectPhaseYVolume(size_t idxTimeStep,
                         std::vector<std::string> &filenames);
  int SelectPhaseZVolume(size_t idxTimeStep,
                         std::vector<std::string> &filenames);

  StatusType Scan(const char* pathDirectory);

 protected:

  StatusType TryDICOM(const char *dcm);
  StatusType ScanDirectory0(const char* pathDirectory, size_t &progress);
  StatusType ScanDirectory(const char* pathDirectory);
  StatusType ScanDICOMDIR(const char* pathDICOMDIR);

  void Clear()
  {
    this->m_NumberOfSeries = 0;
    this->m_NumberOfImages = 0;
    this->m_SeriesMagnitude.clear();
    this->m_SeriesPhase.clear();
    this->m_InfoFromMagnitude.Clear();
    this->m_InfoFromPhase.Clear();
  }

  static StatusType InsertNewPcmrSlice(const gdcm::DataSet &ds,
                                       const char *pathDCM,
                                       std::vector<PCSliceInfo>& container );
  static StatusType InsertNewPcmrSlice(const char *pathDCM,
                                       std::vector<PCSliceInfo>& container);
 private:
  
  PCStudyInfo m_InfoFromMagnitude;
  PCStudyInfo m_InfoFromPhase;
  std::vector<PCSliceInfo> m_SeriesMagnitude;
  std::vector<PCSliceInfo> m_SeriesPhase;
  size_t m_NumberOfSeries;
  size_t m_NumberOfImages;
};

END_SIEMENS_DECLS

#endif
