#ifndef __pcmrBaseImporter_h
#define __pcmrBaseImporter_h

#include "pcmr4DImporter_Export.h"
#include <boost/property_tree/ptree.hpp>
#include "pcmrTypes.h"
#include "itkPhaseContrastImage.h"
#include "itkObject.h"
#include "pcmrEvents.h"
#include "itkCommand.h"
#include <vector>
#include "gdcmTag.h"

using namespace boost;

BEGIN_PCMR_DECLS

#define PCMR_FORMAT_VERSION_MAJOR 0
#define PCMR_FORMAT_VERSION_MINOR 5

class PCMR4DIMPORTER_EXPORT BaseImporter : public itk::Object
{
 public:
  /** Standard class typedefs. */
  typedef BaseImporter                   Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Run-time type information (and related methods). */
  itkTypeMacro(BaseImporter, itk::Object);

  BaseImporter();
  virtual ~BaseImporter();

  StatusType GetStatus()
  {
    return this->m_Status;
  }

  bool HasROI()
  {
    return !(this->m_MinimumX >= this->m_MaximumX || 
             this->m_MinimumY >= this->m_MaximumY);
  }

  void SetSequenceDirectory(const std::string & dir)
  {
    this->m_SequenceDirectory = dir;
  }

  const std::string &GetSequenceDirectory() const
  {
    return this->m_SequenceDirectory;
  }
  
  static const std::string &GetFormatVersion()
  {
    return BaseImporter::FormatVersion;
  }

  virtual StatusType ReadInformation() = 0;
  StatusType SetOutputROI(size_t minX, size_t maxX, size_t minY, size_t maxY);
  StatusType WriteStudy(const char* studyDirectory);
  StatusType WriteRepresentativeSlice(const char* fileName);
  StatusType WriteMagnitudeRepresentativeSlice(const char* fileName);
  StatusType WritePhaseRepresentativeSlice(VelocityComponentType vc, const char* fileName);
  virtual size_t GetNumberOfTimeSteps() = 0;
  virtual float GetLengthOfTimeStep() = 0;
  virtual float GetVelocityEncoding() = 0;
  virtual float GetMinimumVelocity() = 0;
  virtual float GetMaximumVelocity() = 0;
  virtual bool GetInvertComponentX() = 0;
  virtual bool GetInvertComponentY() = 0;
  virtual bool GetInvertComponentZ() = 0;
  virtual float GetDefaultVelocityEncoding();
  int GetTimeValues(std::vector<double> &timeValues);
  virtual std::string GetOneDicom() = 0;
  virtual std::string GetImageOrientationLabel()
    {
      return "ASL";
    }

  void InvokeAwakeEvent()
  {
    // forward the AwakeEvent.
    //std::cout << "forwarding AwakeEvent\n";
    this->InvokeEvent(AwakeEvent());
  }

  virtual void TestEvents()
  {
    this->InvokeEvent(StartScanEvent());
    this->InvokeEvent(StartPrepareWriteEvent());
    this->InvokeEvent(StartWriteEvent());
    this->InvokeEvent(AwakeEvent());
  }

 protected:
  virtual StatusType CheckOutputDirectory(const char* studyDirectory);
  StatusType WriteStudyHeader_STH(const char* studyDirectory);
  StatusType WriteStudyHeader_XML(const char* studyDirectory);
  StatusType WriteStudyHeader(const char* studyDirectory);
  StatusType WriteExtendedDictionary_STH(std::ofstream &out);
  StatusType WriteExtendedDictionary_XML(property_tree::ptree& pt);
  virtual void GetTimeValuesInternal(std::vector<double> &timeValues) = 0;

  virtual StatusType GetRepresentativeSlice(std::string &fileName, size_t &index) = 0;
  virtual StatusType GetMagnitudeRepresentativeSlice(std::string &fileName, size_t &index) = 0;
  virtual StatusType GetPhaseRepresentativeSlice(VelocityComponentType vc,
                                                 std::string &fileName, size_t &index) = 0;
  virtual StatusType PrepareForWriting(const char* studyDirectory) = 0;
  virtual itk::PhaseContrast3DImage::ConstPointer GetPremaskImage(size_t ts) = 0;
  virtual itk::PhaseContrast3DImage::ConstPointer GetMagnitudeImage(size_t ts) = 0;
  virtual itk::PhaseContrast3DImage::ConstPointer GetPhaseImage(size_t ts, size_t component) = 0;

  StatusType &GetStatusRef() { return m_Status; }
  size_t &GetMinimumX() { return m_MinimumX; }
  size_t &GetMaximumX() { return m_MaximumX; }
  size_t &GetMinimumY() { return m_MinimumY; }
  size_t &GetMaximumY() { return m_MaximumY; }

  void AddAwakeObserver(itk::Object *object)
  {
    itk::SimpleMemberCommand<BaseImporter>::Pointer awakeObserver;
    awakeObserver = itk::SimpleMemberCommand<BaseImporter>::New();
    awakeObserver->SetCallbackFunction(this, &BaseImporter::InvokeAwakeEvent);
    object->AddObserver(AwakeEvent(), awakeObserver);
  }

  /*
  class DicomField
  {
  public:
    DicomField(const DicomField& d)
      {
        this->m_Tag = d.m_Tag;
        this->m_Description = d.m_Description;
        this->m_Value = d.m_Value;
      }

    friend std::ostream & operator<<(std::ostream &out, const DicomField& d)
    {
      out << d.m_Description << ":" << d.m_Value;
      return out;
    }

    gdcm::Tag m_Tag;
    std::string m_Description;
    std::string m_Value;
  };
*/
  struct DicomField
  {
    gdcm::Tag m_Tag;
    std::string m_Key;
    std::string m_Description;
    std::string m_Value;
    friend std::ostream & operator<<(std::ostream &out, const DicomField& d)
    {
      out << d.m_Description << ":" << d.m_Value;
      return out;
    }
  };

  void InitExtendedDictionary();
  StatusType FillExtendedDictionary();

 private:
  static std::string FormatVersion;

  StatusType m_Status;
  std::string m_SequenceDirectory;
  size_t m_MinimumX, m_MaximumX, m_MinimumY, m_MaximumY;
  std::vector<DicomField> m_ExtendedDictionary;
};

END_PCMR_DECLS

#endif
