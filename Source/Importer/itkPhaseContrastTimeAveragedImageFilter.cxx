#include "itkPhaseContrastTimeAveragedImageFilter.h"
#include "itkObjectFactory.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageRegionConstIterator.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "pcmrEvents.h"

namespace itk
{

PhaseContrastTimeAveragedImageFilter::PhaseContrastTimeAveragedImageFilter()
{
  this->SetNumberOfRequiredInputs(4);
  this->m_NoiseMaskThreshold = 0.1;
  this->m_NoiseMaskThresholdValue = 0.0;
  this->m_MaximumMagnitude = 0.0;
  this->m_MaximumMagnitudeIndex.Fill(0);
  this->m_MinimumMagnitude = 0.0;
  this->m_MinimumMagnitudeIndex.Fill(0);
  this->m_MaximumVelocity = 0.0;
  this->m_MaximumVelocityIndex.Fill(0);
  this->m_MinimumVelocity = 0.0;
  this->m_MinimumVelocityIndex.Fill(0);
}

void PhaseContrastTimeAveragedImageFilter::SetMagnitudeImage(const PhaseContrastImage* image)
{
  this->SetNthInput(0, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeAveragedImageFilter::SetPhaseXImage(const PhaseContrastImage* image)
{
  this->SetNthInput(1, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeAveragedImageFilter::SetPhaseYImage(const PhaseContrastImage* image)
{
  this->SetNthInput(2, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeAveragedImageFilter::SetPhaseZImage(const PhaseContrastImage* image)
{
  this->SetNthInput(3, const_cast<PhaseContrastImage*>(image) );
}

void PhaseContrastTimeAveragedImageFilter::SetNoiseMaskThreshold( float threshold )
{
  if ( threshold < 0.0 || threshold >= 1.0 )
    {
      threshold = 0.1;
    }
  this->m_NoiseMaskThreshold = threshold;
}

void PhaseContrastTimeAveragedImageFilter::Print( std::ostream & _out )
{
  _out << "NoiseMaskThreshold = " << this->m_NoiseMaskThreshold << "\n";
  _out << "NoiseMaskThresholdValue = " << this->m_NoiseMaskThresholdValue << "\n";
  _out << "MinimumMagnitude = " << this->m_MinimumMagnitude << "\n";
  _out << "MinimumMagnitudeIndex = " << this->m_MinimumMagnitudeIndex << "\n";
  _out << "MaximumMagnitude = " << this->m_MaximumMagnitude << "\n";
  _out << "MaximumMagnitudeIndex = " << this->m_MaximumMagnitudeIndex << "\n";
  _out << "MinimumVelocity = " << this->m_MinimumVelocity << "\n";
  _out << "MinimumVelocityIndex = " << this->m_MinimumVelocityIndex << "\n";
  _out << "MaximumVelocity = " << this->m_MaximumVelocity << "\n";
  _out << "MaximumVelocityIndex = " << this->m_MaximumVelocityIndex << "\n";
  _out << "Output origin = " << this->GetOutput()->GetOrigin() << "\n";
  _out << "Output spacing = " << this->GetOutput()->GetSpacing() << "\n";
  this->GetOutput()->GetLargestPossibleRegion().Print( _out );
}

void PhaseContrastTimeAveragedImageFilter::GenerateOutputInformation()
{
  PhaseContrastImage::ConstPointer inputPtr = this->GetInput();
  PhaseContrast3DImage::Pointer outputPtr = this->GetOutput();
  
  if ( !outputPtr || !inputPtr )
    {
    return;
    }

  std::cout << "Computing output information\n";
  const Superclass::InputImageType::RegionType & inputRegion = inputPtr->GetLargestPossibleRegion();
  const Superclass::InputImageType::PointType & inputOrigin = inputPtr->GetOrigin();
  const Superclass::InputImageType::SpacingType & inputSpacing = inputPtr->GetSpacing();

  Superclass::OutputImageType::RegionType outputRegion;
  Superclass::OutputImageType::PointType outputOrigin;
  Superclass::OutputImageType::SpacingType outputSpacing;
  Superclass::OutputImageType::DirectionType outputDirection;
  
  for( unsigned int dim = 0; dim < 3; dim++ )
    {
      outputRegion.SetSize( dim, inputRegion.GetSize( dim ) );
      outputRegion.SetIndex( dim, inputRegion.GetIndex( dim ) );
      outputOrigin[ dim ] = inputOrigin[ dim ];
      outputSpacing[ dim ] = inputSpacing[ dim ];
    }
  
  outputDirection.SetIdentity();
 
  outputPtr->SetLargestPossibleRegion( outputRegion );
  outputPtr->SetOrigin( outputOrigin );
  outputPtr->SetSpacing( outputSpacing );
  outputPtr->SetDirection( outputDirection );
  outputPtr->SetNumberOfComponentsPerPixel( inputPtr->GetNumberOfComponentsPerPixel() );
} 
  
void PhaseContrastTimeAveragedImageFilter::BeforeThreadedGenerateData()
{
  typedef MinimumMaximumImageCalculator< PhaseContrastImage >  CalculatorType;
  CalculatorType::Pointer calculatorI = CalculatorType::New();
  calculatorI->SetImage( this->GetInput(0) );
  calculatorI->Compute();
  this->m_MaximumMagnitude = calculatorI->GetMaximum();
  this->m_MaximumMagnitudeIndex = calculatorI->GetIndexOfMaximum();
  this->m_MinimumMagnitude = calculatorI->GetMinimum();
  this->m_MinimumMagnitudeIndex = calculatorI->GetIndexOfMinimum();
  this->m_NoiseMaskThresholdValue = this->m_MinimumMagnitude + ( this->m_MaximumMagnitude - this->m_MinimumMagnitude ) * this->m_NoiseMaskThreshold;
  std::cout << "BeforeThreadedGenerateData: " << this->GetNumberOfThreads() << std::endl;
  this->m_MinimumAtThread.reserve( this->GetNumberOfThreads() );
  this->m_MinimumIndexAtThread.reserve( this->GetNumberOfThreads() );
  this->m_MaximumAtThread.reserve( this->GetNumberOfThreads() );
  this->m_MaximumIndexAtThread.reserve( this->GetNumberOfThreads() );
  this->InvokeEvent(pcmr::AwakeEvent());
}

void PhaseContrastTimeAveragedImageFilter::AfterThreadedGenerateData()
{
  this->m_MinimumVelocity = this->m_MinimumVelocity = -1.0;
  for ( unsigned int i = 0; i < this->GetNumberOfThreads(); i++ )
    {
    if ( i == 0 )
      {
      this->m_MinimumVelocity = this->m_MinimumAtThread[0];
      this->m_MinimumVelocityIndex = this->m_MinimumIndexAtThread[0];
      this->m_MaximumVelocity = this->m_MaximumAtThread[0];
      this->m_MaximumVelocityIndex = this->m_MaximumIndexAtThread[0];
      }
    else
      {
      float min = this->m_MinimumAtThread[i];
      float max = this->m_MaximumAtThread[i];
      if ( min < this->m_MinimumVelocity )
        {
        this->m_MinimumVelocity = min;
        this->m_MinimumVelocityIndex = this->m_MinimumIndexAtThread[i];
        }
      else if  ( max > this->m_MaximumVelocity )
        {
        this->m_MaximumVelocity = max;
        this->m_MaximumVelocityIndex = this->m_MaximumIndexAtThread[i];
        }
      }
    }
}

void PhaseContrastTimeAveragedImageFilter::ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
  std::cout << "ThreadedGenerateData ID: " << threadId << std::endl;

  PhaseContrastImage::ConstPointer inMagnitude = this->GetInput(0);
  PhaseContrastImage::ConstPointer inPhaseX = this->GetInput(1);
  PhaseContrastImage::ConstPointer inPhaseY = this->GetInput(2);
  PhaseContrastImage::ConstPointer inPhaseZ = this->GetInput(3);
  PhaseContrast3DImage::Pointer output = this->GetOutput();

  // output iterator for the region of this thread
  ImageRegionIteratorWithIndex<PhaseContrast3DImage> itOut(output, outputRegionForThread);

  // the region to define the iterator for each pixel along time dimension
  Superclass::InputImageRegionType timeAxisRegion;
  Superclass::InputImageRegionType inputFullRegion = inMagnitude->GetLargestPossibleRegion();

  // the 4th dimension of the region must cover all time steps.
  timeAxisRegion.SetIndex( 3, inputFullRegion.GetIndex( 3 ) );
  const unsigned int N = inputFullRegion.GetSize( 3 );
  // timeAxisRegion is 1-dimensional region
  for ( unsigned int i = 0; i < 3; i ++ )
    {
      timeAxisRegion.SetSize( i, 1 );
    }
  timeAxisRegion.SetSize( 3, N );
  
  float &minVelocity = this->m_MinimumAtThread[threadId];
  float &maxVelocity = this->m_MaximumAtThread[threadId];
  bool firstMinMax = true;

  size_t progress = 0;
  while( !itOut.IsAtEnd() )
  {
    const Superclass::OutputImageType::IndexType &currentIndex = itOut.GetIndex();
    for ( unsigned int i = 0; i < 3; i ++ )
      {
        timeAxisRegion.SetIndex( i, currentIndex[i] );
      }
    // input iterator for for each input image
    ImageRegionConstIterator<PhaseContrastImage> itM(inMagnitude, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itX(inPhaseX, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itY(inPhaseY, timeAxisRegion);
    ImageRegionConstIterator<PhaseContrastImage> itZ(inPhaseZ, timeAxisRegion);

    float outValue = 0.0;
    while( !itM.IsAtEnd() )
      {
        float valueM = itM.Value();
        if ( valueM > this->m_NoiseMaskThresholdValue )
          {
            float x = itX.Value();
            float y = itY.Value();
            float z = itZ.Value();
            float valueV2 = (x*x + y*y + z*z);
            float valueV = sqrt( valueV2 );
            if ( firstMinMax )
              {
              minVelocity = maxVelocity = valueV;
              this->m_MinimumIndexAtThread[threadId] = 
                this->m_MaximumIndexAtThread[threadId] = itM.GetIndex();
              firstMinMax = false;
              }
            else if ( valueV < minVelocity )
              {
              minVelocity = valueV;
              this->m_MinimumIndexAtThread[threadId] = itM.GetIndex();
              }
            else if ( valueV > maxVelocity )
              {
              maxVelocity = valueV;
              this->m_MaximumIndexAtThread[threadId] = itM.GetIndex();
              }
            outValue += valueM * valueM * valueV2;
          }
        else
          {
          outValue = 0.0;
          break;
          }
        ++itM;
        ++itX;
        ++itY;
        ++itZ;
      }
    itOut.Set( outValue/N );
    ++itOut;
    if (!(progress%5000))
      {
      this->InvokeEvent(pcmr::AwakeEvent());
      }
    ++progress;
  }
}

}
