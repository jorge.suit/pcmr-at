#include "pcmrPhilipsImporter.h"

#include <fstream>
#include "pcmrPhilipsGDCMIO.h"
#include "pcmrPhilipsImageReader.h"
#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>

using namespace boost::filesystem;

BEGIN_PHILIPS_DECLS

Importer::Importer()
  : BaseImporter()
{
  this->m_AverageTimeStep = -1.0;
  this->m_PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  this->m_EnhancedMRI_X = EnhancedMRI::New();
  this->m_EnhancedMRI_Y = EnhancedMRI::New();
  this->m_EnhancedMRI_Z = EnhancedMRI::New();
  this->m_Negator = NegateImageAdaptor::New();
  NegatePixelAccessor negator;
  this->m_Negator->SetPixelAccessor(negator);
  this->AddAwakeObserver(this->m_PCMRAFilter);
  this->AddAwakeObserver(this->m_EnhancedMRI_X);
  this->AddAwakeObserver(this->m_EnhancedMRI_Y);
  this->AddAwakeObserver(this->m_EnhancedMRI_Z);
}

Importer::~Importer()
{
}

boost::mutex io_mutex;
void read_dicom(EnhancedMRI::Pointer emri)
{
  io_mutex.lock();
  std::cout << "NOTICE: reading dicom " << emri->GetFileName() << "" << std::endl;
  io_mutex.unlock();

  emri->Read();
}

StatusType Importer::ReadInformation()
{
  path p_seqDirectory (this->GetSequenceDirectory());

  if (!exists(p_seqDirectory))
    {
    this->GetStatusRef() = PATH_NOEXISTS;
    return this->GetStatus();
    }

  if (!is_directory(p_seqDirectory))
    {
    this->GetStatusRef() = PATH_NOTDIR;
    return this->GetStatus();
    }
  // REVIEW: here we must check for DICOMDIR, not always we can find
  // the DICOM files IM_0001, IM_0003 and IM_0005 containing the
  // enhanced sequence.
  std::string pathFlowX, pathFlowY, pathFlowZ;
  this->GetStatusRef() = EnhancedMRI::ScanDirectory(p_seqDirectory.string().c_str(),
                                                    pathFlowX,
                                                    pathFlowY,
                                                    pathFlowZ);
  if (this->GetStatus() != OK)
    {
    return this->GetStatus();
    }
  this->m_FileNamePhaseX = pathFlowX;
  this->m_EnhancedMRI_X->SetFileName(pathFlowX.c_str());

  this->m_FileNamePhaseY = pathFlowY;
  this->m_EnhancedMRI_Y->SetFileName(pathFlowY.c_str());

  this->m_FileNamePhaseZ = pathFlowZ;
  this->m_EnhancedMRI_Z->SetFileName(pathFlowZ.c_str());

  this->InvokeEvent(StartScanEvent());
//#define PCMR_SEQUENCIAL_READ
#ifndef PCMR_SEQUENCIAL_READ  
  boost::thread threadX(boost::bind(&read_dicom,
                                    boost::ref(this->m_EnhancedMRI_X)));
  boost::thread threadY(boost::bind(&read_dicom,
                                    boost::ref(this->m_EnhancedMRI_Y)));
  boost::thread threadZ(boost::bind(&read_dicom,
                                    boost::ref(this->m_EnhancedMRI_Z)));
  threadX.join();
  threadY.join();
  threadZ.join();
#endif

#ifdef PCMR_SEQUENCIAL_READ 
  this->m_EnhancedMRI_X->Read();
#endif
  StatusType statusX = this->m_EnhancedMRI_X->GetStatus();
  if (statusX != OK)
    {
    this->GetStatusRef() = statusX;
    this->InvokeEvent(EndScanEvent());
    return this->GetStatus();
    }

#ifdef PCMR_SEQUENCIAL_READ 
  this->m_EnhancedMRI_Y->Read();
#endif
  StatusType statusY = this->m_EnhancedMRI_Y->GetStatus();
  if (statusY != OK)
    {
    this->GetStatusRef() = statusY;
    this->InvokeEvent(EndScanEvent());
    return this->GetStatus();
    }

#ifdef PCMR_SEQUENCIAL_READ 
  this->m_EnhancedMRI_Z->Read();
#endif
  StatusType statusZ = this->m_EnhancedMRI_Z->GetStatus();
  if (statusZ != OK)
    {
    this->GetStatusRef() = statusZ;
    this->InvokeEvent(EndScanEvent());
    return this->GetStatus();
    }
  this->FillExtendedDictionary();
  this->InvokeEvent(EndScanEvent());

  return OK;
}

size_t Importer::GetNumberOfTimeSteps()
{
  return this->m_EnhancedMRI_X->GetNumberOfPhases();
}

float Importer::GetLengthOfTimeStep()
{
  if (this->m_AverageTimeStep <= 0.0)
    {
    this->m_AverageTimeStep = this->ComputeAverageTimeStep();
    }
  return this->m_AverageTimeStep;
}

float Importer::GetVelocityEncoding()
{
  float venc = this->m_EnhancedMRI_X->GetPCVelocity();
  return venc <= 0 ? this->GetDefaultVelocityEncoding() : venc;
}

float Importer::GetMinimumVelocity()
{
  return 0.0;
}

float Importer::GetMaximumVelocity()
{
  return this->GetVelocityEncoding();
}

bool Importer::GetInvertComponentX()
{
  return false;
}

bool Importer::GetInvertComponentY()
{
  return true;
}

bool Importer::GetInvertComponentZ()
{
  return false;
}

std::string Importer::GetOneDicom()
{
  return this->m_EnhancedMRI_X->GetFileName();
}

void Importer::GetTimeValuesInternal(std::vector<double> &timeValues)
{
  const FrameType *aFrame;
  size_t n = this->m_EnhancedMRI_X->GetNumberOfPhases();
  for (size_t i = 0; i < n; i++)
    {
    aFrame = this->m_EnhancedMRI_X->GetFrameInfo(i);
    timeValues.push_back(aFrame->m_TriggerTime);
    }
}

StatusType Importer::GetRepresentativeSlice(std::string &fileName, size_t &sliceIndex)
{
  return this->GetMagnitudeRepresentativeSlice(fileName, sliceIndex);
}

StatusType Importer::GetMagnitudeRepresentativeSlice(std::string &fileName, size_t &sliceIndex)
{
  const FrameType *aFrame = this->m_EnhancedMRI_X->GetFrameInfo(0);
  size_t idealSlice = this->m_EnhancedMRI_X->GetNumberOfSlicesMR()/2;
  size_t idealTimeStep = 3;
  size_t idealFrameIndex = idealSlice * this->m_EnhancedMRI_X->GetNumberOfPhases() + idealTimeStep;
  fileName = this->m_FileNamePhaseX;
  sliceIndex = idealFrameIndex;
  return OK;
}

StatusType Importer::GetPhaseRepresentativeSlice(VelocityComponentType vc,
                                                 std::string &fileName, size_t &sliceIndex)
{
  size_t representativeTimeStep = 3;
  switch (vc)
    {
    case X:
      fileName = this->m_FileNamePhaseX;
    case Y:
      fileName = this->m_FileNamePhaseY;
    case Z:
      fileName = this->m_FileNamePhaseZ;
    default:
      return UNK_COMPLEX_IC;
    }
  size_t numberOfTimeSteps = this->GetNumberOfTimeSteps();
  size_t stackSize = this->m_EnhancedMRI_X->GetNumberOfSlicesMR();
  size_t centralSlice = stackSize / 2;
  // the phase frames are stored in the second half of the big DICOM
  size_t base = this->m_EnhancedMRI_X->GetNumberOfFrames()/2;
  // Philips run first time step index then slice number
  sliceIndex = base + stackSize * centralSlice + representativeTimeStep;
  return OK;
}

StatusType Importer::PrepareForWriting(const char* studyDirectory)
{
  const FrameType *magFrame = this->m_EnhancedMRI_X->GetFrameInfo(0);
  magFrame->Print(std::cout);
  const FrameType *flowFrame = this->m_EnhancedMRI_X->GetFrameInfo(this->m_EnhancedMRI_X->GetNumberOfFrames()/2);
  flowFrame->Print(std::cout);
  RescalerType phaseRescale(flowFrame->m_RescaleSlope/magFrame->m_RescaleSlope,
                            flowFrame->m_RescaleIntercept);
  this->m_Image4DMagnitude = ReadMagnitudeImage(this->m_FileNamePhaseX.c_str(), 
                                                this->m_EnhancedMRI_X->GetNumberOfPhases(), 
                                                this->m_EnhancedMRI_X->GetNumberOfSlicesMR(),
                                                magFrame->m_SpacingBetweenSlices,
                                                this->GetMinimumX(), this->GetMaximumX(),
                                                this->GetMinimumY(), this->GetMaximumY());
  this->m_Image4DPhaseX = ReadPhaseImage(this->m_FileNamePhaseX.c_str(), 
                                         this->m_EnhancedMRI_X->GetNumberOfPhases(), 
                                         this->m_EnhancedMRI_X->GetNumberOfSlicesMR(),
                                         phaseRescale,
                                         flowFrame->m_SpacingBetweenSlices,
                                         this->GetMinimumX(), this->GetMaximumX(),
                                         this->GetMinimumY(), this->GetMaximumY());
  this->m_Image4DPhaseY = ReadPhaseImage(this->m_FileNamePhaseY.c_str(), 
                                         this->m_EnhancedMRI_Y->GetNumberOfPhases(), 
                                         this->m_EnhancedMRI_Y->GetNumberOfSlicesMR(),
                                         phaseRescale,
                                         flowFrame->m_SpacingBetweenSlices,
                                         this->GetMinimumX(), this->GetMaximumX(),
                                         this->GetMinimumY(), this->GetMaximumY());
  this->m_Image4DPhaseZ = ReadPhaseImage(this->m_FileNamePhaseZ.c_str(), 
                                         this->m_EnhancedMRI_Z->GetNumberOfPhases(), 
                                         this->m_EnhancedMRI_Z->GetNumberOfSlicesMR(),
                                         phaseRescale,
                                         flowFrame->m_SpacingBetweenSlices,
                                         this->GetMinimumX(), this->GetMaximumX(),
                                         this->GetMinimumY(), this->GetMaximumY());
  std::cout << "NOTICE: All images read\n";
  this->m_PCMRAFilter->SetMagnitudeImage(this->m_Image4DMagnitude);
  this->m_PCMRAFilter->SetPhaseXImage(this->m_Image4DPhaseX);
  this->m_PCMRAFilter->SetPhaseYImage(this->m_Image4DPhaseY);
  this->m_PCMRAFilter->SetPhaseZImage(this->m_Image4DPhaseZ);
  this->m_PCMRAFilter->Update();
  this->m_PCMRAFilter->Print(std::cout);
  this->m_Negator->SetImage(this->m_Image4DPhaseZ);
  return OK;
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetPremaskImage(size_t ts)
{
  return this->m_PCMRAFilter->GetOutput();
}

template <class TImage4D>
itk::PhaseContrast3DImage::ConstPointer Extract3DVolume(TImage4D *Image4D,
                                                        size_t ts)
{
  typedef typename itk::ExtractImageFilter<TImage4D, itk::PhaseContrast3DImage>
    Extract3DType;

  typename Extract3DType::Pointer extractVOL = Extract3DType::New();
  extractVOL->SetInput(Image4D);
 
  typename TImage4D::IndexType startRegion = Image4D->GetLargestPossibleRegion().GetIndex();
  typename TImage4D::IndexType::SizeType sizeRegion = Image4D->GetLargestPossibleRegion().GetSize();
  startRegion[3] = ts;
  sizeRegion[3] = 0;
  typename TImage4D::RegionType regionDesired(startRegion, sizeRegion);
  extractVOL->SetExtractionRegion(regionDesired);
#if ITK_VERSION_MAJOR >= 4
  extractVOL->SetDirectionCollapseToIdentity(); // This is required.
#endif
  extractVOL->Update();
  return extractVOL->GetOutput();
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetMagnitudeImage(size_t ts)
{
  return Extract3DVolume(this->m_Image4DMagnitude.GetPointer(), ts);
}

itk::PhaseContrast3DImage::ConstPointer Importer::GetPhaseImage(size_t ts, size_t component)
{
  switch (component)
    {
    case 0:
      return Extract3DVolume(this->m_Image4DPhaseX.GetPointer(), ts);
      break;
    case 1:
      return Extract3DVolume(this->m_Image4DPhaseY.GetPointer(), ts);
      break;
    case 2:
      return Extract3DVolume(this->m_Negator.GetPointer(), ts);
      break;
    default:
      return NULL;
    }
}

float Importer::ComputeAverageTimeStep()
{
  float ave = 0.0;
  const FrameType *aFrame = this->m_EnhancedMRI_X->GetFrameInfo(0);
  float t0 = aFrame->m_TriggerTime;
  size_t n = this->m_EnhancedMRI_X->GetNumberOfPhases();
  for (size_t i = 1; i < n; i++)
    {
    aFrame = this->m_EnhancedMRI_X->GetFrameInfo(i);
    float dt = aFrame->m_TriggerTime - t0;
    ave += dt;
    t0 = aFrame->m_TriggerTime;
    }
  return ave / (n-1);
}

END_PHILIPS_DECLS
