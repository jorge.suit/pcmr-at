#ifndef __pcmrPolyMeshToSignedDistanceImageFilter_h
#define __pcmrPolyMeshToSignedDistanceImageFilter_h

#include <vtkSmartPointer.h>
#include "vtkImplicitPolyDataDistance.h"
#include "itkImageSource.h"
#include "itkPhaseContrastImage.h"
#include "pcmrDefs.h"
#include "pcmrFlowComputation_Export.h"

#define __USE_MULTITHREADING__

BEGIN_PCMR_DECLS

class PolyMeshToSignedDistanceImageFilter : public itk::ImageSource<itk::PhaseContrast3DImage>
{
public:
  /** Standard class typedefs. */
  typedef PolyMeshToSignedDistanceImageFilter            Self;
  typedef itk::ImageSource<itk::PhaseContrast3DImage>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;

  typedef itk::PhaseContrast3DImage                      OutputImageType;
  typedef itk::PhaseContrast3DImage::SizeType            SizeType;
  typedef itk::PhaseContrast3DImage::SpacingType         SpacingType;
  typedef itk::PhaseContrast3DImage::DirectionType       DirectionType;
  typedef itk::PhaseContrast3DImage::PointType           PointType;
  typedef itk::PhaseContrast3DImage::IndexType           IndexType;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(PolyMeshToSignedDistanceImageFilter, itk::ImageSource);
 
  /** Set/Get Size */
  itkSetMacro(Size, SizeType);
  itkGetConstMacro(Size, SizeType);

  /** Spacing (size of a pixel) of the output image. The
   * spacing is the geometric distance between image samples.
   * It is stored internally as double, but may be set from
   * float. \sa GetSpacing() */
  itkSetMacro(Spacing, SpacingType);
  virtual void SetSpacing(const double spacing[3]);

  virtual void SetSpacing(const float spacing[3]);

  itkGetConstReferenceMacro(Spacing, SpacingType);

  /** The origin of the output image. The origin is the geometric
   * coordinates of the index (0,0,...,0).  It is stored internally
   * as double but may be set from float.
   * \sa GetOrigin() */
  itkSetMacro(Origin, PointType);
  virtual void SetOrigin(const double origin[3]);

  virtual void SetOrigin(const float origin[3]);

  itkGetConstReferenceMacro(Origin, PointType);

  /** The Direction is a matix of direction cosines
   *  that specify the direction between samples.
   * */
  itkSetMacro(Direction, DirectionType);
  itkGetConstMacro(Direction, DirectionType);

  void SetInputMesh(vtkPolyData *input);
  void SetSignMask(itk::MaskImage *mask)
  {
    this->m_SignMask = mask;
  }
  vtkPolyData * GetInputMesh();

  void EvaluateNormal(const double pt[], double normal[]);
protected:
  PolyMeshToSignedDistanceImageFilter();
  ~PolyMeshToSignedDistanceImageFilter();

  virtual void GenerateOutputInformation();

#if defined(__USE_MULTITHREADING__)
  virtual void BeforeThreadedGenerateData();
  virtual void AfterThreadedGenerateData();
 
  /** Does the real work. */
  virtual void ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);
#else
  virtual void GenerateData();
#endif
  
private:
  PolyMeshToSignedDistanceImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &);  //purposely not implemented
  std::vector<vtkSmartPointer<vtkImplicitPolyDataDistance> > m_ImplicitFunctionAtThread;
  std::vector<vtkSmartPointer<vtkPolyData> > m_MeshInputAtThread;
  itk::MaskImage::Pointer m_SignMask;
  SizeType m_Size;

  SpacingType m_Spacing;

  DirectionType m_Direction;

  PointType m_Origin;        //start value
};

END_PCMR_DECLS

#endif
