#include "pcmrPolyMeshToSignedDistanceImageFilter.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "vtkPolyData.h"

BEGIN_PCMR_DECLS

void PolyMeshToSignedDistanceImageFilter::SetSpacing(const double spacing[3])
{
  SpacingType s;
  for(unsigned int i = 0; i < itk::PhaseContrast3DImage::ImageDimension; ++i)
    {
    s[i] = static_cast<SpacingType::ValueType>(spacing[i]);
    }
  this->SetSpacing(s);
}

void PolyMeshToSignedDistanceImageFilter::SetSpacing(const float spacing[3])
{
  itk::Vector< float, 3 > sf(spacing);
  SpacingType        s;
  s.CastFrom(sf);
  this->SetSpacing(s);
}

void PolyMeshToSignedDistanceImageFilter::SetOrigin(const double origin[3])
{
  PointType p(origin);

  this->SetOrigin(p);
}

void PolyMeshToSignedDistanceImageFilter::SetOrigin(const float origin[3])
{
  itk::Point< float, 3 > of(origin);
  PointType         p;
  p.CastFrom(of);
  this->SetOrigin(p);
}

PolyMeshToSignedDistanceImageFilter::PolyMeshToSignedDistanceImageFilter()
{
  //Initial image is 64 wide in each direction.
  for ( unsigned int i = 0; i < itk::PhaseContrast3DImage::GetImageDimension(); i++ )
    {
    m_Size[i] = 64;
    m_Spacing[i] = 1.0;
    m_Origin[i] = 0.0;
    }
  m_Direction.SetIdentity();
  this->m_MeshInputAtThread.push_back(vtkSmartPointer<vtkPolyData>::New());
  this->m_ImplicitFunctionAtThread.push_back(vtkSmartPointer<vtkImplicitPolyDataDistance>::New());
}

PolyMeshToSignedDistanceImageFilter::~PolyMeshToSignedDistanceImageFilter()
{
  /*
  for(size_t i = 0; i < this->m_ImplicitFunctionAtThread.size(); i++)
    {
    this->m_ImplicitFunctionAtThread[i]->Delete();
    this->m_ImplicitFunctionAtThread[i] = NULL;
    }
  */
}

void PolyMeshToSignedDistanceImageFilter::SetInputMesh(vtkPolyData *input)
{
  this->m_MeshInputAtThread[0] = input;
  this->m_ImplicitFunctionAtThread[0]->SetInput(input);
}

void PolyMeshToSignedDistanceImageFilter::GenerateOutputInformation()
{
  OutputImageType *output;
  IndexType     index;

  index.Fill(0);

  output = this->GetOutput(0);

  OutputImageType::RegionType largestPossibleRegion;
  largestPossibleRegion.SetSize(this->m_Size);
  largestPossibleRegion.SetIndex(index);
  output->SetLargestPossibleRegion(largestPossibleRegion);
  output->SetBufferedRegion(largestPossibleRegion);        // set the region
  output->SetRequestedRegion(largestPossibleRegion);       //

  output->SetSpacing(m_Spacing);
  output->SetOrigin(m_Origin);
  output->SetDirection(m_Direction);
  //output->Print(std::cout);
}

#if defined(__USE_MULTITHREADING__)

void PolyMeshToSignedDistanceImageFilter::BeforeThreadedGenerateData()
{
  if (this->m_ImplicitFunctionAtThread.size() != this->GetNumberOfThreads())
    {
    this->m_MeshInputAtThread.resize(this->GetNumberOfThreads(), NULL);
    this->m_ImplicitFunctionAtThread.resize(this->GetNumberOfThreads(), NULL);
    for(size_t i = 1; i < this->GetNumberOfThreads(); i++)
      {
      this->m_MeshInputAtThread[i] = vtkSmartPointer<vtkPolyData>::New();
      this->m_MeshInputAtThread[i]->DeepCopy(this->m_MeshInputAtThread[0]);
      this->m_ImplicitFunctionAtThread[i] = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
      this->m_ImplicitFunctionAtThread[i]->SetInput(this->m_MeshInputAtThread[i]);
      }
    }
  std::cout << "PolyMeshToSignedDistanceImageFilter is going to use " 
            << this->GetNumberOfThreads() << " threads\n";
}

void PolyMeshToSignedDistanceImageFilter::AfterThreadedGenerateData()
{
  std::cout << "PolyMeshToSignedDistanceImageFilter::AfterThreadedGenerateData\n";
}

void PolyMeshToSignedDistanceImageFilter::ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  vtkImplicitPolyDataDistance *dfun = this->m_ImplicitFunctionAtThread[threadId];

  itk::PhaseContrast3DImage::Pointer output = this->GetOutput();

  // output iterator for the region of this thread
  itk::ImageRegionIteratorWithIndex<itk::PhaseContrast3DImage> itOut(output, outputRegionForThread);
  while( !itOut.IsAtEnd() )
    {
    const Superclass::OutputImageType::IndexType &currentIndex = itOut.GetIndex();
    itk::PhaseContrast3DImage::PointType pt;
    double x[3];
    output->TransformIndexToPhysicalPoint(currentIndex, pt);
    for(int i = 0; i < 3; i++)
      {
      x[i] = pt[i];
      }
    double d = dfun->FunctionValue(x);
    if (this->m_SignMask)
      {
      float f = this->m_SignMask->GetPixel(currentIndex);
      if (!f && d < 0)
        {
        d *= -1;
        }
      }
    itOut.Set(d);
    ++itOut;
    }
}

#else

void PolyMeshToSignedDistanceImageFilter::GenerateData()
{
  std::cout << "PolyMeshToSignedDistanceImageFilter::GenerateData() called\n";
  
  vtkImplicitPolyDataDistance *dfun = this->m_ImplicitFunctionAtThread[0];

  itk::PhaseContrast3DImage::Pointer output = this->GetOutput();

  output->Allocate();   // allocate the image
  // output iterator for the region of this thread
  itk::ImageRegionIteratorWithIndex<itk::PhaseContrast3DImage> itOut(output, output->GetLargestPossibleRegion());
  while( !itOut.IsAtEnd() )
    {
    const Superclass::OutputImageType::IndexType &currentIndex = itOut.GetIndex();
    itk::PhaseContrast3DImage::PointType pt;
    double x[3];
    output->TransformIndexToPhysicalPoint(currentIndex, pt);
    x[0] = pt[0];
    x[1] = pt[1];
    x[2] = pt[2];
    itOut.Set(dfun->FunctionValue(x));
    ++itOut;
    }
}

#endif

void PolyMeshToSignedDistanceImageFilter::EvaluateNormal(const double pt[], double normal[])
{
  vtkImplicitPolyDataDistance *dfun = this->m_ImplicitFunctionAtThread[0];
  dfun->FunctionGradient(pt, normal);
}

END_PCMR_DECLS
