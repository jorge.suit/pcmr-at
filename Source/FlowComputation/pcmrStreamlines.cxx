#include "pcmrException.h"
#include "pcmrStreamlines.h"
#include "vtkNew.h"
#include "vtkStreamTracer.h"
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkPolyDataWriter.h"
#include "vtkPointSource.h"
#include "vtkArrayCalculator.h"

BEGIN_PCMR_DECLS

//#define __SCALE_TO_MMpSEC__

//#define __DUMP_CONFIGURATION__
void DumpConfiguration(const StreamlineConfiguration &conf)
{
  std::cout << "MaximumPropagation = " << conf.MaximumPropagation << std::endl;
  std::cout << "TerminalSpeed = " << conf.TerminalSpeed << std::endl;
  std::cout << "InitialIntegrationStep = " << conf.InitialIntegrationStep << std::endl;
  std::cout << "MinimumIntegrationStep = " << conf.MinimumIntegrationStep << std::endl;
  std::cout << "MaximumIntegrationStep = " << conf.MaximumIntegrationStep << std::endl;
  std::cout << "MaximumError = " << conf.MaximumError << std::endl;
  std::cout << "MaximumNumberOfSteps = " << conf.MaximumNumberOfSteps << std::endl;
  std::cout << "Solver = " << conf.Solver << std::endl;
  std::cout << "Direction = " << conf.Direction << std::endl;
};

StreamlineConfiguration::StreamlineConfiguration()
{
  // default value computed from ImageData, twice the maximum resolution
  this->MaximumPropagation = -1.0;
  this->TerminalSpeed = 0.1;
  this->InitialIntegrationStep = 1.0;
  this->MinimumIntegrationStep = 0.6;
  this->MaximumIntegrationStep = 3.0;
  this->MaximumError = 1.0e-6;
  this->MaximumNumberOfSteps = 2000;
  this->Solver = vtkStreamTracer::RUNGE_KUTTA45;
  this->Direction = vtkStreamTracer::FORWARD;
}

static
StatusType SetFilter(Flow4DReader* dataSet, size_t timeStep,
                     const SeedCollection& seeds,
                     const StreamlineConfiguration &conf,
                     vtkStreamTracer *filter)
{
  if (timeStep >= dataSet->GetNumberOfTimeSteps())
    {
    return INV_TIMESTEP;
    }
  const size_t n = seeds.size();
  if (n == 0)
    {
    return SQ_EMPTY;
    }
  vtkNew<vtkPolyData> pointSet;
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(n);
  for(size_t i = 0; i < n; i++)
    {
    points->SetPoint(i, seeds[i].x, seeds[i].y, seeds[i].z);
    }
  pointSet->SetPoints(points.GetPointer());
  vtkImageData *imgFlow = dataSet->GetFlowImage(Flow4DReader::FlowVector, timeStep);
  if (!imgFlow)
    {
    throw exception_null_pointer();
    }
  switch (conf.Solver)
    {
    case vtkStreamTracer::RUNGE_KUTTA4:
      filter->SetIntegratorTypeToRungeKutta4();
      break;
    case vtkStreamTracer::RUNGE_KUTTA45:
      filter->SetIntegratorTypeToRungeKutta45();
      break;
    default:
      break;
    }
  if (conf.MaximumError > 0)
    {
    filter->SetMaximumError(conf.MaximumError);
    }
  filter->SetMaximumNumberOfSteps(conf.MaximumNumberOfSteps);
  filter->SetSourceData(pointSet.GetPointer());
  
#ifdef __SCALE_TO_MMpSEC__
  vtkArrayCalculator *toMilimeterPerSec = vtkArrayCalculator::New();
  toMilimeterPerSec->AddVectorArrayName("Velocity");
  toMilimeterPerSec->SetFunction("10*Velocity");
  toMilimeterPerSec->SetResultArrayName("Velocity");
  toMilimeterPerSec->SetInputData(imgFlow);
  toMilimeterPerSec->Update();
  //toMilimeterPerSec->GetOutput()->Print(std::cout);
  filter->SetInputConnection(toMilimeterPerSec->GetOutputPort());
  // REVIEW: this technique is taken from vtkAlgorithm
  //  vtkTrivialProducer* tp = vtkTrivialProducer::New();
  //  tp->SetOutput(input);
  //  this->SetInputConnection(port, tp->GetOutputPort());
  //  tp->Delete();

  toMilimeterPerSec->Delete();
#else
  filter->SetInputData(imgFlow);
#endif


  // REVIEW: this parameter should be better understood
  float MaximumPropagation;
  if (conf.MaximumPropagation > 0.0)
    {
    MaximumPropagation = conf.MaximumPropagation;
    }
  else
    {
    int dims[3];
    int maxDim;
    imgFlow->GetDimensions(dims);
    if (dims[0]>dims[1])
      if (dims[0]>dims[2])
        {
        maxDim = dims[0];
        }
      else
        {
         maxDim = dims[2];
        }
    else
      if (dims[1]>dims[2])
        {
        maxDim = dims[1];
        }
      else
        {
        maxDim = dims[2];
        }
    MaximumPropagation = 2 * maxDim;
    }
#ifdef __DUMP_CONFIGURATION__
  StreamlineConfiguration _conf = conf;
  _conf.MaximumPropagation = MaximumPropagation;
  DumpConfiguration(_conf);
#endif

  filter->SetIntegrationStepUnit(vtkStreamTracer::CELL_LENGTH_UNIT);
  filter->SetMaximumPropagation(MaximumPropagation);
  filter->SetTerminalSpeed(conf.TerminalSpeed);
  filter->SetInitialIntegrationStep(conf.InitialIntegrationStep);
  filter->SetMinimumIntegrationStep(conf.MinimumIntegrationStep);
  filter->SetMaximumIntegrationStep(conf.MaximumIntegrationStep);
  filter->SetIntegrationDirection(conf.Direction);
  filter->SetComputeVorticity(0);
  filter->Update();
  return OK;
}

StatusType GenerateStreamlines(Flow4DReader* dataSet, size_t timeStep,
                               const SeedCollection& seeds,
                               const StreamlineConfiguration &conf,
                               const char* outputFile)
{
  if (outputFile == NULL || *outputFile == '\0')
    {
    return INV_FILENAME;
    }
  vtkNew<vtkStreamTracer> filter;
  StatusType status = SetFilter(dataSet, timeStep, seeds, conf, filter.GetPointer());
  if (status != OK)
    {
    return status;
    }

  vtkNew<vtkPolyDataWriter> writer;
  writer->SetFileName(outputFile);
#ifdef __SCALE_TO_MMpSEC__
  vtkNew<vtkArrayCalculator> toCentimeterPerSec;
  toCentimeterPerSec->AddVectorArrayName("Velocity");
  toCentimeterPerSec->SetFunction("0.1*Velocity");
  toCentimeterPerSec->SetResultArrayName("Velocity");
  toCentimeterPerSec->SetInputConnection(filter->GetOutputPort());
  writer->SetInputConnection(toCentimeterPerSec->GetOutputPort());
#else
  writer->SetInputConnection(filter->GetOutputPort());
#endif
  // REVIEW: if the writer fails for some reason we should report it.
  writer->Update();
  
  return OK;
}

StatusType GenerateStreamlines(Flow4DReader* dataSet, size_t timeStep,
                               const SeedCollection& seeds,
                               const StreamlineConfiguration &conf,
                               vtkSmartPointer<vtkPolyData> &streamLines)
{
  streamLines = NULL;
  vtkNew<vtkStreamTracer> filter;
  StatusType status = SetFilter(dataSet, timeStep, seeds, conf, filter.GetPointer());
  if (status != OK)
    {
    return status;
    }
#ifdef __SCALE_TO_MMpSEC__
  vtkNew<vtkArrayCalculator> toCentimeterPerSec;
  toCentimeterPerSec->AddVectorArrayName("Velocity");
  toCentimeterPerSec->SetFunction("0.1*Velocity");
  toCentimeterPerSec->SetResultArrayName("Velocity");
  toCentimeterPerSec->SetInputConnection(filter->GetOutputPort());
  toCentimeterPerSec->Update();
  streamLines = vtkPolyData::SafeDownCast(toCentimeterPerSec->GetOutput());
#else
  filter->Update();
  streamLines = filter->GetOutput();
#endif
  return OK;
}

END_PCMR_DECLS
