#include "vtkNew.h"
#include "pcmrTrackVTKObject.h"
#include "pcmrFlowComputationUtils.h"
#include "pcmrPathlines.h"
#include "pcmrTemporalVelocitySource.h"
#include "vtkParticlePathFilter.h"
#include "vtkPolyDataWriter.h"
#include <vtkPointData.h>
#include <vtkThreshold.h>
#include <vtkUnstructuredGrid.h>

BEGIN_PCMR_DECLS

//#define __THRESHOLD_RESULT__

//#define __TRACK_PATHLINE_POINTER__
#ifdef __TRACK_PATHLINE_POINTER__
#define NEW_TRACKER(n)                                  \
  vtkNew<TrackVTKObject> tracker;                       \
  tracker->SetName(n);

#define THE_TRACKER tracker.GetPointer()

#define TRACK_POINTER(m,p)                      \
  if(m)                                         \
    {                                           \
    m->TrackObjectPointer(p);                   \
    }
#define CHECK_POINTERS(m)                       \
  if(m)                                         \
    {                                           \
    m->CheckPointers();                         \
    }
#else
#define NEW_TRACKER(n)
#define THE_TRACKER NULL
#define TRACK_POINTER
#define CHECK_POINTERS
#endif

static
StatusType SetFilter(Flow4DReader* dataSet,
                     const SeedCollection& seeds,
                     const PathlineConfiguration &conf,
                     vtkParticlePathFilter *filter,
                     TrackVTKObject *tracker=NULL)
{
  // check errors in arguments
  const size_t n = seeds.size();
  if (n == 0)
    {
    return SQ_EMPTY;
    }
  int startTime = conf.StartTime;
  int endTime = conf.EndTime;
  vtkNew<Flow4DReaderAccessor> dataSetAccessor;
  TRACK_POINTER(THE_TRACKER, dataSetAccessor.GetPointer());
  dataSetAccessor->SetFlow4DReader(dataSet);
  if (startTime < 0)
    {
    startTime = 0;
    }
  if (endTime < 0)
    {
    endTime = static_cast<int>(dataSetAccessor->GetNumberOfTimeSteps())-1;
    }
  if (startTime > endTime ||
      startTime >= dataSetAccessor->GetNumberOfTimeSteps())
    {
    return INV_TIMESTEP;
    }

  vtkNew<TemporalVelocitySource> imageSource;
  imageSource->SetPointerTracker(tracker);
  TRACK_POINTER(THE_TRACKER, imageSource.GetPointer());
  // REVIEW: factor to convert to mm/s
  const double vfactor = 10.0;
  imageSource->SetRescaleFactor(vfactor);

  imageSource->SetDataSetAccessor(dataSetAccessor.GetPointer());
  vtkSmartPointer<vtkPolyData> pointSet = CreatePointSetFromCollection(seeds);

  filter->SetForceReinjectionEveryNSteps(0);
  filter->SetIntegratorType(vtkParticleTracerBase::RUNGE_KUTTA4);
  filter->SetInputConnection(0,imageSource->GetOutputPort());
  filter->SetInputData(1, pointSet);
  filter->SetComputeVorticity(0);
  filter->SetTerminalSpeed(conf.TerminalSpeed*vfactor);
  std::vector<double> timeValues;
  dataSetAccessor->GetTimeValues(timeValues);
  for(size_t i = 0; i < timeValues.size(); i++)
    {
    // assume it is given in mili-seconds
    timeValues[i] /= 1000.0;
    }
  size_t timeResolution = conf.TimeStepResolution;
  /*
  if (timeResolution <= 0)
    {
    timeResolution = 1;
    }
  */
  if (startTime >= timeValues.size())
    {
    startTime = static_cast<int>(timeValues.size())-1;
    }
  if (endTime >= timeValues.size())
    {
    endTime = static_cast<int>(timeValues.size())-1;
    }
  if (startTime == endTime)
    {
    timeResolution = 1;
    }
  /*
  if (startTime < 0)
    {
    startTime = timeValues[0];
    }
  if (endTime < 0)
    {
    endTime = timeValues[timeValues.size()-1];
    }
  */
  std::cout << "timeResolution = " << timeResolution<< std::endl;
  if (timeResolution == 1)
    {
    imageSource->SetBaseTimeStep(startTime);
    filter->SetStartTime(timeValues[startTime]);
    filter->SetTerminationTime(timeValues[endTime]);
    filter->Update();
    }
  else
    {
    double t0 = timeValues[startTime];
    filter->SetStartTime(t0);
    imageSource->SetBaseTimeStep(startTime);
    for (int j = startTime+1; j <= endTime; j++)
      {
      std::cout << "Time-step ["<<j-1<<","<<j<<"]\n";
      double t1 = timeValues[j];
      double dt = (t1-t0)/timeResolution;
      for (int i = 1; i < timeResolution; i++)
        {
        double t = t0 + i*dt;
        std::cout << "  sub time-step ("<<i<<") = " << t << std::endl;
        filter->SetTerminationTime(t);
        filter->Update();
        }
      filter->SetTerminationTime(t1);
      filter->Update();
      t0 = t1;
      }
    /*
    if (startTime < timeValues[0])
      {
      startTime = timeValues[0];
      }
    size_t i0;
    for (i0 = 1; 
         i0 < timeValues.size() && 
           startTime >= timeValues[i0]; ++i0) {}
    --i0;
    timeValues[i0] = startTime;
    if (endTime > timeValues[timeValues.size()-1])
      {
      endTime = timeValues[timeValues.size()-1];
      }
    size_t i1;
    for (i1 = timeValues.size()-1;
         i1 > i0 && timeValues[i1] < endTime; i1++) {}
    ++i1;
    timeValues[i1] = endTime;
    filter->SetStartTime(startTime);
    // REVIEW: GetLengthOfTimeStep() is in miliseconds
    double dt0 = dataSetAccessor->GetLengthOfTimeStep()/1000;
    double dt1 = dt0 / conf.TimeStepResolution;
    std::cout << "StartTime = " << startTime << std::endl;
    std::cout << "EndTime = " << endTime << std::endl;
    std::cout << "dt1 = " << dt1 << std::endl;
    std::cout << "dt0 = " << dt0 << std::endl;
    std::cout << "dt1 = " << dt1 << std::endl;
    std::cout << "i0 = " << i0 << std::endl;
    std::cout << "i1 = " << i1 << std::endl;
    size_t n = conf.TimeStepResolution - 1;
    double t0 = timeValues[i0];
    for (size_t i = i0+1; i <= i1; i++)
      {
      std::cout << "i = " << i << std::endl;
      double t1 = timeValues[i];
      double tn = t0 + n*dt1;
      for (double t = t0 + dt1; t <= tn; t += dt1)
        {
        std::cout << "    " << "SetTerminationTime(" << t << ")\n";
        filter->SetTerminationTime(t);
        filter->Update();
        }
      filter->SetTerminationTime(t1);
      filter->Update();
      t0 = t1;
      }
    */
    }
  return OK;
}

StatusType GeneratePathlines(Flow4DReader* dataSet,
                             const SeedCollection& seeds,
                             const PathlineConfiguration &conf,
                             const char* outputFile)
{
  NEW_TRACKER("GeneratePathlines_File");
  if (outputFile == NULL || *outputFile == '\0')
    {
    return INV_FILENAME;
    }
  vtkNew<vtkParticlePathFilter> filter;
  TRACK_POINTER(THE_TRACKER, filter.GetPointer());
  StatusType status = SetFilter(dataSet, seeds, conf, filter.GetPointer(),
                                THE_TRACKER);
  if (status != OK)
    {
    return status;
    }
  vtkNew<vtkPolyDataWriter> writer;
  writer->SetFileName(outputFile);
  writer->SetInputConnection(filter->GetOutputPort());
  // REVIEW: convert back to cm/s
  RescaleVelocity(filter->GetOutput()->GetPointData()->GetVectors("Velocity"), 0.1);
  writer->Update();
  return OK;
}

StatusType GeneratePathlines(Flow4DReader* dataSet,
                             const SeedCollection& seeds,
                             const PathlineConfiguration &conf,
                             vtkSmartPointer<vtkPolyData> &streamLines)
{
  NEW_TRACKER("GeneratePathlines_vtkPolyData");

  vtkNew<vtkParticlePathFilter> filter;
  StatusType status = SetFilter(dataSet, seeds, conf, filter.GetPointer(),
                                THE_TRACKER);
  if (status != OK)
    {
    return status;
    }
  streamLines = filter->GetOutput();
  RescaleVelocity(streamLines->GetPointData()->GetVectors("Velocity"), 0.1);
  return OK;
}

END_PCMR_DECLS
