#ifndef __pcmrFlowComputationUtils_h
#define __pcmrFlowComputationUtils_h

#include "pcmrFlowComputation.h"
#include "vtkSmartPointer.h"

class vtkPolyData;
BEGIN_PCMR_DECLS

class Flow4DReader;

vtkSmartPointer<vtkPolyData> CreatePointSetFromCollection(const SeedCollection& seeds);

pcmr::Flow4DReader *GetDataSetReader(void * opaqueReader);

END_PCMR_DECLS

#endif

