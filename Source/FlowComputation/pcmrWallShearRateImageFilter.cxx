#include "pcmrWallShearRateImageFilter.h"
#include "pcmrImageUtil.h"

BEGIN_PCMR_DECLS

WallShearRateImageFilter::WallShearRateImageFilter()
{
  this->m_Function = WallShearRateFunction::New();
  this->m_Function->SetVelocityUnitFactor(10);
}

StatusType WallShearRateImageFilter::SetInputFromStudy(Flow4DReader *dataSet, size_t timeStep)
{
  StatusType status = this->m_Function->SetImageDataFromStudy(dataSet, timeStep);
  
  if (status != OK)
    {
    return status;
    }
  if (!this->m_Function->GetMaskImage())
    {
    // this is not an error situation
    return NO_MASK;
    }
  this->SetInput(this->m_Function->GetMaskImage());
  return OK;
}

StatusType WallShearRateImageFilter::SetBoundaryMesh(TriangleMeshType *mesh)
{
  StatusType status = this->m_Function->SetBoundaryMesh(mesh);

  if (status != OK)
    {
    return status;
    }
  if (!this->m_Function->GetMaskImage())
    {
    // this is an error situation, because after a mesh there should
    // be a mask, even empty.
    return NO_MASK;
    }
  this->SetInput(this->m_Function->GetMaskImage());
  return OK;
}

void WallShearRateImageFilter::BeforeThreadedGenerateData()
{
  // make sure the internal pipeline of the wall shear rate evaluator
  // is initialized
  itk::PhaseContrast3DImage::PointType params;
  params[0] = 0;
  params[1] = 0;
  params[2] = 0;
  this->m_Function->Evaluate(params);  
}

void WallShearRateImageFilter::ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  std::cout << "ThreadedGenerateData ID: " << threadId << std::endl;

  itk::PhaseContrast3DImage::Pointer output = this->GetOutput();
  itk::MaskImage::ConstPointer input = this->GetInput();

  // output iterator for the region of this thread
  itk::ImageRegionIteratorWithIndex<itk::PhaseContrast3DImage> itOut(output, outputRegionForThread);
  itk::ImageRegionConstIteratorWithIndex<itk::MaskImage> itIn(input, outputRegionForThread);

  while( !itOut.IsAtEnd() )
    {
    // REVIEW: ere we should check for a configurable value
    if (itIn.Get())
      {
      itk::PhaseContrast3DImage::PointType pt;
      output->TransformIndexToPhysicalPoint(itOut.GetIndex(), pt);
      itk::PhaseContrast3DImage::PixelType wsr = this->m_Function->EvaluateAtPhysicalPoint(pt);
      itOut.Set(wsr);
      }
    else
      {
      // REVIEW: here we should set a configurable value
      itOut.Set(0.0);
      }
    ++itOut;
    ++itIn;
    }
}

END_PCMR_DECLS
