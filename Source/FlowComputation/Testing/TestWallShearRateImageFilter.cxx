#include <iostream>
#include "pcmrImageUtil.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrWallShearRateImageFilter.h"
#include "pcmrWallShearRateImage.h"
#include "itkTimeProbe.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestWallShearRateImageFilter, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestWallShearRateImageFilter, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestWallShearRateImageFilter, ComputeWSR)
{
  pcmr::StatusType status;

#ifndef _MSC_VER
  pcmr::WallShearRateImageFilter::Pointer wsrFilter = pcmr::WallShearRateImageFilter::New();

  wsrFilter->SetSplineOrder(2);
  status = wsrFilter->SetInputFromStudy(dataSet, 3);
  ASSERT_EQ(pcmr::OK, status);
#else
  itk::PhaseContrast3DImage::Pointer wsrImage;
#endif
  
  itk::TimeProbe clock;
 
  clock.Start();
#ifndef _MSC_VER
  wsrFilter->Update();
  //wsrFilter->GetOutput()->Print(std::cout);
  pcmr::WriteImage(wsrFilter->GetOutput(), "WallShearRateFilter.vtk");
#else
  status = pcmr::ComputeWallShearRateImage(dataSet, 3, wsrImage);
  pcmr::WriteImage(wsrImage.GetPointer(), "WallShearRateFilter.vtk");
  ASSERT_EQ(pcmr::OK, status);
#endif
  clock.Stop();

  std::cout << "Mean: " << clock.GetMean() << std::endl;
  std::cout << "Total: " << clock.GetTotal() << std::endl;
/*
For level=0:
[123.914, 74.9864, 178.432] has original index [49, 93, 3]
[49, 93, 3] map to original point [124.436, 74.9036, 177.816]
[123.914, 74.9864, 178.432] has interpolated index [49, 93, 3]
[49, 93, 3] map to interpolated point [124.436, 74.9036, 177.816]
Flow at [49, 93, 3] is [-34.3824, -53.8843, 58.9016]
Flow evaluated at index [49, 93, 3] is [-34.3824, -53.8844, 58.9016]
Flow evaluated at point [124.436, 74.9036, 177.816] is [-34.3824, -53.8844, 58.9016]
*/
  double pts[][3] = 
    {
      {124.436, 74.9036, 177.816},
      // coordinate of index [146, 279, 14] used to validate gradient
      {123.846, 74.9036, 178.58} 
    };

  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
