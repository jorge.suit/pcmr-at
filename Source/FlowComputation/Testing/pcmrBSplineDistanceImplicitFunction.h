#ifndef __pcmrBSplineDistanceImplicitFunction_h
#define __pcmrBSplineDistanceImplicitFunction_h

#include "itkPhaseContrastImage.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkComposeImageFilter.h"
#include "vtkImplicitFunction.h"

class BSplineDistanceImplicitFunction : public vtkImplicitFunction
{
public:
  vtkTypeMacro(BSplineDistanceImplicitFunction,vtkImplicitFunction);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description
  // Construct the implicit function.
  static BSplineDistanceImplicitFunction *New();

  // Description
  // Evaluate the distance function approximation with BSpline.
  double EvaluateFunction(double x[3]);
  double EvaluateFunction(double x, double y, double z)
    {return this->vtkImplicitFunction::EvaluateFunction(x, y, z); } ;

  // Description
  // Evaluate gradient.
  void EvaluateGradient(double x[3], double n[3]);

  void SetDistanceFieldGrid(itk::PhaseContrast3DImage *grid);
  void SetDistanceFieldGrid(itk::PhaseContrastVector1DImage *grid);
  itk::PhaseContrastVector1DImage *GetDistanceFieldGrid() const;

  void SetSplineOrder(size_t order);
  size_t GetSplineOrder();
  size_t GetNumberOfFunctionEvaluation() const
  {
    return this->m_NumberOfFunctionEvaluation;
  }
protected:
  BSplineDistanceImplicitFunction();
  ~BSplineDistanceImplicitFunction() {};

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector1DImage> ComposeFilterType;
  typedef itk::BSplineControlPointImageFunction<itk::PhaseContrastVector1DImage> BSplineFunctionType;
  typedef BSplineFunctionType::PointType PointType;
  typedef BSplineFunctionType::GradientType GradientType;

  itk::PhaseContrastVector1DImage *m_Grid;
  ComposeFilterType::Pointer m_ComposeVector;
  BSplineFunctionType::Pointer m_BSplineApproximation;
  size_t m_NumberOfFunctionEvaluation;
private:  
  BSplineDistanceImplicitFunction(const BSplineDistanceImplicitFunction&);  // Not implemented.
  void operator=(const BSplineDistanceImplicitFunction&);  // Not implemented.
};

#endif
