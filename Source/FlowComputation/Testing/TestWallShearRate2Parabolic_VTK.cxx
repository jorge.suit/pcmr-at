#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrWallShearRateFunction2.h"
#include "itkMeshFileReader.h"
#include "itkTimeProbe.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* MeshFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static int TimeStep = -1;
static double Radius = 0;

vtkSmartPointer<vtkPolyDataReader> polyDataReader;

template <class V>
std::ostream & DumpVector(std::ostream & out, V &x, size_t size)
{
  out << "[";
  for(int i = 0; i < size; i++)
    {
    out << (i?", ":"") << x[i];
    }
  out << "]";
  return out;
}

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestWallShearRateFunction2Parabolic_VTK, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,5) << "usage: '" << _ARGV[0] << "' study time-step radius mesh\n";
  StudyDir = _ARGV[1];
  TimeStep = atoi(_ARGV[2]);
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
  ASSERT_TRUE(!ParsePositive("radius", _ARGV[3], Radius));
  MeshFile = _ARGV[4];
}

TEST(TestWallShearRateFunction2Parabolic_VTK, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (TimeStep < 0 || TimeStep >= dataSet->GetNumberOfTimeSteps())
    {
    int ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << TimeStep << "' using default " << ts << std::endl;
    TimeStep = ts;
    }

  polyDataReader = vtkSmartPointer<vtkPolyDataReader>::New();
  polyDataReader->SetFileName(MeshFile);
  polyDataReader->Update();
}

inline void UpdateStatValue(double x, 
                            double &min_x, double &max_x, double& sum_x)
{
  if (x < min_x)
    {
    min_x = x;
    }
  else if (x > max_x)
    {
    max_x = x;
    }
  sum_x += x;
}

struct TestStatistic
{
  size_t count;
  double min_v, max_v, sum_v;
  double min_d, max_d, sum_d;
  double min_g, max_g, sum_g;
  double min_wsr0, max_wsr0, sum_wsr0;
  double min_wsr1, max_wsr1, sum_wsr1;

  TestStatistic()
    : count(0),
      min_v(1.0e6), max_v(-1.0e6), sum_v(0),
      min_d(1.0e6), max_d(-1.0e6), sum_d(0),
      min_g(1.0e6), max_g(-1.0e6), sum_g(0),
      min_wsr0(1.0e10), max_wsr0(-1.0e10), sum_wsr0(0),
      min_wsr1(1.0e10), max_wsr1(-1.0e10), sum_wsr1(0)
  {
  }
  
  static 
  void PrintNumber(std::ostream &o, double x)
  {
    o << std::setw(10) << std::setiosflags(ios::fixed) << std::setprecision(3) << x;
  }

  void Print(std::ostream &o)
  {
    o << std::setw(10) << "Variable" << std::setw(10) <<  "Minimum"
      << std::setw(10) << "Maximum" << std::setw(10) << "Average\n";
    o << std::setw(10) << "Velocity";
    PrintNumber(o, this->min_v);
    PrintNumber(o, this->max_v);
    PrintNumber(o, this->sum_v/this->count);
    o << std::endl;
    o << std::setw(10) << "Distance";
    PrintNumber(o, this->min_d);
    PrintNumber(o, this->max_d);
    PrintNumber(o, this->sum_d/this->count);
    o << std::endl;
    o << std::setw(10) << "Gradient";
    PrintNumber(o, this->min_g);
    PrintNumber(o, this->max_g);
    PrintNumber(o, this->sum_g/this->count);
    o << std::endl;
    o << std::setw(10) << "WSR0";
    PrintNumber(o, this->min_wsr0);
    PrintNumber(o, this->max_wsr0);
    PrintNumber(o, this->sum_wsr0/this->count);
    o << std::endl;
    o << std::setw(10) << "WSR1";
    PrintNumber(o, this->min_wsr1);
    PrintNumber(o, this->max_wsr1);
    PrintNumber(o, this->sum_wsr1/this->count);
    o << std::endl;
  }
};

void UpdateStatFunction(pcmr::WallShearRateFunction2::Pointer wsrFunction,
                        const pcmr::WallShearRateFunction2::PointType &p,
                        TestStatistic & stats)
{
  pcmr::WallShearRateFunction2::GradientType gradient[4];
  pcmr::WallShearRateFunction2::OutputType value[4];
  wsrFunction->EvaluateGradientAtPhysicalPoint(p, value, gradient);
  //std::cout << "velocidad = " << value[1] << std::endl;
  UpdateStatValue(value[1], stats.min_v, stats.max_v, stats.sum_v);
  //std::cout << "distance = " << value[3] << std::endl;
  UpdateStatValue(fabs(value[3]), stats.min_d, stats.max_d, stats.sum_d);
  typedef vnl_vector_ref<pcmr::WallShearRateFunction2::GradientType::ValueType> VectorRefType;
  VectorRefType g = gradient[1].GetVnlVector();
  //std::cout << "gradient = " << g.two_norm() << std::endl;
  UpdateStatValue(g.two_norm(), stats.min_g, stats.max_g, stats.sum_g);
  double wsr0 = wsrFunction->EvaluateAtPhysicalPoint(p);
  UpdateStatValue(wsr0, stats.min_wsr0, stats.max_wsr0, stats.sum_wsr0);
  double normal[3];
  normal[0] = p[0];
  normal[1] = 0.0;
  normal[2] = p[2];
  double wsr1 = wsrFunction->EvaluateAtPhysicalPointWithNormal(p, normal);
  UpdateStatValue(wsr1, stats.min_wsr1, stats.max_wsr1, stats.sum_wsr1);
  if (wsr1 < 300)
    {
    std::cout << "Low WSR value " << wsr1 << " at point " 
              << "[" << p[0] << ", " << p[1] << ", " << p[2] << "]\n"; 
    }
  ++stats.count;
  //std::cout << "stats.sum_g = " << stats.sum_g << " stats.count = " << stats.count << std::endl;
}

TEST(TestWallShearRateFunction2Parabolic_VTK, ComputeWSR)
{
  pcmr::WallShearRateFunction2::Pointer wsrFunction = pcmr::WallShearRateFunction2::New();

  wsrFunction->SetVelocityUnitFactor(10);
  wsrFunction->SetComputeWallCondition(true);
  wsrFunction->SetBoundaryLocator(pcmr::WallShearRateFunction2::BLOCATOR_BINARYMASK);
  wsrFunction->SetSplineOrder(3);
  ASSERT_GE(TimeStep,0) << " time-step must be positive";
  pcmr::StatusType status = wsrFunction->SetImageDataFromStudy(dataSet, TimeStep);
  ASSERT_TRUE(status==pcmr::NO_MASK || status == pcmr::OK);
  status = wsrFunction->SetBoundaryMesh(polyDataReader->GetOutput());
  ASSERT_EQ(pcmr::OK, status);
  wsrFunction->SetCorrectedFlowFileName("WSR2_CorrectedFlow_Function.vtk");
  wsrFunction->SetLevelImageFileName("WSR2_LevelImage_Function.vtk");
  double pts[][3] = 
    {
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {0, 0, 0},
      {3.3, -0.709375, 2.2605309110914633},
      {3.32484, -0.709375, 2.17691},
      {3.60892, -4.96563, 1.61937},
      {-3.1065, 2.19316, 2.51985},
      {-3.11009, -1.37652, -2.51542},
      {-1.7, -0.709375, 3.62077}
    };
  pts[1][2] = Radius/2.0;
  pts[2][2] = -Radius/2.0;
  pts[3][2] = Radius;
  pts[4][2] = -Radius;
  pts[5][2] = Radius-0.05;
  pts[6][2] = Radius-0.25;
  pts[7][2] = Radius-0.1;

  pcmr::WallShearRateFunction2::GradientType gradient[4];
  pcmr::WallShearRateFunction2::OutputType value[4];
  pcmr::WallShearRateFunction2::PointType p;

  for (int i = 0; i < sizeof(pts)/sizeof(pts[0]); i++)
    {
    p = pts[i];
    std::cout << "Evaluating at point " << p << std::endl;
    ASSERT_TRUE(!wsrFunction->EvaluateGradientAtPhysicalPoint(p, value, gradient));
    std::cout << "Value: ";
    DumpVector(std::cout, value, 4) << std::endl;
    std::cout << "Gradient of Vx: ";
    DumpVector(std::cout, gradient[0], 3) << std::endl;
    std::cout << "Gradient of Vy: ";
    DumpVector(std::cout, gradient[1], 3) << std::endl;
    std::cout << "Gradient of Vz: ";
    DumpVector(std::cout, gradient[2], 3) << std::endl;
    std::cout << "Gradient of D: ";
    DumpVector(std::cout, gradient[3], 3) << std::endl;
    std::cout << "WSR0: " << wsrFunction->EvaluateAtPhysicalPoint(p) << std::endl;
    double normal[3];
    normal[0] = pts[i][0];
    normal[1] = 0;
    normal[2] = pts[i][2];
    std::cout << "WSR1: " << wsrFunction->EvaluateAtPhysicalPointWithNormal(p,normal) << std::endl;
    }

  double x = 0, z;
  const double dx = 0.1;
  TestStatistic stats;
  bool first = true;
  p[1] = -0.709375;
  const double dist = Radius;
  while (x <= dist)
    {
    if (first)
      {
      z = dist;
      p[0] = 0; p[2] = z;
      UpdateStatFunction(wsrFunction, p, stats);
      p[2] = -z;
      UpdateStatFunction(wsrFunction, p, stats);
      first = false;
      }
    else
      {
      z = sqrt(dist*dist - x*x);
      p[0] = x;
      p[2] = z;
      UpdateStatFunction(wsrFunction, p, stats);
      p[2] = -z;
      UpdateStatFunction(wsrFunction, p, stats);
      p[0] = -x;
      p[2] = z;
      UpdateStatFunction(wsrFunction, p, stats);
      p[2] = -z;
      UpdateStatFunction(wsrFunction, p, stats);
      }
    x += dx;
    }
  stats.Print(std::cout);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}

/*
./TestWallShearRate2Parabolic ../../../../../../biomedical/data/Studies/Parabolic_UPC_LABSON_0.5 0 4 ../../../../../../biomedical/data/Studies/Parabolic_UPC_LABSON_0.5/WSR/sim_mesh.vtk 
*/
