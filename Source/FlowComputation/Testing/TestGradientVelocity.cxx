#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkVectorMagnitudeImageFilter.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkBSplineControlPointImageFunction.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkMath.h"
#include "pcmrImageUtil.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestInterpolateMaskedFlow, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestInterpolateMaskedFlow, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestInterpolateMaskedFlow, TestInterpolate)
{
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                3);
  ASSERT_TRUE(vtkImgX!=NULL);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                3);
  ASSERT_TRUE(vtkImgY!=NULL);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                3);
  ASSERT_TRUE(vtkImgZ!=NULL);
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerMask->SetInput(vtkMask);
  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());
  typedef itk::MaskImageFilter<itk::PhaseContrastVector3DImage,itk::MaskImage> MaskFilterType;
  MaskFilterType::Pointer masker = MaskFilterType::New();
  masker->SetInput(composer->GetOutput());
  masker->SetMaskImage(importerMask->GetOutput());
  masker->Update();

  pcmr::WriteImage(masker->GetOutput(), "MaskedFlowOriginal.vtk");

  itk::MaskImage::SpacingType spacing;
  itk::MaskImage::PointType origin;
  itk::MaskImage::RegionType region;
  const size_t level = 3;
  pcmr::GetSubdivisionInfo(masker->GetOutput(), level, origin, spacing, region);
  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector3DImage> BSplineFilterType;
  BSplineFilterType::Pointer bsplineFilter = BSplineFilterType::New();
  bsplineFilter->SetOrigin(origin);
  bsplineFilter->SetSpacing(spacing);
  bsplineFilter->SetSize(region.GetSize());
  bsplineFilter->SetSplineOrder( 3 );
  bsplineFilter->SetInput(masker->GetOutput());
  bsplineFilter->Update();

  pcmr::WriteImage(bsplineFilter->GetOutput(), "MaskedFlowInterpolated.vtk");
  
  typedef itk::VectorMagnitudeImageFilter<itk::PhaseContrastVector3DImage,itk::PhaseContrast3DImage> MagnitudeFilterType;
  MagnitudeFilterType::Pointer magnitudeFilter = MagnitudeFilterType::New();
  magnitudeFilter->SetInput(bsplineFilter->GetOutput());
  pcmr::WriteImage(magnitudeFilter->GetOutput(), "MagMaskedFlowInterpolated.vtk");

  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrast3DImage,itk::Covariant3DImage> GradientFilterType;
  GradientFilterType::Pointer gradientFilter = GradientFilterType::New();
  gradientFilter->SetInput(magnitudeFilter->GetOutput());
  pcmr::WriteImage(gradientFilter->GetOutput(), "GradMagMaskedFlowInterpolated.vtk");
  
  itk::PhaseContrastVector3DImage::PointType::ValueType p[3] = 
    {123.914038689804, 74.9864054886203, 178.431703246562};
  itk::PhaseContrastVector3DImage::PointType pt = p, ptOriginal, pt1;
  itk::PhaseContrastVector3DImage::IndexType indexOriginal, indexInterpolated;
  composer->GetOutput()->TransformPhysicalPointToIndex(pt, indexOriginal);
  std::cout << pt << " has original index " << indexOriginal << std::endl;
  composer->GetOutput()->TransformIndexToPhysicalPoint(indexOriginal, ptOriginal);
  std::cout << indexOriginal << " map to original point " << ptOriginal << std::endl;

  bsplineFilter->GetOutput()->TransformPhysicalPointToIndex(pt, indexInterpolated);
  std::cout << pt << " has interpolated index " << indexInterpolated << std::endl;

  bsplineFilter->GetOutput()->TransformIndexToPhysicalPoint(indexInterpolated, pt1);
  std::cout << indexInterpolated << " map to interpolated point " << pt1 << std::endl;

  std::cout << "u computed at " << indexInterpolated << " is " 
            << magnitudeFilter->GetOutput()->GetPixel(indexInterpolated) << std::endl;
  itk::Covariant3DImage::PixelType g = gradientFilter->GetOutput()->GetPixel(indexInterpolated);
  std::cout << "Gradient of u computed at " << indexInterpolated << " is " << g << std::endl;
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
