#include "itkPhaseContrastImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkBinaryFillholeImageFilter.h"
#include "itkSimpleFilterWatcher.h"
#include "itkVotingBinaryIterativeHoleFillingImageFilter.h"
//#include "itkVotingBinaryHoleFillFloodingImageFilter.h"
#include "itkBinaryClosingByReconstructionImageFilter.h"
#include "itkFlatStructuringElement.h"

typedef itk::FlatStructuringElement<3> StructuringElementType;

typedef itk::ImageFileReader<itk::MaskImage> MaskReaderType;
typedef itk::ImageFileWriter<itk::MaskImage> MaskWriterType;
typedef itk::BinaryFillholeImageFilter<itk::MaskImage> BinaryFillholeType;
typedef itk::VotingBinaryIterativeHoleFillingImageFilter<itk::MaskImage> VotingBinaryIterativeHoleFillingType;
//typedef itk::VotingBinaryHoleFillFloodingImageFilter<itk::MaskImage> VotingBinaryHoleFillFloodingType;
typedef itk::BinaryClosingByReconstructionImageFilter<itk::MaskImage,StructuringElementType> BinaryClosingByReconstruction;

int main(int argc, char* argv[])
{
  if (argc != 3)
    {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input_mask output_mask\n";
    return -1;
    }
  
  MaskReaderType::Pointer reader = MaskReaderType::New();
  
  reader->SetFileName(argv[1]);
  
  StructuringElementType::RadiusType elementRadius;
  elementRadius.Fill(5);
  StructuringElementType structuringElement = StructuringElementType::Box(elementRadius);

  itk::MaskImage::SizeType radius = {10,10,10};
  BinaryClosingByReconstruction::Pointer fillHoles = BinaryClosingByReconstruction::New();
  //VotingBinaryIterativeHoleFillingImageFilter::Pointer fillHoles = VotingBinaryIterativeHoleFillingType::New();
  //VotingBinaryIterativeHoleFillingType::Pointer fillHoles = VotingBinaryIterativeHoleFillingType::New();
  //BinaryFillholeType::Pointer fillHoles = BinaryFillholeType::New();
  //fillHoles->SetMajorityThreshold(0);
  //fillHoles->SetRadius(radius);
  itk::SimpleFilterWatcher watcherFill(fillHoles, "FillHoles");
  
  fillHoles->SetKernel(structuringElement);

  fillHoles->SetInput(reader->GetOutput());

  MaskWriterType::Pointer writer = MaskWriterType::New();
  writer->SetInput(fillHoles->GetOutput());
  writer->SetFileName(argv[2]);
  writer->Update();
  
  return 0;
}
