#include <iostream>
#include "gtest/gtest.h"
#include "pcmrCFDVelocityToImageFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkXMLUnstructuredGridReader.h"
#include "vtkNew.h"
#include "itkImageFileWriter.h"
#include <vtksys/SystemTools.hxx>
#include "itkMultiplyImageFilter.h"

static int _ARGC = 0;
static char **_ARGV = NULL;
static char *CFDMeshFile = NULL;
static char *PrefixOutput = NULL;
static vtkSmartPointer<vtkUnstructuredGrid> CFDMesh;
static int ExtraPad = 8;
static double Origin[] = {0, 0, 0};
static int Dimensions[] = {50, 50, 50};
static double Spacing[] = { 1.5, 1.5, 1.5 };
static int SampleSize = 25;

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

TEST(TestCFDVelocityToImageFilter, CommandLineArgs)
{
  ASSERT_TRUE( _ARGV != NULL );
  ASSERT_TRUE( _ARGC == 3 || _ARGC == 6 || _ARGC == 7 ) << "usage: '" << _ARGV[0] << "' CFDMesh PrefixOutput ?sx sy sz n?\n";
  CFDMeshFile = _ARGV[1];
  PrefixOutput = _ARGV[2];
  if( _ARGC >= 6 )
    {
    double spacing;
    if( ParsePositive( "sx", _ARGV[ 3 ], spacing) == 0 )
      {
      Spacing[ 0 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 0 ] = 1.0;
      }
    if( ParsePositive( "sy", _ARGV[ 4 ], spacing) == 0 )
      {
      Spacing[ 1 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 1 ] = 1.0;
      }
    if( ParsePositive( "sz", _ARGV[ 5 ], spacing) == 0 )
      {
      Spacing[ 2 ] = spacing;
      }
    else
      {
      std::cout << "assuming default spacing = 1.0\n";
      Spacing[ 2 ] = 1.0;
      }
    }
  if ( _ARGC == 7 )
    {
    int nsample;
    if( ParsePositive( "n", _ARGV[ 6 ], nsample ) == 0 )
      {
      SampleSize = nsample;
      }
    else
      {
      std::cout << "assuming SampleSize = 25\n";
      SampleSize = 25;
      }
    }

}

TEST( TestCFDVelocityToImageFilter, ReadCFD )
{
  ASSERT_TRUE( CFDMeshFile != NULL );
  vtkSmartPointer<vtkXMLUnstructuredGridReader> reader =  vtkSmartPointer<vtkXMLUnstructuredGridReader>::New( );
  reader->SetFileName( CFDMeshFile );
  reader->Update( );
  CFDMesh = reader->GetOutput( );
}

template <class T>
void DumpArray( const char* name, const T *array, int size = 3 )
{
  std::cout << name << " = [";
  for( int i = 0; i < size; i++ )
    {
    std::cout << ( !i ? "" : ", " ) << array[ i ];
    }
  std::cout << "]\n";
}

TEST( TestCFDVelocityToImageFilter, ComputeGeometry )
{
  ASSERT_TRUE( CFDMesh != NULL );
  double bounds[ 6 ];
  CFDMesh->GetBounds( bounds );
  int n[ 3 ];
  for( int i = 0; i < 3; i++ )
    {
    double min = bounds[ 2*i ];
    int n = int( ceil(double( bounds[ 2*i + 1 ] - min )/Spacing[i] ) );
    Dimensions[ i ] = n + 2 * ExtraPad;
    Origin[ i ] = min - ExtraPad * Spacing[i];
    }
  DumpArray( "Bounds", bounds, 6 );
  DumpArray( "Origin", Origin );
  DumpArray( "Dimensions", Dimensions );
}

TEST( TestCFDVelocityToImageFilter, Voxelize )
{
  pcmr::CFDVelocityToImageFilter::Pointer voxelizer = pcmr::CFDVelocityToImageFilter::New( );
  voxelizer->SetInputCFD( CFDMesh );
  voxelizer->SetOrigin( Origin );
  pcmr::CFDVelocityToImageFilter::SizeType _dimensions;
  for( int i = 0; i < 3; i++ )
    {
    _dimensions[ i ] = Dimensions[ i ];
    }
  voxelizer->SetSize( _dimensions );
  DumpArray( "Spacing", Spacing );

  voxelizer->SetSpacing( Spacing );
  voxelizer->Update( );
  typedef itk::MultiplyImageFilter<pcmr::CFDVelocityToImageFilter::OutputImageType> MultiplyImageFilterType; 
  MultiplyImageFilterType::Pointer convertToCM = MultiplyImageFilterType::New( );
  convertToCM->SetConstant2( 0.1 );
  typedef itk::ImageFileWriter<pcmr::CFDVelocityToImageFilter::OutputImageType> ImageWriterType;
  ImageWriterType::Pointer writer = ImageWriterType::New( );
  writer->SetInput( convertToCM->GetOutput( ) );

 for( int idx = 0; idx < 3; idx++ )
    {
    convertToCM->SetInput( voxelizer->GetOutput( idx ) );
    std::string prefix( PrefixOutput );
    prefix += "_";
    prefix += ('0' + idx);
    prefix += ".vtk";
    writer->SetFileName( prefix );
    writer->Update( );
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
