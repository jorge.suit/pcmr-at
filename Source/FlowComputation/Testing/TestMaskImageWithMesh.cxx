#include <iostream>
#include "gtest/gtest.h"
#include "itkMesh.h"
#include "itkMeshFileReader.h"
#include "itkImageFileReader.h"
#include "pcmrImageUtil.h"
#include "itkPhaseContrastImage.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include "itkMaskImageFilter.h"

typedef itk::Mesh<float, 3>  TriangleMeshType;
typedef itk::MeshFileReader<TriangleMeshType> MeshReaderType;
typedef itk::ImageFileReader<itk::PhaseContrast3DImage> ImageReaderType;

static int _ARGC = 0;
static char **_ARGV = NULL;
static const char* ImageFileInput = NULL;
static const char* ImageFileOutput = NULL;
static const char* MeshFileInput = NULL;
static MeshReaderType::Pointer  polyDataReader = NULL;
static ImageReaderType::Pointer imageReader = NULL;

TEST(TestMaskImageWithMesh, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,4) << "usage: '" << _ARGV[0] << "' input_image input_mesh output_mesh\n";
  ImageFileInput = _ARGV[1];
  MeshFileInput = _ARGV[2];
  ImageFileOutput = _ARGV[3];
}

TEST(TestMaskImageWithMesh, TestLoadImage)
{
  ASSERT_TRUE(ImageFileInput!=NULL);
  imageReader = ImageReaderType::New();
  imageReader->SetFileName(ImageFileInput);
  try
    {
    imageReader->Update();
    }
  catch(...)
    {
    std::cout << "Unable to read " << ImageFileInput << std::endl;
    imageReader = NULL;
    }
}

TEST(TestMaskImageWithMesh, TestLoadMesh)
{
  ASSERT_TRUE(MeshFileInput!=NULL);
  polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName(MeshFileInput);
  try
    {
    polyDataReader->Update();
    }
  catch(...)
    {
    std::cout << "Unable to read " << MeshFileInput << std::endl;
    polyDataReader = NULL;
    }
}

TEST(TestMaskImageWithMesh, TestApplyMask)
{
  ASSERT_TRUE(imageReader.GetPointer()!=NULL);
  ASSERT_TRUE(polyDataReader.GetPointer()!=NULL);
  typedef itk::TriangleMeshToBinaryImageFilter<TriangleMeshType,itk::MaskImage> TriangleMeshToBinaryImageFilterType;
  TriangleMeshToBinaryImageFilterType::Pointer rasterFilter = TriangleMeshToBinaryImageFilterType::New();
  rasterFilter->SetInput(polyDataReader->GetOutput());
  const itk::MaskImage::PointType &origin = imageReader->GetOutput()->GetOrigin();
  const itk::MaskImage::SpacingType &spacing = imageReader->GetOutput()->GetSpacing();
  const itk::MaskImage::RegionType &region = imageReader->GetOutput()->GetLargestPossibleRegion();
  rasterFilter->SetOrigin(origin);
  rasterFilter->SetSpacing(spacing);
  rasterFilter->SetSize(region.GetSize());
  rasterFilter->SetInsideValue(1);
  rasterFilter->SetOutsideValue(0);
  rasterFilter->Update();
  typedef itk::MaskImageFilter<itk::PhaseContrast3DImage,itk::MaskImage> MaskFilterType;
  MaskFilterType::Pointer masker = MaskFilterType::New();
  masker->SetInput(imageReader->GetOutput());
  masker->SetMaskImage(rasterFilter->GetOutput());
  masker->Update();

  pcmr::WriteImage(masker->GetOutput(), ImageFileOutput);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
