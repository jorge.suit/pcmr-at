#include <iostream>
#include "pcmrPathlines.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* SeedPointsFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static pcmr::SeedCollection SeedPoints;

TEST(TestPathlines, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,2) << "usage: '" << _ARGV[0] << "' study ?SeedPoints.txt?\n";
  StudyDir = _ARGV[1];
  if (_ARGC > 2)
    {
    SeedPointsFile = _ARGV[2];
    }
}

TEST(TestPathlines, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestPathlines, TestEmptySeeds)
{
  ASSERT_TRUE(dataSet);
  pcmr::SeedCollection seeds;
  pcmr::PathlineConfiguration conf;
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, seeds, conf, "filename");
  ASSERT_EQ(pcmr::SQ_EMPTY, status) << "should return SQ_EMPTY" << "\n";
}

TEST(TestPathlines, TestNullFile)
{
  ASSERT_TRUE(dataSet);
  pcmr::SeedCollection seeds;
  pcmr::PathlineConfiguration conf;
  seeds.push_back(pcmr::SeedPoint());
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, seeds, conf,
                                                    (char*)NULL);
  ASSERT_EQ(pcmr::INV_FILENAME, status) << "should return INV_FILENAME" << "\n";
  status = pcmr::GeneratePathlines(dataSet, seeds, conf, "");
  ASSERT_EQ(pcmr::INV_FILENAME, status) << "should return INV_FILENAME" << "\n";
}

TEST(TestPathlines, TestReadPoints)
{
  ASSERT_TRUE(SeedPointsFile!=NULL);
  std::ifstream fin(SeedPointsFile);
  ASSERT_TRUE(fin.good());
  size_t n = 0;
  double v[3];
  size_t i = 0;
  while (!fin.eof())
    {
    fin >> v[i++];
    if (i==3)
      {
      SeedPoints.push_back(pcmr::SeedPoint(v[0], v[1], v[2]));
      i = 0;
      ++n;
      }
    }
  std::cout << n << " SeedPoints were read\n";
}

TEST(TestPathlines, TestGenerateFile)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  pcmr::PathlineConfiguration conf;
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, SeedPoints, conf, "TestPathline.vtk");
  ASSERT_EQ(pcmr::OK, status) << "TestGenerate: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
}

TEST(TestPathlines, TestResolutionGenerateFile)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);
  pcmr::PathlineConfiguration conf;
  conf.TimeStepResolution = 10;
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, SeedPoints, conf,
                                                    "TestPathlineResolution.vtk");
  ASSERT_EQ(pcmr::OK, status) << "TestGenerate: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
}

TEST(TestPathlines, TestGenerateFileTerminalSpeed)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  pcmr::PathlineConfiguration conf;
  conf.TerminalSpeed = 20;
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, SeedPoints, conf, "TestPathline_TS10.vtk");
  ASSERT_EQ(pcmr::OK, status) << "TestGenerate: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
}

TEST(TestPathlines, TestGeneratePointer)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  pcmr::PathlineConfiguration conf;
  vtkSmartPointer<vtkPolyData> pathLines;
  pcmr::StatusType status = pcmr::GeneratePathlines(dataSet, SeedPoints, conf,
                                                    pathLines);
  ASSERT_EQ(pcmr::OK, status) << "TestGenerate: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
  ASSERT_TRUE(pathLines);
  pathLines->Print(std::cout);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
