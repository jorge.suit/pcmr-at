#ifndef __TestHelper_h
#define __TestHelper_h

#include "itkImageFileWriter.h"
#include "itkVTKImageIO.h"

template <class TImage>
void WriteImage(TImage * img, const char *path)
{
  typedef itk::ImageFileWriter<TImage>  WriterType;
  typedef itk::VTKImageIO ImageIOType;
  typename WriterType::Pointer writer = WriterType::New();
  ImageIOType::Pointer vtkIO = ImageIOType::New();
  writer->SetInput(img);
  writer->SetFileName(path);
  writer->SetImageIO(vtkIO);
  std::cout << "Writing " << path << " ...\n"; 
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & err )
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    //return EXIT_FAILURE;
    }
  std::cout << "Finished writing " << path << " ...\n"; 
  return;
}

template <class TImage>
void GetSubdivisionInfo(TImage *image, const size_t level,
                        typename TImage::PointType &origin,
                        typename TImage::SpacingType &spacing,
                        typename TImage::RegionType &region)
{
  typedef typename TImage::RegionType RegionType;

  image->Update();
  origin = image->GetOrigin();
  spacing = image->GetSpacing();
  region = image->GetLargestPossibleRegion();

  if (level == 0)
    {
    return;
    }
  if (spacing[0] < spacing[1])
    {
    if (spacing[0] < spacing[2])
      {
      region.SetSize(0, region.GetSize(0)*level);
      region.SetSize(1, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[0])*level));
      region.SetSize(2, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(2)*(spacing[2]/spacing[0])*level));
      spacing[0] /= level;
      spacing[2] = spacing[1] = spacing[0];
      }
    else
      {
      region.SetSize(0, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[2])*level));
      region.SetSize(1, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[2])*level));
      region.SetSize(2, region.GetSize(2)*level);
      spacing[2] /= level;
      spacing[1] = spacing[0] = spacing[2];
      }
    }
  else
    {
    if (spacing[1] < spacing[2])
      {
      region.SetSize(0, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[1])*level));
      region.SetSize(1, region.GetSize(1)*level);
      region.SetSize(2, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(2)*(spacing[2]/spacing[1])*level));
      spacing[1] /= level;
      spacing[2] = spacing[0] = spacing[1];
      }
    else
      {
      region.SetSize(0, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(0)*(spacing[0]/spacing[2])*level));
      region.SetSize(1, itk::Math::Round<typename RegionType::SizeValueType>(region.GetSize(1)*(spacing[1]/spacing[2])*level));
      region.SetSize(2, region.GetSize(2)*level);
      spacing[2] /= level;
      spacing[1] = spacing[0] = spacing[2];
      }    
    }
}

#endif
