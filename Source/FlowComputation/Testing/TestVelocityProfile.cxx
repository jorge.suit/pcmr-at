#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrFlow4DReader.h"
#include "itkTimeProbe.h"
#include "vtkDataArray.h"
#include "vtkPointData.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
const char* argPX = NULL;
const char* argPY = NULL;
const char* argPZ = NULL;

TEST(TestVelocityProfile, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,5) << "usage: '" << _ARGV[0] << "' study px py pz\n";
  StudyDir = _ARGV[1];
  argPX = _ARGV[2];
  argPY = _ARGV[3];
  argPZ = _ARGV[4];
}

TEST(TestVelocityProfile, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestVelocityProfile, ExtractProfile)
{
  ASSERT_TRUE(dataSet);
  itk::TimeProbe clock;
  vtkImageData *vtkImgX, *vtkImgY, *vtkImgZ;
  vtkDataArray *pointScalars;
  vtkIdType id;
  //double p[] = {83.229, 127.5, 40};
  //double p[] = {117.8, 44.799, 210.84};
  //Entrada: (127.807690832458,82.4242323053261,189.370646477244)
  //double p[] = {127.807690832458,82.4242323053261,189.370646477244};
  //Salida: (152.435740507637,195.878353067524,191.26101085604)
  //double p[] = {152.435740507637,195.878353067524,191.26101085604};
  //Supra 1: (142.102236291776,-17.9707408776521,198.172077386556)
  //double p[] = {142.102236291776,-17.9707408776521,198.172077386556};
  //Supra 2: (117.34619799874,-16.5666020903507,222.965315734942)
  double p[3];
  char *end = NULL;
  p[0] = strtod(argPX, &end);
  ASSERT_TRUE(end==NULL || !*end) << "failed reading PX from " << argPX << std::endl; 
  p[1] = strtod(argPY, &end);
  ASSERT_TRUE(end==NULL || !*end) << "failed reading PY from " << argPY << std::endl; 
  p[2] = strtod(argPZ, &end);
  ASSERT_TRUE(end==NULL || !*end) << "failed reading PZ from " << argPZ << std::endl; 
  std::cout << "\"Velocity profile at " << "(" << p[0] << "," << p[1] << "," << p[2] << ")\"\n"; 
  std::cout << "Vx;Vy;VZ\n";
  for (int t = 0; t < dataSet->GetNumberOfTimeSteps(); t++)
    {
    clock.Start();
    vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,t);
    id = vtkImgX->FindPoint(p);
    pointScalars = vtkImgX->GetPointData()->GetScalars();
    double vx = pointScalars->GetTuple1(id);
    vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,t);
    pointScalars = vtkImgY->GetPointData()->GetScalars();
    double vy = pointScalars->GetTuple1(id);
    vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,t);
    pointScalars = vtkImgZ->GetPointData()->GetScalars();
    double vz = pointScalars->GetTuple1(id);
    std::cout << vx << ";" << vy << ";" << vz << std::endl;
    clock.Stop();
    }
  std::cout << "Mean Time: " << clock.GetMean() << std::endl;
  std::cout << "Total Time: " << clock.GetTotal() << std::endl;
  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}


/*
Para la segmentación hecha con SNAP de 4DFLOW_SampleDataset_Aorta_3D_24SL

Entrada: (127.807690832458,82.4242323053261,189.370646477244)
Salida: (152.435740507637,195.878353067524,191.26101085604)
Supra 1: (142.102236291776,-17.9707408776521,198.172077386556)
Supra 2: (117.34619799874,-16.5666020903507,222.965315734942)
*/
