#include <iostream>
#include "pcmrImageUtil.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrWallShearRateImageFilter.h"
#include "pcmrWallShearRateImage.h"
#include "itkTimeProbe.h"
#include "itkMeshFileReader.h"
#include "itkImageToVTKImageFilter.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkProbeFilter.h"

typedef itk::MeshFileReader<pcmr::TriangleMeshType> MeshReaderType;

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* MeshFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static MeshReaderType::Pointer  polyDataReader = NULL;

static int timeStep = -1;

TEST(TestWallShearRateImageFilterMesh, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,4) << "usage: '" << _ARGV[0] << "' study ts mesh\n";
  StudyDir = _ARGV[1];
  timeStep = atoi(_ARGV[2]);
  ASSERT_GE(timeStep,0) << " time-step must be positive";
  MeshFile = _ARGV[3];
}

TEST(TestWallShearRateImageFilterMesh, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";

  if (timeStep < 0 || timeStep >= dataSet->GetNumberOfTimeSteps())
    {
    double ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << timeStep << "' using default " << ts << std::endl;
    timeStep = ts;
    }
  polyDataReader = MeshReaderType::New();
  polyDataReader->SetFileName(MeshFile);
  polyDataReader->Update();
}

TEST(TestWallShearRateImageFilterMesh, ComputeWSR)
{
  pcmr::StatusType status;

  ASSERT_TRUE(dataSet.GetPointer()!=NULL);
#ifndef _MSC_VER
  pcmr::WallShearRateImageFilter::Pointer wsrFilter = pcmr::WallShearRateImageFilter::New();

  wsrFilter->SetCorrectedFlowFileName("WSR_CorrectedFlow.vtk");
  wsrFilter->SetLevelImageFileName("WSR_LevelImage.vtk");
  wsrFilter->SetSplineOrder(2);
  wsrFilter->SetComputeWallCondition(true);
  ASSERT_GE(timeStep,0) << " time-step must be positive";
  status = wsrFilter->SetInputFromStudy(dataSet, timeStep);
  ASSERT_TRUE(status==pcmr::NO_MASK || status == pcmr::OK);
  status = wsrFilter->SetBoundaryMesh(polyDataReader->GetOutput());
  ASSERT_EQ(pcmr::OK, status);  
  pcmr::WriteImage(wsrFilter->GetMaskImage(), "MaskImageFromMesh.vtk");
#else
  itk::PhaseContrast3DImage::Pointer wsrImage;
#endif
  
  itk::TimeProbe clock;
 
  clock.Start();
#ifndef _MSC_VER
  wsrFilter->Update();
  //wsrFilter->GetOutput()->Print(std::cout);
  pcmr::WriteImage(wsrFilter->GetOutput(), "WallShearRateFilterMesh.vtk");
  // Now interpolate the boundary mesh on the WSR result
  typedef itk::ImageToVTKImageFilter<itk::PhaseContrast3DImage> ConvertToVTK;
  ConvertToVTK::Pointer cvtWSR = ConvertToVTK::New();
  cvtWSR->SetInput(wsrFilter->GetOutput());
  cvtWSR->Update();
  vtkSmartPointer<vtkPolyDataReader> boundaryReader = vtkPolyDataReader::New();
  boundaryReader->SetFileName(MeshFile);
  vtkSmartPointer<vtkProbeFilter> probeFilter = vtkProbeFilter::New();
  probeFilter->SetInputConnection(boundaryReader->GetOutputPort());
  probeFilter->SetSourceData(cvtWSR->GetOutput());
  vtkSmartPointer<vtkPolyDataWriter> boundaryWriter = vtkPolyDataWriter::New();
  boundaryWriter->SetInputConnection(probeFilter->GetOutputPort());
  boundaryWriter->SetFileName("WallShearRateInterpolatedMesh.vtk");
  boundaryWriter->Update();
#else
  status = pcmr::ComputeWallShearRateImage(dataSet, 3, wsrImage);
  pcmr::WriteImage(wsrImage.GetPointer(), "WallShearRateFilterMesh.vtk");
  ASSERT_EQ(pcmr::OK, status);
#endif
  clock.Stop();

  std::cout << "Mean: " << clock.GetMean() << std::endl;
  std::cout << "Total: " << clock.GetTotal() << std::endl;
/*
For level=0:
[123.914, 74.9864, 178.432] has original index [49, 93, 3]
[49, 93, 3] map to original point [124.436, 74.9036, 177.816]
[123.914, 74.9864, 178.432] has interpolated index [49, 93, 3]
[49, 93, 3] map to interpolated point [124.436, 74.9036, 177.816]
Flow at [49, 93, 3] is [-34.3824, -53.8843, 58.9016]
Flow evaluated at index [49, 93, 3] is [-34.3824, -53.8844, 58.9016]
Flow evaluated at point [124.436, 74.9036, 177.816] is [-34.3824, -53.8844, 58.9016]
*/
  double pts[][3] = 
    {
      {124.436, 74.9036, 177.816},
      // coordinate of index [146, 279, 14] used to validate gradient
      {123.846, 74.9036, 178.58} 
    };

  
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}

/*

./TestWallShearRateImageFilterMesh
../../../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL
../../../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL/Segmentation/4DFLOW_snap_d0.vtk

*/
