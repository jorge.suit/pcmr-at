#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkGenericDataObjectWriter.h>
#include <vtkCylinderSource.h>
#include <vtkTriangleFilter.h>
#include <vtkButterflySubdivisionFilter.h>
#include <vtkLoopSubdivisionFilter.h>
#include <vtkLinearSubdivisionFilter.h>

//#define __CHECK_POINTS_DATASET_0__
#define __GENERATE_ALL_DOMAIN__

int ParsePositive(const char* name, const char* value, double &v)
{
  v = atof(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int ParsePositive(const char* name, const char* value, int &v)
{
  v = atoi(value);
  if (v <= 0)
    {
    std::cerr << "Invalid " << name << " argument: " << value << std::endl;
    return -1;
    }
  return 0;
}

int main(int argc, char *argv[])
{
  if (argc < 7 || argc > 8)
    {
    std::cout << "Usage: " << argv[0] << "length radius dx dy dz output ?(0|1){mesh}?\n";
    return -1;
    }
  double length, radius;
  if (ParsePositive("length", argv[1], length))
    {
    return -1;
    }
  if (ParsePositive("radius", argv[2], radius))
    {
    return -1;
    }
  double delta_x, delta_y, delta_z;
  if (ParsePositive("dx", argv[3], delta_x))
    {
    return -1;
    }
  if (ParsePositive("dy", argv[4], delta_y))
    {
    return -1;
    }
  if (ParsePositive("dz", argv[5], delta_z))
    {
    return -1;
    }

  std::cout << "length = " << length << std::endl;
  std::cout << "radius = " << radius << std::endl;
  int writeMesh = 0;
  if (argc >= 8 && ParsePositive("write_mesh", argv[7], writeMesh))
    {
    return -1;
    }
  
  const double v0 = 100;
  const size_t extraSize = 10;
  const double diameter = 2*radius;
  size_t nx = static_cast<size_t>(ceil(diameter/delta_x)) + extraSize;
  size_t ny = static_cast<size_t>(ceil(length/delta_y)) + extraSize;
  size_t nz = static_cast<size_t>(ceil(diameter/delta_z)) + extraSize;
  double size_x = nx * delta_x;
  double size_y = ny * delta_y;
  double size_z = nz * delta_z;
  double min_x = -size_x/2; 
  double min_y = -size_y/2;
  double min_z = -size_z/2;
  const double halfLength = length * 0.5;

  // Create an image data
  vtkSmartPointer<vtkImageData> imageData[5];
#define VX 0
#define VY 1
#define VZ 2
#define MAG 3
#define MASK 4
  for(int i = 0; i < 5; i++)
    {
    imageData[i] = vtkSmartPointer<vtkImageData>::New(); 
    // Specify the size of the image data
    imageData[i]->SetDimensions(nx, ny, nz);
    imageData[i]->SetOrigin(min_x, min_y, min_z);
    imageData[i]->SetSpacing(delta_x, delta_y, delta_z);
    if (i == MASK)
      {
      imageData[i]->AllocateScalars(VTK_UNSIGNED_SHORT,1);
      }
    else
      {
      imageData[i]->AllocateScalars(VTK_FLOAT,1);
      }
    }
     
  int* dims = imageData[0]->GetDimensions();
 
  std::cout << "Dims: " << " x: " << dims[0] << " y: " << dims[1] << " z: " << dims[2] << std::endl;
 
  std::cout << "Number of points: " << imageData[0]->GetNumberOfPoints() << std::endl;
  std::cout << "Number of cells: " << imageData[0]->GetNumberOfCells() << std::endl;

  const bool AllDomain = false;
  const double ExtraRadius = 2 * delta_x;
  for (int iz = 0; iz < dims[2]; iz++)
    {
    double z = min_z + iz*delta_z;
    for (int iy = 0; iy < dims[1]; iy++)
      {
      double y = min_y + iy*delta_y;
      for (int ix = 0; ix < dims[0]; ix++)
        {
        float* pixel;
        double x = min_x + ix*delta_x;
        double r = sqrt(x*x + z*z);
#ifdef __CHECK_POINTS_DATASET_0__
        if (ix == 53 && iy == 9 && iz == 4)
          {
          std::cout << "[53,9,4] is at distance r = " << r << " from center\n";
          std::cout << "it speed is " << v0*(1-pow(r/radius,2)) << std::endl;
          }
#endif
        // do something with v
        bool inside = fabs(y) <= halfLength && r <= (radius + ExtraRadius);
        if (AllDomain || inside)
          {
          pixel = static_cast<float*>(imageData[VY]->GetScalarPointer(ix,iy,iz));
          pixel[0] = v0*(1-pow(r/radius,2));
          //std::cout << "Interior point " << pixel[0] << "\n";
          pixel = static_cast<float*>(imageData[MAG]->GetScalarPointer(ix,iy,iz));
          pixel[0] = v0;
          unsigned short* pixel_us = static_cast<unsigned short*>(imageData[MASK]->GetScalarPointer(ix,iy,iz));
          pixel_us[0] = inside;
          }
        else
          {
          pixel = static_cast<float*>(imageData[VY]->GetScalarPointer(ix,iy,iz));
          pixel[0] = 0.0;
          pixel = static_cast<float*>(imageData[MAG]->GetScalarPointer(ix,iy,iz));
          pixel[0] = 0.0;
          unsigned short* pixel_us = static_cast<unsigned short*>(imageData[MASK]->GetScalarPointer(ix,iy,iz));
          pixel_us[0] = 0;
          }
        pixel = static_cast<float*>(imageData[VX]->GetScalarPointer(ix,iy,iz));
        pixel[0] = 0.0;
        pixel = static_cast<float*>(imageData[VZ]->GetScalarPointer(ix,iy,iz));
        pixel[0] = 0.0;
        }
      }
    }
  std::string prefix(argv[6]);
  const char* suffix[] = {"vct_0_0", "vct_0_1", "vct_0_2", "mag_0", "mask"};
  vtkSmartPointer<vtkGenericDataObjectWriter> writer = vtkSmartPointer<vtkGenericDataObjectWriter>::New();
  writer->SetFileTypeToBinary();
  for(int i = 0; i < 5; i++)
    {
    std::string output(prefix);
    output += suffix[i];
    output += ".vtk";
    writer->SetInputData(imageData[i]);
    writer->SetFileName(output.c_str());
    writer->Update();
    }
  if (writeMesh)
    {
    std::string output(prefix);
    output += "mesh.vtk";
    vtkSmartPointer<vtkCylinderSource> cyl = vtkCylinderSource::New();
    cyl->SetCenter(0, 0, 0);
    cyl->SetRadius(radius);
    cyl->SetHeight(length);
    cyl->SetResolution(20);
    cyl->CappingOn();
    vtkSmartPointer<vtkTriangleFilter> triangleFilter =
      vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->SetInputConnection(cyl->GetOutputPort());
    vtkSmartPointer<vtkLinearSubdivisionFilter> split =
      vtkSmartPointer<vtkLinearSubdivisionFilter>::New();
    split->SetInputConnection(triangleFilter->GetOutputPort());
    split->SetNumberOfSubdivisions(5);
    writer->SetInputConnection(split->GetOutputPort());
    writer->SetFileName(output.c_str());
    writer->Update();
    }
  return EXIT_SUCCESS;
}


// This data set is wrong, radius should be ~ 15 not 33
// ./TestSimulateParabolic 200 33 100 100 30 sim 1
// updating [53, 9, 2] = [0, 0, 0, 2.64] from [53, 9, 4] = [0, 22.0416, 0, -3.8147e-06]results in a high velocity [-0, -1.52541e+07, -0]

// This is the Tarrasa data-set
// ./TestSimulateParabolic 22.7 4 0.5 0.5 0.5 sim 1
// ./TestSimulateParabolic 22.7 4 0.1 0.1 0.1 sim
