#include <iostream>
#include "pcmrImageUtil.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "pcmrTurbulenceKineticEnergy.h"
#include "vtkImageData.h"
#include "itkTimeProbe.h"
#include "itkVTKImageToImageFilter.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestTKE, CommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,2) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestTKE, LoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestTKE, ComputeTKE)
{
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  pcmr::StatusType status;

  vtkImageData *imageTKE;
  itk::TimeProbe clock;
 
  clock.Start();
  status = ComputeTKE(dataSet, imageTKE);
  ASSERT_EQ(pcmr::OK, status);
  clock.Stop();

  std::cout << "Mean: " << clock.GetMean() << std::endl;
  std::cout << "Total: " << clock.GetTotal() << std::endl;
  ImporterType::Pointer importerTKE = ImporterType::New();
  importerTKE->SetInput(imageTKE);
  pcmr::WriteImage(importerTKE->GetOutput(), "SampleTKE.vtk");
  imageTKE->Delete();
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
