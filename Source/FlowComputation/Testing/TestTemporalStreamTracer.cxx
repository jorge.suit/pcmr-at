#include <iostream>
#include "pcmrTemporalVelocitySource.h"
#include "vtkTemporalStreamTracer.h"
#include "pcmrFlowComputationUtils.h"
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkTemporalInterpolator.h"
#include "vtkNew.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* SeedPointsFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static pcmr::SeedCollection SeedPoints;

TEST(TestTemporalStreamTracer, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,2) << "usage: '" << _ARGV[0] << "' study ?SeedPoints.txt?\n";
  StudyDir = _ARGV[1];
  if (_ARGC > 2)
    {
    SeedPointsFile = _ARGV[2];
    }
}

TEST(TestTemporalStreamTracer, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestTemporalStreamTracer, TestReadPoints)
{
  ASSERT_TRUE(SeedPointsFile!=NULL);
  std::ifstream fin(SeedPointsFile);
  ASSERT_TRUE(fin.good());
  size_t n = 0;
  double v[3];
  size_t i = 0;
  while (!fin.eof())
    {
    fin >> v[i++];
    if (i==3)
      {
      SeedPoints.push_back(pcmr::SeedPoint(v[0], v[1], v[2]));
      i = 0;
      ++n;
      }
    }
  std::cout << n << " SeedPoints were read\n";
}

TEST(TestTemporalStreamTracer, TestGenerateFile)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  vtkNew<pcmr::TemporalVelocitySource> imageSource;
  // REVIEW: factor to convert to mm/s
  imageSource->SetRescaleFactor(10.0);
  vtkNew<pcmr::Flow4DReaderAccessor> dataSetAccessor;
  dataSetAccessor->SetFlow4DReader(dataSet);
  imageSource->SetDataSetAccessor(dataSetAccessor.GetPointer());
  vtkNew<vtkPolyData> pointSet;
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(SeedPoints.size());
  for(size_t i = 0; i < SeedPoints.size(); i++)
    {
    points->SetPoint(i, SeedPoints[i].x, SeedPoints[i].y, SeedPoints[i].z);
    }
  pointSet->SetPoints(points.GetPointer());

  vtkNew<vtkTemporalInterpolator> temporalInterp;
  temporalInterp->SetInputConnection(imageSource->GetOutputPort());

  vtkNew<vtkTemporalStreamTracer> filter;

  filter->SetSourceData(pointSet.GetPointer());
  filter->SetInputConnection(imageSource->GetOutputPort());
  filter->Update();
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
