#include "pcmrBSplineDistanceImplicitFunction.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(BSplineDistanceImplicitFunction);

BSplineDistanceImplicitFunction::BSplineDistanceImplicitFunction()
{
  this->m_ComposeVector = ComposeFilterType::New();
  this->m_BSplineApproximation = BSplineFunctionType::New();
  this->m_BSplineApproximation->SetSplineOrder(2);
  this->m_NumberOfFunctionEvaluation = 0;
}

void BSplineDistanceImplicitFunction::SetSplineOrder(size_t order)
{
  this->m_BSplineApproximation->SetSplineOrder(order);
}

size_t BSplineDistanceImplicitFunction::GetSplineOrder()
{
  BSplineFunctionType::ArrayType order = this->m_BSplineApproximation->GetSplineOrder();
  return order[0];
}

void BSplineDistanceImplicitFunction::SetDistanceFieldGrid(itk::PhaseContrast3DImage *grid)
{
  this->m_ComposeVector->SetInput(0, grid);
  this->SetDistanceFieldGrid(this->m_ComposeVector->GetOutput());
}

void BSplineDistanceImplicitFunction::SetDistanceFieldGrid(itk::PhaseContrastVector1DImage *grid)
{
  grid->Update();
  itk::PhaseContrastVector1DImage::SpacingType spacing = grid->GetSpacing();
  itk::PhaseContrastVector1DImage::PointType origin = grid->GetOrigin();
  itk::PhaseContrastVector1DImage::RegionType region = grid->GetLargestPossibleRegion();
  //region.Print(std::cout);
  this->m_BSplineApproximation->SetOrigin(origin);
  this->m_BSplineApproximation->SetSpacing(spacing);
  this->m_BSplineApproximation->SetSize(region.GetSize());
  this->m_BSplineApproximation->SetInputImage(grid);
  this->m_Grid = grid;
}

itk::PhaseContrastVector1DImage* BSplineDistanceImplicitFunction::GetDistanceFieldGrid() const
{
  return this->m_Grid;
}

double BSplineDistanceImplicitFunction::EvaluateFunction(double x[3])
{
  PointType point;
  point[0] = x[0];
  point[1] = x[1];
  point[2] = x[2];
  ++this->m_NumberOfFunctionEvaluation;
  return this->m_BSplineApproximation->EvaluateAtParametricPoint(point)[0];
}

void BSplineDistanceImplicitFunction::EvaluateGradient(double x[3], double n[3])
{
  PointType point;
  point[0] = x[0];
  point[1] = x[1];
  point[2] = x[2];
  GradientType g = this->m_BSplineApproximation->EvaluateGradientAtParametricPoint(point);
  n[0] = g(0,0);
  n[1] = g(0,1);
  n[2] = g(0,2);
}


void BSplineDistanceImplicitFunction::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  this->m_BSplineApproximation->Print(os);
}
