#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "itkSimpleFilterWatcher.h"
#include "itkVTKImageToImageFilter.h"
#include "pcmrWallShearRateFunction2.h"
#include "itkMeshFileReader.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include "itkAntiAliasBinaryImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkExtensionVelocitiesImageFilter.h"
#include "pcmrImageUtil.h"

typedef itk::Mesh<float,3> TriangleMeshType;
typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static int TimeStep = -1;
static bool BoundaryIsMesh = true;
static const char* BoundaryPath = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
TriangleMeshType::Pointer InputMesh = NULL;
itk::MaskImage::Pointer Mask = NULL;
itk::PhaseContrast3DImage::Pointer DistanceField = NULL;

class ShowProgressObject
{
public:
  ShowProgressObject(itk::ProcessObject* o)
    {m_Process = o;}
  void ShowProgress()
    {std::cout << "Progress " << m_Process->GetProgress() << std::endl;}
  itk::ProcessObject::Pointer m_Process;
};

TEST(TestExtrapolateVelocity, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(_ARGC,5) << "usage: '" << _ARGV[0] << "' study ts (-mesh|-mask) mesh_ormask_file\n";
  StudyDir = _ARGV[1];
  TimeStep = atoi(_ARGV[2]);
  EXPECT_GE(TimeStep,0) << " time-step must be positive";
  if (strcmp(_ARGV[3], "-mesh"))
    {
    BoundaryIsMesh = true;
    }
  else if(strcmp(_ARGV[3], "-mask"))
    {
    BoundaryIsMesh = false;
    }
  else
    {
    EXPECT_TRUE(false) << "wrong boundary type at argument 3, must be -mesh or -mask, assuming -mesh";
    }
  BoundaryPath = _ARGV[4];
}

TEST(TestExtrapolateVelocity, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  if (TimeStep < 0 || TimeStep >= dataSet->GetNumberOfTimeSteps())
    {
    double ts = dataSet->GetNumberOfTimeSteps() / 5;
    std::cout << "Invalid time-step '" << TimeStep << "' using default " << ts << std::endl;
    TimeStep = ts;
    }
  if (BoundaryIsMesh)
    {
    typedef itk::MeshFileReader<TriangleMeshType> MeshReaderType;
    MeshReaderType::Pointer  polyDataReader = MeshReaderType::New();
    polyDataReader->SetFileName(BoundaryPath);
    polyDataReader->Update();
    InputMesh = polyDataReader->GetOutput();
    }
}

TEST(TestExtrapolateVelocity, TestComputeMask)
{
  ASSERT_TRUE(dataSet);
  if (BoundaryIsMesh)
    {
    ImporterType::Pointer importerX = ImporterType::New();
    importerX->SetInput(dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX, TimeStep));
    importerX->Update();
    itk::PhaseContrast3DImage *imageX = importerX->GetOutput();
    
    typedef itk::TriangleMeshToBinaryImageFilter<TriangleMeshType,itk::MaskImage> TriangleMeshToBinaryImageFilterType;
    TriangleMeshToBinaryImageFilterType::Pointer rasterFilter = TriangleMeshToBinaryImageFilterType::New();
    rasterFilter->SetInput(InputMesh);
    const itk::MaskImage::PointType &origin = imageX->GetOrigin();
    const itk::MaskImage::SpacingType &spacing = imageX->GetSpacing();
    const itk::MaskImage::RegionType &region = imageX->GetLargestPossibleRegion();
    rasterFilter->SetOrigin(origin);
    rasterFilter->SetSpacing(spacing);
    rasterFilter->SetSize(region.GetSize());
    rasterFilter->SetInsideValue(1);
    rasterFilter->SetOutsideValue(0);
    rasterFilter->Update();
    Mask = rasterFilter->GetOutput();
    }
  else
    {
    typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
    MaskImporterType::Pointer importerMask = MaskImporterType::New();
    vtkImageData *vtkMask = dataSet->GetMaskImage(TimeStep);
    ASSERT_TRUE(vtkMask!=NULL);
    importerMask->SetInput(vtkMask);
    importerMask->Update();
    Mask = importerMask->GetOutput();
    }
}

//#define USE_SIGNEDMAURER 1

TEST(TestExtrapolateVelocity, TestComputeDistance)
{
  ASSERT_TRUE(dataSet);
  ASSERT_TRUE(Mask);
  

#ifdef USE_SIGNEDMAURER
  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
#else
  typedef itk::AntiAliasBinaryImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
#endif

  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(Mask);
#ifdef USE_SIGNEDMAURER
  distanceFilter->SquaredDistanceOff();
  distanceFilter->InsideIsPositiveOn();
#else
  distanceFilter->SetMaximumRMSError(0.005);
  distanceFilter->SetNumberOfLayers(6);
#endif
  distanceFilter->UseImageSpacingOn();
 
  pcmr::WriteImage(distanceFilter->GetOutput(), "TEV_DistanceField.vtk");
  DistanceField = distanceFilter->GetOutput();
}

TEST(TestExtrapolateVelocity, TestExtrapolate)
{
  ASSERT_TRUE(DistanceField);
  ImporterType::Pointer importerY = ImporterType::New();
  importerY->SetInput(dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY, TimeStep));
  importerY->Update();
  itk::PhaseContrast3DImage *imageY = importerY->GetOutput();
  const unsigned int AuxDimension = 1;
  typedef itk::ExtensionVelocitiesImageFilter<itk::PhaseContrast3DImage,float,AuxDimension> ExtrapolateFilterType;
  ExtrapolateFilterType::Pointer extrapolate = ExtrapolateFilterType::New();
  itk::SimpleFilterWatcher watcherFill(extrapolate, "Extrapolate");
  extrapolate->SetInput(DistanceField);
  extrapolate->SetInputVelocityImage(imageY , 0);

  extrapolate->SetLevelSetValue( 0.0 );
  extrapolate->NarrowBandingOn();
  extrapolate->SetNarrowBandwidth( 5 );
  extrapolate->Update();
  pcmr::WriteImage(extrapolate->GetOutputVelocityImage(0), "TEV_ImageY.vtk");
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
