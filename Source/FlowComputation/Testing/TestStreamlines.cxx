#include <iostream>
#include "pcmrStreamlines.h"
#include "pcmrFlow4DReader.h"
#include "vtkPolyData.h"
#include "gtest/gtest.h"

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static const char* SeedPointsFile = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;
static pcmr::SeedCollection SeedPoints;

TEST(TestStreamlines, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,2) << "usage: '" << _ARGV[0] << "' study ?SeedPoints.txt?\n";
  StudyDir = _ARGV[1];
  if (_ARGC > 2)
    {
    SeedPointsFile = _ARGV[2];
    }
}

TEST(TestStreamlines, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestStreamlines, TestInvalidTimeStep)
{
  ASSERT_TRUE(dataSet);
  size_t T = dataSet->GetNumberOfTimeSteps();
  pcmr::StreamlineConfiguration conf;
  pcmr::SeedCollection seeds;
  pcmr::StatusType status = pcmr::GenerateStreamlines(dataSet, T+1, seeds, conf, "filename");
  ASSERT_EQ(pcmr::INV_TIMESTEP, status);
}

TEST(TestStreamlines, TestEmptySeeds)
{
  ASSERT_TRUE(dataSet);
  pcmr::SeedCollection seeds;
  pcmr::StreamlineConfiguration conf;
  pcmr::StatusType status = pcmr::GenerateStreamlines(dataSet, 3, seeds,
                                                      conf, "filename");
  ASSERT_EQ(pcmr::SQ_EMPTY, status);
}

TEST(TestStreamlines, TestNullFile)
{
  ASSERT_TRUE(dataSet);
  pcmr::StreamlineConfiguration conf;
  pcmr::SeedCollection seeds;
  seeds.push_back(pcmr::SeedPoint());
  pcmr::StatusType status = pcmr::GenerateStreamlines(dataSet, 3, seeds,
                                                      conf, (char*)NULL);
  ASSERT_EQ(pcmr::INV_FILENAME, status);
  status = pcmr::GenerateStreamlines(dataSet, 3, seeds, conf, "");
  ASSERT_EQ(pcmr::INV_FILENAME, status);
}

TEST(TestStreamlines, TestReadPoints)
{
  ASSERT_TRUE(SeedPointsFile!=NULL);
  std::ifstream fin(SeedPointsFile);
  ASSERT_TRUE(fin.good());
  size_t n = 0;
  double v[3];
  size_t i = 0;
  while (!fin.eof())
    {
    fin >> v[i++];
    if (i==3)
      {
      SeedPoints.push_back(pcmr::SeedPoint(v[0], v[1], v[2]));
      i = 0;
      ++n;
      }
    }
  std::cout << n << " SeedPoints were read\n";
}

TEST(TestStreamlines, TestGenerateFile)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  pcmr::StreamlineConfiguration conf;
  /*conf.TerminalSpeed = 0.1;
  conf.InitialIntegrationStep = 0.1;
  conf.MinimumIntegrationStep = 0.1;
  conf.MaximumIntegrationStep = 2;*/
  pcmr::StatusType status = pcmr::GenerateStreamlines(dataSet, 3, SeedPoints,
                                                      conf, "TestStreamline.vtk");
  ASSERT_EQ(pcmr::OK, status) << "TestGenerateFile: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
}

TEST(TestStreamlines, TestGeneratePointer)
{
  ASSERT_TRUE(dataSet);
  ASSERT_GT(SeedPoints.size(),0);

  vtkSmartPointer<vtkPolyData> streamLines;
  pcmr::StreamlineConfiguration conf;
  pcmr::StatusType status = pcmr::GenerateStreamlines(dataSet, 3, SeedPoints,
                                                      conf, streamLines);
  ASSERT_EQ(pcmr::OK, status) << "TestGeneratePointer: "
                              << pcmr::GetStatusDescription(status) <<  "\n";
  ASSERT_TRUE(streamLines);
  streamLines->Print(std::cout);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
