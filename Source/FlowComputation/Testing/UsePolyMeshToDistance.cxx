#include "pcmrPolyMeshToSignedDistanceImageFilter.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyData.h"
#include "pcmrImageUtil.h"
#include "itkBinaryThresholdImageFilter.h"

//#define __USE_NORMAL_FILTER__

int main(int argc, const char *argv[])
{
  pcmr::PolyMeshToSignedDistanceImageFilter::Pointer distanceFilter = pcmr::PolyMeshToSignedDistanceImageFilter::New();
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  typedef pcmr::PolyMeshToSignedDistanceImageFilter::OutputImageType DistanceImageType;

  reader->SetFileName(argv[1]);

#if defined(__USE_NORMAL_FILTER__)
  vtkSmartPointer<vtkPolyDataNormals> polyNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
  polyNormals->SetInputConnection(reader->GetOutputPort());
  //polyNormals->AutoOrientNormalsOn();
  polyNormals->ComputeCellNormalsOn();
  polyNormals->Update();

  polyNormals->Print(std::cout);
#else
  reader->Update();
#endif

#if 0
  double bounds[6];
  reader->GetOutput()->GetBounds(bounds);
  double origin[3];
  double extend[3];
  extend[0] = bounds[1] - bounds[0];
  extend[1] = bounds[3] - bounds[2];
  extend[2] = bounds[5] - bounds[4];

  double ex[3];
  ex[0] = extend[0]*0.2;
  ex[1] = extend[1]*0.2;
  ex[2] = extend[2]*0.2;
  origin[0] = bounds[0] - ex[0];
  origin[1] = bounds[2] - ex[1];
  origin[2] = bounds[4] - ex[2];

  pcmr::PolyMeshToSignedDistanceImageFilter::SizeType size;
  size[0] = 90;
  size[1] = 130;
  size[2] = 90;

  double spacing[3];
  spacing[0] = (extend[0] + 2*ex[0])/size[0];
  spacing[1] = (extend[1] + 2*ex[1])/size[1];
  spacing[2] = (extend[2] + 2*ex[2])/size[2];

#else
  double origin[3] = {-6.5, -14, -6.5};
  double spacing[3] = {0.5, 0.5, 0.5};
  pcmr::PolyMeshToSignedDistanceImageFilter::SizeType size = {26, 56, 26};
#endif

  std::cout << "Origin = " 
            << "[" << origin[0] << ", " << origin[1] << ", " << origin[2] << "]" << std::endl;
  distanceFilter->SetOrigin(origin);
  distanceFilter->SetSpacing(spacing);
  distanceFilter->SetSize(size);
#if defined(__USE_NORMAL_FILTER__)
  distanceFilter->SetInputMesh(polyNormals->GetOutput());
#else
  distanceFilter->SetInputMesh(reader->GetOutput());
#endif
  distanceFilter->Update();
  //distanceFilter->GetOutput()->Print(std::cout);
  pcmr::WriteImage(distanceFilter->GetOutput(), "/tmp/polymesh_dmap.vtk");

  typedef itk::BinaryThresholdImageFilter<DistanceImageType, itk::MaskImage> ThresholdFilterType;
  ThresholdFilterType::Pointer thresholder = ThresholdFilterType::New();
  thresholder->SetInput(distanceFilter->GetOutput());
  thresholder->SetLowerThreshold(-1000);
  thresholder->SetUpperThreshold(0);
  thresholder->SetInsideValue(1);
  thresholder->SetOutsideValue(0);
  pcmr::WriteImage(thresholder->GetOutput(), "/tmp/polymesh_mask.vtk");
  
  return 0;
}
