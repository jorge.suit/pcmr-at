#include <iostream>
#include "pcmrFlow4DReader.h"
#include "gtest/gtest.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "itkVectorMagnitudeImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkMaskImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkBSplineControlPointImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkMath.h"
#include "TestHelper.h"

template< class TIVector, class TOVector>
class NormalizeVector
{
public:
  NormalizeVector() {};
  ~NormalizeVector() {};
  bool operator!=( const NormalizeVector & ) const
    {
    return false;
    }
  bool operator==( const NormalizeVector & other ) const
    {
    return !(*this != other);
    }
  inline TOVector operator()( const TIVector & A ) const
    {
      TOVector v( A );
      v.Normalize();
      return v;
    }
};

static int _ARGC = 0;
static char **_ARGV = NULL;

static const char* StudyDir = NULL;
static pcmr::Flow4DReader::Pointer dataSet = NULL;

TEST(TestWallShearRate, TestCommandLineArgs)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_GE(_ARGC,1) << "usage: '" << _ARGV[0] << "' study\n";
  StudyDir = _ARGV[1];
}

TEST(TestWallShearRate, TestLoadDataSet)
{
  ASSERT_TRUE(StudyDir!=NULL);
  dataSet = pcmr::Flow4DReader::New();

  pcmr::StatusType status = dataSet->SetDirectory(StudyDir);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
}

TEST(TestWallShearRate, TestComputeWSS)
{
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  typedef itk::VTKImageToImageFilter<itk::MaskImage> MaskImporterType;
   ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();
  MaskImporterType::Pointer importerMask = MaskImporterType::New();
  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                3);
  ASSERT_TRUE(vtkImgX!=NULL);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                3);
  ASSERT_TRUE(vtkImgY!=NULL);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                3);
  ASSERT_TRUE(vtkImgZ!=NULL);
  vtkImageData *vtkMask = dataSet->GetMaskImage(3);
  ASSERT_TRUE(vtkMask!=NULL);

  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);
  importerMask->SetInput(vtkMask);

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeVectorFilter;
  ComposeVectorFilter::Pointer composerFlow = ComposeVectorFilter::New();
  composerFlow->SetInput(0, importerX->GetOutput());
  composerFlow->SetInput(1, importerY->GetOutput());
  composerFlow->SetInput(2, importerZ->GetOutput());

  typedef itk::MaskImageFilter<itk::PhaseContrastVector3DImage,itk::MaskImage> MaskFilterType;
  MaskFilterType::Pointer masker = MaskFilterType::New();
  masker->SetInput(composerFlow->GetOutput());
  masker->SetMaskImage(importerMask->GetOutput());
  masker->Update();
  WriteImage(masker->GetOutput(), "MaskedFlowOriginal.vtk");

  typedef itk::SignedMaurerDistanceMapImageFilter<itk::MaskImage,itk::PhaseContrast3DImage> DistanceFilterType;
  DistanceFilterType::Pointer distanceFilter = DistanceFilterType::New();
  distanceFilter->SetInput(importerMask->GetOutput());
  distanceFilter->SquaredDistanceOff();
  distanceFilter->UseImageSpacingOn();
 
  WriteImage(distanceFilter->GetOutput(), "DistanceFieldOriginal.vtk");
  itk::MaskImage::SpacingType spacing;
  itk::MaskImage::PointType origin;
  itk::MaskImage::RegionType region;

  const size_t level = 3;
  GetSubdivisionInfo(distanceFilter->GetOutput(), level, origin, spacing, region);

  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector3DImage> BSplineVectorFilterType;
  BSplineVectorFilterType::Pointer bsplineFlow = BSplineVectorFilterType::New();
  bsplineFlow->SetOrigin(origin);
  bsplineFlow->SetSpacing(spacing);
  bsplineFlow->SetSize(region.GetSize());
  bsplineFlow->SetSplineOrder( 3 );
  bsplineFlow->SetInput(masker->GetOutput());
  bsplineFlow->Update();
  WriteImage(bsplineFlow->GetOutput(), "MaskedFlowInterpolated.vtk");

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector1DImage> ComposeScalarFilter;
  ComposeScalarFilter::Pointer composerDistance = ComposeScalarFilter::New();
  composerDistance->SetInput(0, distanceFilter->GetOutput());
  composerDistance->Update();

  typedef itk::BSplineControlPointImageFilter<itk::PhaseContrastVector1DImage> BSplineScalarFilterType;
  BSplineScalarFilterType::Pointer bsplineDistance = BSplineScalarFilterType::New();
  //region.Print(std::cout);
  bsplineDistance->SetOrigin(origin);
  bsplineDistance->SetSpacing(spacing);
  bsplineDistance->SetSize(region.GetSize());
  bsplineDistance->SetSplineOrder( 3 );
  bsplineDistance->SetInput(composerDistance->GetOutput());
  bsplineDistance->Update();
  //bsplineDistance->GetOutput()->Print(std::cout);
  WriteImage(bsplineDistance->GetOutput(), "DistanceFieldInterpolated.vtk");

  typedef itk::VectorMagnitudeImageFilter<itk::PhaseContrastVector3DImage,itk::PhaseContrast3DImage> MagnitudeFilterType;
  MagnitudeFilterType::Pointer magnitudeFilter = MagnitudeFilterType::New();
  magnitudeFilter->SetInput(bsplineFlow->GetOutput());
  WriteImage(magnitudeFilter->GetOutput(), "MagMaskedFlowInterpolated.vtk");

  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrast3DImage,itk::Covariant3DImage> GradientFilterType;
  GradientFilterType::Pointer gradientVelocity = GradientFilterType::New();
  gradientVelocity->SetInput(magnitudeFilter->GetOutput());
  WriteImage(gradientVelocity->GetOutput(), "GradMagdMaskedFlowInterpolate.vtk");

  typedef itk::GradientRecursiveGaussianImageFilter<itk::PhaseContrastVector1DImage,itk::Covariant3DImage> GradientVector1DFilterType;
  GradientVector1DFilterType::Pointer gradientDistance = GradientVector1DFilterType::New();
  gradientDistance->SetInput(bsplineDistance->GetOutput());
  WriteImage(gradientDistance->GetOutput(), "GradientDistanceFieldInterpolated.vtk");

  typedef itk::UnaryFunctorImageFilter<itk::Covariant3DImage, itk::Covariant3DImage, NormalizeVector<itk::Covariant3DImage::PixelType,itk::Covariant3DImage::PixelType> > NormalizeFilterType;
  NormalizeFilterType::Pointer normalizeFilter = NormalizeFilterType::New();
  normalizeFilter->SetInput(gradientDistance->GetOutput());
  WriteImage(normalizeFilter->GetOutput(), "NGradientDistanceFieldInterpolated.vtk");

  typedef itk::MultiplyImageFilter<itk::Covariant3DImage,itk::Covariant3DImage,itk::PhaseContrast3DImage> MultiplyCovariantFilterType;
  MultiplyCovariantFilterType::Pointer multiplyCovariant = MultiplyCovariantFilterType::New();
  multiplyCovariant->SetInput1(normalizeFilter->GetOutput());
  multiplyCovariant->SetInput2(gradientVelocity->GetOutput());
  /*
  normalizeFilter->Update();
  gradientVelocity->Update();
  normalizeFilter->GetOutput()->Print(std::cout);
  gradientVelocity->GetOutput()->Print(std::cout);
  */
  WriteImage(multiplyCovariant->GetOutput(), "WsrInterpolated.vtk");
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}

/*
DATASET STRUCTURED_POINTS
DIMENSIONS 132 192 24 
SPACING 1.7708333730698000e+00 1.7708333730698000e+00 2.5000000000000000e+00 
ORIGIN 3.7665241241455078e+01 -8.9783882141113281e+01 1.7031616210937500e+02 
POINT_DATA 608256

DIMENSIONS 132 192 24 
SPACING 1.7708333730698000e+00 1.7708333730698000e+00 2.5000000000000000e+00 
ORIGIN 3.7665241241455078e+01 -8.9783882141113281e+01 1.7031616210937500e+02 
POINT_DATA 608256

*/
