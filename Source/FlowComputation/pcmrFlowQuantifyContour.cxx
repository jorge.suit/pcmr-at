#include "pcmrFlowQuantifyContour.h"
#include <vtkSmartPointer.h>
#include <vtkImplicitSelectionLoop.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkIdList.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>

BEGIN_PCMR_DECLS

const int MaxContourResolution = 1000;

int FlowQuantifyVtkContour(vtkPolyData *planeInterpolated,
                           vtkParametricSpline *splineContour,
                           double planeNormal[],
                           int contourResolution,
                           std::map<std::string,float> &table)
{
  table["area"] = 0.0;
  table["flow"] = 0.0;
  table["max_velocity"] = 0.0;
  table["min_velocity"] = 0.0;
  table["flow_forward"] = 0.0;
  table["flow_backward"] = 0.0;

  vtkSmartPointer<vtkPoints> contourPoints =
    vtkSmartPointer<vtkPoints>::New();

  if (contourResolution <= 4 || contourResolution > MaxContourResolution)
    {
    contourResolution = 50;
    }
  contourPoints->SetNumberOfPoints(contourResolution - 1);
  float delta = 1.0/(contourResolution-1);
  double u[3], Pt[3], Du[3];
  for (int i = 0; i < contourResolution-1; i++)
    {
    u[0] = i*delta;
    splineContour->Evaluate(u, Pt, Du);
    contourPoints->SetPoint(i, Pt);
    }
  vtkSmartPointer<vtkImplicitSelectionLoop> selectionLoop =
    vtkSmartPointer<vtkImplicitSelectionLoop>::New();
  selectionLoop->AutomaticNormalGenerationOff();
  selectionLoop->SetNormal(planeNormal);
  selectionLoop->SetLoop(contourPoints);
  vtkSmartPointer<vtkExtractPolyDataGeometry> extractCell =
    vtkSmartPointer<vtkExtractPolyDataGeometry>::New();
  extractCell->SetImplicitFunction(selectionLoop);
  extractCell->ExtractInsideOn();
  extractCell->ExtractBoundaryCellsOn();
  extractCell->SetInputData(planeInterpolated);
  extractCell->Update();
  vtkPolyData* insidePolyData = extractCell->GetOutput();
  std::cout << "There are " << insidePolyData->GetNumberOfPolys() 
            << " polys inside contour." << std::endl;

  ///////// Get Point Vectors ///////////
  vtkDataArray *pointVectors = insidePolyData->GetPointData()->GetVectors();
  if(pointVectors)
    { 
    std::cout << "There are " << pointVectors->GetNumberOfTuples() 
              << " point vectors." << std::endl;
    
    /*
    for(vtkIdType i = 0; i < pointNormalsRetrieved->GetNumberOfTuples(); i++)
      {
      double pN[3];
      pointNormalsRetrieved->GetTuple(i, pN);
      std::cout << "Point normal " << i << ": " 
                << pN[0] << " " << pN[1] << " " << pN[2] << std::endl;
      }
     */
 
    }
  else
    {
    std::cout << "No point vectors." << std::endl;
    }

  vtkPoints *allPoints = insidePolyData->GetPoints();
  bool areaComputed = false;
  double quadArea = 0.0;
  vtkSmartPointer<vtkIdList> idList = vtkSmartPointer<vtkIdList>::New();
  insidePolyData->GetPolys()->InitTraversal();
  double normal[3];
  double nodeVelocity[3];
  selectionLoop->GetNormal(normal);
  double modNormal = vtkMath::Normalize(normal);
  /*
  std::cout << "modNormal = " << modNormal << std::endl;
  std::cout << "normal = " 
            << normal[0] << ", " << normal[1] << ", " << normal[2]
            << std::endl;
  */
  double totalFlow = 0.0;
  double minVelocity = 0.0;
  double maxVelocity = -1.0;
  double forwardFlow = 0.0;
  double backwardFlow = 0.0;
  while(insidePolyData->GetPolys()->GetNextCell(idList))
    {
    if (idList->GetNumberOfIds() != 4)
      {
      std::cout << "Unexpected cell of side " << idList->GetNumberOfIds()
                << std::endl;
      continue;
      }
    if (!areaComputed)
      {
      double pt0[3], pt1[3], pt2[3];
      insidePolyData->GetPoints()->GetPoint(idList->GetId(0), pt0);
      insidePolyData->GetPoints()->GetPoint(idList->GetId(1), pt1);
      insidePolyData->GetPoints()->GetPoint(idList->GetId(2), pt2);
      quadArea = vtkTriangle::TriangleArea(pt0, pt1, pt2) * 2;
      areaComputed = true;
      }
    double flow = 0.0;
    for(int i = 0; i< 4; i++)
      {
      pointVectors->GetTuple(idList->GetId(i), nodeVelocity);
      double velocity = vtkMath::Norm(nodeVelocity);
      if (maxVelocity < 0.0)
        {
        minVelocity = maxVelocity = velocity;
        }
      else if (velocity > maxVelocity)
        {
          maxVelocity = velocity;
        }
      else if (velocity < minVelocity)
        {
          minVelocity = velocity;
        }
      /*
      std::cout << "nodeVelocity = " 
            << nodeVelocity[0] << ", " << nodeVelocity[1] << ", " << nodeVelocity[2]
            << std::endl;
      */
      flow += vtkMath::Dot(nodeVelocity, normal);
      //std::cout << "flow = " << flow << std::endl;
      }
    totalFlow += flow;
    if (flow < 0)
      {
      backwardFlow += flow;
      }
    else
      {
      forwardFlow += flow;
      }
    }
  const double factorFlow = quadArea/4;
  std::cout << "factorFlow = " << factorFlow << std::endl;
  totalFlow *= factorFlow;
  backwardFlow *= factorFlow;
  forwardFlow *= factorFlow;
  std::cout << "C++ - Element area: " << quadArea << std::endl;
  std::cout << "C++ - Plane Elements Inside: " << insidePolyData->GetNumberOfPolys() << std::endl;
  
  table["area"] = quadArea * insidePolyData->GetNumberOfPolys();
  table["flow"] = totalFlow;
  table["flow_forward"] = forwardFlow;
  table["flow_backward"] = backwardFlow;
  table["max_velocity"] = maxVelocity;
  table["min_velocity"] = minVelocity;
  return OK;
}

END_PCMR_DECLS
