#ifndef __pcmrStreamlines_h
#define __pcmrStreamlines_h

#include "pcmrTypes.h"
#include "pcmrFlowComputation.h"
#include "pcmrFlow4DReader.h"
#include "pcmrFlowComputation_Export.h"

class vtkPolyData;

BEGIN_PCMR_DECLS

struct PCMRFLOWCOMPUTATION_EXPORT StreamlineConfiguration
{
  double MaximumPropagation;
  double TerminalSpeed;
  double InitialIntegrationStep;
  double MinimumIntegrationStep;
  double MaximumIntegrationStep;
  double MaximumError;
  size_t MaximumNumberOfSteps;
  int    Solver;
  int    Direction;
  StreamlineConfiguration();
};

PCMRFLOWCOMPUTATION_EXPORT
StatusType GenerateStreamlines(Flow4DReader* DataSet, size_t timeStep,
                               const SeedCollection&,
                               const StreamlineConfiguration &conf,
                               const char* OutputFile);

PCMRFLOWCOMPUTATION_EXPORT
StatusType GenerateStreamlines(Flow4DReader* DataSet, size_t timeStep,
                               const SeedCollection&,
                               const StreamlineConfiguration &conf,
                               vtkSmartPointer<vtkPolyData> &streamLines);

END_PCMR_DECLS

#endif
