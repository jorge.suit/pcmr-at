#include "pcmrSmoothExtrapolatedLayersFilter.h"

BEGIN_PCMR_DECLS
template <class TInputImage, class TMaskImage>
SmoothExtrapolatedLayersFilter<TInputImage,TMaskImage>::SmoothExtrapolatedLayersFilter()
{
  this->SetNumberOfRequiredInputs( 2 );
  this->m_GaussianSmoother = SmoothingFilterType::New();
  this->m_ThresholdInside = ThresholdImageType::New();
  this->m_ThresholdOutside = ThresholdImageType::New();
  this->m_MaskInside = MaskFilterType::New();
  this->m_MaskOutside = MaskFilterType::New();
  this->m_AddImages = AddFilterType::New();

  this->m_ThresholdInside->SetInsideValue( 1 );
  this->m_ThresholdInside->SetOutsideValue( 0 );
  this->m_ThresholdInside->SetLowerThreshold( 1 );
  this->m_ThresholdInside->SetUpperThreshold( 2 );

  this->m_ThresholdOutside->SetInsideValue( 1 );
  this->m_ThresholdOutside->SetOutsideValue( 0 );
  this->m_ThresholdOutside->SetLowerThreshold( 3 );
  this->m_ThresholdOutside->SetUpperThreshold( 128 );

  this->m_MaskInside->SetMaskImage( this->m_ThresholdInside->GetOutput( ) );

  this->m_MaskOutside->SetMaskImage( this->m_ThresholdOutside->GetOutput( ) );
  this->m_MaskOutside->SetInput( this->m_GaussianSmoother->GetOutput( ) );

  this->m_AddImages->SetInput1( this->m_MaskInside->GetOutput( ) );
  this->m_AddImages->SetInput2( this->m_MaskOutside->GetOutput( ) );
}

template <class TInputImage, class TMaskImage>
void SmoothExtrapolatedLayersFilter<TInputImage,TMaskImage>
::GenerateData()
{
  this->m_GaussianSmoother->SetInput( this->GetInput( 0 ) );
  const itk::PhaseContrast3DImage::SpacingType &spacing = this->GetInput( 0 )->GetSpacing();
  SigmaArrayType sigmas;
  for( int i = 0; i < 3; i ++ )
    {
    sigmas[ i ] = spacing[ i ];
    }
  this->m_GaussianSmoother->SetSigmaArray( sigmas );

  this->m_ThresholdInside->SetInput( this->GetLayerMaskImage( ) );
  this->m_ThresholdOutside->SetInput( this->GetLayerMaskImage( ) );

  this->m_MaskInside->SetInput( this->GetInput( 0 ) );

  this->m_AddImages->GraftOutput( this->GetOutput( ) );
  this->m_AddImages->Update();
  this->GraftOutput( this->m_AddImages->GetOutput() );
}

template <class TInputImage, class TMaskImage>
void SmoothExtrapolatedLayersFilter<TInputImage,TMaskImage>
::SetLayerMaskImage( const itk::MaskImage * maskLayer )

{
  this->SetNthInput( 1, const_cast<TMaskImage *>(maskLayer) );
}

template <class TInputImage, class TMaskImage>
const itk::MaskImage * SmoothExtrapolatedLayersFilter<TInputImage,TMaskImage>
::GetLayerMaskImage() const
{
  // const_cast<const TMaskImage*>( 
  return static_cast<TMaskImage*>(const_cast<itk::DataObject *>(this->itk::ProcessObject::GetInput(1)));
}


template <class TInputImage, class TMaskImage>
void SmoothExtrapolatedLayersFilter<TInputImage,TMaskImage>
::PrintSelf( std::ostream& os, itk::Indent indent ) const
{
  Superclass::PrintSelf(os,indent);
  this->m_GaussianSmoother->Print( os, indent );
}

END_PCMR_DECLS
