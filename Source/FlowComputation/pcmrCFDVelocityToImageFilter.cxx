#include "pcmrCFDVelocityToImageFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkGenericCell.h"
#include "vtkMinimalStandardRandomSequence.h"
#include "vtkNew.h"
#include "itkImageRegionIteratorWithIndex.h"

BEGIN_PCMR_DECLS

void CFDVelocityToImageFilter::SetSpacing( const double spacing[ 3 ] )
{
  SpacingType s;
  for( unsigned int i = 0; i < itk::PhaseContrast3DImage::ImageDimension; ++i )
    {
    s[ i ] = static_cast<SpacingType::ValueType>( spacing[ i ] );
    }
  this->SetSpacing(s);
}

void CFDVelocityToImageFilter::SetSpacing( const float spacing[ 3 ] )
{
  itk::Vector< float, 3 > sf( spacing );
  SpacingType        s;
  s.CastFrom( sf );
  this->SetSpacing( s );
}

void CFDVelocityToImageFilter::SetOrigin(const double origin[3])
{
  PointType p(origin);

  this->SetOrigin(p);
}

void CFDVelocityToImageFilter::SetOrigin(const float origin[3])
{
  itk::Point< float, 3 > of(origin);
  PointType         p;
  p.CastFrom(of);
  this->SetOrigin(p);
}

CFDVelocityToImageFilter::CFDVelocityToImageFilter()
{
  //Initial image is 64 wide in each direction.
  for ( unsigned int i = 0; i < itk::PhaseContrast3DImage::GetImageDimension(); i++ )
    {
    m_Size[i] = 64;
    m_Spacing[i] = 1.0;
    m_Origin[i] = 0.0;
    }
  m_Direction.SetIdentity();
  this->SetNumberOfRequiredOutputs( 3 );
  for( int i = 0; i < 3; i++ )
    {
    this->ProcessObject::SetNthOutput( i, this->MakeOutput( i ) );
    }
  this->m_SampleSize = 25;
}

CFDVelocityToImageFilter::~CFDVelocityToImageFilter()
{
  /*
  for(size_t i = 0; i < this->m_ImplicitFunctionAtThread.size(); i++)
    {
    this->m_ImplicitFunctionAtThread[i]->Delete();
    this->m_ImplicitFunctionAtThread[i] = NULL;
    }
  */
}

void CFDVelocityToImageFilter::SetInputCFD( vtkUnstructuredGrid *input )
{
  this->m_CFDVelocity = input;
}

vtkUnstructuredGrid * CFDVelocityToImageFilter::GetInputCFD()
{
  return this->m_CFDVelocity;
}

void CFDVelocityToImageFilter::GenerateOutputInformation( )
{
  OutputImageType *output;
  IndexType     index;

  index.Fill(0);

  OutputImageType::RegionType largestPossibleRegion;
  largestPossibleRegion.SetSize( this->m_Size );
  largestPossibleRegion.SetIndex( index );
  for( int idx = 0; idx < this->GetNumberOfRequiredOutputs( ); idx++ )
    {
    output = this->GetOutput( idx );
    output->SetLargestPossibleRegion( largestPossibleRegion );
    output->SetBufferedRegion( largestPossibleRegion );        // set the region
    output->SetRequestedRegion( largestPossibleRegion );       //
    output->SetSpacing( m_Spacing );
    output->SetOrigin( m_Origin );
    output->SetDirection( m_Direction );
    //output->Print(std::cout);
    }
}

void CFDVelocityToImageFilter::GenerateData( )
{
  std::cout << "pcmrCFDVelocityToImageFilter::GenerateData() called\n";
  
  itk::PhaseContrast3DImage::Pointer output[ 3 ];
  for( int idx = 0; idx < 3; idx++ )
    {
    output[ idx ] = this->GetOutput( idx );
    output[ idx ]->Allocate( );
    output[ idx ]->FillBuffer( 0.0 );
    }
  itk::Image<unsigned, 3>::Pointer counter = itk::Image<unsigned, 3>::New( );
  counter->SetRegions( output[ 0 ]->GetLargestPossibleRegion( ) );
  counter->SetSpacing( output[ 0 ]->GetSpacing( ) );
  counter->SetOrigin( output[ 0 ]->GetOrigin( ) );
  counter->SetDirection( output[ 0 ]->GetDirection( ) );
  counter->Allocate( );
  counter->FillBuffer( 0 );

  vtkDataArray *velocity = this->m_CFDVelocity->GetPointData( )->GetArray( "Velocity" );
  assert( velocity );
  assert( velocity->GetNumberOfComponents( ) == 3 );
  std::cout << "Number Of Points = " << this->m_CFDVelocity->GetNumberOfPoints( ) << std::endl;
  // process first the vertexes of the mesh
  for( vtkIdType ip = 0; ip < this->m_CFDVelocity->GetNumberOfPoints( ); ip++ )
    {
    PointType pt;
    IndexType index;
    double p[3];
    this->m_CFDVelocity->GetPoint( ip, p );
    for( int i = 0; i < 3; i++ )
      {
      pt[ i ] = p[ i ];
      }
    if( output[ 0 ]->TransformPhysicalPointToIndex( pt, index ) )
      {
      double v[3];
      velocity->GetTuple( ip, v );
      unsigned &pixelCount = counter->GetPixel( index );
      unsigned n = pixelCount;
      ++pixelCount;
      for( int idx = 0; idx < 3; idx++ )
        {
        // reference of pixel velocity
        PixelType &pixelVel = output[ idx ]->GetPixel( index ); 
        pixelVel = ( pixelVel * n + v[ idx ] ) / pixelCount;
        }
      }
    }
  // now process the interior of each cell
  const int sizeCell = this->m_CFDVelocity->GetMaxCellSize( );
  double *weights = new double[ sizeCell ];
  vtkNew<vtkGenericCell> cell;
  unsigned int randomSample = this->GetSampleSize( );
  if ( !randomSample )
    {
    randomSample = 25;
    }
  std::cout << "using randomSample = " << randomSample << std::endl;
  double pcoords[3];
  vtkNew<vtkMinimalStandardRandomSequence> randomSequence;
  randomSequence->Next();
  for( vtkIdType ic = 0; ic < this->m_CFDVelocity->GetNumberOfCells( ); ic++ )
    {
    PointType pt;
    IndexType index;
    double p[3];
    // retrieve the current cell
    this->m_CFDVelocity->GetCell( ic, cell.GetPointer( ) );
    // sample velocity for the current cell
    for( int j = 0; j < randomSample; j++ )
      {
      // generate a random parametric coordinate
      for( int i = 0; i < 3; i++ )
        {
        pcoords[i] = randomSequence->GetValue();
        randomSequence->Next();
        }
      // find the global coordinates from pcoords, also the
      // interpolation weights.
      int subId = 0;
      cell->EvaluateLocation( subId, pcoords, p, weights );
      for( int i = 0; i < 3; i++ )
        {
        pt[ i ] = p[ i ];
        }
      // locate the voxel for this point
      if( output[ 0 ]->TransformPhysicalPointToIndex( pt, index ) )
        {
        double v[3];
        v[0] = v[1] = v[2] = 0;
        // interpolate the velocity using the weights generated
        for( int iw = 0; iw < cell->GetNumberOfPoints( ); iw++ )
          {
          // obtain the global point id for the current cell vertex
          vtkIdType idPt = cell->GetPointId( iw );
          double vp[3];
          velocity->GetTuple( idPt, vp );
          // acummulate the weighted velocity
          v[0] += weights[iw]*vp[0];
          v[1] += weights[iw]*vp[1];
          v[2] += weights[iw]*vp[2];
         }
        // cummulative average after computing the sampled velocity
        unsigned &pixelCount = counter->GetPixel( index );
        unsigned n = pixelCount;
        ++pixelCount;
        for( int idx = 0; idx < 3; idx++ )
          {
          // reference of pixel velocity
          PixelType &pixelVel = output[ idx ]->GetPixel( index ); 
          pixelVel = ( pixelVel * n + v[ idx ] ) / pixelCount;
          }
        }
      }
    }
  delete []weights;
}

END_PCMR_DECLS
