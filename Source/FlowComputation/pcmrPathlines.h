#ifndef __pcmrParticleTracer_h
#define __pcmrParticleTracer_h

#include "pcmrTypes.h"
#include "pcmrFlowComputation.h"
#include "pcmrFlowComputation_Export.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

BEGIN_PCMR_DECLS

class Flow4DReader;

struct PathlineConfiguration
{
  double TerminalSpeed;
  size_t TimeStepResolution;
  int StartTime;
  int EndTime;

  PathlineConfiguration()
  {
    TerminalSpeed = 0.1;
    TimeStepResolution = 1;
    StartTime = -1;
    EndTime = -1;
  }
};

PCMRFLOWCOMPUTATION_EXPORT
StatusType GeneratePathlines(Flow4DReader* dataSet,
                             const SeedCollection& seeds,
                             const PathlineConfiguration &conf,
                             const char* outputFile);

PCMRFLOWCOMPUTATION_EXPORT
StatusType GeneratePathlines(Flow4DReader* dataSet,
                             const SeedCollection& seeds,
                             const PathlineConfiguration &conf,
                             vtkSmartPointer<vtkPolyData> &streamLines);

END_PCMR_DECLS

#endif
