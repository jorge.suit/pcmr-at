#include <vtkImageData.h>
#include "pcmrTurbulenceKineticEnergy.h"
#include "pcmrOnlineVariance.h"

BEGIN_PCMR_DECLS

StatusType ComputeTKE(Flow4DReader* DataSet, vtkImageData *OutputTKE)
{
  size_t nPhases =  DataSet->GetNumberOfTimeSteps();

  // allocate the pointers to the scalar arrays of the input images.
  struct pFlowBuffer
  {
    float *pPixel[3];
  };
  pFlowBuffer *pPixelIn = new pFlowBuffer[nPhases];
  for(size_t t = 0; t < nPhases; t++)
    {
#if 0
    vtkImageData *imageData = DataSet->GetFlowImage(Flow4DReader::FlowVector, t);
    if (t == 0)
      {
      imageData->Print(std::cout);
      }
#endif
    pPixelIn[t].pPixel[0] = static_cast<float*>(DataSet->GetFlowImage(Flow4DReader::FlowX, t)->GetScalarPointer());
    pPixelIn[t].pPixel[1] = static_cast<float*>(DataSet->GetFlowImage(Flow4DReader::FlowY, t)->GetScalarPointer());
    pPixelIn[t].pPixel[2] = static_cast<float*>(DataSet->GetFlowImage(Flow4DReader::FlowZ, t)->GetScalarPointer());
    }
  // Create the output image.
  // OutputTKE = vtkImageData::New();
  // The output image has the same geometry an topology
  OutputTKE->CopyStructure(DataSet->GetFlowImage(Flow4DReader::FlowX, 0));
  // The pixel type of the output image is float
  OutputTKE->AllocateScalars(VTK_FLOAT,1);
  // pointer to the output raw data
  float *pPixelOut = static_cast<float*>(OutputTKE->GetScalarPointer());
  // the online variance calculator
  OnlineVariance var[3];

//#define TRACE_EVERY_N  
#ifdef TRACE_EVERY_N
  const int traceEvery = 9999;
#endif

  float flow[3];
  const vtkIdType numberOfPoints = OutputTKE->GetNumberOfPoints();
  // loop over all pixels
  for (vtkIdType i = 0; i < numberOfPoints; i++)
    {
#ifdef TRACE_EVERY_N
    if (!(i % traceEvery))
      {
      std::cout << "tracing point number " << i << std::endl;
      }
#endif
    // initialize the accumulator of the variance calculator
    for(size_t ic = 0; ic < 3; ic++)
      {
      var[ic].Reset();
      }
    // iterate over time for current pixel.
    for (size_t t = 0; t < nPhases; t++)
      {
#ifdef TRACE_EVERY_N
      if (!(i % traceEvery))
        {      
        std::cout << "    ";
        }
#endif
      // loop over the 3 components
      for(size_t ic = 0; ic < 3; ic++)
        {
        float v = *pPixelIn[t].pPixel[ic];
        ++(pPixelIn[t].pPixel[ic]);
#ifdef TRACE_EVERY_N
        if (!(i % traceEvery))
          {
          if (ic != 0)
            {
            std::cout << ",";
            }
          std::cout << v;
          }
#endif
        // accumulate the component of the flow in the variance calculator
        var[ic].AddNewData(v);
        }
#ifdef TRACE_EVERY_N
      if (!(i % traceEvery))
        {      
        std::cout << "\n";
        }
#endif
      }
#ifdef TRACE_EVERY_N
      if (!(i % traceEvery))
        {
        std::cout << "Computed variance:\n";
        std::cout << "    ";
        }
#endif
    // compute the kinetic energy of the variance computed.
    double tke = 0;
    for(size_t ic = 0; ic < 3; ic++)
      {
      double v = var[ic].GetVariance();
#ifdef TRACE_EVERY_N
      if (!(i % traceEvery))
        {
        if (ic != 0)
          {
          std::cout << ", ";
          }
        std::cout << v;
        }
#endif
      tke += v*v;
      }
    // convert cm^2/s^2 to m^2/s^2
    *pPixelOut = tke*0.5e-4;
    ++pPixelOut;
#ifdef TRACE_EVERY_N
    std::cout << std::endl;
#endif
    }

  delete []pPixelIn;
  return OK;
}

END_PCMR_DECLS
