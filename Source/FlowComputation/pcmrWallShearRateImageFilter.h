#ifndef __pcmrWallShearRateImageFilter_h
#define __pcmrWallShearRateImageFilter_h

#include "pcmrDefs.h"
#include "itkImageToImageFilter.h"
#include "pcmrWallShearRateFunction.h"

BEGIN_PCMR_DECLS

class WallShearRateImageFilter : public itk::ImageToImageFilter<itk::MaskImage,itk::PhaseContrast3DImage>
{
 public:
  /** Standard class typedefs. */
  typedef WallShearRateImageFilter             Self;
  typedef itk::ImageToImageFilter<itk::MaskImage, itk::PhaseContrast3DImage> Superclass;
  typedef itk::SmartPointer< Self >        Pointer;
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(WallShearRateImageFilter, itk::ImageToImageFilter);

  StatusType SetInputFromStudy(Flow4DReader *dataSet, size_t timeStep);

  itk::MaskImage *GetMaskImage() const
  {
    return this->m_Function->GetMaskImage();
  }

  StatusType SetBoundaryMesh(TriangleMeshType *mesh);

  void SetSplineOrder(unsigned int n)
  {
    this->m_Function->SetSplineOrder(n);
  }

  unsigned int GetSplineOrder()
  {
    return this->m_Function->GetSplineOrder();
  }

  void SetCorrectedFlowFileName(const std::string & fileName)
  {
    this->m_Function->SetCorrectedFlowFileName(fileName);
  }

  void SetLevelImageFileName(const std::string & fileName)
  {
    this->m_Function->SetLevelImageFileName(fileName);    
  }

  bool GetComputeWallCondition(void) const
  {
    return this->m_Function->GetComputeWallCondition();
  }
  
  void SetComputeWallCondition(bool c)
  {
    this->m_Function->SetComputeWallCondition(c);
  }

protected:
  /** Does the real work. */
  virtual void ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);

  WallShearRateImageFilter();
  ~WallShearRateImageFilter() {}

  virtual void BeforeThreadedGenerateData();

private:
  WallShearRateImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &);  //purposely not implemented

  WallShearRateFunction::Pointer m_Function;
};

END_PCMR_DECLS

#endif
