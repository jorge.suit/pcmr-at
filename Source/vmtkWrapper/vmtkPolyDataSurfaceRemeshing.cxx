/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vmtkPolyDataSurfaceRemeshing.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vmtkPolyDataSurfaceRemeshing);

//----------------------------------------------------------------------------
vmtkPolyDataSurfaceRemeshing::vmtkPolyDataSurfaceRemeshing()
{
  this->m_SurfaceRemeshFilter = vtkSmartPointer<vtkvmtkPolyDataSurfaceRemeshing>::New();
  this->TargetEdgeLength = 1.0;
}

//----------------------------------------------------------------------------
vmtkPolyDataSurfaceRemeshing::~vmtkPolyDataSurfaceRemeshing()
{
}

int vmtkPolyDataSurfaceRemeshing::ProcessRequest(
  vtkInformation *request,
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  return this->m_SurfaceRemeshFilter->ProcessRequest( request, inputVector, outputVector );
}

//----------------------------------------------------------------------------
void vmtkPolyDataSurfaceRemeshing::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "Target Area: " << this->m_SurfaceRemeshFilter->GetTargetArea() << std::endl;
  os << indent << "TargetEdgeLength: " << this->GetTargetEdgeLength() << std::endl;
  os << indent << "TargetAreaFactor: " << this->m_SurfaceRemeshFilter->GetTargetAreaFactor() << std::endl;
  os << indent << "TargetEdgeLengthFactor: " << this->GetTargetEdgeLengthFactor() << std::endl;

  os << indent << "TriangleSplitFactor: " << this->m_SurfaceRemeshFilter->GetTriangleSplitFactor() << std::endl;
  os << indent << "MaxArea: " << this->m_SurfaceRemeshFilter->GetMaxArea() << std::endl;
  os << indent << "MinArea: " << this->m_SurfaceRemeshFilter->GetMinArea() << std::endl;
  os << indent << "MaxEdgeLength: " << this->GetMaxEdgeLength() << std::endl;
  os << indent << "MinEdgeLength: " << this->GetMinEdgeLength() << std::endl;
  os << indent << "NumberOfIterations: " << this->m_SurfaceRemeshFilter->GetNumberOfIterations() << std::endl;
  os << indent << "NumberOfConnectivityOptimizationIterations: " << this->m_SurfaceRemeshFilter->GetNumberOfConnectivityOptimizationIterations() << std::endl;
  //os << indent << "CellEntityIdsArrayName: " << this->m_SurfaceRemeshFilter->GetCellEntityIdsArrayName() << std::endl;
  //os << indent << "TargetAreaArrayName: " << this->m_SurfaceRemeshFilter->GetTargetAreaArrayName() << std::endl;
  int smode = this->m_SurfaceRemeshFilter->GetElementSizeMode();
  os << indent << "ElementSizeMode: " << smode << " (" << (smode==vtkvmtkPolyDataSurfaceRemeshing::TARGET_AREA ? "AREA" :(smode==vtkvmtkPolyDataSurfaceRemeshing::TARGET_AREA_ARRAY ? "AREA_ARRAY" : "UNDEFINED")) << ")" << std::endl;
  os << indent << "MinAreaFactor: " << this->m_SurfaceRemeshFilter->GetMinAreaFactor() << std::endl;
  os << indent << "AspectRatioThreshold: " << this->m_SurfaceRemeshFilter->GetAspectRatioThreshold() << std::endl;
  os << indent << "InternalAngleTolerance: " << this->m_SurfaceRemeshFilter->GetInternalAngleTolerance() << std::endl;
  os << indent << "NormalAngleTolerance: " << this->m_SurfaceRemeshFilter->GetNormalAngleTolerance() << std::endl;
  os << indent << "CollapseAngleThreshold: " << this->m_SurfaceRemeshFilter->GetCollapseAngleThreshold() << std::endl;
  os << indent << "Relaxation: " << this->m_SurfaceRemeshFilter->GetRelaxation() << std::endl;
  os << indent << "PreserveBoundaryEdges: " << this->m_SurfaceRemeshFilter->GetPreserveBoundaryEdges() << std::endl;
}
