/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME vmtkPolyDataSurfaceRemeshing - Wrapper class for vtkvmtkPolyDataSurfaceRemeshing.
// .SECTION Description
//  vmtkPolyDataSurfaceRemeshing is a simple class that uses vtk_module to add tcl wrapping to vtkvmtkPolyDataSurfaceRemeshing.  This class can be
// copied and modified to produce your own classes.

#ifndef __vmtkPolyDataSurfaceRemeshing_h
#define __vmtkPolyDataSurfaceRemeshing_h

#include "vmtkWrapperModule.h" // export macro
#include "vtkObject.h"
#include "vtkSmartPointer.h"

#include "vtkvmtkPolyDataSurfaceRemeshing.h"

class VMTKWRAPPER_EXPORT vmtkPolyDataSurfaceRemeshing : public vtkPolyDataAlgorithm
{
public:
  static vmtkPolyDataSurfaceRemeshing * New();
  vtkTypeMacro(vmtkPolyDataSurfaceRemeshing, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  vtkSetMacro(TargetEdgeLength,double);
  vtkGetMacro(TargetEdgeLength,double);
  vtkSetMacro(TargetEdgeLengthFactor,double);
  vtkGetMacro(TargetEdgeLengthFactor,double);
  vtkSetMacro(MaxEdgeLength,double);
  vtkGetMacro(MaxEdgeLength,double);
  vtkSetMacro(MinEdgeLength,double);
  vtkGetMacro(MinEdgeLength,double);

  void SetTriangleSplitFactor( double f )
  {
    this->m_SurfaceRemeshFilter->SetTriangleSplitFactor( f );
  }

  double GetTriangleSplitFactor( )
  {
    return this->m_SurfaceRemeshFilter->GetTriangleSplitFactor( );
  }

  void SetTargetArea( double a )
  {
    this->m_SurfaceRemeshFilter->SetTargetArea( a );
  }

  double GetTargetArea( )
  {
    return this->m_SurfaceRemeshFilter->GetTargetArea();
  }

  void SetMinAreaFactor( double a )
  {
    this->m_SurfaceRemeshFilter->SetMinAreaFactor( a );
  }

  double GetMinAreaFactor( )
  {
    return this->m_SurfaceRemeshFilter->GetMinAreaFactor();
  }

  void SetCollapseAngleThreshold( double a )
  {
    this->m_SurfaceRemeshFilter->SetCollapseAngleThreshold( a );
  }

  double GetCollapseAngleThreshold( )
  {
    return this->m_SurfaceRemeshFilter->GetCollapseAngleThreshold();
  }

  void SetPreserveBoundaryEdges( int b )
  {
    this->m_SurfaceRemeshFilter->SetPreserveBoundaryEdges( b );
  }
  
  int GetPreserveBoundaryEdges( )
  {
    return this->m_SurfaceRemeshFilter->GetPreserveBoundaryEdges();
  }

  void PreserveBoundaryEdgesOn( )
  {
    this->m_SurfaceRemeshFilter->PreserveBoundaryEdgesOn();
  }

  void PreserveBoundaryEdgesOff( )
  {
    this->m_SurfaceRemeshFilter->PreserveBoundaryEdgesOff();
  }

  virtual int ProcessRequest(vtkInformation *request, 
			     vtkInformationVector **inputVector,
			     vtkInformationVector *outputVector);

protected:
  vmtkPolyDataSurfaceRemeshing();
  ~vmtkPolyDataSurfaceRemeshing();

  vtkSmartPointer<vtkvmtkPolyDataSurfaceRemeshing> m_SurfaceRemeshFilter;

  double TargetEdgeLength;
  double TargetEdgeLengthFactor;
  double MaxEdgeLength;
  double MinEdgeLength;

private:
  vmtkPolyDataSurfaceRemeshing(const vmtkPolyDataSurfaceRemeshing&);  // Not implemented.
  void operator=(const vmtkPolyDataSurfaceRemeshing&);  // Not implemented.
};

#endif
