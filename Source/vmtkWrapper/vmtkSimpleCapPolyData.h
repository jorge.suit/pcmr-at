/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME vmtkSimpleCapPolyData - Wrapper class for vtkvmtkSimpleCapPolyData.
// .SECTION Description
//  vmtkSimpleCapPolyData is a simple class that uses vtk_module to add tcl wrapping to vtkvmtkSimpleCapPolyData.  This class can be
// copied and modified to produce your own classes.

#ifndef __vmtkSimpleCapPolyData_h
#define __vmtkSimpleCapPolyData_h

#include "vmtkWrapperModule.h" // export macro
#include "vtkObject.h"
#include "vtkSmartPointer.h"

#include "vtkvmtkSimpleCapPolyData.h"

class VMTKWRAPPER_EXPORT vmtkSimpleCapPolyData : public vtkPolyDataAlgorithm
{
public:
  static vmtkSimpleCapPolyData * New();
  vtkTypeMacro(vmtkSimpleCapPolyData, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  void SetCellEntityIdsArrayName( const char *name )
  {
    this->m_Capper->SetCellEntityIdsArrayName( name );
  }

  const char* GetCellEntityIdsArrayName()
  {
    return this->m_Capper->GetCellEntityIdsArrayName();
  }

  virtual int ProcessRequest(vtkInformation *request, 
			     vtkInformationVector **inputVector,
			     vtkInformationVector *outputVector);

protected:
  vmtkSimpleCapPolyData();
  ~vmtkSimpleCapPolyData();

  vtkSmartPointer<vtkvmtkSimpleCapPolyData> m_Capper;

private:
  vmtkSimpleCapPolyData(const vmtkSimpleCapPolyData&);  // Not implemented.
  void operator=(const vmtkSimpleCapPolyData&);  // Not implemented.
};

#endif
