/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME vmtkCapPolyData - Wrapper class for vtkvmtkCapPolyData.
// .SECTION Description
//  vmtkCapPolyData is a simple class that uses vtk_module to add tcl wrapping to vtkvmtkCapPolyData.

#ifndef __vmtkCapPolyData_h
#define __vmtkCapPolyData_h

#include "vmtkWrapperModule.h" // export macro
#include "vtkObject.h"
#include "vtkSmartPointer.h"

#include "vtkvmtkCapPolyData.h"

class VMTKWRAPPER_EXPORT vmtkCapPolyData : public vtkPolyDataAlgorithm
{
public:
  static vmtkCapPolyData * New();
  vtkTypeMacro(vmtkCapPolyData, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  void SetCellEntityIdsArrayName( const char *name )
  {
    this->m_Capper->SetCellEntityIdsArrayName( name );
  }

  const char* GetCellEntityIdsArrayName()
  {
    return this->m_Capper->GetCellEntityIdsArrayName();
  }

  void SetDisplacement( double d )
  {
    this->m_Capper->SetDisplacement( d );
  }
  
  double GetDisplacement()
  {
    return this->m_Capper->GetDisplacement();
  }

  void SetInPlaneDisplacement( double d )
  {
    this->m_Capper->SetInPlaneDisplacement( d );
  }
  
  double GetInPlaneDisplacement()
  {
    return this->m_Capper->GetInPlaneDisplacement();
  }

  virtual int ProcessRequest(vtkInformation *request, 
			     vtkInformationVector **inputVector,
			     vtkInformationVector *outputVector);

protected:
  vmtkCapPolyData();
  ~vmtkCapPolyData();

  vtkSmartPointer<vtkvmtkCapPolyData> m_Capper;

private:
  vmtkCapPolyData(const vmtkCapPolyData&);  // Not implemented.
  void operator=(const vmtkCapPolyData&);  // Not implemented.
};

#endif
