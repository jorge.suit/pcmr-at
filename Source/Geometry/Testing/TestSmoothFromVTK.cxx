#include "pcmrTaubinSmoother.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkNew.h"
#include <assert.h>

class vtkMeshVisitor
{
public:
  void InitVisitor( vtkPolyData *pd )
  {
    this->m_Mesh = pd;
    this->m_IdNode = 0;
    pd->GetPolys()->InitTraversal();
  }

  vtkPolyData *m_Mesh;
  vtkIdType m_IdNode;

  static
  bool NextNode(float &x, float &y, float &z, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkPoints *points = visitor->m_Mesh->GetPoints();
    if ( visitor->m_IdNode >= points->GetNumberOfPoints( ) )
      {
      return false;
      }
    double pt[3];
    points->GetPoint(visitor->m_IdNode, pt);
    
    x = pt[0];
    y = pt[1];
    z = pt[2];
    ++visitor->m_IdNode;
    return true;
  }

  static
  bool NextTriangle(size_t &n1, size_t &n2, size_t &n3, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkIdType *idPts = 0;
    vtkIdType nPts = 0;
    vtkCellArray *cells = visitor->m_Mesh->GetPolys( );
    if ( cells->GetNextCell( nPts, idPts ) )
      {
      assert( nPts == 3 );
      n1 = idPts[ 0 ];
      n2 = idPts[ 1 ];
      n3 = idPts[ 2 ];
      return true;
      }
    else
      {
      return false;
      }
  }
};

int main(int argc, const char *argv[])
{
  if ( argc != 3 )
    {
    std::cerr << "Usage:\n";
    std::cerr << argv[0] << " input.vtk output.vtk\n";
    return -1;
    }
  vtkNew<vtkPolyDataReader> polyDataReader;
  polyDataReader->SetFileName( argv[1] );
  polyDataReader->Update();

  TaubinSmoother smoother;
  vtkMeshVisitor vtkVisitor;
  polyDataReader->GetOutput( )->Print( std::cout );
  vtkVisitor.InitVisitor( polyDataReader->GetOutput( ) );
  smoother.build_external(&vtkMeshVisitor::NextNode, &vtkMeshVisitor::NextTriangle, &vtkVisitor);
 
  smoother.save_to_binary_stereo_lithography_file( "/tmp/check_external_mesh.stl" );

  //smoother.fix_cracks();

  float n = 15;
  float lambda = 0.85;
  float mu = -0.7;
  TaubinSmoother::method met = TaubinSmoother::laplace;

  smoother.taubin_smooth(met, lambda, mu, n);

  smoother.save_to_binary_stereo_lithography_file( argv[2] );
  return 0;
}
