#include "mesh.h"
#include <stdlib.h>

#include <string>
using std::string;


int main(int argc, char **argv)
{
  if(argc != 6 && argc != 7)
    {
    cout << "Example usage: " << argv[0] << " input.stl output.stl n lambda mu ?method?" << endl;
    cout << "lambda in [0..1]\n";
    cout << "mu in [-1..0]\n";
    cout << "method is lapace, curvature or inverse_edge\n";
    return 1;
    }

  indexed_mesh mesh;
  
  if(false == mesh.load_from_binary_stereo_lithography_file(argv[1]))
    {
    cout << "Error: Could not properly read file " << argv[1] << endl;
    return 2;
    }

  mesh.fix_cracks();
  
  
  string out_file_name = argv[2];
  
  //out_file_name = "smoothed_" + out_file_name;
  int n = atoi(argv[3]);
  if (n < 0)
    {
    std::cout << "invalid number of iterations '" << n << "' will use 3\n";
    n = 3;
    }

  float lambda = atof(argv[4]);
  float mu = atof(argv[5]);
  std::cout << "using parameter lambda = " << lambda << std::endl;
  std::cout << "using parameter mu = " << mu << std::endl;
  indexed_mesh::method met;
  if (argc == 6)
    {
    met = indexed_mesh::laplace;
    }
  else if(!strcmp("laplace", argv[6]))
    {
    met = indexed_mesh::laplace;
    }
  else if(!strcmp("curvature", argv[6]))
    {
    met = indexed_mesh::curvature;
    }
  else if(!strcmp("inverse_edge", argv[6]))
    {
    met = indexed_mesh::inverse_edge;
    }
  else
    {
    std::cout << "invalid method requested '" << argv[6] << "', using laplace\n";
    met = indexed_mesh::laplace;
    }
  mesh.taubin_smooth(met, lambda, mu, n);
  //mesh.taubin_smooth(indexed_mesh::laplace, lambda, mu, 15);
  //mesh.taubin_smooth(indexed_mesh::curvature, lambda, mu, 10);
  //out_file_name = "/tmp/smoothed_taubin.stl";
  
  //mesh.curvature_normal_smooth(0.8);
  //out_file_name = "/tmp/smoothed_curvature.stl";
  
  if(false == mesh.save_to_binary_stereo_lithography_file(out_file_name.c_str()))
    {
    cout << "Error: Could not properly write file " << out_file_name << endl;
    return 2;
    }

  return 0;
}

