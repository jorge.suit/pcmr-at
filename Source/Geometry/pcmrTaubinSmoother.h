#ifndef __pcmrTaubinSmoother_h
#define __pcmrTaubinSmoother_h

#include "Geometry_Export.h"
#include "pcmrTaubinSmootherPrimitives.h"

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <iomanip>
using std::setiosflags;

#include <ios>
using std::ios_base;
using std::ios;

#include <set>
using std::set;

#include <vector>
using std::vector;

#include <limits>
using std::numeric_limits;

#include <cstring> // for memcpy()
#include <cctype>

typedef bool (*ptrfVisitNodeType)(float &x, float &y, float &z, void *data);
typedef bool (*ptrfVisitTriangleType)(size_t &n1, size_t &n2, size_t &n3, void *data);

class PCMRGEOMETRY_EXPORT TaubinSmoother
{
public:
  enum method {
    laplace,
    curvature,
    inverse_edge
  };
  
  void clear(void)
  {
    triangles.clear();
    vertices.clear();
    vertex_to_triangle_indices.clear();
    vertex_to_vertex_indices.clear();
    vertex_normals.clear();
    triangle_normals.clear();
    boundary_vertex.clear();
  }

  vector<indexed_triangle> triangles;
  vector<vertex_3> vertices;
  vector< vector<size_t> > vertex_to_triangle_indices;
  vector< vector<size_t> > vertex_to_vertex_indices;
  vector<vertex_3> vertex_normals;
  vector<vertex_3> triangle_normals;
  vector<int> boundary_vertex;

  bool build_external(ptrfVisitNodeType visitNode, ptrfVisitTriangleType visitTriangle,
		      void *data = NULL);
  bool load_from_binary_stereo_lithography_file(const char *const file_name, const bool generate_normals = true, const size_t buffer_width = 65536);
  bool save_to_binary_stereo_lithography_file(const char *const file_name, const size_t buffer_width = 65536);
  bool save_to_povray_mesh2_file(const char *const file_name, const bool write_vertex_normals = false);

  void set_max_extent(float max_extent);

  // See: Geometric Signal Processing on Polygonal Meshes by G. Taubin
  void laplace_smooth(const float scale);
  void taubin_smooth(const method m, const float lambda, const float mu, const size_t steps);
  void curvature_normal_smooth(const float scale);
  void inverse_edge_length_smooth(const float scale);

  void fix_cracks(void);

private:
  method m_method;
  void compute_boundary(void);
  void generate_vertex_normals(void);
  void generate_triangle_normals(void);
  void generate_vertex_and_triangle_normals(void);
  void regenerate_vertex_and_triangle_normals_if_exists(void);
  template<typename T> void eliminate_vector_duplicates(vector<T> &v);
  bool merge_vertex_pair(const size_t keeper, const size_t goner);
};


#endif
