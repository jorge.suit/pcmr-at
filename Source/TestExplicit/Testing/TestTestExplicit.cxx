#include "TestExplicit.h"
#include "pcmrFlow4DReader.h"
#include "itkVTKImageToImageFilter.h"

int main(int argc, char **argv)
{
  std::cout << typeid(TestExplicitImage).name() << std::endl;
  typedef itk::VTKImageToImageFilter<TestExplicitImage> ImporterType;

  assert(argc==2);
  assert(argv[1]!=NULL);
  pcmr::Flow4DReader::Pointer dataSet = pcmr::Flow4DReader::New();
  pcmr::StatusType status = dataSet->SetDirectory(argv[1]);
  if (status != pcmr::OK)
    {
    dataSet = NULL;
    }
  assert(pcmr::OK==status);
  ImporterType::Pointer importer = ImporterType::New();
  importer->SetInput(dataSet->GetMaskImage(3));
  DumpImage(importer->GetOutput());
  TestExplicitCustomFilter::Pointer filter = TestExplicitCustomFilter::New();
  filter->SetInput(importer->GetOutput());
  DumpImage(filter->GetOutput());
  return 0;
}
