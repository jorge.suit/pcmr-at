#ifndef __TestExplicit_h

#include "itkImage.h"
#include "itkImageToImageFilter.h"
#include "TestExplicit_Export.h"

extern template class TESTEXPLICIT_EXPORT itk::Image<unsigned char,3>;
typedef itk::Image<unsigned char,3> TestExplicitImage;

template class TESTEXPLICIT_EXPORT 
itk::ImageToImageFilter<TestExplicitImage,TestExplicitImage>;
typedef itk::ImageToImageFilter<TestExplicitImage,TestExplicitImage> ImageToImageFilterUC3UC3;

TESTEXPLICIT_EXPORT 
void DumpImage(TestExplicitImage *image);

class TESTEXPLICIT_EXPORT TestExplicitCustomFilter : public ImageToImageFilterUC3UC3
{
 public:
  /** Standard class typedefs. */
  typedef TestExplicitCustomFilter         Self;
  typedef ImageToImageFilterUC3UC3         Superclass;
  typedef itk::SmartPointer< Self >        Pointer;
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(TestExplicitCustomFilter,Superclass);

protected:
  /** Does the real work. */
  virtual void ThreadedGenerateData(const Superclass::OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);

  TestExplicitCustomFilter();
  virtual ~TestExplicitCustomFilter();
private:
  TestExplicitCustomFilter(const Self &); //purposely not implemented
  void operator=(const Self &);  //purposely not implemented
};

#endif
