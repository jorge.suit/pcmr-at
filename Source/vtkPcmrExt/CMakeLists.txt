use_module_dir(Common Geometry)

set( VTKPCMREXT_VERSION_MAJOR 1 )
set( VTKPCMREXT_VERSION_MINOR 0 )

include( ${VTK_CMAKE_DIR}/vtkExternalModuleMacros.cmake )

set( vtkPcmrExt_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} )
set( vtkPcmrExt_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} )

message( STATUS "VTK_RENDERING_BACKEND = ${VTK_RENDERING_BACKEND}" )

#message( STATUS "VTK_MODULES_ENABLED = ${VTK_MODULES_ENABLED}" )
list(FIND VTK_MODULES_ENABLED vtkIOMPIImage vtkIOMPIImage_ENABLED )
set( vtk_required vtkInteractionWidgets vtkInteractionStyle
  vtkRenderingVolume${VTK_RENDERING_BACKEND} vtkRenderingContext${VTK_RENDERING_BACKEND} vtkFiltersGeneral vtkImagingMorphological vtkIOImage)
if( NOT vtkIOMPIImage_ENABLED EQUAL -1 )
  list( APPEND vtk_required vtkIOMPIImage vtkFiltersParallelDIY2 )
endif()

message( STATUS "vtk_required = ${vtk_required}" )
vtk_module( vtkPcmrExt
  DEPENDS
  ${vtk_required}
)

# Source files.
set( vtkPcmrExt_SRCS
  vtkPcmrImagePlaneWidget.cxx
  vtkPcmrPointLocator.cxx
  vtkPcmrWrapper.cxx
  vtkPcmrMaskSmoother.cxx
)

message( STATUS "vtk-module = ${vtk-module}" )
# Build and link library.
vtk_module_library( vtkPcmrExt ${vtkPcmrExt_SRCS} )
target_link_libraries( vtkPcmrExt PRIVATE 
  pcmrGeometry ${vtkIOLegacy_LIBRARIES} )
target_link_libraries( vtkPcmrExtTCL PRIVATE 
  vtkInteractionWidgetsTCL )

#${ITK_LIBRARIES}
if( WRITE_INTERMEDIATE_RESULTS )
  set_target_properties( vtkPcmrExt PROPERTIES 
    COMPILE_FLAGS "-D__WRITE_INTERMEDIATE_RESULTS__")
  target_link_libraries( vtkPcmrExt PRIVATE ${ITK_LIBRARIES} )
else( )
  target_link_libraries( vtkPcmrExt PRIVATE
    ${ITKCommon_LIBRARIES} ${ITKVTK_LIBRARIES} )
endif( )

configure_file (
  "${CMAKE_CURRENT_SOURCE_DIR}/pkgIndex.tcl.in"
  "${CMAKE_CURRENT_BINARY_DIR}/pkgIndex.tcl.in"
  )

file( GENERATE
  OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/pkgIndex.tcl"
  INPUT  "${CMAKE_CURRENT_BINARY_DIR}/pkgIndex.tcl.in" )


if(WIN32)
  install( TARGETS vtkPcmrExt RUNTIME DESTINATION bin )
else(WIN32)
  install( TARGETS vtkPcmrExt RUNTIME LIBRARY DESTINATION lib )
endif(WIN32)

set( VTKPCMREXTTCL_INSTALL_DIR ${TCL_INSTALL_DIR}/vtkPcmrExt${VTKPCMREXT_VERSION_MAJOR}.${VTKPCMREXT_VERSION_MAJOR} )

#install( TARGETS vtkPcmrExtTCL RUNTIME LIBRARY DESTINATION ${VTKPCMREXTTCL_INSTALL_DIR} )
install( FILES "${CMAKE_CURRENT_BINARY_DIR}/pkgIndex.tcl" vtkPcmrWrapper.tcl
  COMPONENT RUNTIME DESTINATION ${VTKPCMREXTTCL_INSTALL_DIR} )
