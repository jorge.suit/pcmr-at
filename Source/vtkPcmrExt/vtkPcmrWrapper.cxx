/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
#include "vtkPcmrWrapper.h"

#include "vtkParametricSpline.h"
#include "vtkAxis.h"
#include "vtkPoints.h"
#include "vtkMath.h"

#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkPcmrWrapper);

//----------------------------------------------------------------------------
vtkPcmrWrapper::vtkPcmrWrapper() : vtkObject()
{
}

//----------------------------------------------------------------------------
vtkPcmrWrapper::~vtkPcmrWrapper()
{
}

// Set the title from a C-string
void vtkPcmrWrapper::AxisSetTitle( vtkAxis *axis, const char *title) 
{
  axis->SetTitle( title );
}


void vtkPcmrWrapper::ParametricSplineProjectOn( vtkParametricSpline *spline, 
                                                double x0, double y0, double z0,
                                                double nx, double ny, double nz )
{
  vtkPoints *points = spline->GetPoints( );
  if ( !points )
    {
    return;
    }
  double p[ 3 ];
  double po[ 3 ];
  double proj[ 3 ];
  double normal[ 3 ];

  normal[ 0 ] = nx;
  normal[ 1 ] = ny;
  normal[ 2 ] = nz;
  for( int i = 0; i < points->GetNumberOfPoints( ); i++ )
    {
    points->GetPoint( i, p );
    po[ 0 ] = p[ 0 ] - x0;
    po[ 1 ] = p[ 1 ] - y0;
    po[ 2 ] = p[ 2 ] - z0;

    double t = vtkMath::Dot( normal,po );

    proj[ 0 ] = p[ 0 ] - t * normal[ 0 ];
    proj[ 1 ] = p[ 1 ] - t * normal[ 1 ];
    proj[ 2 ] = p[ 2 ] - t * normal[ 2 ];
    points->SetPoint( i, proj );
    }
  spline->Modified( );
}

//----------------------------------------------------------------------------
void vtkPcmrWrapper::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
