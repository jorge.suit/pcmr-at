package require vtkcommoncore
package require snit

snit::type vtkWrapper {
  typecomponent wrapper
  delegate typemethod * to wrapper except New

  typeconstructor {
    set wrapper [vtkPcmrWrapper New] 
  }
}
