/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/
// .NAME vtkPcmrImagePlaneWidget - Example class using VTK.
// .SECTION Description
// vtkPcmrImagePlaneWidget is a simple class that uses VTK.  This class can be
// copied and modified to produce your own classes.

#ifndef __vtkPcmrImagePlaneWidget_h
#define __vtkPcmrImagePlaneWidget_h

#include "vtkPcmrExtModule.h" // export macro
#include "vtkImagePlaneWidget.h"

class VTKPCMREXT_EXPORT vtkPcmrImagePlaneWidget : public vtkImagePlaneWidget
{
public:
  static vtkPcmrImagePlaneWidget* New();
  vtkTypeMacro(vtkPcmrImagePlaneWidget, vtkImagePlaneWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the vtkImageData* input for the vtkImageReslice.
  // override to avoid redraw if the plane orientation has not changed
  void SetInputConnection(vtkAlgorithmOutput* aout);

protected:
  vtkPcmrImagePlaneWidget();
  ~vtkPcmrImagePlaneWidget();

private:
  vtkPcmrImagePlaneWidget(const vtkPcmrImagePlaneWidget&);  // Not implemented.
  void operator=(const vtkPcmrImagePlaneWidget&);  // Not implemented.
};

#endif
