/*=========================================================================
This source has no copyright.  It is intended to be copied by users
wishing to create their own VTK classes locally.
=========================================================================*/

#define __USE_VTK_MARCHING_CUBE__
#define __USE_BSPLINE_INTERPOLATOR__

#include <stdint.h>

#include "vtkPcmrMaskSmoother.h"

#include "vtkImageData.h"
#include "vtkPolyData.h"

#include "vtkObjectFactory.h"

#include "itkPhaseContrastImage.h"

#include "itkAntiAliasBinaryImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBSplineInterpolateImageFunction.h"
#include "itkConstantPadImageFilter.h"
#ifndef __USE_VTK_MARCHING_CUBE__
#include "itkCuberilleImageToMeshFilter.h"
#endif
#include "itkImageToVTKImageFilter.h"
#include "itkFlatStructuringElement.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkMesh.h"
#include "itkTriangleCell.h"
#include "itkVotingBinaryIterativeHoleFillingImageFilter.h"
#include "itkVTKImageToImageFilter.h"
#include "itkVTKPolyDataWriter.h"

#include "vtkImageMarchingCubes.h"
#include "vtkImageContinuousDilate3D.h"
#include "vtkImageContinuousErode3D.h"
#include "vtkNew.h"
#include "vtkQuadricDecimation.h"

#include "pcmrTaubinSmoother.h"

typedef itk::PhaseContrast3DImage ImageType;
typedef itk::MaskImage MaskType;

typedef itk::AntiAliasBinaryImageFilter<MaskType,ImageType> LevelSetFilterType;
typedef itk::BinaryBallStructuringElement<MaskType::PixelType, 3> BallKernelType;
typedef itk::FlatStructuringElement<3> FlatKernelType;
typedef itk::BinaryDilateImageFilter<MaskType,MaskType,FlatKernelType> BinaryDilateFilterType;
typedef itk::BinaryErodeImageFilter<MaskType,MaskType,FlatKernelType> BinaryErodeFilterType;
typedef itk::BinaryMorphologicalOpeningImageFilter<MaskType,MaskType,BallKernelType> BinaryOpeningFilterType;
typedef itk::ConstantPadImageFilter<MaskType,MaskType> PadFilterType;
typedef itk::Mesh< int8_t, 3 > MeshType;
#ifdef __USE_BSPLINE_INTERPOLATOR__
typedef itk::BSplineInterpolateImageFunction< ImageType, float, float > InterpolatorType;
#else
typedef itk::LinearInterpolateImageFunction< ImageType > InterpolatorType;
#endif
#ifndef __USE_VTK_MARCHING_CUBE__
typedef itk::CuberilleImageToMeshFilter< ImageType, MeshType, InterpolatorType > CuberilleType;
#endif
typedef itk::ImageToVTKImageFilter< ImageType > ImageToVTKImageFilterType;
typedef itk::VotingBinaryIterativeHoleFillingImageFilter<MaskType> HoleFillingFilterType;
typedef itk::VTKPolyDataWriter< MeshType > MeshFileWriterType;
typedef itk::VTKImageToImageFilter<MaskType> VTKImageToMaskFilterType;
typedef itk::ImageToVTKImageFilter<MaskType> MaskToVTKImageFilterType;

#ifdef  __WRITE_INTERMEDIATE_RESULTS__
#include "itkImageFileWriter.h"
#include "vtkPolyDataWriter.h"
typedef itk::ImageFileWriter<MaskType> MaskWriterType;
typedef itk::ImageFileWriter<ImageType> ImageWriterType;
#endif

// vtkMeshVisitor

class vtkMeshVisitor
{
public:
  void InitVisitor( vtkPolyData *pd )
  {
    this->m_Mesh = pd;
    this->m_IdNode = 0;
    pd->GetPolys()->InitTraversal();
  }

  vtkPolyData *m_Mesh;
  vtkIdType m_IdNode;

  static
  bool NextNode(float &x, float &y, float &z, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkPoints *points = visitor->m_Mesh->GetPoints();
    if ( visitor->m_IdNode >= points->GetNumberOfPoints( ) )
      {
      return false;
      }
    double pt[3];
    points->GetPoint(visitor->m_IdNode, pt);

    x = pt[0];
    y = pt[1];
    z = pt[2];
    ++visitor->m_IdNode;
    return true;
  }

  static
  bool NextTriangle(size_t &n1, size_t &n2, size_t &n3, void *data)
  {
    vtkMeshVisitor *visitor = static_cast<vtkMeshVisitor*>(data);
    vtkIdType *idPts = 0;
    vtkIdType nPts = 0;
    vtkCellArray *cells = visitor->m_Mesh->GetPolys( );
    if ( cells->GetNextCell( nPts, idPts ) )
      {
      assert( nPts == 3 );
      n1 = idPts[ 0 ];
      n2 = idPts[ 1 ];
      n3 = idPts[ 2 ];
      return true;
      }
    else
      {
      return false;
      }
  }
};

// itkMeshVisitor

class itkMeshVisitor
{
public:
  void InitVisitor( MeshType *mesh )
  {
    this->m_Mesh = mesh;
    this->m_IdNode = 0;
    this->m_CellIterator = mesh->GetCells()->Begin();
  }

  MeshType *m_Mesh;
  size_t m_IdNode;
  MeshType::CellsContainer::ConstIterator m_CellIterator;

  static
  bool NextNode(float &x, float &y, float &z, void *data)
  {
    itkMeshVisitor *visitor = static_cast<itkMeshVisitor*>(data);
    if ( visitor->m_IdNode >= visitor->m_Mesh->GetNumberOfPoints( ) )
      {
      return false;
      }
    MeshType::PointType pt;

    visitor->m_Mesh->GetPoint(visitor->m_IdNode, &pt);

    x = pt[0];
    y = pt[1];
    z = pt[2];
    ++visitor->m_IdNode;
    return true;
  }
  static
  bool NextTriangle(size_t &n1, size_t &n2, size_t &n3, void *data)
  {
    itkMeshVisitor *visitor = static_cast<itkMeshVisitor*>(data);
    if ( visitor->m_CellIterator == visitor->m_Mesh->GetCells()->End() )
      {
      return false;
      }
    typedef MeshType::CellType CellType;
    typedef itk::TriangleCell< CellType > TriangleType;
    CellType * cell = visitor->m_CellIterator.Value();
    if ( cell->GetType() == CellType::TRIANGLE_CELL )
      {
      TriangleType * triangle = dynamic_cast<TriangleType *>( cell );
      TriangleType::PointIdConstIterator it = triangle->PointIdsBegin();
      n1 = *it++;
      n2 = *it++;
      n3 = *it++;
      }
    ++visitor->m_CellIterator;
    return true;
  }

};

vtkStandardNewMacro(vtkPcmrMaskSmoother);

//----------------------------------------------------------------------------
vtkPcmrMaskSmoother::vtkPcmrMaskSmoother() : vtkObject()
{
  this->m_Mask = NULL;
  this->m_BoundarySmoothedFinal = vtkSmartPointer<vtkPolyData>::New();
  for( int i = 0; i < 3; i++ )
    {
    this->m_Padding[ i ] = 0;
    }
}

//----------------------------------------------------------------------------
vtkPcmrMaskSmoother::~vtkPcmrMaskSmoother()
{
}

void vtkPcmrMaskSmoother::SetInputMask( vtkImageData *mask )
{
  if ( mask == this->m_Mask )
    {
    return;
    }
  if ( !mask )
    {
    mask->Delete();
    }
  this->m_Mask = mask;
  this->m_Mask->Register( this );
  this->Modified();
}

vtkImageData *vtkPcmrMaskSmoother::GetInputMask()
{
  return this->m_Mask;
}

void vtkPcmrMaskSmoother::SetPadding( int pad_i, int pad_j, int pad_k )
{
  if ( pad_i < 0 )
    {
    vtkErrorMacro( << "pad_i value must be greater than 0" << pad_i );
    }

  if ( pad_j < 0 )
    {
    vtkErrorMacro( << "pad_j value must be greater than 0" << pad_j );
    }

  if ( pad_k < 0 )
    {
    vtkErrorMacro( << "pad_k value must be greater than 0" << pad_k );
    }
  this->m_Padding[0] = pad_i;
  this->m_Padding[1] = pad_j;
  this->m_Padding[2] = pad_k;
  this->Modified();
}

int * vtkPcmrMaskSmoother::GetPadding()
{
  return this->m_Padding;
}

vtkPolyData *vtkPcmrMaskSmoother::GetBoundarySmoothed()
{
  if ( !this->m_Mask )
    {
    vtkErrorMacro( << "Mask is NULL" );
    }
  if( this->m_BoundarySmoothedFinal->GetMTime() < this->GetMTime() )
    {
    this->ComputeBoundarySmoothed();
    }
  return this->m_BoundarySmoothedFinal;
}

void vtkPcmrMaskSmoother::ComputeBoundarySmoothed()
{
#ifdef __WRITE_INTERMEDIATE_RESULTS__
  MaskWriterType::Pointer writerMask = MaskWriterType::New();
  ImageWriterType::Pointer writerImage = ImageWriterType::New();
  vtkNew<vtkPolyDataWriter> polydataWriter;
  MeshFileWriterType::Pointer itkMeshWriter = MeshFileWriterType::New();
#endif

  this->m_BoundarySmoothedFinal->Initialize();

  VTKImageToMaskFilterType::Pointer importVTKMask = VTKImageToMaskFilterType::New();
  importVTKMask->SetInput( this->m_Mask );

  // ----------------- Padding Image -----------------
  itk::MaskImage::SizeType lowerExtendRegion, upperExtendRegion;

  for( int i = 0; i < 3; i++ )
    {
    lowerExtendRegion[i] = upperExtendRegion[i] = this->m_Padding[i];
    }
  PadFilterType::Pointer padFilter = PadFilterType::New();

  padFilter->SetPadLowerBound(lowerExtendRegion);
  padFilter->SetPadUpperBound(upperExtendRegion);
  padFilter->SetInput( importVTKMask->GetOutput( ) );

  // Erode => Dilate from VTK

  MaskToVTKImageFilterType::Pointer connectVTK_Erode = MaskToVTKImageFilterType::New( );
  connectVTK_Erode->SetInput( padFilter->GetOutput( ) );
  connectVTK_Erode->Update( );
  vtkNew<vtkImageContinuousErode3D> vtkErodeImage;
  vtkErodeImage->SetInputData( connectVTK_Erode->GetOutput( ) );
  vtkErodeImage->SetKernelSize( 2, 2, 2 );
  vtkNew<vtkImageContinuousDilate3D> vtkDilateImage;
  vtkDilateImage->SetKernelSize( 2, 2, 2 );
  vtkDilateImage->SetInputConnection( vtkErodeImage->GetOutputPort( ) );
  VTKImageToMaskFilterType::Pointer connectVTK_Dilate = VTKImageToMaskFilterType::New( );
  vtkDilateImage->Update( );
  connectVTK_Dilate->SetInput( vtkDilateImage->GetOutput( ) );


  // ----------------- Hole Filling -----------------

  unsigned int radiusFill = 2;
  itk::MaskImage::SizeType radiusFillArray;
  radiusFillArray[0] = radiusFillArray[1] = radiusFillArray[2] = radiusFill;
  unsigned int maxiterFill = 10;
  unsigned int majorityThreshold = 1;
  HoleFillingFilterType::Pointer fillholeFilter = HoleFillingFilterType::New();
  fillholeFilter->SetRadius( radiusFillArray );
  fillholeFilter->SetBackgroundValue( 0 );
  fillholeFilter->SetForegroundValue( 1 );
  fillholeFilter->SetMajorityThreshold( majorityThreshold );
  fillholeFilter->SetMaximumNumberOfIterations( maxiterFill );
  fillholeFilter->SetInput( connectVTK_Dilate->GetOutput( ) );
  std::cout << "Applying hole-filling to mask ...\n";
  fillholeFilter->Update();
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  writerMask->SetInput( fillholeFilter->GetOutput( ) );
  writerMask->SetFileName( "/tmp/fill-hole.vtk" );
  writerMask->Update();
#endif

#if 0
  // ----------------- Binary Erode -----------------

  BinaryErodeFilterType::Pointer erodeFilter = BinaryErodeFilterType::New( );
  erodeFilter->SetForegroundValue( 1 );
  erodeFilter->SetBackgroundValue( 0 );
  erodeFilter->SetRadius( 1 );
  erodeFilter->SetInput( fillholeFilter->GetOutput( ) );
  std::cout << "Testing binary opening on mask ...\n";
  erodeFilter->Update();
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  writerMask->SetInput( erodeFilter->GetOutput( ) );
  writerMask->SetFileName( "/tmp/binary_erode.vtk" );
  writerMask->Update();
#endif

  // ----------------- Binary Dilate -----------------

  BinaryDilateFilterType::Pointer dilateFilter = BinaryDilateFilterType::New( );
  dilateFilter->SetForegroundValue( 1 );
  dilateFilter->SetBackgroundValue( 0 );
  dilateFilter->SetRadius( 1 );
  dilateFilter->SetInput( erodeFilter->GetOutput( ) );
  std::cout << "Testing binary dilate on mask ...\n";
  dilateFilter->Update();
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  writerMask->SetInput( dilateFilter->GetOutput( ) );
  writerMask->SetFileName( "/tmp/binary_dilate.vtk" );
  writerMask->Update();
#endif

#endif

#if 0
  // ----------------- Morphological Opening -----------------

  BinaryOpeningFilterType::Pointer openingFilter = BinaryOpeningFilterType::New( );
  openingFilter->SetForegroundValue( 1 );
  openingFilter->SetBackgroundValue( 0 );
  openingFilter->SetRadius( 2 );
  openingFilter->SetInput( padFilter->GetOutput( ) );
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  std::cout << "Testing binary opening on mask ...\n";
  openingFilter->Update();
  writerMask->SetInput( openingFilter->GetOutput( ) );
  writerMask->SetFileName( "/tmp/binary_opening.vtk" );
  writerMask->Update();
#endif

#endif
  // ----------------- Antialiasing -----------------

  unsigned int numberOfLayers = 6;
  LevelSetFilterType::Pointer levelsetFilter = LevelSetFilterType::New();
  levelsetFilter->SetInput( connectVTK_Dilate->GetOutput() );
  levelsetFilter->SetNumberOfLayers( numberOfLayers );
  levelsetFilter->UseImageSpacingOn();
  levelsetFilter->SetMaximumRMSError(0.007);

  std::cout << "Applying antialias to binary mask ...\n";
  levelsetFilter->Update();
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  writerImage->SetInput(levelsetFilter->GetOutput());
  writerImage->SetFileName("/tmp/levelset.vtk");
  writerImage->Update();
#endif

  // ----------------- Marching Cube -----------------

#ifdef __USE_VTK_MARCHING_CUBE__

  ImageToVTKImageFilterType::Pointer connectVTK = ImageToVTKImageFilterType::New( );
  connectVTK->SetInput( levelsetFilter->GetOutput( ) );
  connectVTK->Update( );
  vtkNew<vtkImageMarchingCubes> marchingCubes;
  marchingCubes->SetInputData( connectVTK->GetOutput( ) );
  marchingCubes->SetNumberOfContours( 1 );
  marchingCubes->SetValue(0, -1 );
  marchingCubes->ComputeScalarsOff( );
  marchingCubes->ComputeNormalsOff( );
  marchingCubes->ComputeGradientsOff( );

  std::cout << "Applying marching cube to level-set ...\n";
  marchingCubes->Update();
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  marchingCubes->Update();
  polydataWriter->SetInputConnection( marchingCubes->GetOutputPort( ) );
  polydataWriter->SetFileName( "/tmp/marching-cubes.vtk" );
  polydataWriter->Update( );
#endif

#else
  double SurfaceDistanceThreshold = 0.5;
  double StepLength = 0.25;
  double StepLengthRelax = 0.95;
  unsigned int MaximumNumberOfSteps = 50;
  CuberilleType::Pointer cuberille = CuberilleType::New();
  levelsetFilter->Update();
  cuberille->SetInput( levelsetFilter->GetOutput( ) );
  cuberille->SetIsoSurfaceValue( -0.5 );
  InterpolatorType::Pointer interpolator = InterpolatorType::New();
#if __USE_BSPLINE_INTERPOLATOR__
  interpolator->SetSplineOrder( 3 );
#endif
  cuberille->SetInterpolator( interpolator );
  cuberille->SetGenerateTriangleFaces( true );
  cuberille->SetProjectVerticesToIsoSurface( true );
  cuberille->SetProjectVertexSurfaceDistanceThreshold( SurfaceDistanceThreshold );
  cuberille->SetProjectVertexStepLength( StepLength );
  cuberille->SetProjectVertexStepLengthRelaxationFactor( StepLengthRelax );
  cuberille->SetProjectVertexMaximumNumberOfSteps( MaximumNumberOfSteps );

  std::cout << "Applying cuberille to level-set ...\n";
  cuberille->Update( );
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  itkMeshWriter->SetInput( cuberille->GetOutput( ) );
  itkMeshWriter->SetFileName( "/tmp/cuberille.vtk" );
  itkMeshWriter->Update( );
#endif

#endif

#if 0
// ============================================================================
  double SurfaceDistanceThreshold = 0.5;
  double StepLength = 0.25;
  double StepLengthRelax = 0.95;
  unsigned int MaximumNumberOfSteps = 50;
  CuberilleType::Pointer cuberille = CuberilleType::New();
  cuberille->SetInput( levelsetFilter->GetOutput( ) );
  cuberille->SetIsoSurfaceValue( -0.5 );
  InterpolatorType::Pointer interpolator = InterpolatorType::New();
#if USE_BSPLINE_INTERPOLATOR
  interpolator->SetSplineOrder( 3 );
#endif
  cuberille->SetInterpolator( interpolator );
  cuberille->SetGenerateTriangleFaces( true );
  cuberille->SetProjectVerticesToIsoSurface( true );
  cuberille->SetProjectVertexSurfaceDistanceThreshold( SurfaceDistanceThreshold );
  cuberille->SetProjectVertexStepLength( StepLength );
  cuberille->SetProjectVertexStepLengthRelaxationFactor( StepLengthRelax );
  cuberille->SetProjectVertexMaximumNumberOfSteps( MaximumNumberOfSteps );

#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  std::cout << "Applying cuberille to level-set ...\n";
  cuberille->Update( );
  itkMeshWriter->SetInput( cuberille->GetOutput( ) );
  itkMeshWriter->SetFileName( "/tmp/cuberille.vtk" );
  itkMeshWriter->Update( );
#endif
// ============================================================================
#endif

  // ----------------- Taubin phase I -----------------

  TaubinSmoother taubinSmoother;

#ifdef __USE_VTK_MARCHING_CUBE__
  typedef vtkMeshVisitor MeshVisitorType;
  #define isoContour marchingCubes
#else
  typedef itkMeshVisitor MeshVisitorType;
  #define isoContour cuberille
#endif

  MeshVisitorType meshVisitor;

  meshVisitor.InitVisitor( isoContour->GetOutput( ) );
  taubinSmoother.build_external( &MeshVisitorType::NextNode,
                                 &MeshVisitorType::NextTriangle,
                                 &meshVisitor);
  float taubin1Steps = 20;
  float taubin1Lambda = 0.8;
  float taubin1Mu = -0.3;
  TaubinSmoother::method taubin1Method = TaubinSmoother::laplace;

  taubinSmoother.taubin_smooth( taubin1Method, taubin1Lambda,
                                taubin1Mu, taubin1Steps);

  vtkNew< vtkPolyData > smoothMesh1;
  smoothMesh1->ShallowCopy( marchingCubes->GetOutput( ) );
  for( size_t i = 0; i < smoothMesh1->GetNumberOfPoints(); i++)
    {
    smoothMesh1->GetPoints()->SetPoint(i,
                                       taubinSmoother.vertices[i].x,
                                       taubinSmoother.vertices[i].y,
                                       taubinSmoother.vertices[i].z);
    }

#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  polydataWriter->SetInputData( smoothMesh1.GetPointer() );
  polydataWriter->SetFileName( "/tmp/taubin_phase_I.vtk" );
  polydataWriter->Update( );
#endif

  // ----------------- Decimation -----------------

  vtkNew< vtkQuadricDecimation > decimationFilter;
  decimationFilter->SetInputData( smoothMesh1.GetPointer( ) );
  decimationFilter->SetTargetReduction( 0.9 );

  std::cout << "Applying quadric-decimation to smoothed mesh ...\n";
  decimationFilter->Update( );
#if defined(__WRITE_INTERMEDIATE_RESULTS__)
  polydataWriter->SetInputConnection( decimationFilter->GetOutputPort( ) );
  polydataWriter->SetFileName( "/tmp/quadric_decimation.vtk" );
  polydataWriter->Update( );
#endif

  // ----------------- Taubin phase II -----------------

  meshVisitor.InitVisitor( decimationFilter->GetOutput( ) );
  taubinSmoother.clear();
  taubinSmoother.build_external( &MeshVisitorType::NextNode,
                                 &MeshVisitorType::NextTriangle, &meshVisitor );
  float taubin2Steps = 10;
  float taubin2Lambda = 0.6;
  float taubin2Mu = -0.1;
  TaubinSmoother::method taubin2Method = TaubinSmoother::laplace;

  taubinSmoother.taubin_smooth(taubin2Method, taubin2Lambda, taubin2Mu, taubin2Steps);

  this->m_BoundarySmoothedFinal->ShallowCopy( decimationFilter->GetOutput( ) );
  for( size_t i = 0; i < this->m_BoundarySmoothedFinal->GetNumberOfPoints(); i++)
    {
    this->m_BoundarySmoothedFinal->GetPoints()->SetPoint(i,
                                                         taubinSmoother.vertices[i].x,
                                                         taubinSmoother.vertices[i].y,
                                                         taubinSmoother.vertices[i].z);
    }
}

//----------------------------------------------------------------------------
void vtkPcmrMaskSmoother::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
