#ifndef __pcmrPolynomialFit_h
#define __pcmrPolynomialFit_h

#include "pcmrDefs.h"
#include "Numeric_Export.h"
#include "vnl/vnl_vector.h"
#include "vnl/vnl_vector_ref.h"
#include "vnl/vnl_real_polynomial.h"

BEGIN_PCMR_DECLS

typedef vnl_vector<double> VnlVector;
typedef vnl_vector_ref<double> VnlVectorRef;
typedef vnl_real_polynomial VnlPolynomial;

void PCMRNUMERIC_EXPORT PolynomialFit(const VnlVector &x,
				      const VnlVector &y, 
				      unsigned deg,
                                      VnlPolynomial &fit);

double PCMRNUMERIC_EXPORT PolynomialFit(const VnlVector &x,
					const VnlVector &y, 
					unsigned deg);

END_PCMR_DECLS

#endif
