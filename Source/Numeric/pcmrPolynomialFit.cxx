#include "pcmrPolynomialFit.h"
#include "vnl/vnl_fastops.h"
#include "vnl/vnl_matrix.h"
#include "vnl/algo/vnl_cholesky.h"
#include <iostream>

BEGIN_PCMR_DECLS

typedef vnl_matrix<double> VnlMatrix;

void PolynomialFit( const VnlVector &x,
                    const VnlVector &y, 
                    unsigned deg,
                    VnlVector &fit )
{
  VnlVector cursor( x.size( ) );
  cursor.fill( 1.0 );
  VnlMatrix A( x.size( ), deg + 1 );
  for( int i = 0; i <= deg; i++ )
    {
    A.set_column( deg - i, cursor );
    if ( i < deg )
      {
      for( int j = 0; j < x.size( ); j++ )
        {
        cursor[ j ] *= x[ j ];
        }
      }
    }
  //std::cout << "A = " << A;
  VnlMatrix AtA( deg + 1, deg + 1 );
  vnl_fastops::AtA( AtA, A);
  //std::cout << "AtA = " << AtA;

  vnl_cholesky chol( AtA );

  vnl_fastops::AtB( fit, A, y );
  
  chol.solve(fit, &fit);
  //std::cout << "fit = " << fit << std::endl;
}

void PolynomialFit(const VnlVector &x,
                   const VnlVector &y, 
                   unsigned deg,
                   VnlPolynomial &fit)
{
  fit.coefficients().set_size(deg+1);
  PolynomialFit(x, y, deg, fit.coefficients());
}

double PolynomialFit(const VnlVector &x,
                     const VnlVector &y, 
                     unsigned deg)
{
  VnlVector fit(deg+1);

  PolynomialFit(x, y, deg, fit);
  return fit[0];
}

END_PCMR_DECLS
