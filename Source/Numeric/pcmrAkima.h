#ifndef __pcmrAkima_h__
#define __pcmrAkima_h__

#include "pcmrFitErrorCodes.h"
#include "Numeric_Export.h"
#include <stdlib.h>

BEGIN_PCMR_DECLS

class PCMRNUMERIC_EXPORT Akima
{
 public:

  Akima();
  ~Akima();

  FitErrorCode SetKnots(const double x[], const double fx[], size_t size);
  FitErrorCode Evaluate(double x, double &fx);

 private:
  struct Coefficient
  {
    double b, c, d;
  };

  struct Knot
  {
    double x, fx;
  };

  void Clear();
  FitErrorCode ComputeCoefficients();
  size_t FindKnot(double x);

  size_t m_Size;
  Knot *m_Knots;
  Coefficient *m_Coefficients;
};

END_PCMR_DECLS

#endif
