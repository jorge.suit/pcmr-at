#include "PolyFit.hpp"

int main(int argc, char *argv[])
{
  std::vector<float> oX;
  std::vector<float> oY;

  double data_x[5] = { 3.53862,   3.97765,  7.51627, 10.8693, 11.4939};
  double data_y[5] = { -41.9883,  -46.6048, -42.7943, -42.2081, -44.1133};

  for (int i = 0; i < sizeof(data_x)/sizeof(data_x[0]); i++)
    {
    oX.push_back(data_x[i]);
    oY.push_back(data_y[i]);
    }

  std::vector<float> coef = polyfit(oX, oY, 3);
  std::cout << "coef = ";
  for(int i = 0; coef.size(); i++)
    {
    std::cout << coef[i] << " ";
    }
  std::cout << std::endl;
  return 0;
}
