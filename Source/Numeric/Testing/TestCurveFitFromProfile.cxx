#include "pcmrProfileData.h"
#include "pcmrCurveFit.h"
#include <iostream>

void PrintVector( double a[], size_t n )
{
  std::cout << "[";
  for( int i = 0; i < n; i++ )
    {
    std::cout << (i ? ", " : " ") << a[i];
    }
  std::cout << "]";
}

void PrintVector( const char *msg, double a[], size_t n )
{
  std::cout << msg << " ";
  PrintVector( a, n );
  std::cout << std::endl;
}

void Evaluate( pcmr::CurveFit &curve,
               const double x[], const double y[], size_t n  )
{
  pcmr::FitErrorCode status = curve.SetData( x, y, n );
  if (status != pcmr::ERROR_OK)
    {
    std::cout << "error in curve.SetData " << status << std::endl;
    }
  else
    {
    curve.Print( std::cout );
    double x_new[] = {2, 1, 0.5, 0.25, 0.1, 0.0, -0.1, -0.25, -0.5, -1, -2};
    for( int i = 0; i < sizeof(x_new)/sizeof(x_new[0]); ++i )
      {
      std::cout << "f(" << x_new[i] << ") = " << curve.Evaluate( x_new[i] ) << std::endl;
      }
    }
}

int main( int argc, char *argv[] )
{
  double *x = NULL;
  double *y = NULL;
  std::string header;

  if( argc != 2 )
    {
    std::cout << "usage: " << argv[0] << " profile.txt\n";
    return -1;
    }

  int n = ReadProfileData( argv[1], x, y, header );
  if ( !n )
    {
    std::cout << "could not read data from \"" << argv[1] << "\"\n";
    return -1;
    }
  std::cout << header << std::endl;
  std::cout << n << " points where read\n";
  PrintVector( "x:", x, n );
  PrintVector( "y:", y, n );

  std::cout << "Evaluating with Akima\n";
  pcmr::CurveAkimaFit akima;
  Evaluate( akima, x, y, n );

  std::cout << "Evaluating with Polynomial\n";
  pcmr::CurvePolynomialFit polyFit;
  Evaluate( polyFit, x, y, n );

  std::cout << "Evaluating with NaturalSpline\n";
  pcmr::CurveNaturalSplineFit splineFit;
  Evaluate( splineFit, x, y, n );
  std::cout << splineFit.Evaluate( -0.321705 ) << std::endl;
  ReleaseProfileData( x, y );
  return 0;
}
