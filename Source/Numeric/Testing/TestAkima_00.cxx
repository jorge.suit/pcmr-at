#include "pcmrAkima.h"
#include <iostream>

int main(int argc, const char *argv[])
{
  double data_x[5] = { 0.0, 1.0, 2.0, 3.0, 4.0 };
  double data_y[5] = { 0.0, 1.0, 2.0, 3.0, 4.0 };
  double test_x[4] = { 0.0, 0.5, 1.0, 2.0 };
  double test_y[4] = { 0.0, 0.5, 1.0, 2.0 };

  pcmr::Akima akima;

  akima.SetKnots(data_x, data_y, 5);
  for(size_t i = 0; i < sizeof(test_x)/sizeof(test_x[0]); ++i)
    {
    double fx;
    pcmr::FitErrorCode status = akima.Evaluate(test_x[i], fx);
    if (status == pcmr::ERROR_OK)
      {
      std::cout << "f(" << test_x[i] << ") = " << fx << std::endl;
      }
    }
  return 0;
}
