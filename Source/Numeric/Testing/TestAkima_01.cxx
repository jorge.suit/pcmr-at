#include "pcmrAkima.h"
#include <iostream>

int main(int argc, const char *argv[])
{
  /*
  double data_x[5] = { 0.496308, 0.992616, 1.4889, 2.04588, 2.54219 };
  double data_y[5] = { 21.87,    42.1875,  59.375, 75,     85.9375 };
  */

  //double data_x[5] = { 0.496329,   0.992657, 1.48899, 2.0458,  2.54212 };
  //double data_y[5] = { 0.0851988,  21.875,   42.1875, 60.9375, 75 };
  
  double data_x[5] = { 3.53862,   3.97765,  7.51627, 10.8693, 11.4939};
  double data_y[5] = { -41.9883,  -46.6048, -42.7943, -42.2081, -44.1133};
  
  pcmr::Akima akima;

  akima.SetKnots(data_x, data_y, 5);
  double fx;
  double test[] = {-0.5, 0.0, 1, 2, 3, 3.2, 3.4, 3.5};
  for(int i = 0; i < sizeof(test)/sizeof(test[0]); i++)
    {
    pcmr::FitErrorCode status = akima.Evaluate(test[i], fx);
    std::cout << "status = " << status << std::endl;
    if (status == pcmr::ERROR_OK)
      {
      std::cout << "f(" << test[i] << ") = " << fx << std::endl;
      }
    }

  return 0;
}
