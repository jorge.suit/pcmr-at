#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_chebyshev.h>
#include <gsl/gsl_spline.h>
#include "pcmrProfileData.h"
#include <iostream>

struct FunctionDataType
{
  gsl_interp_accel *acc;
  gsl_spline *spline;
};

double
f (double x, void *p)
{
  FunctionDataType *functionData = static_cast<FunctionDataType*>( p );
  return gsl_spline_eval( functionData->spline, x, functionData->acc );
}

int main ( int argc, char *argv[] )
{
  double *x = NULL;
  double *y = NULL;
  std::string header;

  if( argc != 2 )
    {
    std::cout << "usage: " << argv[0] << " profile.txt\n";
    return -1;
    }

  int n = ReadProfileData( argv[1], x, y, header );
  std::cout << header << std::endl;
  std::cout << n << " points where read\n";

  FunctionDataType functionData;

  functionData.acc = gsl_interp_accel_alloc( );
  functionData.spline = gsl_spline_alloc( gsl_interp_linear, n );
  gsl_spline_init( functionData.spline, x, y, n );

  gsl_cheb_series *cs = gsl_cheb_alloc( 10 );

  gsl_function F;

  F.function = f;
  F.params = &functionData;

  gsl_cheb_init( cs, &F, x[0], x[n-1] );

  for ( int i = 0; i < n; i++ )
    {
      double r10 = gsl_cheb_eval_n (cs, 3, x[ i ] );
      double r40 = gsl_cheb_eval( cs, x[ i ] );
      printf( "%g %g %g %g\n", 
              x[i], GSL_FN_EVAL( &F, x[ i ] ), r10, r40 );
    }

  std::cout << "=========================\n";
  std::cout << "Extrpolating below 0 ...\n";
  std::cout << "=========================\n";
  double _x[] = { 0, -0.1, -0.2, -0.3, -0.4, -0.5, -1, -1.5, -2, -3, -4, -5};
  for ( int i = 0; i < sizeof(_x)/sizeof(_x[0]); i++ )
    {
      double r10 = gsl_cheb_eval_n (cs, 3, _x[ i ] );
      double r40 = gsl_cheb_eval( cs, _x[ i ] );
      printf( "%g %g %g\n", 
              _x[i], r10, r40 );
    }

  ReleaseProfileData( x, y );
  gsl_spline_free( functionData.spline );
  gsl_interp_accel_free( functionData.acc );

  gsl_cheb_free (cs);

  return 0;
}
