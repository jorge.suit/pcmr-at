#ifndef __pcmrFlow4DReader_h
#define __pcmrFlow4DReader_h

#include "vtkSmartPointer.h"
#include "vtkImageData.h"
#include "vtkCommand.h"
#include "pcmrTypes.h"
#include "itkPhaseContrastImage.h"
#include "pcmrHeaderReader.h"
#include "itkObject.h"
#include "itkObjectFactory.h"
#include "itkImageToVTKImageFilter.h"
#include <boost/unordered_map.hpp> 

BEGIN_PCMR_DECLS

typedef vtkSmartPointer<vtkImageData> vtkImagePointer;

class PCMRDATASETREADER_EXPORT Flow4DReader : public itk::Object
{
 public:

  static const double LargeArteryBloodViscosity;

  /** Standard class typedefs. */
  typedef Flow4DReader                   Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(Flow4DReader, itk::Object);

  typedef enum 
  {
    // X component of the velocity field
    FlowX,
    // Y component of the velocity field
    FlowY,
    // Z component of the velocity field
    FlowZ,
    // Modulus of the velocity field
    FlowModulus,
    // Composed velocity field
    FlowVector
  } Component;

  typedef enum 
  {
    FileFlowX, FileFlowY, FileFlowZ, FileMagnitude
  } FileComponent;

  Flow4DReader();
  virtual ~Flow4DReader();

  StatusType Clear();

  StatusType SetDirectory(const char* directory);
  const char* GetDirectory() const
  {
    return this->m_Directory.c_str();
  }

  std::string GetFormatVersion() const
  {
    return this->m_HeaderReader->GetFormatVersion();
  }

  std::string GetSourceSequence() const
  {
    return this->m_HeaderReader->GetSourceSequence();
  }

  size_t GetNumberOfTimeSteps() const
  {
    return this->m_TimeValues.size();
  }

  double GetLengthOfTimeStep() const
  {
    return this->m_LengthOfTimeStep;
  }

  const std::vector<double> &GetTimeValues() const
  {
    return this->m_TimeValues;
  }

  int GetTimeValues(std::vector<double> &timeValues) const;

  double GetVelocityEncoding() const
  {
    return this->m_HeaderReader->GetVelocityEncoding();
  }

  double GetMinimumVelocity() const
  {
    return this->m_HeaderReader->GetMinimumVelocity();
  }

  double GetMaximumVelocity() const
  {
    return this->m_HeaderReader->GetMaximumVelocity();
  }

  std::string GetImageOrientation() const
  {
    return this->m_HeaderReader->GetImageOrientation();
  }

  std::string GetGeometryUnits() const
  {
    return this->m_HeaderReader->GetGeometryUnits();
  }

  std::string GetVelocityUnits() const
  {
    return this->m_HeaderReader->GetVelocityUnits();
  }

  double GetBloodViscocity(double defaultValue = LargeArteryBloodViscosity)
  {
    return this->m_HeaderReader->GetBloodViscocity(defaultValue);
  }

  bool GetInvertComponentX()
  {
    return this->m_HeaderReader->GetInvertComponentX();
  }

  bool GetInvertComponentY()
  {
    return this->m_HeaderReader->GetInvertComponentY();
  }

  bool GetInvertComponentZ()
  {
    return this->m_HeaderReader->GetInvertComponentZ();
  }

  StatusType GetExtendedProperties(std::vector<std::string> &properties) const
  {
    return this->m_HeaderReader->GetExtendedProperties(properties);
  }

  StatusType GetExtendedProperty(const std::string& key, HeaderReader::Property &property) const
  {
    return this->m_HeaderReader->GetExtendedProperty(key, property);
  }

  vtkImageData* GetFlowImage(Component component, size_t timestep);

  vtkImageData* GetMagnitudeImage(size_t timestep);

  vtkImageData* GetMaskImage(size_t timestep);

  vtkImageData* GetPremaskImage(size_t timestep) const
  {
    return this->m_PremaskImage;
  }

protected:
  StatusType ComputeTimeValues();
  void ReleaseImages();
  void ResizeContainers();
  StatusType LoadMaskForTimeStep(size_t timestep);
  StatusType LoadTimeStep(size_t timestep);
  StatusType GetMaskFileName(size_t timestep, std::string &fileName);
  StatusType GetFlowFileName(size_t timestep, FileComponent component,
                             std::string &fileName);
  itk::PhaseContrast3DImage::Pointer LoadItkImage(std::string pathImage,
                                                  bool invert);
  template <class T>
  vtkImagePointer ConvertItkImageToVtkImage(typename T::Pointer ptrItkImage)
    {
      typedef typename itk::ImageToVTKImageFilter<T> ITK2VTKType;
      typename ITK2VTKType::Pointer itk2vtk = ITK2VTKType::New();
      itk2vtk->SetInput(ptrItkImage);
      itk2vtk->Update();
      vtkImageData *vtkImg = itk2vtk->GetOutput();
      vtkImagePointer ptrImage = vtkImagePointer::New();
      ptrImage->DeepCopy(vtkImg);
      //std::cout << "Ref(vtkImg) =" << vtkImg->GetReferenceCount() << "\n";
      this->TrackImageDataPointer(ptrImage.GetPointer());
      //vtkImagePointer pointer(vtkImg);
      //std::cout << "Ref(vtkImg) =" << vtkImg->GetReferenceCount() << "\n";
      //pointer->Register(NULL);
      //std::cout << "Ref(vtkImg) =" << vtkImg->GetReferenceCount() << "\n";
      return ptrImage;
    }

  vtkImagePointer LoadVtkImage(std::string pathImage);
  StatusType LoadPremaskImage();
  void BuildVectorImage(itk::PhaseContrast3DImage *flowX,
                        itk::PhaseContrast3DImage *flowY,
                        itk::PhaseContrast3DImage *flowZ,
                        size_t timestep);

private:
  void OnImageDelete(vtkObject *obj, unsigned long event, void*data);
  void CheckTrackedPointers();
  void TrackImageDataPointer(vtkImageData *ptr);

  HeaderReader::Pointer m_HeaderReader;
  std::string m_Directory;
  std::vector<vtkImagePointer> m_FlowXImage;
  std::vector<vtkImagePointer> m_FlowYImage;
  std::vector<vtkImagePointer> m_FlowZImage;
  std::vector<vtkImagePointer> m_FlowModulusImage;
  std::vector<vtkImagePointer> m_FlowVectorImage;
  std::vector<vtkImagePointer> m_MagnitudeImage;
  std::vector<vtkImagePointer> m_MaskImage;
  //std::vector<itk::PhaseContrastVector3DImage::Pointer> m_ItkFlowVectorImage;
  vtkImagePointer m_PremaskImage;

  typedef boost::unordered_map<vtkImageData*, int> PointerMap;
  PointerMap m_PointerMap;

  double m_LengthOfTimeStep;
  std::vector<double> m_TimeValues;
};

END_PCMR_DECLS

#endif

