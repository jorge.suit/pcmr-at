#include <boost/foreach.hpp>
#include "pcmrFlow4DReader.h"
#include <iostream>
#include "gtest/gtest.h"
#include <vtkStructuredPointsWriter.h>
#include <vtkNew.h>

static int _ARGC = 0;
static char **_ARGV = NULL;

TEST(TestReadMask, TestReadMask)
{
  ASSERT_TRUE(_ARGV!=NULL);
  ASSERT_EQ(2, _ARGC) << "usage: '" << _ARGV[0] << "' study\n";

  pcmr::Flow4DReader::Pointer reader = pcmr::Flow4DReader::New();

  pcmr::StatusType status = reader->SetDirectory(_ARGV[1]);
  ASSERT_EQ(pcmr::OK, status) << "SetDirectory: " << pcmr::GetStatusDescription(status) << "\n";
  
  vtkImageData * premask = reader->GetPremaskImage(3);
  std::cout << "premask(" << premask->GetReferenceCount() << ")\n";
  vtkImageData * mask = reader->GetMaskImage(3);
  std::cout << "mask(" << mask->GetReferenceCount() << ")\n";
  
  EXPECT_TRUE(mask!=NULL);
  if (mask)
    {
    // if instead of vtkNew we use vtkSmartPointer then we should use
    // delete at the end of the scope.
    vtkNew<vtkStructuredPointsWriter> writer;
    writer->SetFileName("mask.vtk");
    writer->SetInputData(mask);
    writer->Update();
    // if this delete is not invoke then the mask is reported as alive
    //writer->Delete();
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  _ARGC = argc;
  _ARGV = argv;
  return RUN_ALL_TESTS();
}
