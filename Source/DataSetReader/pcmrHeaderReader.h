#ifndef __pcmrHeaderReader_h
#define __pcmrHeaderReader_h

#include "pcmrTypes.h"
#include "pcmrDataSetReader_Export.h"
#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include "itkObject.h"
#include "itkObjectFactory.h"

BEGIN_PCMR_DECLS

class PCMRDATASETREADER_EXPORT HeaderReader : public itk::Object
{
public:
  /** Standard class typedefs. */
  typedef HeaderReader                   Self;
  typedef itk::Object                    Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
 
  /** Run-time type information (and related methods). */
  itkTypeMacro(HeaderReader, itk::Object);

  struct Property {
    std::string name;
    std::string description;
    std::string value;
  };

  HeaderReader() {}
  virtual ~HeaderReader();

  StatusType SetFileName(const char* path);

  const char* GetFileName() const 
  {
    return m_FileName.c_str();
  }

  std::string GetFormatVersion() const;
  std::string GetSourceSequence() const;
  size_t GetNumberOfTimeSteps() const;
  double GetLengthOfTimeStep() const;
  double GetBloodViscocity(double defaultValue) const;
  double GetVelocityEncoding() const;
  double GetMinimumVelocity() const;
  double GetMaximumVelocity() const;
  std::string GetImageOrientation() const;
  std::string GetGeometryUnits() const;
  std::string GetVelocityUnits() const;
  bool GetInvertComponentX() const;
  bool GetInvertComponentY() const;
  bool GetInvertComponentZ() const;
  StatusType GetTimeValues(std::vector<double> &timeValues) const;
  StatusType GetExtendedProperties(std::vector<std::string> &properties) const;
  StatusType GetExtendedProperty(const std::string& key, Property &property) const;
 private:
  std::string m_FileName;
  boost::property_tree::ptree m_PropertyTree;
};

END_PCMR_DECLS

#endif
