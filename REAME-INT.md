To create development container

```
docker run --name vtk-itk-dev -it -e DISPLAY=$DISPLAY \
  --mount type=bind,source="$(pwd)",target=/source \
  --mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix \
  josp/ubuntu-vtk-itk-dev:8.0-4.12 /bin/bash
```

To start a running development container

```
docker start vtk-itk-dev -i
```
