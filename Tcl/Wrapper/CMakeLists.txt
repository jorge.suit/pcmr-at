# The version number.
set( PCMRTCL_VERSION_MAJOR 1 )
set( PCMRTCL_VERSION_MINOR 5 )

set( WRAPPER_INSTALL_DIR ${TCL_INSTALL_DIR}/Pcmr4d${PCMRTCL_VERSION_MAJOR}.${PCMRTCL_VERSION_MINOR} )

add_subdirectory(generic)
add_subdirectory(library)
