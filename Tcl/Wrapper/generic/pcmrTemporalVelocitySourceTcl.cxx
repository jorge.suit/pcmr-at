#include "pcmrTemporalVelocitySource.h"
#include "pcmrTemporalVelocitySourceTcl.h"
#include "pcmrTclDataSetAccessor.h"
#include "vtkAlgorithmOutput.h"
#include "vtkSmartPointer.h"
#include "vtkNew.h"
#include "vtkTclUtil.h"
#include "vtkCallbackCommand.h"

#define __OBSERVER_DELETION__

#ifdef __OBSERVER_DELETION__
void OnDeleteObject(vtkObject* obj, 
                    unsigned long eid, void* clientdata, void *calldata)
{
  std::cout << "about to kill object "<< obj << std::endl;
}
#endif

class ClientData_TemporalSource
{
public:
  vtkSmartPointer<pcmr::TemporalVelocitySource> ImageSource;
  pcmr::TclDataSetAccessor *TclDataSet;
  Tcl_Command token;
  ClientData_TemporalSource()
  {
    this->TclDataSet = NULL;
    this->token = NULL;
  }

  ~ClientData_TemporalSource()
  {
    std::cout << "~ClientData_TemporalSource()\n";
    if (this->TclDataSet)
      {
      delete this->TclDataSet;
      }
  }

  void PrintAlgorithm()
  {
    this->ImageSource->Print(std::cout);
  }

  vtkAlgorithmOutput *GetOutputPort()
  {
    return this->ImageSource->GetOutputPort();
  }

  const std::vector<double>& GetTimeValues()
  {
    return this->ImageSource->GetTimeValues();
  }

  void SetTclDataSetCommand(const char *cmd)
  {
    this->TclDataSet->SetTclDataSetCommand(cmd);
    this->ImageSource->SetDataSetAccessor(this->TclDataSet);
  }
  
  const char* GetTclDataSetCommand()
  {
    return this->TclDataSet->GetTclDataSetCommand();
  }

  void SetRescaleFactor(double f)
  {
    this->ImageSource->SetRescaleFactor(f);
  }
};

int PCMR_TemporalVelocitySourceProc(ClientData clientData,
                                    Tcl_Interp *interp,
                                    int objc, Tcl_Obj *const objv[])
{
  ClientData_TemporalSource *cd = static_cast<ClientData_TemporalSource*>(clientData);
  const char* cmd = Tcl_GetString(objv[0]);
  int status;

  if (!cd)
    {
    Tcl_AppendResult(interp, "TemporalVelocitySource command '", cmd,
                     "' is corrupted: clientData is NULL:",
                     NULL);
    return TCL_ERROR;
    }
  if (objc < 2)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                     cmd, " arg ?arg ...?",
                     NULL);
    return TCL_ERROR;
    }
  const char* subcmd = Tcl_GetString(objv[1]);
  /* Print */
  if (!strcmp(subcmd, "Print"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " Print",
                       NULL);
      return TCL_ERROR;
      }
    cd->PrintAlgorithm();
    return TCL_OK;
    }
  /* Delete*/
  if (!strcmp(subcmd, "Delete"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " Delete",
                       NULL);
      return TCL_ERROR;
      }
    std::cout << "Tcl_DeleteCommandFromToken(interp, cd->token); "<< cd->token << "\n";
    Tcl_DeleteCommandFromToken(interp, cd->token);
    std::cout << "sali\n";
    return TCL_OK;
    }
  /* SetTclDataSet */
  if (!strcmp(subcmd, "SetTclDataSet"))
    {
    if (objc != 3)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " SetTclDataSet cmd",
                       NULL);
      return TCL_ERROR;
      }
    // REVIEW: check here if this command is a valid PCMR4DDataSetReader
    cd->SetTclDataSetCommand(Tcl_GetString(objv[2]));
    return TCL_OK;
    }
  if (!strcmp(subcmd, "GetTclDataSet"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " GetTclDataSet",
                       NULL);
      return TCL_ERROR;
      }
    // REVIEW: check here if this command is a valid PCMR4DDataSetReader
    const char * cmd = cd->GetTclDataSetCommand();
    Tcl_SetObjResult(interp, Tcl_NewStringObj(cmd, -1));
    return TCL_OK;
    }
  /* GetOutputPort */
  if (!strcmp(subcmd, "GetOutputPort"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " GetOutputPort",
                       NULL);
      return TCL_ERROR;
      }
    vtkAlgorithmOutput *algOutput = cd->GetOutputPort();
    vtkTclGetObjectFromPointer(interp,(void *)(algOutput),"vtkAlgorithmOutput");
    return TCL_OK;
    }
  /* SetBaseTimeStep */
    if (!strcmp(subcmd, "SetBaseTimeStep"))
    {
    int i;
    status = Tcl_GetIntFromObj(interp, objv[2], &i);
    if (status != TCL_ERROR) 
      {
      if (i < 0 || i >= cd->ImageSource->GetNumberOfTimeStepsInternal())
        {
        Tcl_AppendResult(interp, "invalid time-step index '",
                         Tcl_GetString(objv[2]), "': out of range",
                         NULL);
        status = TCL_ERROR;
        }
      cd->ImageSource->SetBaseTimeStep(i);
      status = TCL_OK;
      }
    return status;
    }
  /* GetBaseTimeStep */
    if (!strcmp(subcmd, "GetBaseTimeStep"))
    {
    Tcl_SetObjResult(interp, Tcl_NewIntObj(cd->ImageSource->GetBaseTimeStep()));
    return TCL_OK;
    }
  /* GetTimeValues */
  if (!strcmp(subcmd, "GetTimeValues"))
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " GetTimeValues",
                       NULL);
      return TCL_ERROR;
      }
    const std::vector<double>& timeValues = cd->GetTimeValues();
    if (timeValues.size())
      {
      Tcl_Obj **items = new Tcl_Obj*[timeValues.size()];
      for (size_t i = 0; i < timeValues.size(); i++)
        {
        items[i] = Tcl_NewDoubleObj(timeValues[i]);
        }
      Tcl_SetObjResult(interp, Tcl_NewListObj(static_cast<int>(timeValues.size()), items));
      delete []items;
      }
    else
      {
      Tcl_ResetResult(interp);
      }
    return TCL_OK;
    }
  if (!strcmp(subcmd, "SetRescaleFactor"))
    {
    if (objc != 3)
      {
      Tcl_AppendResult(interp, "wrong # of arguments, should be:\n",
                       cmd, " SetRescaleFactor f",
                       NULL);
      return TCL_ERROR;
      }
    double f;
    status = Tcl_GetDoubleFromObj(interp, objv[2], &f);
    if (status != TCL_ERROR)
      {
      cd->SetRescaleFactor(f);
      }
    return status;
    }
  Tcl_AppendResult(interp, "invalid subcommand '", subcmd, "' should be:\n",
                   cmd, " Delete\n",
                   cmd, " SetTclDataSet cmdName\n",
                   cmd, " GetTclDataSet\n",
                   cmd, " GetOutputPort\n",
                   cmd, " SetBaseTimeStep i\n",
                   cmd, " GetBaseTimeStep\n",
                   cmd, " GetTimeValues\n",
                   cmd, " SetRescaleFactor f",
                   NULL);
  return TCL_ERROR;
}

void PCMR_DeleteVelocitySourceProc(ClientData clientData)
{
  printf("PCMR_DeleteImporterHandler %p\n", clientData);
  ClientData_TemporalSource *cd = static_cast<ClientData_TemporalSource*>(clientData);
  delete cd;
}

#define __DEBUG_HERE__ \
  std::cout << __FILE__ << ":" << __LINE__ << std::endl;

int PCMR_CreateTemporalVelocitySource(ClientData clientData,
                                      Tcl_Interp *interp,
                                      int objc, Tcl_Obj *const objv[])
{
  
  static int icmd = 0;

  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 1)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, NULL);
    return TCL_ERROR;
    }
  ClientData_TemporalSource *cd = new ClientData_TemporalSource;
  
  //cd->ImageSource = pcmr::TemporalVelocitySource::New()->GetPointer();
  vtkNew<pcmr::TemporalVelocitySource> pp;
  cd->ImageSource = pp.GetPointer();
  cd->ImageSource->SetRescaleFactor(10.0);
  cd->TclDataSet = new pcmr::TclDataSetAccessor;
  cd->TclDataSet->SetTclInterpreter(interp);
  char buffer[256];
  sprintf(buffer, "TemporalSource%i", icmd++);
  Tcl_Command token =
    Tcl_CreateObjCommand(interp, buffer,
                         PCMR_TemporalVelocitySourceProc,
                         static_cast<ClientData>(cd),
                         PCMR_DeleteVelocitySourceProc);
  
  if (token) 
    {
#ifdef __OBSERVER_DELETION__
    vtkCallbackCommand* callback = vtkCallbackCommand::New();
    callback->SetCallback(OnDeleteObject);
    cd->ImageSource->AddObserver(vtkCommand::DeleteEvent, callback, 1.0);
    callback->Delete();
#endif
    cd->token = token;
    Tcl_SetResult(interp, buffer, TCL_VOLATILE);
    std::cout << "Command created with cd = " << cd << std::endl;
    return TCL_OK;
    }
  else
    {
    cd->token = NULL;
    Tcl_AppendResult(interp, "fatal error: failed Tcl_CreateObjCommand",
                     NULL);
    delete cd;
    return TCL_ERROR;
  }
}

int PCMR_TemporalVelocitySource_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "TemporalVelocitySource",
                       PCMR_CreateTemporalVelocitySource, NULL, NULL);

  return TCL_OK;
}
