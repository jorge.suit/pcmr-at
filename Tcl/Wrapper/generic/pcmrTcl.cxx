#include "pcmrFlow4DReaderTcl.h"
#include "pcmrTemporalVelocitySourceTcl.h"
#include "pcmrSampleCFDMeshToStudyTcl.h"
#include "pcmrTcl.h"
#include "pcmrVersion.h"

int Pcmr4d_Init(Tcl_Interp *interp)
{
  if (Tcl_InitStubs(interp, "8.4", 0) == NULL) {
    return TCL_ERROR;
  }
  if (Tcl_PkgRequire(interp, "Tcl", "8.4", 0) == NULL) {
    return TCL_ERROR;
  }
  
  PCMR_ImporterTcl_Init( interp );
  PCMR_FlowQuantifyContourTcl_Init( interp );
  PCMR_Flow4DReaderTcl_Init( interp );
  PCMR_TemporalVelocitySource_Init( interp );
  PCMR_SampleCFDMeshToStudy_Init( interp );

  char pkgName[]="Pcmr4d";
  char pkgVers[]=PCMRTCL_VERSION_STRING;
  Tcl_PkgProvide(interp, pkgName, pkgVers);
  return TCL_OK;
}
