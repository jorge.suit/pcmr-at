#ifndef __pcmrTclCore_h__
#define __pcmrTclCore_h__

#include "itkObject.h"
#include <tcl.h>

struct tclCreator;

typedef itk::Object::Pointer (*MakeITKObjectType)();

tclCreator *Make_TclCreator(MakeITKObjectType makeObj, Tcl_ObjCmdProc* proc);

int Make_Command(tclCreator *creator, Tcl_Interp *interp,
                 const char* name);

itk::Object* GetITKObjectFromClientData(ClientData clientData);

int DispatchCommonSubcommands(ClientData     clientData,
                              Tcl_Interp    *interp,
                              int            objc,
                              Tcl_Obj *const objv[],
                              itk::Object* &pointer);

#endif
