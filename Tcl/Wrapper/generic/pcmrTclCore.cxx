#include "pcmrTclCore.h"
#include "itkCommand.h"
#include <boost/thread/mutex.hpp>
#include <sstream>

//#define __TRACE__

struct tclCreator
{
  MakeITKObjectType makeObject;
  Tcl_ObjCmdProc *tclProc;

  tclCreator(MakeITKObjectType make, Tcl_ObjCmdProc *proc)
  {
    this->makeObject = make;
    this->tclProc = proc;
  }

  int Make_Command(Tcl_Interp *interp, const char* name)
  {
#ifdef __TRACE__    
    std::cout << "Make_Command: " << this << std::endl;
#endif
    Tcl_Command token = Tcl_CreateObjCommand(interp, name,
                                             tclCreator::CreateCommand, this,
                                             tclCreator::DeleteProc);
    return token == NULL ? TCL_ERROR : TCL_OK;
  }

  static void DeleteProc(ClientData clientData)
  {
#ifdef __TRACE__    
    std::cout << "DeleteProc: " << clientData << std::endl;
#endif    
    tclCreator *cd = static_cast<tclCreator*>(clientData);
    delete cd;
  }

  static boost::mutex _imutex;
  static size_t iCmd;
  static void GetNextCommandName(std::string &name);

  static int CreateCommand(ClientData clientData,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[]);
};

boost::mutex tclCreator::_imutex;
size_t tclCreator::iCmd = 0;

void tclCreator::GetNextCommandName(std::string &name)
{
  name = "itkTclTemp";
  std::stringstream ss;
  tclCreator::_imutex.lock();
  ss << (++tclCreator::iCmd);
  tclCreator::_imutex.unlock();
  name += ss.str();;
}

struct tclITKClientData
{
  itk::Object *pointer;
  Tcl_Command token;
  Tcl_Interp *interp;

  tclITKClientData(itk::Object::Pointer ptr)
  {
    this->pointer = ptr.GetPointer();
    this->pointer->Register();
    this->token = NULL;
    this->interp = NULL;
    if (this->pointer)
      {
#ifdef __TRACE__    
      std::cout << "Setting DeleteEvent\n";
#endif
      itk::CStyleCommand::Pointer itkCommand = itk::CStyleCommand::New();
      itkCommand->SetCallback(tclITKClientData::OnDeleteObject);
      itkCommand->SetConstCallback(tclITKClientData::OnDeleteObject_Const);
      itkCommand->SetClientData(this);
      this->pointer->AddObserver(itk::DeleteEvent(), itkCommand);
      }
  }

  ~tclITKClientData()
  {
#ifdef __TRACE__    
    std::cout << "~tclITKClientData()\n";
#endif
    if (pointer)
      {
#ifdef __TRACE__    
      pointer->Print(std::cout);
#endif
      pointer->UnRegister();
      }
  }

  int CreateCommand(Tcl_Interp *interp, Tcl_ObjCmdProc *proc);

  static void OnDeleteObject(itk::Object *o, const itk::EventObject &e,
                             void* clientData);

  static void OnDeleteObject_Const(const itk::Object *o,
                                   const itk::EventObject &e,
                                   void* clientData);

  static void DeleteProc(ClientData clientData);
};

int tclITKClientData::CreateCommand(Tcl_Interp *interp, Tcl_ObjCmdProc *proc)
{
  if (!pointer)
    {
    Tcl_AppendResult(interp,
                     "could not create command from NULL itk::Object",
                     NULL );
    return TCL_ERROR;
    }
  this->interp = interp;
  std::string nameCmd;
  tclCreator::GetNextCommandName(nameCmd);
  Tcl_Command _token =
    Tcl_CreateObjCommand(interp, nameCmd.c_str(), proc,
                         static_cast<ClientData>(this),
                         tclITKClientData::DeleteProc);
  if (_token) 
    {
    this->token = _token;
    Tcl_SetResult(interp, const_cast<char*>(nameCmd.c_str()), TCL_VOLATILE);
    return TCL_OK;
    }
  else
    {
    this->token = NULL;
    Tcl_AppendResult(interp, "fatal error: failed Tcl_CreateObjCommand",
                     NULL);
    return TCL_ERROR;
    } 
}

void tclITKClientData::OnDeleteObject(itk::Object *o,
                                      const itk::EventObject &e,
                                      void* clientData)
{
#ifdef __TRACE__    
  std::cout << "tclITKClientData::OnDeleteObject: " << clientData << std::endl;
#endif
  tclITKClientData::OnDeleteObject_Const(o, e, clientData);
}

void tclITKClientData::OnDeleteObject_Const(const itk::Object *o,
                                            const itk::EventObject &e,
                                            void* clientData)
{
#ifdef __TRACE__    
  std::cout << "tclITKClientData::OnDeleteObject_Const: " << clientData << std::endl;
#endif
  tclITKClientData *cd = static_cast<tclITKClientData*>(clientData);
#ifdef __TRACE__    
  std::cout << "cd->token=" << cd->token << std::endl;
#endif
  // I'm deleting the object pointer, so prevent it from being delete
  // twice
  cd->pointer = NULL;
  if (cd->token)
    {
#ifdef __TRACE__    
    std::cout << "Tcl_DeleteCommandFromToken(cd->interp, cd->token);\n";
#endif
    Tcl_DeleteCommandFromToken(cd->interp, cd->token);
    }
}

void tclITKClientData::DeleteProc(ClientData clientData)
{
#ifdef __TRACE__    
  std::cout << "tclITKClientData::DeleteProc " << clientData << "\n";
#endif
  tclITKClientData *cd = static_cast<tclITKClientData*>(clientData);
  cd->token = NULL;
  delete cd;
}

int tclCreator::CreateCommand(ClientData clientData,
                              Tcl_Interp *interp,
                              int objc, Tcl_Obj *const objv[])
{
  tclCreator *creator = static_cast<tclCreator*>(clientData);

  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 1)
    {
    Tcl_AppendResult(interp, "wrong # of arguments, should be\n",
                     "    ", cmd, NULL);
    return TCL_ERROR;
    }
  tclITKClientData *cd = new tclITKClientData((creator->makeObject)());
  int status = cd->CreateCommand(interp, creator->tclProc);
#ifdef __TRACE__    
  std::cout << "AFTER cd->CreateCommand(interp, creator->tclProc);\n";
  std::cout << "cd->token = " << cd->token << std::endl;
#endif
  if (status != TCL_OK)
    {
    delete cd;
    }
  return status;  
}

tclCreator *Make_TclCreator(MakeITKObjectType makeObj, Tcl_ObjCmdProc* proc)
{
  return new tclCreator(makeObj, proc);
}

int Make_Command(tclCreator *creator, Tcl_Interp *interp, const char* name)
{
  return creator->Make_Command(interp, name);
}

itk::Object* GetITKObjectFromClientData(ClientData clientData)
{
 tclITKClientData *cd = static_cast<tclITKClientData*>(clientData);
 if (cd)
   {
#ifdef __TRACE__    
   std::cout << "GetITKObjectFromClientData:\n";
   std::cout << "cd->token = " << cd->token << std::endl;
#endif
   return cd->pointer;
   }
 else
   {
   return NULL;
   }
}

int DispatchCommonSubcommands(ClientData     clientData,
                              Tcl_Interp    *interp,
                              int            objc,
                              Tcl_Obj *const objv[],
                              itk::Object* &pointer)
{
  const char* cmd = Tcl_GetString(objv[0]);
  pointer = GetITKObjectFromClientData(clientData);

  if (!pointer)
    {
    Tcl_AppendResult(interp, "command '", cmd,
                     "' is corrupted: clientData is NULL",
                     NULL);
    return TCL_ERROR;
    }
  if (objc < 2)
    {
    Tcl_AppendResult(interp, "wrong number of arguments, should be '", cmd,
                     " subcommand ?args?'",
                     NULL);
    return TCL_ERROR;
    }
  const char *subcmd = Tcl_GetString(objv[1]);
  if (strcmp(subcmd, "Delete") == 0)
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong number of arguments, should be '", cmd,
                       " Delete",
                       NULL);
      return TCL_ERROR;      
      }
    pointer->Delete();
    return TCL_OK;
    }
  if (strcmp(subcmd, "Print") == 0)
    {
    if (objc != 2)
      {
      Tcl_AppendResult(interp, "wrong number of arguments, should be '", cmd,
                       " Print",
                       NULL);
      return TCL_ERROR;      
      }
    pointer->Print(std::cout);
    return TCL_OK;
    }
  return TCL_CONTINUE;
}

