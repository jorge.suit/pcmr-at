#include "vtkTclUtil.h"
#include "pcmrSampleCFDMeshToStudy.h"
#include "pcmrSampleCFDMeshToStudyTcl.h"

int PCMR_SampleCFDMeshToStudy_Proc(ClientData clientData,
                                   Tcl_Interp *interp,
                                   int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);

  if (objc != 9)
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "wrong # of arguments, should be\n",
                     "    ", cmd, " ugrid pathPrefix sx sy sz sizeSample padExtra factorVelocity",
                     NULL);
    return TCL_ERROR;
    }
  int error = 0;
  const char* argUGRID = Tcl_GetString(objv[1]);
  vtkUnstructuredGrid *meshCFD = static_cast<vtkUnstructuredGrid *>(
    vtkTclGetPointerFromObject(argUGRID,"vtkUnstructuredGrid",
                               interp, error));
  if (error || !meshCFD)
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "could not convert '", argUGRID, 
                     "' to vtkUnstructuredGrid", NULL);
    return TCL_ERROR;
    }
  const char *pathPrefix = Tcl_GetString( objv[ 2 ] );
  double spacing[ 3 ];
  int tcl_status;
  for( int i = 0; i < 3; i++ )
    {
    tcl_status = Tcl_GetDoubleFromObj( interp, objv[ 3 + i ], spacing + i );
    if ( tcl_status != TCL_OK )
      {
      return tcl_status;
      }
    }
  int size;
  tcl_status = Tcl_GetIntFromObj( interp, objv[ 6 ], &size );
  if ( tcl_status != TCL_OK )
    {
    return tcl_status;
    }
  if ( size <= 0 )
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "wrong sample size argument '", Tcl_GetString( objv[ 6 ] ),
                     "' must be integer positive",
                     NULL);
    return TCL_ERROR;
   }
  unsigned int sizeSample = (unsigned int)size;
  tcl_status = Tcl_GetIntFromObj( interp, objv[ 7 ], &size );
  if ( tcl_status != TCL_OK )
    {
    return tcl_status;
    }
  if ( size <= 0 )
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "wrong pad extra argument '", Tcl_GetString( objv[ 7 ] ),
                     "' must be integer positive",
                     NULL);
    return TCL_ERROR;
   }
  unsigned int padExtra = (unsigned int)size;
  double factorVelocity;
  tcl_status = Tcl_GetDoubleFromObj( interp, objv[ 8 ], &factorVelocity );
  if ( tcl_status != TCL_OK )
    {
    return tcl_status;
    }
  if ( factorVelocity <= 0 )
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "wrong velocity factor '", Tcl_GetString( objv[ 8 ] ),
                     "' must be positive",
                     NULL);
    return TCL_ERROR;
   }
  pcmr::StatusType pcmr_status = 
    pcmr::SampleCFDMeshToStudy( meshCFD, pathPrefix, spacing, padExtra, sizeSample, factorVelocity );
  if ( pcmr_status != pcmr::OK )
    {
    Tcl_AppendResult(interp,
                     "SampleCFDMeshToStudy ERROR --\n",
                     "FAILED!!!",
                     NULL);
    return TCL_ERROR;
    
    }
  return TCL_OK;
}

int PCMR_SampleCFDMeshToStudy_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "SampleCFDMeshToStudy",
                       PCMR_SampleCFDMeshToStudy_Proc, NULL, NULL);
  return TCL_OK;
}
