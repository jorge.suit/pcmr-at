#include <tcl.h>
#include "pcmrFlowQuantifyContour.h"
#include "vtkTclUtil.h"


int PCMR_FlowQuantifyVtkContourProc(ClientData clientData,
                                    Tcl_Interp *interp,
                                    int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 4 && objc != 5)
    {
    Tcl_AppendResult(interp,
                     "FlowQuantifyVtkContour ERROR --\n",
                     "wrong # of arguments, should be\n",
                     "    ", cmd, " PolyData Spline Normal ?Resolution?",
                     NULL);
    return TCL_ERROR;
    }
  int error = 0;
  const char* argPolyData = Tcl_GetString(objv[1]);
  vtkPolyData *grid = static_cast<vtkPolyData *>(
    vtkTclGetPointerFromObject(argPolyData,"vtkPolyData",
                               interp, error));
  if (error || !grid)
    {
    Tcl_AppendResult(interp,
                     "FlowQuantifyVtkContour ERROR --\n",
                     "could not convert '", argPolyData, 
                     "' to vtkPolyData", NULL);
    return TCL_ERROR;
    }
  const char* argSpline = Tcl_GetString(objv[2]);
  vtkParametricSpline *spline = static_cast<vtkParametricSpline *>(
    vtkTclGetPointerFromObject(argSpline,"vtkParametricSpline",
                               interp, error));
  if (error || !spline)
    {
    Tcl_AppendResult(interp,
                     "FlowQuantifyVtkContour ERROR --\n",
                     "could not convert '", argSpline, 
                     "' to vtkParametricSpline", NULL);
    return TCL_ERROR;
    }
  int status;
  int lengthNormal = 0;
  status = Tcl_ListObjLength(interp, objv[3], &lengthNormal);
  if (status != TCL_OK)
    {
    Tcl_AppendResult(interp,
                     "FlowQuantifyVtkContour ERROR --\n",
                     "could not retrive length of argument Normal '",
                     Tcl_GetString(objv[3]), "'", NULL);
    return TCL_ERROR;
    }
  if (lengthNormal != 3)
    {
    Tcl_AppendResult(interp,
                     "FlowQuantifyVtkContour ERROR --\n",
                     "argument Normal must be list of 3 elements, received: '",
                     Tcl_GetString(objv[3]), "'", NULL);
    return TCL_ERROR;
    }
  double normal[3];
  for(int i = 0; i < 3; i++)
    {
    Tcl_Obj *item;
    Tcl_ListObjIndex(interp, objv[3], i, &item);
    status = Tcl_GetDoubleFromObj(interp, item, normal+i);
    if (status != TCL_OK)
      {
      Tcl_AppendResult(interp,
                       "FlowQuantifyVtkContour ERROR --\n",
                       "the normal must be list of 3 numbers, received: '",
                       Tcl_GetString(objv[3]), "'", NULL);
      return TCL_ERROR;
      }
    }
  int resolution = 30;
  if (objc == 5)
    {
    std::cout << "resolution provided " << Tcl_GetString(objv[4]) << std::endl;
    status = Tcl_GetIntFromObj(interp, objv[4], &resolution);
    if (status != TCL_OK)
      {
      Tcl_AppendResult(interp,
                       "FlowQuantifyVtkContour ERROR --\n",
                       "the resolution must be an integer > 3, received: '",
                       Tcl_GetString(objv[4]), "'", NULL);
      return TCL_ERROR;
      }
    if (resolution < 4)
      {
      Tcl_AppendResult(interp,
                       "FlowQuantifyVtkContour ERROR --\n",
                       "the resolution must be an integer > 3, received: '",
                       Tcl_GetString(objv[4]), "'", NULL);
      return TCL_ERROR;
      }
    }
  std::cout << "using resolution = " << resolution << std::endl;
  typedef std::map<std::string,float> table_map_t;
  table_map_t table;
  // REVIEW: contourResolution must be read from objv
  pcmr::FlowQuantifyVtkContour(grid, spline, normal, resolution, table);
  Tcl_Obj *objTable = Tcl_NewListObj(0, NULL);
  for(table_map_t::iterator it = table.begin(); it != table.end(); ++it)
    {
    std::string key = it->first;
    float value = it->second;
    Tcl_Obj* objKey = Tcl_NewStringObj(key.c_str(), -1);
    Tcl_Obj* objValue = Tcl_NewDoubleObj(value);
    Tcl_ListObjAppendElement(interp, objTable, objKey);
    Tcl_ListObjAppendElement(interp, objTable, objValue);
    }
  Tcl_SetObjResult(interp, objTable);
  return TCL_OK;
}

int PCMR_FlowQuantifyContourTcl_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "FlowQuantifyVtkContour",
                       PCMR_FlowQuantifyVtkContourProc, NULL, NULL);
  return TCL_OK;
}
