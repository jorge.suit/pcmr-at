#include "pcmrFlow4DReaderTcl.h"
#include "pcmrFlow4DReader.h"
#include "pcmrTclCore.h"

int PCMR_Cmd_Flow4DReader(ClientData     clientData,
                         Tcl_Interp    *interp,
                         int            objc,
                         Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);
  itk::Object *pointer;
  int tclStatus;
  
  tclStatus = DispatchCommonSubcommands(clientData, interp, objc, objv, pointer);
  if (tclStatus != TCL_CONTINUE)
    {
    return tclStatus;
    }
  pcmr::Flow4DReader *reader = dynamic_cast<pcmr::Flow4DReader*>(pointer);
  if (reader == NULL)
    {
    Tcl_AppendResult(interp, "Flow4DReader command '", cmd,
                     "' is corrupt: could not convert clientData->pointer to Flow4DReader",
                     NULL);
    return TCL_ERROR;
    }
  const char* subcmd = Tcl_GetString(objv[1]);
  if (!strcmp(subcmd, "SetDirectory"))
    {
    if (objc == 3)
      {
      pcmr::StatusType status = reader->SetDirectory(Tcl_GetString(objv[2]));
      if (status != pcmr::OK)
        {
        Tcl_AppendResult(interp, pcmr::GetStatusDescription(status),
                         NULL);
        return TCL_ERROR;
        }
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " SetDirectory path'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetNumberOfTimeSteps"))
    {
    if (objc == 2)
      {
      size_t n = reader->GetNumberOfTimeSteps();
      Tcl_SetObjResult(interp, Tcl_NewIntObj(n));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetNumberOfTimeSteps'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetLengthOfTimeStep"))
    {
    if (objc == 2)
      {
      float d = reader->GetLengthOfTimeStep();
      Tcl_SetObjResult(interp, Tcl_NewDoubleObj(d));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetNumberOfTimeSteps'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetDirectory"))
    {
    if (objc == 2)
      {
      Tcl_SetObjResult(interp, Tcl_NewStringObj(reader->GetDirectory(), -1));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetDirectory'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetFormatVersion"))
    {
    if (objc == 2)
      {
      std::string version(reader->GetFormatVersion());
      Tcl_SetObjResult(interp, Tcl_NewStringObj(version.c_str(), -1));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetFormatVersion'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetSourceSequence"))
    {
    if (objc == 2)
      {
      std::string src(reader->GetSourceSequence());
      Tcl_SetObjResult(interp, Tcl_NewStringObj(src.c_str(), -1));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetSourceSequence'",
                       NULL);
      return TCL_ERROR;
      }
    }
  if (!strcmp(subcmd, "GetVelocityEncoding"))
    {
    if (objc == 2)
      {
      Tcl_SetObjResult(interp, Tcl_NewDoubleObj(reader->GetVelocityEncoding()));
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult(interp, "wrong number of arguments, must be '", cmd,
                     " GetSourceSequence'",
                       NULL);
      return TCL_ERROR;
      }
    }
  Tcl_AppendResult(interp, "wrong subcommand '", subcmd, "', must be:\n",
                   "\tDelete\n",
                   "\tPrint\n",
                   "\tSetDirectory path\n"
                   "\tGetNumberOfTimeSteps\n",
                   "\tGetLengthOfTimeStep\n",
                   "\tGetDirectory\n",
                   "\tGetFormatVersion\n",
                   "\tGetSourceSequence",
                   "\tGetVelocityEncoding",
                   NULL);
  return TCL_ERROR;
}

itk::Object::Pointer PCMR_Make_Flow4DReader()
{
  itk::Object::Pointer reader = pcmr::Flow4DReader::New().GetPointer();
  return reader;
}

int PCMR_Flow4DReaderTcl_Init(Tcl_Interp *interp)
{
  tclCreator *creator = Make_TclCreator(&PCMR_Make_Flow4DReader,
                                        &PCMR_Cmd_Flow4DReader);
  return Make_Command(creator, interp, "Flow4DReader");
}
