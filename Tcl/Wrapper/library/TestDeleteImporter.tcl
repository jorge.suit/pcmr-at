set lib [file normalize ../generic/libpcmr4dtcl.so]
puts "to load $lib"
load ../generic/libpcmr4dtcl.so Pcmr4d

set importerSiemens [SiemensImporter]
$importerSiemens AddObserver DeleteEvent "puts Die_$importerSiemens"
puts "BEFORE: exists? [expr {[info command $importerSiemens] ne {}?yes:no}]"
rename $importerSiemens ""
#$importerSiemens Delete
puts "AFTER: exists? [expr {[info command $importerSiemens] ne {}?yes:no}]"
puts "BYE!"