package require ClipBox

snit::widget ClipImage {
  component Box -public Box
  component Canvas

  option -image -default "" -configuremethod _ConfImage
  delegate option -boxvisible to Box as -visible
  delegate option -background to Canvas
  delegate option * to hull
  constructor { args } {
    $self _CreateCanvas
    $self configurelist $args
  }

  method _ConfImage { o v } {
    if {[winfo exists $Canvas]} {
      $Canvas delete clipI
      if {$v ne ""} {
        $Canvas create image 0 0 -image $v -tags clipI -anchor nw
        set w [image width $v]
        incr w -2
        set h [image height $v]
        incr h -2
        $Canvas configure -width $w -height $h
        update
      }
      $Box Reset
    }
    set options(-image) $v
  }
  
  method _CreateCanvas { } {
    set Canvas [canvas $win.c]
    grid $win.c -row 0 -column 0 -sticky snew
    set Box [ClipBox %AUTO% -canvas $Canvas -fill gray10 -stipple gray25]
  }
}