package require snit

snit::type MedicalVolumeOrienter {

  typevariable OrthogonalViewNames  {Sagittal Axial Coronal}
  typevariable AxisNames {X Y Z}

  typevariable AnatomicalView -array {
    A,bodyplane Coronal
    P,bodyplane Coronal
    L,bodyplane Sagittal
    R,bodyplane Sagittal
    S,bodyplane Axial
    I,bodyplane Axial
    A,next P
    P,next A
    L,next R
    R,next L
    S,next I
    I,next S
    Sagittal,near    "L"
    Sagittal,far     "R"
    Sagittal,partner "Axial"
    Sagittal,down    "I"
    Axial,near       "S"
    Axial,far        "I"
    Axial,partner    "Sagittal"
    Axial,down       "L"
    Coronal,near     "A"
    Coronal,far      "P"
    Coronal,partner  "Axial"
    Coronal,down     "I"
  }

  variable Orientation

  typemethod GetOrthogonalViewNames { } {
    return $OrthogonalViewNames
  }

  typemethod CheckViewName { viewName } {
    if {[lsearch $OrthogonalViewNames $viewName] == -1} {
      set choices "[join [lrange $OrthogonalViewNames 0 end-1] ,] or [lindex $OrthogonalViewNames end]"
      error "Invalid view name '$viewName' for '$type', should be $choices"
    }
    return 1
  }

  typemethod CheckAxisValue { axis } {
    set idx [lsearch $AxisNames $axis]
    if {$idx == -1} {
      set choices "[join [lrange $AxisNames 0 end-1] ,] or [lindex $AxisNames end]"
      error "Invalid axis '$axis' for '$type', should be $choices"
    }
    return $idx
  }

  typemethod GetAxisFromIndex { iaxis } {
    if { $iaxis < 0 || $iaxis > 2} {
      error "invalid axis index '$iaxis', must be: 0, 1, or 2"
    }
    return [lindex $AxisNames $iaxis]
  }

  typemethod CheckOrientation { value } {
    if {[string length $value] != 3} {
      error "invalid orientation value '$value', must be a string of length 3"
    }
  }

  constructor {} {
    set Orientation "ASL"
  }

  destructor {
  }

  method SetOrientation { value } {
    $type CheckOrientation $value
    set Orientation $value
  }

  method GetOrientation {} {
    return $Orientation
  }

  method GetPlaneLabel { axis } {
    set idx [$type CheckAxisValue $axis]
    set axisLabel [string index $Orientation $idx]
  }

  method GetPlaneName { axis } {
    return [$self GetBodyPlaneName $axis]
  }

  method GetBodyPlaneName { axis } {
    set idx [$type CheckAxisValue $axis]
    set axisDir [string index $Orientation $idx]
    return $AnatomicalView($axisDir,bodyplane)
  }

  method GetOrientationLabel { axis } {
    set idx [$type CheckAxisValue $axis]
    set axisDir [string index $Orientation $idx]
    return [list $axisDir $AnatomicalView($axisDir,next)]
  }

  method GetAxisFromView { viewName } {
    foreach p {near far} {
      set i [string first $AnatomicalView($viewName,$p) $Orientation]
      if {$i != -1} {
        return [lindex $AxisNames $i]
      }
    }
    error "could not find axis for view '$viewName', wrong image orientation '$Orientation'"
  }

  method GetAxisIndexForView { viewName } {
    $type CheckViewName $viewName
    return [Get${viewName}Axis]
  }

  # Align the camera so that it faces the desired widget
  #
  # Sagittal
  #
  #    - look from  L(eft) to R(ight)
  #    - A(nterior) located at the left of the screen (Coronal)
  #    - I(nferior) located at the botton (Axial)
  #
  # Axial
  #
  #    - look from S(uperior) to I(nferior)
  #    - A(nterior) located at the left of the screen
  #    - L(eft) located at the botton (Sagittal)
  #
  # Coronal
  #
  #    - look from A(anterior) to P(osterior)
  #    - R(ight) located at the left of the screen
  #    - I(nferior) located at the botton (Axial)

  method AlignCameraToAxis { renderWidget axis } {
    set bodyPlane [$self GetBodyPlaneName $axis]
    foreach {c(X) c(Y) c(Z)} [$renderWidget GetImageCenter] break
    foreach {s(X) s(Y) s(Z)} [$renderWidget GetImageSpacing] break
    array set up {
      X 0
      Y 0
      Z 0
    }
    # direction from center to camera position
    array set focal {
      X 0
      Y 0
      Z 0
    }
    array set otherAxes {
      X {Y Z}
      Y {X Z}
      Z {X Y}
    }

    set nearfar [$self GetOrientationLabel $axis]
    if {[lindex $nearfar 0] eq $AnatomicalView($bodyPlane,near)} {
      set focal($axis) -1
    } else {
      set focal($axis) 1
    }
    foreach a $otherAxes($axis) {
      if {[$self GetBodyPlaneName $a] eq $AnatomicalView($bodyPlane,partner)} {
        set nf [$self GetOrientationLabel $a]
        if {[lindex $nf 0] eq $AnatomicalView($bodyPlane,down)} {
          set up($a) 1
        } else {
          set up($a) -1
        }
        break
      }
    }
    foreach a {X Y Z} {
      set p($a) [expr {$c($a) + $focal($a)*$s($a)}]
    }
    set camera [$renderWidget Renderer GetActiveCamera]
    $camera SetViewUp $up(X) $up(Y) $up(Z)
    $camera SetFocalPoint $c(X) $c(Y) $c(Z)
    $camera SetPosition $p(X) $p(Y) $p(Z)
    $camera OrthogonalizeViewUp
    $renderWidget Renderer ResetCamera
    #ren1 ResetCameraClippingRange
    $renderWidget RenderWindow Render
  }

  # Align the widget back into orthonormal position,
  # set the slider to reflect the widget's position,
  # call AlignCamera to set the camera facing the widget
  #
  method AlignViewToAxis {renderWidget axis} {
    set planeWidget [$renderWidget GetPlaneWidget $axis]

    set po [$planeWidget GetPlaneOrientation]
    # first align the planeWidget if it is not aligned
    if { $po == 3 } {
      $planeWidget SetPlaneOrientationTo${axis}Axes
      set slice_number [$renderWidget GetSlicerPosition $axis]
      $planeWidget SetSliceIndex $slice_number
    }
    # update the scroll widget with the slice_number
    $self AlignCameraToAxis $renderWidget $axis
  }

  method AlignToView { renderWidget {viewName ""} } {
    if {$viewName eq ""} {
      set viewName [$self GetBodyPlaneName "Z"]
      puts "AlignToView $viewName"
    }
    set axis [$self GetAxisFromView $viewName]
    $self AlignViewToAxis $renderWidget $axis
    $renderWidget EnableSlicer $axis
  }
}
