package require vtk
package require snit
package require Img
package require VTKUtil

snit::type ImageThumbnailManager {
  typemethod Make { Image filename args } {
    array set opts {
      -slicepos -1
      -flipaxis -1
      -size 64
      -axis 2
    }
    array set opts $args
    if {[VTKUtil::GetVTKMajorVersion] <= 5} {
      $Image Update
    }
    set extent [$Image GetExtent]
    set ia0 [expr {2*$opts(-axis)}]
    set ia1 [expr {$ia0+1}]
    set imin [lindex $extent $ia0]
    set imax [lindex $extent $ia1]
    if {$opts(-slicepos)>=$imin && $opts(-slicepos)<=$imax} {
      set i $opts(-slicepos)
    } else {
      set i [expr {($imax + $imin) / 2}]
    }

    # vtkExtractVOI to extract the mean slice
    set extract [vtkExtractVOI New]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $extract SetInputData $Image
    } else {
      $extract SetInput $Image
    }
    $extract SetVOI {*}[lreplace $extent $ia0 $ia1 $i $i]
    $extract SetSampleRate 1 1 1
    $extract Update

    # vtkImageResize to resize up to the thumbnail size requested
    set resize [vtkImageResize New]
    $resize SetInputConnection [$extract GetOutputPort]
    set thumbSize {1 1 1}
    set taxis $i
    set j 0
    for {set i 0} {$i < 3} {incr i} {
      if {$i ne $opts(-axis)} {
        lappend taxis $i
        set j1 [expr {$i*2}]
        set j2 [expr {$i*2+1}]
        set thumbInfo($j,size) [expr {[lindex $extent $j2]-[lindex $extent $j1]}]
        set thumbInfo($j,index) $i
        incr j
      }
    }
    set size0 $thumbInfo(0,size)
    set size1 $thumbInfo(1,size)
    if {$size0>$size1} {
      set i $thumbInfo(0,index)
      set thumbSize [lreplace $thumbSize $i $i $opts(-size)]
      set i $thumbInfo(1,index)
      set thumbSize [lreplace $thumbSize $i $i [expr {($size1*$opts(-size))/$size0}]]
    } else {
      set i $thumbInfo(1,index)
      set thumbSize [lreplace $thumbSize $i $i $opts(-size)]
      set i $thumbInfo(0,index)
      set thumbSize [lreplace $thumbSize $i $i [expr {($size0*$opts(-size))/$size1}]]
    }
    puts "thumbSize = $thumbSize"
    $resize SetOutputDimensions {*}$thumbSize
    $resize Update

    # flip requested axis
    if {$opts(-flipaxis)>=0 && $opts(-flipaxis)<=2} {
      set flip [vtkImageFlip New]
      $flip SetInputConnection [$resize GetOutputPort]
      $flip SetFilteredAxis $opts(-flipaxis)
      $flip Update
      set targetFilter $flip
    } else {
      set targetFilter $resize
    }

    # vtkImageMapToWindowLevelColors to obtaing a color representation
    # of the thumbnail.
    foreach {rmin rmax} [[$resize GetOutput] GetScalarRange] break
    set w [expr {$rmax - $rmin}]
    set l [expr {0.5*($rmax + $rmin)}]
    set wlMapper [vtkImageMapToWindowLevelColors New]
    $wlMapper SetWindow $w
    $wlMapper SetLevel   $l
    $wlMapper SetInputConnection [$targetFilter GetOutputPort]

    # vtkPNGWriter the thumbnail writer
    set writer [vtkPNGWriter New]
    $writer SetInputConnection [$wlMapper GetOutputPort]
    $writer SetFileName $filename
    $writer SetFileDimensionality 2
    $writer Write

    # release the pipeline
    $writer Delete
    $wlMapper Delete
    if {$targetFilter ne $resize} {
      $targetFilter Delete
    }
    $resize Delete
    $extract Delete
  }

  variable Cache
  option -directory -configuremethod _ConfDirectory
  option -slicepos -default -1
  option -size -default 64

  constructor { args } {
    set options(-directory) [pwd]
    $self configurelist $args
  }

  destructor {
    $self ClearCache
  }

  # Create the thumbnail, write it to disk, read it back and store it
  # in Cache
  method MakeThumbnail { Image name } {
    array set axisId {
      X 0
      Y 1
      Z 2
    }
    set filename [$self GetThumbnailFileName $name]
    set axisThumb Z
    set axisFlip Y
    $type Make $Image $filename \
        -slicepos $options(-slicepos) \
        -flipaxis $axisId($axisFlip) -axis $axisId($axisThumb) \
        -size $options(-size)
    return [$self GetThumbnail $name]
  }

  method _ConfDirectory { o v } {
    if {$options(-directory) eq $v} {
      return
    }
    $self ClearCache
    if { $v eq "" } {
      set v [pwd]
    }
    set options(-directory) $v
  }

  method GetThumbnailFileName { name } {
    return [file join $options(-directory) ${name}.png]
  }

  method GetThumbnail { name } {
    if {[info exists Cache($name)]} {
      return $Cache($name)
    }
    set filename [$self GetThumbnailFileName $name]
    if {![file exists $filename]} {
      return ""
    }
    set Cache($name) [image create photo -file $filename]
    return $Cache($name)
  }

  method ClearCache { } {
    foreach i [array names Cache] {
      image delete $Cache($i)
    }
    array unset Cache
  }
}
