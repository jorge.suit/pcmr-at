package require snit

snit::widgetadaptor CScale {
  option -from -default 0
  option -to -default 1
  option -variable -default "" -configuremethod _ConfVariable
  option -value -configuremethod _ConfValue -cgetmethod _CGetValue
  option -command -default ""

  delegate option * to hull
  delegate method * to hull

  constructor {args} {
    installhull using ttk::scale -command [mymethod _CmdOnChange] \
        -from 0 -to 100
    $self configurelist $args
  }

  method _ConfVariable {o v} {
  }

  method _ConfValue {o y} {
    set x [$self _ConvertToInternalDomain $y]
    $hull configure -value $x
    return $y
  }

  method _CGetValue {o} {
    set x [$hull cget -value]
    return [$self _ConvertToExternalDomain $x]
  }

  method get {args} {
    set l [llength $args]
    if {$l != 0 && $l != 2} {
      error "wrong # args: should be \"$win get ?x y?\""
    }
    set caller [uplevel {namespace current}]
    set x [$hull get {*}$args]
    if {$caller eq "::ttk::scale"} {
      return $x
    }
    return [$self _ConvertToExternalDomain $x]
  }

  method set { y } {
    set caller [uplevel {namespace current}]
    if {$caller eq "::ttk::scale"} {
      set res [$hull set $y]
      $self _UpdateExternalVariable [$self _ConvertToExternalDomain [$hull get]]
      return $res
    }
    $hull set [$self _ConvertToInternalDomain $y]
    return $y
  }

  method _UpdateExternalVariable { y } {
    if {$options(-variable) ne ""} {
      uplevel #0 set $options(-variable) $y
    }
  }

  method _ConvertToInternalDomain { y } {
    set f $options(-from)
    set t $options(-to)
    return [expr {100.0 * ($y - $f) / ($t - $f)}]
  }

  method _ConvertToExternalDomain { x } {
    set f $options(-from)
    set t $options(-to)
    return [expr {$x * ($t - $f) / 100.0 + $f}]
  }

  method _InvokeExternalCommand { y } {
    if {$options(-command) ne ""} {
      uplevel #0 {*}$options(-command) $y
    }
  }

  method _CmdOnChange {v} {
    set y [$self _ConvertToExternalDomain $v]
    $self _UpdateExternalVariable $y
    $self _InvokeExternalCommand $y
  }
}