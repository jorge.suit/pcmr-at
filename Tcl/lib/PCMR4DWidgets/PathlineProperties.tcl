package require snit
package require wcb

namespace import msgcat::*

snit::widgetadaptor PathlineProperties {

  variable TerminalSpeed 0.1
  variable Resolution 10
  variable TraceLength 2
  variable ReinjectEvery 0

  variable Widgets -array {}

  delegate option * to hull
  delegate method * to hull

  constructor { args } {
    installhull using PropertyContainer
    $self configurelist $args
    $self buildMainFrame
    trace add variable [myvar TerminalSpeed] write \
        [mymethod cbEnableIfNotEmpty]
    trace add variable [myvar Resolution] write [mymethod cbEnableIfNotEmpty]
    trace add variable [myvar TraceLength] write [mymethod cbEnableIfNotEmpty]
    trace add variable [myvar ReinjectEvery] write [mymethod cbEnableIfNotEmpty]
  }
  
  destructor {
  }

  method buildMainFrame { } {
    set f [$self GetMainFrame]
    # TerminalSpeed
    ttk::label $f.lbTerminalSpeed -text [mc "Terminal Speed"]:
    ttk::entry $f.entTerminalSpeed -textvariable [myvar TerminalSpeed]
    wcb::callback $f.entTerminalSpeed before insert \
        wcb::checkEntryForReal

    # Resolution
    ttk::label $f.lbResolution -text [mc "Resolution"]:
    ttk::spinbox $f.spinResolution -from 1 -to 15 \
        -textvariable [myvar Resolution] -state disabled
    wcb::callback $f.spinResolution before insert \
        {PropertyContainer::checkEntryIntRange 0 15}

    # TraceLength
    ttk::label $f.lbTraceLength -text [mc "Trace Length"]:
    set Widgets(spinbox,TraceLength) \
        [ttk::spinbox $f.spinTraceLength -from 0 -to 10 \
             -textvariable [myvar TraceLength]]
    wcb::callback $f.spinTraceLength before insert \
        {PropertyContainer::checkEntryIntRange 0 10}

    # ReinjectEvery
    ttk::label $f.lbReinjectEvery -text [mc "Reinject Every"]:
    set Widgets(spinbox,ReinjectEvery) \
        [ttk::spinbox $f.spinReinjectEvery -from 0 -to 10 \
             -textvariable [myvar ReinjectEvery]]
    wcb::callback $f.spinReinjectEvery before insert \
        {PropertyContainer::checkEntryIntRange 0 10}

    grid $f.lbTerminalSpeed -row 0 -column 0 -sticky w
    grid $f.entTerminalSpeed -row 0 -column 1 -sticky we
    grid $f.lbResolution -row 1 -column 0 -sticky w
    grid $f.spinResolution -row 1 -column 1 -sticky w
    grid $f.lbTraceLength -row 2 -column 0 -sticky w
    grid $f.spinTraceLength -row 2 -column 1 -sticky w
    grid $f.lbReinjectEvery -row 3 -column 0 -sticky w
    grid $f.spinReinjectEvery -row 3 -column 1 -sticky w
    grid columnconfigure $f 2 -weight 1
    grid rowconfigure $f 4 -weight 1

    return $f
  }

  method GetLabel { } {
    return [mc "Pathline Options"]
  }

  method GetTargetType { } {
    return "Pathline"
  }

  method ApplyToTarget { target } {
  }
  
  method GetTerminalSpeed { } {
    return $TerminalSpeed
  }

  method GetResolution { } {
    return $Resolution
  }

  method GetTraceLength { } {
    return $TraceLength
  }

  method GetReinjectEvery { } {
    return $ReinjectEvery
  }

  method cbEnableIfNotEmpty { name args } {
    upvar \#0 $name v
    if {$v eq ""} {
      $self DisableApply
    } else {
      $self EnableApply
    }
  }

  method cbAfterModify { args } {
    if {$TerminalSpeed eq ""} {
      $self DisableApply
      return
    }
    if {$Resolution eq ""} {
      $self DisableApply
      return
    }
    $self EnableApply
  }

  method _OnOpenDataSet { } {
    set T [[$self GetDataSet] GetNumberOfTimeStep]
    incr T -1
    foreach v {TraceLength ReinjectEvery} {
      wcb::callback $Widgets(spinbox,$v) before insert \
          [list PropertyContainer::checkEntryIntRange 0 $T]
      $Widgets(spinbox,$v) configure -from 0 -to $T
    }
  }

  method CopyPropertiesFrom { obj } {
    # empty
  }  
}
