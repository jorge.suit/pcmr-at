package require vtk
package require snit
package require msgcat
package require hook

namespace import msgcat::*

snit::widget StreamlineManager {

  component PlaneWidget      -public PlaneWidget
  component ProbeFilter      -public ProbeFilter
  component ContourMapper    -public ContourMapper
  component ContourActor     -public ContourActor

  component SplineWidget     -public SplineWidget
  component ImplicitLoop     -public ImplicitLoop
  component ExtractCell      -public ExtractCell
  component CleanContour     -public CleanContour

  component MaskPoints       -public MaskPoints

  component RungeKutta4      -public RungeKutta4
  component Streamer         -public Streamer
  component GeometryCleaner  -public GeometryCleaner
  component Tubes            -public Tubes
  component StreamlineMapper -public StreamlineMapper
  component StreamlineActor  -public StreamlineActor

  component LookupTable      -public LookupTable

  variable SeedSourceType "Plane"
  variable WidgetPlaced 0
  variable Widgets
  variable DataSet
  variable VoxelSize
  variable Progress -array {
    value,absolute 0
    value,relative 0
    counter 0
    aborted 0    
  }
  variable ContourResolution 30
  variable NumberOfControlPoints 10
  variable SplineWidgetInteractionCounter 0

  delegate option * to hull
  delegate method * to hull
  
  option -lookuptable -default "" -readonly yes
  
  constructor {args} {
    catch {
    $self configurelist $args
    ttk::label $win.lbProbeRes -text [mc "Probe Resolution"]:
    CScale $win.scProbeRes -from 0.5 -to 3.0 -value 1.0
    set Widgets(scale,proberes) $win.scProbeRes
    bind $Widgets(scale,proberes) <ButtonRelease> [mymethod _CmdChangeProbeResolution] 
    ttk::label $win.lbLoRes -text [mc "Low Resolution"]:
    set Widgets(label,lores) $win.lbLoRes
    CScale $win.scLoRes -command [mymethod _CmdChangeLowResolution] \
        -from 0 -to 0.1 -value 0.01
    set Widgets(scale,lores) $win.scLoRes 
    ttk::label $win.lbHiRes -text [mc "High Resolution"]:
    set Widgets(label,hires) $win.lbHiRes
    CScale $win.scHiRes -from 0.0 -to 1.0 -value 0.8
    set Widgets(scale,hires) $win.scHiRes
    bind $Widgets(scale,hires) <ButtonRelease> [mymethod _CmdChangeHighResolution] 
    set icn [Icons GetInstance]
    set Widgets(menubutton,plane) [ttk::menubutton $win.mbtnPlane \
                                       -text [mc "Plane widget"]: \
                                       -menu $win.mbtnPlane.m]
    $self _defineMenuPlane $win.mbtnPlane.m

    Button $win.btnEnablePlaneWidget -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetPlaneWidgetVisibility planewidget]
    set Widgets(button,planewidget) $win.btnEnablePlaneWidget
    ttk::label $win.lbEnableContourPlane -text [mc "Contour Plane"]:
    Button $win.btnEnableContourPlane -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetContourVisibility contourplane]
    set Widgets(button,contourplane) $win.btnEnableContourPlane
    ttk::label $win.lbEnableStreamline -text [mc "Streamline"]:
    Button $win.btnEnableStreamline -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetStreamlineVisibility streamline]
    set Widgets(button,streamline) $win.btnEnableStreamline
    set fSeedSource [$self _CreateFrameSeedSource]

    grid $win.lbProbeRes -row 0 -column 0 -sticky e
    grid $win.scProbeRes -row 0 -column 1 -sticky w
    grid $win.lbLoRes -row 1 -column 0 -sticky e
    grid $win.scLoRes -row 1 -column 1 -sticky w
    grid $win.lbHiRes -row 2 -column 0 -sticky e
    grid $win.scHiRes -row 2 -column 1 -sticky w
    grid $win.mbtnPlane -row 3 -column 0 -sticky e
    grid $win.btnEnablePlaneWidget -row 3 -column 1 -sticky w
    grid $win.lbEnableContourPlane -row 4 -column 0 -sticky e
    grid $win.btnEnableContourPlane -row 4 -column 1 -sticky w
    grid $win.lbEnableStreamline -row 5 -column 0 -sticky e
    grid $win.btnEnableStreamline -row 5 -column 1 -sticky w
    grid $win.fSeedSource -row 6 -column 0 -columnspan 3 -sticky snew

    grid rowconfigure $win 7 -weight 1
    grid columnconfigure $win 2 -weight 1
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    catch {
      $self _buildVTKObjects
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    puts "Streamline Object is $self"
  }

  destructor {
    if {$LookupTable ne ""} {
      $LookupTable Delete
    }
    $StreamlineActor Delete
    $StreamlineMapper Delete
    $Tubes Delete
    $GeometryCleaner Delete
    $Streamer Delete
    $RungeKutta4 Delete
    
    $MaskPoints Delete

    $CleanContour Delete
    $ExtractCell Delete
    $ImplicitLoop Delete
    $SplineWidget Delete

    $ContourActor Delete
    $ContourMapper Delete
    $ProbeFilter Delete
    $PlaneWidget Delete
  }

  method _defineMenuPlane { m } {
    menu $m -tearoff no
    $m add command \
        -label [mc "Copy from flow quantifier"] \
        -command [mymethod _CmdCopyPlaneFrom "FlowQuantifyManager"]
    $m add command \
        -label [mc "Copy from pathline"] \
        -command [mymethod _CmdCopyPlaneFrom "PathlineManager"]
  }

  method _CreateFrameSeedSource { } {
    set icn [Icons GetInstance]
    set f [ttk::labelframe $win.fSeedSource -text [mc "Seeds Source"]]
    ttk::radiobutton $f.rPlane -text [mc "Plane"] \
        -value "Plane" -variable [myvar SeedSourceType] \
        -command [mymethod _CmdChangeSeedSource]
    ttk::radiobutton $f.rLoop -text [mc "Loop"] \
        -value "Loop" -variable [myvar SeedSourceType] \
        -command [mymethod _CmdChangeSeedSource]
    Button $f.btnEnableSplineWidget -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetSplineWidgetVisibility splinewidget]
    set Widgets(button,splinewidget) $f.btnEnableSplineWidget
    grid $f.rPlane -row 0 -column 0 -sticky w
    grid $f.rLoop -row 1 -column 0 -sticky w
    grid $f.btnEnableSplineWidget  -row 1 -column 1 -sticky w
    grid rowconfigure $f 2 -weight 1
    return $f
  }

  method _CmdChangeSeedSource { } {
    if {$SeedSourceType eq "Plane"} {
      set planeSource [$PlaneWidget GetPolyDataAlgorithm]
      $MaskPoints SetInputConnection [$planeSource GetOutputPort]
    } else {
      $MaskPoints SetInputConnection [$CleanContour GetOutputPort]
    }
    hook call $self <Render>    
  }

  method SetActivePlaneInfo { planeInfo } {
    foreach {key value} $planeInfo {
      $PlaneWidget Set${key} {*}$value
    }
  }

  method SetActiveSplineInfo { splineInfo } {
    set spline [$SplineWidget GetRepresentation]
    $spline SetNumberOfHandles [llength $splineInfo]
    set i 0
    foreach p $splineInfo {
      $spline SetHandlePosition $i {*}$p
      incr i
    }
  }

  method CopyPlaneFrom { objClass } {
    hook call $self <CopyPlane> -from $objClass -to $self
    hook call $self <CopySpline> -from $objClass -to $self
    $self SetPlaneWidgetResolution $VoxelSize
    $self RepositionSplineWidget
    hook call $self <Render>
  }

  method _CmdCopyPlaneFrom { objClass } {
    $self CopyPlaneFrom $objClass
  }

  method DisableWidgets { } {
    $Widgets(scale,lores) state disabled
    $Widgets(scale,hires) state disabled
    $Widgets(menubutton,plane) state disabled
    $Widgets(button,planewidget) configure -state disabled
    $Widgets(button,contourplane) configure -state disabled
    $Widgets(button,streamline) configure -state disabled
  }
  
  method EnableWidgets { } {
    $Widgets(scale,lores) state !disabled
    $Widgets(scale,hires) state !disabled
    $Widgets(menubutton,plane) state !disabled
    $Widgets(button,planewidget) configure -state normal
    $Widgets(button,contourplane) configure -state normal
    $Widgets(button,streamline) configure -state normal
  }

  method SetVisibility {v} {
    set icn [Icons GetInstance]
    set commands [list "$SplineWidget SetEnabled" \
                      "$PlaneWidget SetEnabled" \
                      "$ContourActor SetVisibility" \
                      "$StreamlineActor SetVisibility"]
    foreach c $commands {
      {*}$c $v
    }
    set widgets {planewidget splinewidget contourplane streamline}
    set img [expr {$v ? [$icn Get "light_bulb_on" -size 16]:
                   [$icn Get "light_bulb_off" -size 16]}]
    foreach w $widgets {
      $Widgets(button,$w) configure -image $img
    }
  }

  method _CmdClickVisibility { met w } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set img [$Widgets(button,$w) cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $Widgets(button,$w) configure -image $img
    $self $met $v    
  }

  method GetVisibility { } {
    return [expr {[$PlaneWidget GetEnabled] || 
                  [$ContourActor GetVisibility] ||
                  [$SplineWidget GetEnabled] ||
                  [$StreamlineActor GetVisibility]}]
  }

  method SetSplineWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $SplineWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetPlaneWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $PlaneWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetContourVisibility {v} {
    set previous [$self GetVisibility]
    $ContourActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetStreamlineVisibility {v} {
    set previous [$self GetVisibility]
    $StreamlineActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method _buildVTKObjects { } {
    # PlaneWidget
    install PlaneWidget using vtkPlaneWidget $win.planeWidget 
    $PlaneWidget NormalToXAxisOn
    $PlaneWidget SetRepresentationToOff
    $PlaneWidget SetHandleSize 0.02
    
    $PlaneWidget AddObserver StartInteractionEvent \
        [mymethod OnPlaneWidgetStartInteraction]
    $PlaneWidget AddObserver EndInteractionEvent \
        [mymethod OnPlaneWidgetEndInteraction]
    $PlaneWidget AddObserver InteractionEvent \
        [mymethod OnPlaneWidgetInteraction]

    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    # Filter to probe the velocity magnitude along a plane
    install ProbeFilter using vtkProbeFilter $win.probeFilter
    $ProbeFilter SetInputConnection [$planeSource GetOutputPort]
     
    install MaskPoints using vtkMaskPoints $win.maskPoints
    $MaskPoints SetInputConnection [$planeSource GetOutputPort]
    $MaskPoints RandomModeOn

    $self _CreateSplineWidget

    # Mapper for velocity magnitude contour
    install ContourMapper using vtkPolyDataMapper $win.contourMapper
    $ContourMapper SetInputConnection [$ProbeFilter GetOutputPort]
    $ContourMapper SetScalarModeToUsePointFieldData
    $ContourMapper SelectColorArray "Velocity"
    $ContourMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $ContourMapper SetLookupTable $options(-lookuptable)
    }

    # Actor for velocity magnitude contour
    install ContourActor using vtkActor $win.contourActor
    $ContourActor SetMapper $ContourMapper
    $ContourActor VisibilityOff

    # StreamLine
    install RungeKutta4 using vtkRungeKutta4 $win.rk4
    install Streamer using vtkStreamTracer $win.streamer
    $Streamer SetMaximumPropagation 500
    $Streamer SetIntegrationStepUnit 2
    if {0} {
      $Streamer SetInitialIntegrationStep 1.0
      $Streamer SetMinimumIntegrationStep 0.4
      $Streamer SetMaximumIntegrationStep 3.0
    } else {
      $Streamer SetInitialIntegrationStep 0.5
    }
    $Streamer SetComputeVorticity 0
    $Streamer SetIntegrationDirectionToForward

    $Streamer SetIntegrator $RungeKutta4
    $Streamer AddObserver StartEvent [mymethod _OnStartEvent]
    $Streamer AddObserver ProgressEvent [mymethod _OnProgressEvent]
    $Streamer AddObserver EndEvent [mymethod _OnEndEvent]
    $Streamer SetSourceConnection [$MaskPoints GetOutputPort]

    #REVIEW: if tubes are not used then GeometryCleaner is not needed
    install GeometryCleaner using vtkCleanPolyData $win.cleanFilter
    $GeometryCleaner SetInputConnection [$Streamer GetOutputPort]
    $GeometryCleaner PointMergingOn
    $GeometryCleaner SetTolerance 0.001

    install Tubes using vtkTubeFilter $win.tubes
    $Tubes SetInputConnection [$GeometryCleaner GetOutputPort]

    $Tubes SetRadius 0.2
    $Tubes SetNumberOfSides 4

    install StreamlineMapper using vtkPolyDataMapper $win.streamlineMapper
    #$StreamlineMapper SetInputConnection [$Tubes GetOutputPort]
    $StreamlineMapper SetInputConnection [$Streamer GetOutputPort]
    $StreamlineMapper SetScalarModeToUsePointFieldData
    $StreamlineMapper SelectColorArray "Velocity"
    $StreamlineMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $StreamlineMapper SetLookupTable $options(-lookuptable)
    }

    install StreamlineActor using vtkActor $win.streamlineActor
    $StreamlineActor SetMapper $StreamlineMapper
    $StreamlineActor VisibilityOff
  }

  method GetLabel { } {
    return "Streamline"
  }

  method AddToRenderer { renderer } {
    $renderer AddActor $StreamlineActor
    $renderer AddActor $ContourActor
    $PlaneWidget SetInteractor [$renderer GetInteractor]
    $SplineWidget SetInteractor [$renderer GetInteractor]
  }

  method _CreateSplineWidget { } {
    install SplineWidget using vtkSplineWidget2 $win.splineWidget
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    set spline [$SplineWidget GetRepresentation]
    $spline SetPlaneSource $planeSource
    $spline SetNumberOfHandles $NumberOfControlPoints
    $spline ProjectToPlaneOn
    $spline SetProjectionNormal 3
    $spline ClosedOn
    $SplineWidget SetPriority 1
    $SplineWidget AddObserver StartInteractionEvent \
        [mymethod OnSplineWidgetBeginInteraction]
    $SplineWidget AddObserver InteractionEvent \
        [mymethod OnSplineWidgetInteraction]
    $SplineWidget AddObserver EndInteractionEvent \
        [mymethod OnSplineWidgetEndInteraction]

    # ImplicitLoop
    install ImplicitLoop using vtkImplicitSelectionLoop $win.implicitLoop
    $ImplicitLoop AutomaticNormalGenerationOff

    # ExtractCell
    install ExtractCell using vtkExtractPolyDataGeometry $win.extractCell
    $ExtractCell SetInputConnection [$planeSource GetOutputPort]
    
    # CleanContour: to remove isolated points.
    install CleanContour using vtkCleanPolyData $win.cleanContour
    $CleanContour SetInputConnection [$ExtractCell GetOutputPort]
    $CleanContour PointMergingOff

  }

  method SetDataSet { dataSet } {
    if {$options(-lookuptable) eq ""} {
      install LookupTable using vtkLookupTable $win.lookupTable
      $LookupTable SetHueRange 0.667 0.0
      $LookupTable SetVectorModeToMagnitude
      $ContourMapper SetLookupTable $LookupTable
      $StreamlineMapper SetLookupTable $LookupTable
    }
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    puts "ON OPEN DATA SET ******************"
    if {$LookupTable ne ""} {
      $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
      $LookupTable Build
    }
    set min -1
    foreach s [$DataSet GetVoxelSpacing] {
      if {$min == -1 || $s < $min} {
        set min $s
      }
    }
    set VoxelSize $min
    $self EnableWidgets
  }

  method SetPlaneWidgetResolution { s } {
    set r [$Widgets(scale,proberes) get]
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    VTKUtil::SetPlaneElementSize $planeSource [expr {$s/$r}]
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData ""
    } else {
      $PlaneWidget SetInput ""
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData ""
      $Streamer SetInputData ""
    } else {
      $ProbeFilter SetSource ""
      $Streamer SetInput ""
    }
    set WidgetPlaced 0
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    set imgVector [$DataSet GetVelocityImage -component FLOW -vtk yes]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $PlaneWidget SetInputData $imgVector
    } else {
      $PlaneWidget SetInput $imgVector
    }
    if {!$WidgetPlaced} {
      $PlaneWidget PlaceWidget
      $self SetPlaneWidgetResolution $VoxelSize
      $self ResetSplinePosition
      $self SetMaskToHighResolution
      set WidgetPlaced 1
    }
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $ProbeFilter SetSourceData $imgVector
      $Streamer SetInputData $imgVector 
    } else {
      $ProbeFilter SetSource $imgVector
      $Streamer SetInput $imgVector
    }
    hook call $self <Render>
  }

  method ResetSplinePosition { } {
    set points [VTKUtil::GetRegularPolygonPoints $PlaneWidget $NumberOfControlPoints]
    set spline [$SplineWidget GetRepresentation]
    set i 0
    foreach p $points {
      $spline SetHandlePosition $i {*}$p
      incr i
    }
    $self UpdateImplicitLoop
  }

  method SetInteractiveMask { } {
    $self SetMaskToLowResolution
  }

  method SetNormalMask { } {
    $self SetMaskToHighResolution
  }

  method OnPlaneWidgetStartInteraction { } {
    $self SetInteractiveMask
  }

  method OnPlaneWidgetEndInteraction { } {
    $self SetNormalMask
  }

  method OnPlaneWidgetInteraction { } {
    $self SetPlaneWidgetResolution $VoxelSize
    $self RepositionSplineWidget
  }

  method RepositionSplineWidget { } {
    set spline [$SplineWidget GetRepresentation]
    $spline SetProjectionPosition 0.0
    $self UpdateImplicitLoop
  }

  method GetPlaneResolution {plane s} {
    set origin  [$plane GetOrigin]
    set point1 [$plane GetPoint1]
    set point2 [$plane GetPoint2]
    set s1 0
    set s2 0
    foreach co $origin c1 $point1 c2 $point2 {
      set d1 [expr {$c1-$co}] 
      set d2 [expr {$c2-$co}]
      set s1 [expr {$s1 + $d1*$d1}]
      set s2 [expr {$s2 + $d2*$d2}]
    }
    set n1 [expr {int(ceil(sqrt($s1)/$s))}]
    set n2 [expr {int(ceil(sqrt($s2)/$s))}]
    return [list $n1 $n2]
  }

  method GetHighResolution { } {
    set v [$Widgets(scale,hires) get]
    return $v
  }

  method GetLowResolution { } {
    set v [$Widgets(scale,lores) get]
    return $v
  }

  method GetTotalSeeds { } {
    if {$SeedSourceType eq "Plane"} {
      return [$self GetTotalSeedsInPlane]
    } else {
      return [$self GetTotalSeedsInLoop]
    }
  }
  
  method GetTotalSeedsInPlane { } {
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    $planeSource Update
    return [[$planeSource GetOutput] GetNumberOfPoints]
  }

  method GetTotalSeedsInLoop { } {
    $CleanContour Update
    return [[$CleanContour GetOutput] GetNumberOfPoints]
  }

  method SetMaskToLowResolution { } {
    set s [$self GetLowResolution]
    set n [$self GetTotalSeedsInPlane]
    $MaskPoints SetMaximumNumberOfPoints [expr {int($s*$n)}]
  }

  method SetMaskToHighResolution { } {
    set s [$self GetHighResolution]
    set n [$self GetTotalSeeds]
    $MaskPoints SetMaximumNumberOfPoints [expr {int($s*$n)}]
  }

  method _CmdChangeProbeResolution { } {
    $self SetPlaneWidgetResolution $VoxelSize
    $self _CmdChangeHighResolution
  }

  method _CmdChangeHighResolution { } {
    set v [$Widgets(scale,hires) get]
    set n [$self GetTotalSeedsInPlane]
    $MaskPoints SetMaximumNumberOfPoints [expr {int($v*$n)}]
    if {[$self GetVisibility]} {
      hook call $self <Render>
    }
  }

  method _CmdChangeLowResolution { v } {
  }

  method _ResetProgressControl { } {
    array set Progress {
      value,absolute 0
      value,relative 0
      counter 0
      aborted 0
    }
  }

  method _OnStartEvent { } {
    #puts "$type _OnStartEvent"
    $self _ResetProgressControl
    hook call $self <Status> [mc "Start streamlines ..."]
    update idletask
  }

  method _OnProgressEvent { } {
    set p [$Streamer GetProgress]
    #puts "$type _OnProgressEvent $p"
    if {$p != $Progress(value,absolute)} {
      set $Progress(value,absolute) $p
      hook call $self <Status> [mc "Streamline progress %g" $p]
    } else {
      if {[incr Progress(counter)] >= 5} {
        set Progress(aborted) 1
        $Streamer AbortExecuteOn
        hook call $self <Status> [mc "Streamline aborted"]
      }
    }
    update idletask
  }

  method _OnEndEvent { } {
    #puts "$self _OnEndEvent"
    if {!$Progress(aborted)} {
      hook call $self <Status> [mc "Streamline finished"]
    }
    $self _ResetProgressControl
    update idletask
  }

  method OnSplineWidgetBeginInteraction { } {
    $self SetInteractiveMask
    set SplineWidgetInteractionCounter 0
  }

  method OnSplineWidgetInteraction { } {
    if {[incr InteractionCounter]%10} {
      $self UpdateImplicitLoop
    }
  }

  method OnSplineWidgetEndInteraction { } {
    $self UpdateImplicitLoop
    set InteractionCounter 0
    $self SetNormalMask
  }

  method UpdateImplicitLoop { } {
    set spline [$SplineWidget GetRepresentation]
    set parametricSpline [$spline GetParametricSpline]
    set points [$ImplicitLoop GetLoop]
    if {$points eq ""} {
      set points [vtkPoints New]
      $points SetNumberOfPoints [expr {$ContourResolution-1}]
    }
    set i 0
    set sample [SampleParametricSpline $parametricSpline $ContourResolution]
    set l [llength $sample]
    #puts "Lenght of sample $l"
    incr l -1
    foreach pt $sample {
      #if {$i == 0} {
      #  puts "$i -- $pt"
      #}
      $points SetPoint $i {*}$pt
      if {[incr i] == $l} break
    }
    $points Modified
    $ImplicitLoop SetLoop $points
    $ImplicitLoop SetNormal {*}[$PlaneWidget GetNormal]
    $ImplicitLoop AutomaticNormalGenerationOff
    $ExtractCell ExtractInsideOn
    $ExtractCell ExtractBoundaryCellsOn
    $ExtractCell SetImplicitFunction $ImplicitLoop

    # REVIEW: could I delete points?
  }

  method GetActivePlaneInfo { } {
    set planeInfo {}
    foreach m {Origin Point1 Point2} {
      lappend planeInfo $m [$PlaneWidget Get$m]
    }
    return $planeInfo
  }

  method GetActiveSplineInfo { } {
    set splineInfo {}
    set spline [$SplineWidget GetRepresentation]
    set coordinates [$spline GetHandlePositions]
    for {set i 0} {$i < [$coordinates GetNumberOfTuples]} {incr i} {
      lappend splineInfo [$coordinates GetTuple3 $i]
    }
    return $splineInfo
  }

}

if {0} {
set str .mainPCMR4D.pnw.fNotebook.nb.vispw.frameContent.visStreamlineManager
$str GeometryCleaner GetOutput
vtkPolyDataWriter pwriter
pwriter SetInputConnection [$str GeometryCleaner GetOutputPort]
pwriter SetFileName /tmp/str.vtk
pwriter SetFileTypeToASCII
pwriter Update
}