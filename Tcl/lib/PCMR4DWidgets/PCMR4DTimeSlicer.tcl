package require Ttk
package require snit
#package require ScrolledFrame
package require BWidget
package require ImageThumbnailManager
package require Icons

snit::widget PCMR4DTimeSlicer {

  variable Thumbnailer ""
  variable defaultBG
  variable CurrentTime -1
  variable Player -array {
    playing 0
    repeat 0
  }
  variable Widgets

  delegate option * to hull
  delegate method * to hull

  option -dataset -default "" -configuremethod _ConfDataSet
  option -command -default ""
  delegate option -size to Thumbnailer

  method cmdPlay { } {
    set btn $win.fanim.play
    set icn [Icons GetInstance]
    if {$Player(playing)} {
      $btn configure -image [$icn Get "player_play" -size 16]
      set Player(playing) 0
    } else {
      $btn configure -image [$icn Get "player_stop" -size 16]
      set Player(playing) 1
    }
    after idle [mymethod _AnimateSequence]
  }

  method cmdRepeat { } {
    set btn $win.fanim.repeat
    if {$Player(repeat)} {
      $btn configure -relief groove
      set Player(repeat) 0
    } else {
      $btn configure -relief sunken
      set Player(repeat) 1
    }
  }

  method _AnimateSequence { } {
    while {$Player(playing)} {
      set ds $options(-dataset)
      if {$ds eq ""} {
        # toogle player state
        $self cmdPlay
      } else {
        set maxT [expr {[$ds GetNumberOfTimeStep]-1}]
        set t [$ds GetCurrentTimeStep]
        if {!$Player(repeat) && $t == $maxT} {
          # toogle player state
          $self cmdPlay
        }
        set t [$ds GetNextTimeStep]
        $self ActivateTimeStep $t
        update
      }
    }
  }

  constructor { args } {
    set Thumbnailer [ImageThumbnailManager %AUTO%]
    set fanim [frame $win.fanim]
    set icn [Icons GetInstance]
    set Widgets(button,play) [Button $fanim.play -image [$icn Get "player_play" -size 16] -relief link \
                                  -command [mymethod cmdPlay] -state disabled]
    set Widgets(button,repeat) [Button $fanim.repeat -image [$icn Get "arrow_repeat" -size 16] -relief groove \
                                  -command [mymethod cmdRepeat] -state disabled]

    # -command "$win.sf xview"
    set Widgets(scrollbar) [ttk::scrollbar $fanim.hs -orient horizontal]
    TKUtil::ScrollBarState $Widgets(scrollbar) disabled
    grid $fanim.play -row 0 -column 0 -sticky snew
    grid $fanim.repeat -row 0 -column 1 -sticky snew
    grid $fanim.hs -row 0 -column 2 -sticky snew
    grid columnconfigure $fanim 2 -weight 1
    #ScrolledFrame::ScrolledFrame $win.sf -xscrollcommand "$win.hs set" -fill y
    # -xscrollcommand "$win.fanim.hs set"
    set Widgets(scrolledframe) [ScrollableFrame $win.sf  -constrainedheight 1]
    $win.sf configure -height 0
    $self configurelist $args
    grid $win.sf -row 0 -column 0 -sticky ew
    grid $fanim -row 1 -column 0 -sticky new
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 2 -weight 1
    #grid configure [$win.sf getframe]
    #[$win.sf getframe] configure -height [$Thumbnailer cget -size]
  }

  destructor {
    if {$Thumbnailer ne ""} {
      $Thumbnailer destroy
    }
  }

  method ActivateTimeStep { t {force 0}} {
    if {$options(-dataset) ne ""} {
      $self _OnButton1 $t $force
    }
  }

  method GetCurrentTime {} {
    return $CurrentTime
  }
  method _NormalTimeStep { t } {
    set f [$win.sf getframe]
    $f.thumb$t configure -bd 1 -relief flat -bg $defaultBG
    return $f.thumb$t
  }

  method _HighlightTimeStep { t } {
    set f [$win.sf getframe]
    $f.thumb$t configure -bd 2 -relief sunken -bg white
    return $f.thumb$t
  }

  method _ConfDataSet { o ds } {
    if {$ds ne "" && ([catch {$ds info type} dsType] || $dsType ne "::PCMR4DDataSet")} {
      error "could not set -dataset, '$ds' must be a valid PCMR4DDataSet"
    }
    $self _ClearThumbnails
    set options(-dataset) $ds
    if {$ds ne ""} {
      set thumbDir [$self _CheckThumbnailDirectory]
      $Thumbnailer configure -directory $thumbDir
      $self _FillThumbnailFromDataSet
      #grid configure [$win.sf getframe]
      $self ActivateTimeStep 0 1
      $self _EnableWidgets
    } else {
      $Thumbnailer configure -directory ""
      $win.sf configure -height 0
      set CurrentTime -1
      $self _DisableWidgets
    }
  }

  method _EnableWidgets { } {
    $Widgets(button,play) configure -state normal
    $Widgets(button,repeat) configure -state normal
    TKUtil::ScrollBarState $Widgets(scrollbar) !disabled
    $Widgets(scrollbar) configure -command "$Widgets(scrolledframe) xview"
    $Widgets(scrolledframe) configure -xscrollcommand "$Widgets(scrollbar) set"
  }

  method _DisableWidgets { } {
    $Widgets(button,play) configure -state disabled
    $Widgets(button,repeat) configure -state disabled
    TKUtil::ScrollBarState $Widgets(scrollbar) disabled
    $Widgets(scrollbar) configure -command ""
    $Widgets(scrolledframe) configure -xscrollcommand ""
  }

  method _ClearThumbnails { } {
    set ds $options(-dataset)
    if {$ds ne ""} {
      set f [$win.sf getframe]
      foreach w [winfo children $f] {
        destroy $w
      }
    }
  }

  method _FillThumbnailFromDataSet { } {
    set ds $options(-dataset)
    set f [$win.sf getframe]
    set refresh 0
    for {set t 0} {$t < [$ds GetNumberOfTimeStep]} {incr t} {
      set thumbName "Thumbnail_$t"
      set thumbImg [$Thumbnailer GetThumbnail $thumbName]
      if {$thumbImg eq ""} {
        $ds SetEventEnabled ChangeTimeStep 0
        $ds ChangeTimeStep $t
        $ds SetEventEnabled ChangeTimeStep 1
        set velocity [$ds GetVelocityImage -vtk yes]
        set thumbImg [$Thumbnailer MakeThumbnail $velocity $thumbName]
        set refresh 1
      }
      set tvalue [expr {([$ds GetLengthOfTimeStep]*$t)/1000}]
      label $f.thumb$t -image $thumbImg -highlightthickness 1 -compound top \
          -text "t=$t ([format %.3f $tvalue])"
      bind $f.thumb$t <1> [string map "%t $t" [mymethod _OnButton1 %t]]
      grid $f.thumb$t -row 0 -column $t -sticky snew  -ipadx 1 -ipady 1
      if {$refresh} {
        set refresh 0
        update
        $self _ResizeScrollableFrame
      }
    }
    set defaultBG [$f.thumb0 cget -background]
    after idle [mymethod _ResizeScrollableFrame]
  }

  method _ResizeScrollableFrame { } {
    $win.sf configure -height [winfo reqheight [$win.sf getframe]]
  }

  method _OnButton1 { t {force 0} } {
    set ds $options(-dataset)
    set t0 [$ds GetCurrentTimeStep]
    if {!$force && $t0 == $t} {
      return
    }
    $self _NormalTimeStep $t0
    set lb [$self _HighlightTimeStep $t]
    $ds ChangeTimeStep $t
    set CurrentTime $t
    if {$options(-command) ne ""} {
      set tv [expr {([$ds GetLengthOfTimeStep]*$t)/1000}]
      set script [string map "%i $t %t $tv" $options(-command)]
      uplevel #0 $script
    }
  }

  method _CheckThumbnailDirectory { } {
    set ds $options(-dataset)
    set baseDir [$ds GetDirectory]
    set thumbDir [file join $baseDir "Thumbnail"]
    if {![file exists $thumbDir]} {
      file mkdir $thumbDir
    }
    if {![file isdirectory $thumbDir]} {
      error "Path exists but it is not a directory: '$thumbDir'"
    }
    return $thumbDir
  }
}
