package require vtk
package require snit
package require msgcat
package require hook

namespace import msgcat::*

snit::widget PathlineManager {

  component PlaneWidget      -public PlaneWidget
  component ProbeFilter      -public ProbeFilter
  component ContourMapper    -public ContourMapper
  component ContourActor     -public ContourActor

  component SplineWidget     -public SplineWidget
  component ImplicitLoop     -public ImplicitLoop
  component ExtractCell      -public ExtractCell
  component CleanContour     -public CleanContour

  component TemporalSource   -public TemporalSource
  component Pathline         -public Pathline
  component ScaleToCM        -public ScaleToCM
  component ClipAgeUpper     -public ClipAgeUpper
  component ClipAgeLower     -public ClipAgeLower
  component ToPolyData       -public ToPolyData
  component Tubes            -public Tubes
  component PathlineMapper   -public PathlineMapper
  component PathlineActor    -public PathlineActor

  component LookupTable      -public LookupTable

  variable TimeValues
  variable SeedSourceType "Plane"
  variable WidgetPlaced 0
  variable Widgets
  variable DataSet
  variable VoxelSize

  variable ContourResolution 30
  variable NumberOfControlPoints 10

  variable PreviousPathlineVisibility 0
  variable LastPathlineMTime 0

  delegate option * to hull
  delegate method * to hull
  
  option -lookuptable -default "" -readonly yes
  
  constructor {args} {
    catch {
    $self configurelist $args
     set icn [Icons GetInstance]
    set Widgets(menubutton,plane) [ttk::menubutton $win.mbtnPlane \
                                       -text [mc "Plane widget"]: \
                                       -menu $win.mbtnPlane.m]
    $self _defineMenuPlane $win.mbtnPlane.m

    Button $win.btnEnablePlaneWidget -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetPlaneWidgetVisibility planewidget]
    set Widgets(button,planewidget) $win.btnEnablePlaneWidget
    ttk::label $win.lbEnableContourPlane -text [mc "Contour Plane"]:
    Button $win.btnEnableContourPlane -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetContourVisibility contourplane]
    set Widgets(button,contourplane) $win.btnEnableContourPlane
    ttk::label $win.lbEnablePathline -text [mc "Pathline"]:
    Button $win.btnEnablePathline -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetPathlineVisibility pathline]
    set Widgets(button,pathline) $win.btnEnablePathline
    set fSeedSource [$self _CreateFrameSeedSource]

    grid $win.mbtnPlane -row 0 -column 0 -sticky e
    grid $win.btnEnablePlaneWidget -row 0 -column 1 -sticky w
    grid $win.lbEnableContourPlane -row 1 -column 0 -sticky e
    grid $win.btnEnableContourPlane -row 1 -column 1 -sticky w
    grid $win.lbEnablePathline -row 2 -column 0 -sticky e
    grid $win.btnEnablePathline -row 2 -column 1 -sticky w
    grid $win.fSeedSource -row 3 -column 0 -columnspan 2 -sticky snew

    grid rowconfigure $win 4 -weight 1
    grid columnconfigure $win 2 -weight 1
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    catch {
      $self _buildVTKObjects
    } msg
    puts "AAAAAAAAAAAAA: $msg"
    puts "Pathline Object is $self"
  }

  destructor {
    if {$LookupTable ne ""} {
      $LookupTable Delete
    }
    $PathlineActor Delete
    $PathlineMapper Delete
    $Tubes Delete
    $ToPolyData Delete
    $ClipAgeUpper Delete
    $ClipAgeLower Delete
    $ScaleToCM Delete
    $Pathline Delete
    $TemporalSource Delete

    $CleanContour Delete
    $ExtractCell Delete
    $ImplicitLoop Delete
    $SplineWidget Delete

    $ContourActor Delete
    $ContourMapper Delete
    $ProbeFilter Delete
    $PlaneWidget Delete
  }

  method _defineMenuPlane { m } {
    menu $m -tearoff no
    $m add command \
        -label [mc "Copy from flow quantifier"] \
        -command [mymethod _CmdCopyPlaneFrom "FlowQuantifyManager"]
    $m add command \
        -label [mc "Copy from streamline"] \
        -command [mymethod _CmdCopyPlaneFrom "StreamlineManager"]
  }

  method _CreateFrameSeedSource { } {
    set icn [Icons GetInstance]
    set f [ttk::labelframe $win.fSeedSource -text [mc "Seeds Source"]]
    ttk::radiobutton $f.rPlane -text [mc "Plane"] \
        -value "Plane" -variable [myvar SeedSourceType] \
        -command [mymethod _CmdChangeSeedSource]
    ttk::radiobutton $f.rLoop -text [mc "Loop"] \
        -value "Loop" -variable [myvar SeedSourceType] \
        -command [mymethod _CmdChangeSeedSource]
    Button $f.btnEnableSplineWidget -relief link \
        -image [$icn Get "light_bulb_off" -size 16] \
        -command [mymethod _CmdClickVisibility \
                      SetSplineWidgetVisibility splinewidget]
    set Widgets(button,splinewidget) $f.btnEnableSplineWidget
    grid $f.rPlane -row 0 -column 0 -sticky w
    grid $f.rLoop -row 1 -column 0 -sticky w
    grid $f.btnEnableSplineWidget  -row 1 -column 1 -sticky w
    grid rowconfigure $f 2 -weight 1
    return $f
  }

  method _CmdChangeSeedSource { } {
    if {$SeedSourceType eq "Plane"} {
       set planeSource [$PlaneWidget GetPolyDataAlgorithm]
      $Pathline SetInputConnection 1 [$planeSource GetOutputPort]
    } else {
      $Pathline SetInputConnection 1 [$CleanContour GetOutputPort]
    }
    [$PlaneWidget GetPolyDataAlgorithm] Modified
    $self UpdatePathline [$PathlineActor GetVisibility]
    hook call $self <Render>    
  }

  method SetActivePlaneInfo { planeInfo } {
    foreach {key value} $planeInfo {
      $PlaneWidget Set${key} {*}$value
    }
  }

  method SetActiveSplineInfo { splineInfo } {
    set spline [$SplineWidget GetRepresentation]
    $spline SetNumberOfHandles [llength $splineInfo]
    set i 0
    foreach p $splineInfo {
      $spline SetHandlePosition $i {*}$p
      incr i
    }
  }

  method CopyPlaneFrom { objClass } {
    hook call $self <CopyPlane> -from $objClass -to $self
    hook call $self <CopySpline> -from $objClass -to $self
    $self SetPlaneWidgetResolution $VoxelSize
    $self RepositionSplineWidget
    hook call $self <Render>
  }

  method _CmdCopyPlaneFrom { objClass } {
    $self CopyPlaneFrom $objClass
  }

  method DisableWidgets { } {
    $Widgets(menubutton,plane) state disabled
    $Widgets(button,planewidget) configure -state disabled
    $Widgets(button,contourplane) configure -state disabled
    $Widgets(button,pathline) configure -state disabled
  }
  
  method EnableWidgets { } {
    $Widgets(menubutton,plane) state !disabled
    $Widgets(button,planewidget) configure -state normal
    $Widgets(button,contourplane) configure -state normal
    $Widgets(button,pathline) configure -state normal
  }

  method SetVisibility {v} {
    set icn [Icons GetInstance]
    set commands [list "$SplineWidget SetEnabled" \
                      "$PlaneWidget SetEnabled" \
                      "$ContourActor SetVisibility" \
                      "$self UpdatePathline" \
                       "$PathlineActor SetVisibility"]
    foreach c $commands {
      {*}$c $v
    }
    set widgets {planewidget splinewidget contourplane pathline}
    set img [expr {$v ? [$icn Get "light_bulb_on" -size 16]:
                   [$icn Get "light_bulb_off" -size 16]}]
    foreach w $widgets {
      $Widgets(button,$w) configure -image $img
    }
  }

  method _CmdClickVisibility { met w } {
    set icn [Icons GetInstance]
    set imgON [$icn Get "light_bulb_on" -size 16]
    set imgOFF [$icn Get "light_bulb_off" -size 16]
    set img [$Widgets(button,$w) cget -image]
    if {$img eq $imgON} {
      set v 0
      set img $imgOFF
    } else {
      set v 1
      set img $imgON
    }
    $Widgets(button,$w) configure -image $img
    $self $met $v    
  }

  method GetVisibility { } {
    return [expr {[$PlaneWidget GetEnabled] || 
                  [$ContourActor GetVisibility] ||
                  [$SplineWidget GetEnabled] ||
                  [$PathlineActor GetVisibility]}]
  }

  method SetSplineWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $SplineWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetPlaneWidgetVisibility {v} {
    set previous [$self GetVisibility]
    $PlaneWidget SetEnabled $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetContourVisibility {v} {
    set previous [$self GetVisibility]
    $ContourActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method SetPathlineVisibility {v} {
    set previous [$self GetVisibility]
    $self UpdatePathline $v
    $PathlineActor SetVisibility $v
    hook call $self <Render>
    set current [$self GetVisibility]
    if {$current != $previous} {
      hook call $self <Visibility>
    }
  }

  method _buildVTKObjects { } {
    # PlaneWidget
    install PlaneWidget using vtkPlaneWidget $win.planeWidget 
    $PlaneWidget NormalToXAxisOn
    $PlaneWidget SetRepresentationToOff
    $PlaneWidget SetHandleSize 0.02
    
    $PlaneWidget AddObserver StartInteractionEvent \
        [mymethod OnPlaneWidgetStartInteraction]
    $PlaneWidget AddObserver EndInteractionEvent \
        [mymethod OnPlaneWidgetEndInteraction]
    $PlaneWidget AddObserver InteractionEvent \
        [mymethod OnPlaneWidgetInteraction]

    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    # Filter to probe the velocity magnitude along a plane
    install ProbeFilter using vtkProbeFilter $win.probeFilter
    $ProbeFilter SetInputConnection [$planeSource GetOutputPort]
     
    $self _CreateSplineWidget

    # Mapper for velocity magnitude contour
    install ContourMapper using vtkPolyDataMapper $win.contourMapper
    $ContourMapper SetInputConnection [$ProbeFilter GetOutputPort]
    $ContourMapper SetScalarModeToUsePointFieldData
    $ContourMapper SelectColorArray "Velocity"
    $ContourMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $ContourMapper SetLookupTable $options(-lookuptable)
    }

    # Actor for velocity magnitude contour
    install ContourActor using vtkActor $win.contourActor
    $ContourActor SetMapper $ContourMapper
    $ContourActor VisibilityOff

    # Pathline
    set TemporalSource [TemporalVelocitySource]
    $TemporalSource SetRescaleFactor 10

    install Pathline using vtkParticlePathFilter $win.pathline
    $Pathline SetInputConnection 0 [$TemporalSource GetOutputPort]
    $Pathline SetInputConnection 1 [$planeSource GetOutputPort]
    $Pathline SetComputeVorticity 0
    $Pathline SetTerminalSpeed 1
    $Pathline SetIntegratorType 1
    $Pathline AddObserver StartEvent [mymethod OnPathlineOnStartEvent]
    $Pathline AddObserver ProgressEvent [mymethod OnPathlineProgressEvent]
    $Pathline AddObserver EndEvent [mymethod OnPathlineEndEvent]
    set LastPathlineMTime [$Pathline GetMTime]

    install ScaleToCM using vtkArrayCalculator $win.scaleToCM
    $ScaleToCM SetInputConnection [$Pathline GetOutputPort]
    $ScaleToCM AddVectorVariable "V" "Velocity" 0 1 2
    $ScaleToCM SetFunction "0.1*V"
    $ScaleToCM SetResultArrayName "Velocity"

    # REVIEW: here we can not use vtkClipPolyData because it only
    # operate on the active scalars
    install ClipAgeUpper using vtkClipDataSet $win.clipAgeUpper
    $ClipAgeUpper SetInputArrayToProcess 0 0 0 0 "ParticleAge"
    $ClipAgeUpper SetInputConnection [$ScaleToCM GetOutputPort]
    $ClipAgeUpper SetValue 1.0
    $ClipAgeUpper InsideOutOn

    install ClipAgeLower using vtkClipDataSet $win.clipAgeLower
    $ClipAgeLower SetInputArrayToProcess 0 0 0 0 "ParticleAge"
    $ClipAgeLower SetInputConnection [$ClipAgeUpper GetOutputPort]
    $ClipAgeLower SetValue 0.0
    $ClipAgeLower InsideOutOff
    #$ClipAge AddObserver ProgressEvent [mymethod OnClipAgeProgressEvent]

    install ToPolyData using vtkGeometryFilter $win.toPolyData
    $ToPolyData SetInputConnection [$ClipAgeLower GetOutputPort]
    $ToPolyData MergingOn
    $ToPolyData Update

    install Tubes using vtkTubeFilter $win.tubes
    $Tubes SetInputConnection [$ToPolyData GetOutputPort]
    $Tubes SetRadius 0.05
    $Tubes SetNumberOfSides 4

    install PathlineMapper using vtkPolyDataMapper $win.pathlineMapper
    #$PathlineMapper SetInputConnection [$Tubes GetOutputPort]
    $PathlineMapper SetInputConnection [$ToPolyData GetOutputPort]

    $PathlineMapper SetScalarModeToUsePointFieldData
    $PathlineMapper SelectColorArray "Velocity"
    $PathlineMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $PathlineMapper SetLookupTable $options(-lookuptable)
    }

    install PathlineActor using vtkActor $win.pathlineActor
    $PathlineActor SetMapper $PathlineMapper
    $PathlineActor VisibilityOff
  }

  method OnClipAgeProgressEvent { args } {
    puts "OnClipAgeProgressEvent $args"
  }

  method GetLabel { } {
    return "Pathline"
  }

  method AddToRenderer { renderer } {
    $renderer AddActor $PathlineActor
    $renderer AddActor $ContourActor
    $PlaneWidget SetInteractor [$renderer GetInteractor]
    $SplineWidget SetInteractor [$renderer GetInteractor]
  }

  method _CreateSplineWidget { } {
    install SplineWidget using vtkSplineWidget2 $win.splineWidget
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    set spline [$SplineWidget GetRepresentation]
    $spline SetPlaneSource $planeSource
    $spline SetNumberOfHandles $NumberOfControlPoints
    $spline ProjectToPlaneOn
    $spline SetProjectionNormal 3
    $spline ClosedOn
    $SplineWidget SetPriority 1
    $SplineWidget AddObserver StartInteractionEvent \
        [mymethod OnSplineWidgetBeginInteraction]
    $SplineWidget AddObserver InteractionEvent \
        [mymethod OnSplineWidgetInteraction]
    $SplineWidget AddObserver EndInteractionEvent \
        [mymethod OnSplineWidgetEndInteraction]

    # ImplicitLoop
    install ImplicitLoop using vtkImplicitSelectionLoop $win.implicitLoop
    $ImplicitLoop AutomaticNormalGenerationOff

    # ExtractCell
    install ExtractCell using vtkExtractPolyDataGeometry $win.extractCell
    $ExtractCell SetInputConnection [$planeSource GetOutputPort]
    
    # CleanContour: to remove isolated points.
    install CleanContour using vtkCleanPolyData $win.cleanContour
    $CleanContour SetInputConnection [$ExtractCell GetOutputPort]
    $CleanContour PointMergingOff

  }

  method SetDataSet { dataSet } {
    if {$options(-lookuptable) eq ""} {
      install LookupTable using vtkLookupTable $win.lookupTable
      $LookupTable SetHueRange 0.667 0.0
      $LookupTable SetVectorModeToMagnitude
      $ContourMapper SetLookupTable $LookupTable
      $PathlineMapper SetLookupTable $LookupTable
    }
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    puts "ON OPEN DATA SET ******************"
    if {$LookupTable ne ""} {
      $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
      $LookupTable Build
    }
    set min -1
    foreach s [$DataSet GetVoxelSpacing] {
      if {$min == -1 || $s < $min} {
        set min $s
      }
    }
    set VoxelSize $min
    $TemporalSource SetTclDataSet $DataSet
    $Pathline SetInputConnection 0 [$TemporalSource GetOutputPort]
    set TimeValues [$TemporalSource GetTimeValues]
    set tmax [lindex $TimeValues end]
    $Pathline SetTerminationTime $tmax
    $ClipAgeUpper SetValue $tmax
    $self EnableWidgets
  }

  method PathlineModified { } {
    return [expr {$Pathline eq "" || 
                  [$Pathline GetMTime] > $LastPathlineMTime}]
  }

  method UpdatePathline { v } {
    puts "$v && [$self PathlineModified]"
    if {$v && [$self PathlineModified]} {
      $self RecomputePathline 5
      set LastPathlineMTime [$Pathline GetMTime]
    }
  }

  method RecomputePathline { r } {
    set t0 [lindex $TimeValues 0]
    $Pathline SetStartTime $t0
    foreach t1 [lrange $TimeValues 1 end] {
      set dt [expr {double($t1 - $t0)/double($r)}]
      set n [expr {$r - 1}]
      for {set i 1} {$i <= $n} {incr i} {
        $Pathline SetTerminationTime [expr {$t0 + $i*$dt}]
        $Pathline Update
      }
      $Pathline SetTerminationTime $t1
      $Pathline Update
      set t0 $t1
    }
  }

  method SetPlaneWidgetResolution { s } {
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    VTKUtil::SetPlaneElementSize $planeSource $s
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    $PlaneWidget SetInputData ""
    $ProbeFilter SetSourceData ""
    $Pathline SetInputData ""
    set WidgetPlaced 0
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    set imgVector [$DataSet GetVelocityImage -component FLOW -vtk yes]
    $PlaneWidget SetInputData $imgVector
    if {!$WidgetPlaced} {
      $PlaneWidget PlaceWidget
      $self SetPlaneWidgetResolution $VoxelSize
      $self ResetSplinePosition
      set WidgetPlaced 1
    }
    $ProbeFilter SetSourceData $imgVector
    #$Pathline SetInputData $imgVector 
    $ClipAgeUpper SetValue [lindex $TimeValues $t]
    set t0 [expr {$t-2}]
    if {$t0 < 0} {
      set t0 0
    }
    $ClipAgeLower SetValue [lindex $TimeValues $t0]
    hook call $self <Render>
  }

  method ResetSplinePosition { } {
    set points [VTKUtil::GetRegularPolygonPoints $PlaneWidget $NumberOfControlPoints]
    set spline [$SplineWidget GetRepresentation]
    set i 0
    foreach p $points {
      $spline SetHandlePosition $i {*}$p
      incr i
    }
    $self UpdateImplicitLoop
  }

  method TemporalDisablePathline { } {
    set PreviousPathlineVisibility [$PathlineActor GetVisibility]
    $PathlineActor SetVisibility 0
  }

  method TemporalRestorePathline { } {
    puts "$self UpdatePathline $PreviousPathlineVisibility"
    $self UpdatePathline $PreviousPathlineVisibility
    $PathlineActor SetVisibility $PreviousPathlineVisibility
  }

  method OnPlaneWidgetStartInteraction { } {
    $self TemporalDisablePathline
  }

  method OnPlaneWidgetEndInteraction { } {
    $Pathline Modified
    $self TemporalRestorePathline
  }

  method OnPlaneWidgetInteraction { } {
    $self SetPlaneWidgetResolution $VoxelSize
    $self RepositionSplineWidget
  }

  method RepositionSplineWidget { } {
    set spline [$SplineWidget GetRepresentation]
    $spline SetProjectionPosition 0.0
    $self UpdateImplicitLoop
  }

  method GetPlaneResolution {plane s} {
    set origin  [$plane GetOrigin]
    set point1 [$plane GetPoint1]
    set point2 [$plane GetPoint2]
    set s1 0
    set s2 0
    foreach co $origin c1 $point1 c2 $point2 {
      set d1 [expr {$c1-$co}] 
      set d2 [expr {$c2-$co}]
      set s1 [expr {$s1 + $d1*$d1}]
      set s2 [expr {$s2 + $d2*$d2}]
    }
    set n1 [expr {int(ceil(sqrt($s1)/$s))}]
    set n2 [expr {int(ceil(sqrt($s2)/$s))}]
    return [list $n1 $n2]
  }

  method GetHighResolution { } {
    set v [$Widgets(scale,hires) get]
    return $v
  }

  method GetLowResolution { } {
    set v [$Widgets(scale,lores) get]
    return $v
  }

  method GetTotalSeeds { } {
    if {$SeedSourceType eq "Plane"} {
      return [$self GetTotalSeedsInPlane]
    } else {
      return [$self GetTotalSeedsInLoop]
    }
  }
  
  method GetTotalSeedsInPlane { } {
    set planeSource [$PlaneWidget GetPolyDataAlgorithm]
    $planeSource Update
    return [[$planeSource GetOutput] GetNumberOfPoints]
  }

  method GetTotalSeedsInLoop { } {
    $CleanContour Update
    return [[$CleanContour GetOutput] GetNumberOfPoints]
  }

  method OnPathlineOnStartEvent { } {
    hook call $self <Status> [mc "Start pathlines ..."]
    update idletask
  }

  method OnPathlineProgressEvent { } {
    set p [$Pathline GetProgress]
    puts "OnPathlineProgressEvent $p"
    hook call $self <Status> [mc "Pathline progress %g" $p]
    update idletask
  }

  method OnPathlineEndEvent { } {
    hook call $self <Status> [mc "Pathline finished"]
    update idletask
  }

  method OnSplineWidgetBeginInteraction { } {
    $self TemporalDisablePathline
  }

  method OnSplineWidgetInteraction { } {
  }

  method OnSplineWidgetEndInteraction { } {
    $self UpdateImplicitLoop
    $Pathline Modified
    $self TemporalRestorePathline
  }

  method UpdateImplicitLoop { } {
    set spline [$SplineWidget GetRepresentation]
    set parametricSpline [$spline GetParametricSpline]
    set points [$ImplicitLoop GetLoop]
    if {$points eq ""} {
      set points [vtkPoints New]
      $points SetNumberOfPoints [expr {$ContourResolution-1}]
    }
    set i 0
    set sample [SampleParametricSpline $parametricSpline $ContourResolution]
    set l [llength $sample]
    #puts "Lenght of sample $l"
    incr l -1
    foreach pt $sample {
      #if {$i == 0} {
      #  puts "$i -- $pt"
      #}
      $points SetPoint $i {*}$pt
      if {[incr i] == $l} break
    }
    $points Modified
    $ImplicitLoop SetLoop $points
    $ImplicitLoop SetNormal {*}[$PlaneWidget GetNormal]
    $ImplicitLoop AutomaticNormalGenerationOff
    $ExtractCell ExtractInsideOn
    $ExtractCell ExtractBoundaryCellsOn
    $ExtractCell SetImplicitFunction $ImplicitLoop

    # REVIEW: could I delete points?
  }

  method GetActivePlaneInfo { } {
    set planeInfo {}
    foreach m {Origin Point1 Point2} {
      lappend planeInfo $m [$PlaneWidget Get$m]
    }
    return $planeInfo
  }

  method GetActiveSplineInfo { } {
    set splineInfo {}
    set spline [$SplineWidget GetRepresentation]
    set coordinates [$spline GetHandlePositions]
    for {set i 0} {$i < [$coordinates GetNumberOfTuples]} {incr i} {
      lappend splineInfo [$coordinates GetTuple3 $i]
    }
    return $splineInfo
  }

}
