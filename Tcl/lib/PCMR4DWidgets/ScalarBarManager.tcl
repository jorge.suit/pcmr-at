package require vtk
package require snit
package require msgcat
package require hook

namespace import msgcat::*

# SetHueRange 0.50 0.87, SetSaturationRange 0.4 0.9 de azul claro a magenta pasndo por azul
# SetHueRange 0.3 0 , SetSaturationRange 1 1, de verde a rojo pasando por amarillo
snit::widget ScalarBarManager {
  component LookupTable -public LookupTable
  component ScalarBar   -public ScalarBar
  component BarWidget  -public BarWidget
  
  variable DataSet

  delegate method SetVisibility to BarWidget as SetEnabled
  delegate method GetVisibility to BarWidget as GetEnabled
  delegate method SetInteractor to BarWidget

  delegate option * to hull
  constructor {args} {
    $self configurelist $args
    $self _buildVTKObjects
  }

  destructor {
    $BarWidget Delete
    $ScalarBar Delete
    $LookupTable Delete
  }
  
  method _buildVTKObjects {} {
    set ::__sb $win
    install LookupTable using vtkLookupTable $win.lut
    $LookupTable SetHueRange 0.667 0.0
    $LookupTable SetVectorModeToMagnitude
    install ScalarBar using vtkScalarBarActor $win.scalarBar
    $ScalarBar SetOrientationToHorizontal
    $ScalarBar SetLookupTable $LookupTable
    install BarWidget using vtkScalarBarWidget $win.barWidget
    $BarWidget SetScalarBarActor $ScalarBar
    $ScalarBar SetTitle " "
  }

  method GetLookupTable { } {
    return $LookupTable
  }

  method GetLabel { } {
    return "Scalar Bar"
  }

  method AddToRenderer { renderer } {
    $BarWidget SetInteractor [$renderer GetInteractor]
  }


  method SetDataSet { dataSet } {
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
    $LookupTable Build
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
  }
}

# Red to Yellow
if {0} {
  Scale: Linear
  HueRange: (0, 0.1)
  SaturationRange: (0.95, 0.4)
  ValueRange: (0.3, 1)
  AlphaRange: (1, 1)
}

# Blue to Cyan
if {0} {
  Scale: Linear
  HueRange: (0.667, 0.5)
  SaturationRange: (0.95, 0.4)
  ValueRange: (0.3, 1)
  AlphaRange: (1, 1)
}

# Black to White
if {0} {
  Scale: Linear
  HueRange: (0, 1)
  SaturationRange: (0, 0)
  ValueRange: (0, 1)
}
