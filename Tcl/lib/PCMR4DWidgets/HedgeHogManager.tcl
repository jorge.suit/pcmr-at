package require vtk
package require snit
package require msgcat
package require hook
package require CScale
package require VTKUtil

namespace import msgcat::*

snit::widget HedgeHogManager {

  component MaskPoints   -public MaskPoints
  component HedgeHog     -public HedgeHog
  component VectorNorm   -public VectorNorm
  component VectorMapper -public VectorMapper
  component VectorActor  -public VectorActor
  component LookupTable  -public LookupTable

  variable DataSet
  variable Widgets

  delegate option * to hull
  delegate method * to hull
  
  delegate method SetVisibility to VectorActor
  delegate method GetVisibility to VectorActor

  option -lookuptable -default "" -readonly yes

  constructor {args} {
    $self configurelist $args
    ttk::label $win.lbScale -text [mc "Arrow scale"]:
    CScale $win.scFactor -command  [mymethod _OnSetFactor] -value 0.05
    set Widgets(scale,Factor) $win.scFactor
    ttk::label $win.lbDensity -text [mc "Arrow density"]:
    ttk::scale $win.scDensity \
        -command  [mymethod _OnSetDensity] -from 100 -to 1 -value 5
    set Widgets(scale,Density) $win.scDensity
    grid $win.lbScale -row 0 -column 0 -sticky e
    grid $win.scFactor -row 0 -column 1 -sticky w
    grid $win.lbDensity -row 1 -column 0 -sticky e
    grid $win.scDensity -row 1 -column 1 -sticky w
    grid rowconfigure $win 2 -weight 1
    grid columnconfigure $win 2 -weight 1
    $self _buildVTKObjects
    $self SetVisibility 0
  }

  destructor {
    $VectorActor Delete
    $VectorMapper Delete
    $HedgeHog Delete
    $VectorNorm Delete
    $MaskPoints Delete
    if {$LookupTable ne ""} {
      $LookupTable Delete
    }
  }

  method DisableWidgets { } {
    $Widgets(scale,Factor) state disabled
    $Widgets(scale,Density) state disabled
  }

  method EnableWidgets { } {
    $Widgets(scale,Factor) state !disabled
    $Widgets(scale,Density) state !disabled
  }

  method _buildVTKObjects { } {
    # FILTER: MaskPoints
    install MaskPoints using vtkMaskPoints $win.maskPoints
    $MaskPoints RandomModeOn
    $MaskPoints SetOnRatio [$self GetOnRatioFromScale]
    #$MaskPoint SetMaximumNumberOfPoints [$imgVector GetNumberOfPoints]

    # FILTER: VectorNorm
    install VectorNorm using vtkVectorNorm $win.vectorNorm
    $VectorNorm NormalizeOff
    $VectorNorm SetInputConnection [$MaskPoints GetOutputPort]

    # FILTER: HedgeHog
    install HedgeHog using vtkHedgeHog $win.hedgeHog
    $HedgeHog SetInputConnection [$VectorNorm GetOutputPort]
    $HedgeHog SetScaleFactor [$self GetVectorFactorFromScale]

    # MAPPER: VectorMapper
    install VectorMapper using vtkPolyDataMapper $win.vectorMapper
    $VectorMapper SetInputConnection [$HedgeHog GetOutputPort]
    $VectorMapper UseLookupTableScalarRangeOn
    if {$options(-lookuptable) ne ""} {
      $VectorMapper SetLookupTable $options(-lookuptable)
    }
    # ACTOR: VectorActor
    install VectorActor using vtkActor $win.vectorActor
    $VectorActor SetMapper $VectorMapper
  }
  
  method GetLabel { } {
    return "Vectors"
  }

  method AddToRenderer { renderer } {
    $renderer AddActor $VectorActor
  }

  method SetDataSet { dataSet } {
    if {$options(-lookuptable) eq ""} {
      install LookupTable using vtkLookupTable $win.lookupTable
      $LookupTable SetHueRange 0.667 0.0
      $LookupTable SetVectorModeToMagnitude
      $VectorMapper SetLookupTable $LookupTable
    }
    set DataSet $dataSet
    $dataSet AddObserver AfterOpenEvent [mymethod OnOpenDataSet]
    $dataSet AddObserver ChangeTimeStep [mymethod OnChangeTimeStep %T]
    $dataSet AddObserver BeforeCloseEvent [mymethod OnCloseDataSet]
  }

  method OnOpenDataSet { } {
    if {$LookupTable ne ""} {
      $LookupTable SetRange 0 [$DataSet GetVelocityEncoding]
      $LookupTable Build
    }
    $self EnableWidgets
  }

  method OnCloseDataSet { } {
    $self SetVisibility 0
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $MaskPoints SetInputData ""
    } else {
      $MaskPoints SetInput ""
    }
    $self DisableWidgets
  }

  method OnChangeTimeStep { t } {
    set imgVector [$DataSet GetVelocityImage -component FLOW -vtk yes]
    $MaskPoints SetMaximumNumberOfPoints [$imgVector GetNumberOfPoints]
    if {[VTKUtil::GetVTKMajorVersion] > 5} {
      $MaskPoints SetInputData $imgVector
    } else {
      $MaskPoints SetInput $imgVector
    }
  }

  method GetOnRatioFromScale { } {
    return [expr {int([$win.scDensity get])}]
  }

  method GetVectorFactorFromScale { } {
    return [$win.scFactor get]
  }
  
  method _OnSetDensity { v } {
    set r [$self GetOnRatioFromScale]
    if {$r != [$MaskPoints GetOnRatio]} {
      $MaskPoints SetOnRatio $r
      hook call $self <Render>
    }
  }

  method _OnSetFactor { v } {
    if {$v != [$HedgeHog GetScaleFactor]} {
      $HedgeHog SetScaleFactor $v
      hook call $self <Render>
    }
  }
}