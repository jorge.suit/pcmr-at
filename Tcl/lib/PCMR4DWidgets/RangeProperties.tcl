package require vtk
package require snit
package require wcb

namespace import msgcat::*

snit::widgetadaptor RangeProperties {

  variable MinimumVelocity 0
  variable MaximumVelocity 0

  variable Widgets -array {}

  delegate option * to hull
  delegate method * to hull

  constructor { args } {
    installhull using PropertyContainer
    $self configurelist $args
    $self buildMainFrame

  }
  
  destructor {
  }

  method buildMainFrame { } {
    set f [$self GetMainFrame]

    # MinimumVelocity
    ttk::label $f.lbMinimumVelocity -text [mc "Minimum Velocity"]:
    ttk::entry $f.entMinimumVelocity -textvariable [myvar MinimumVelocity]
    wcb::callback $f.entMinimumVelocity before insert \
        wcb::checkEntryForReal
    wcb::callback $f.entMinimumVelocity after insert \
        [mymethod cbAfterModifyEntry]
    wcb::callback $f.entMinimumVelocity after delete \
        [mymethod cbAfterModifyEntry]

    # MaximumVelocity
    ttk::label $f.lbMaximumVelocity -text [mc "Maximum Velocity"]:
    ttk::entry $f.entMaximumVelocity -textvariable [myvar MaximumVelocity]
    wcb::callback $f.entMaximumVelocity before insert \
        wcb::checkEntryForReal
    wcb::callback $f.entMaximumVelocity after insert \
        [mymethod cbAfterModifyEntry]
    wcb::callback $f.entMaximumVelocity after delete \
        [mymethod cbAfterModifyEntry]

    grid $f.lbMinimumVelocity -row 0 -column 0 -sticky w
    grid $f.entMinimumVelocity -row 0 -column 1 -sticky w
    grid $f.lbMaximumVelocity -row 1 -column 0 -sticky w
    grid $f.entMaximumVelocity -row 1 -column 1 -sticky w
    grid columnconfigure $f 2 -weight 1
    grid rowconfigure $f 2 -weight 1
    return $f
  }

  method GetLabel { } {
    return [mc "Range Options"]
  }

  method ApplyToTarget { target } {
  }

  method GetTargetType { } {
    return "LookupTable"
  }

  method GetMinimum { } {
    return $MinimumVelocity
  }

  method GetMaximum { } {
    return $MaximumVelocity
  }

  method GetScalarRange { } {
    return [list $MinimumVelocity $MaximumVelocity]
  }

  method cbAfterModifyEntry { args } {
    if {$MinimumVelocity ne "" && $MaximumVelocity ne "" &&
      $MinimumVelocity < $MaximumVelocity} {
      $self EnableApply
    } else {
      $self DisableApply
    }
  }

  method _OnOpenDataSet { } {
    set MinimumVelocity 0
    set MaximumVelocity [[$self GetDataSet] GetVelocityEncoding]
    $LookupTable SetRange $MinimumVelocity $MaximumVelocity
    $LookupTable Build
  }

  method CopyPropertiesFrom { obj } {
    # empty
  }
}
