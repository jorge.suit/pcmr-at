package require vtk
package require vmtkWrapperTCL
package require snit
package require msgcat
package require hook
package require fileutil
package require jobexec

namespace import msgcat::*

snit::widget ExportFEMeshManager {

  typevariable VmtkPrefixPath /home/jsperez/Sources/Kitware/Builds/vmtk.git/Install
  typevariable JobFinished 0

  variable Widgets
  variable DataSet
  variable TemporaryBoundaryMesh

  delegate option * to hull
  delegate method * to hull

  hulltype ttk::frame

  typeconstructor {
    set ::env(PATH) ${VmtkPrefixPath}/bin:$::env(PATH)
    if { $::tcl_platform(platform) eq "unix" && [info exists ::env(LD_LIBRARY_PATH)] } {
      set ::env(LD_LIBRARY_PATH) ${VmtkPrefixPath}/lib:$::env(LD_LIBRARY_PATH)
    } else {
      set ::env(LD_LIBRARY_PATH) ${VmtkPrefixPath}/lib
    }
    if { [info exists ::env(PYTHONPATH)] } {
      #set ::env(PYTHONPATH) ${VmtkPrefixPath}/$VMTKHOME/lib/python2.7/site-packages:$::env(PYTHONPATH)
    } else {
      #set ::env(PYTHONPATH) ${VmtkPrefixPath}/lib/python2.7/site-packages
    }
  }

  typemethod _OnJobStart { job } {
  }

  typemethod _OnJobFinish { w job } {
    $w insert end "Finishing job $job"
    $w see end
    $job destroy
    set JobFinished 1
  }

  typemethod _OnJobOutput { w job data } {
    $w insert end "$data\n"
    $w see end
  }

  constructor {args} {
    $self configurelist $args
    ttk::label $win.lbPathFile -text [mc "Output File"]:
    ttk::entry $win.entPathFile -textvariable [myvar PathFile]

    grid $win.lbPathFile -row 0 -column 0 -sticky w
    grid $win.entPathFile -row 0 -column 1 -sticky w

    ttk::frame $win.faction -borderwidth 1 -relief sunken
    ttk::button $win.faction.apply -text [mc "Export"] \
        -command [mymethod _CmdComputeExportMesh]

    grid $win.faction.apply
    grid $win.faction -row 2 -column 0 -columnspan 3 -sticky snew

    grid rowconfigure $win 1 -weight 1
    grid columnconfigure $win 2 -weight 1
  }

  destructor {
  }

  method DisableWidgets { } {
  }

  method EnableWidgets { } {
  }

  method GetLabel { } {
    return "FE Mesh"
  }

  method AddToRenderer { renderer } {
  }

  method SetDataSet { dataSet } {
    set DataSet $dataSet
  }

  method OnOpenDataSet { } {
  }

  method OnCloseDataSet { } {
  }

  method OnChangeTimeStep { t } {
  }

  method SetVisibility { v } {
  }

  method GetVisibility { } {
    return 1
  }

  method _CmdComputeExportMesh_NoSirve { } {
    hook call $self <RequestBoundaryMesh> -setmethod [mymethod _SetBoundaryMesh]
    if { $TemporaryBoundaryMesh ne "" } {
      vtkTrivialProducer producer
      # Identify boundaries
      vtkFeatureEdges featureEdges
      featureEdges SetInputData $TemporaryBoundaryMesh
      featureEdges BoundaryEdgesOn
      featureEdges FeatureEdgesOff
      featureEdges ManifoldEdgesOff
      featureEdges NonManifoldEdgesOff
      featureEdges Update
      puts [[featureEdges GetOutput] Print]
      if { [[featureEdges GetOutput] GetNumberOfLines] } {
        puts "There are [[featureEdges GetOutput] GetNumberOfLines] boundary edges"
        # Remesh before capping
        vmtkPolyDataSurfaceRemeshing remesher
        remesher SetInputData $TemporaryBoundaryMesh
        remesher SetTriangleSplitFactor 5
        remesher SetTargetArea 0.1
        remesher SetMinAreaFactor 0.05
        remesher SetCollapseAngleThreshold 0.2
        # Cap if there are boundaries
        vmtkCapPolyData capBoundary
        capBoundary SetInputConnection [remesher GetOutputPort]
        capBoundary SetCellEntityIdsArrayName "CellsID"
        capBoundary SetInPlaneDisplacement 0.0
        capBoundary SetDisplacement 0.0
        capBoundary Update
        producer SetOutput [capBoundary GetOutput]
        remesher Delete
        capBoundary Delete
      } else {
        puts "There are no boundary edges"
        producer SetOutput $TemporaryBoundaryMesh
      }
      # Export STL to temporary directory
      vtkSTLWriter writerPolyData
      writerPolyData SetInputConnection [producer GetOutputPort]
      writerPolyData SetFileName "/tmp/fe_boundary.stl"
      writerPolyData Update

      # Invoke mesher
      set TemporaryBoundaryMesh ""
      # Delete filters
      featureEdges Delete
      producer Delete
    } else {
      puts "There is no boundary mesh computed"
    }
  }

  method _CreateTemporaryDir { } {
    set tmpdir [fileutil::tempdir]
    set dir [file join $tmpdir mesh[pid]]
    file delete -force $dir
    file mkdir $dir
    return $dir
  }

  method _CmdComputeExportMesh { } {
    hook call $self <RequestBoundaryMesh> -setmethod [mymethod _SetBoundaryMesh]
    if { $TemporaryBoundaryMesh ne "" } {
      set dir [$self _CreateTemporaryDir]
      set stlInput [file join $dir "input.stl"]
      vtkSTLWriter writer
      writer SetInputData $TemporaryBoundaryMesh
      writer SetFileName $stlInput
      writer Update
      writer Delete
      set pathMesh [$self _InvokeMesher $stlInput]
      if { $pathMesh ne "" } {
        set pathMeshProbed [$self _ProbeVelocityAtMeshFile $pathMesh]
        if { $pathMeshProbed ne "" } {
          $self _ComputeWSS $pathMeshProbed
        }
      }
    } else {
      hook call $self <Status> [mc "There is no boundary mesh computed"]
    }
  }

  method _GetOutputWindow { args } {
    array set opts {
      -title "Job's Output"
      -clear 1
    }
    array set opts $args
    if { ![winfo exists $win.outputJob] } {
      toplevel $win.outputJob
      text $win.outputJob.t -wrap none \
          -yscrollcommand "$win.outputJob.scrolly set" \
          -xscrollcommand "$win.outputJob.scrollx set"
      ttk::scrollbar $win.outputJob.scrolly \
          -orient v -command "$win.outputJob.t yview"
      ttk::scrollbar $win.outputJob.scrollx \
          -orient h -command "$win.outputJob.t xview"
      grid $win.outputJob.t -row 0 -column 0 -sticky snew
      grid $win.outputJob.scrolly -row 0 -column 1 -sticky ns
      grid $win.outputJob.scrollx -row 1 -column 0 -sticky ew
      grid columnconfigure $win.outputJob 0 -weight 1
      grid rowconfigure $win.outputJob 0 -weight 1
    }
    wm title $win.outputJob $opts(-title)
    if { $opts(-clear) } {
      $win.outputJob.t delete 1.0 end
    }
    return $win.outputJob
  }

  method _InvokeMesher { pathSTL } {
    set dir [file dirname $pathSTL]
    set pathMesh [file join $dir output.vtk]
    set wLog [$self _GetOutputWindow -clear 1 -title "Mesher Output"]
    set job [ jobexec create %AUTO% \
                  -command "vmtkmeshgenerator" \
                  -arguments [list -ifile $pathSTL -ofile $pathMesh \
                                  -boundarylayer 0] \
                  -verbose 1 \
                  -inbackground 1 \
                  -jobstartupcmd [mytypemethod _OnJobStart] \
                  -onoutputcmd [mytypemethod _OnJobOutput $wLog] ]
    $job configure -jobshutdowncmd [mytypemethod _OnJobFinish $wLog ]
    hook call $self <Status> [mc "Generating FE mesh ..."]
    $job execute
    vwait [mytypevar JobFinished]
    if { [file exists $pathMesh] } {
      hook call $self <Status> [mc "Mesh generated at %s" $pathMesh]
      return $pathMesh
    } else {
      hook call $self <Status> [mc "Failed mesh generation, no mesh file was found"]
      return ""
    }
  }

  method _SetBoundaryMesh { bmesh } {
    set TemporaryBoundaryMesh $bmesh
  }

  method _ProbeVelocityAtMeshFile { pathMesh } {
    set dir [file dirname $pathMesh]
    set ext [file extension $pathMesh]
    if { $ext eq ".vtk" } {
      vtkUnstructuredGridReader reader
    } elseif { $ext eq ".vtu" } {
      vtkXMLUnstructuredGridReader reader
    } else {
      error "Unknown file extension $ext. Must be .vtk or .vtu"
    }
    reader SetFileName $pathMesh
    reader Update
    set meshWithVelocity [$self _ProbeVelocityAtMesh [reader GetOutput]]
    reader Delete
    puts [$meshWithVelocity Print]
    if { $ext eq ".vtk" } {
      vtkUnstructuredGridWriter writer
    } elseif { $ext eq ".vtu" } {
      vtkXMLUnstructuredGridWriter writer
    }
    set pathOutput [file join $dir "[file rootname [file tail $pathMesh]]_velocity${ext}"]
    writer SetFileName $pathOutput
    writer SetInputData $meshWithVelocity
    writer Update
    hook call $self <Status> [mc "Velocity interpolated on mesh written to %s" $pathOutput]
    $meshWithVelocity Delete
    writer Delete
    return $pathOutput
  }

  method _ProbeVelocityAtMesh { mesh } {
    vtkProbeFilter probe
    probe SetInputData $mesh
    probe PassCellArraysOn
    probe SetSourceData [$DataSet GetVelocityImage -component FLOW -vtk yes]
    hook call $self <Status> [mc "Interpolating velocity at FE mesh ..."]
    probe Update
    hook call $self <Status> [mc "Finished interpolating velocity at FE mesh ..."]
    set meshProbed [probe GetOutput]
    $meshProbed Register ""
    probe Delete
    return $meshProbed
  }

  method _ComputeWSS { pathMesh } {
    set dir [file dirname $pathMesh]
  }
}
