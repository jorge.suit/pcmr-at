#
# jobexec.tcl --
#   An abstraction of a command to run.
#   This OO component allows to execute a command in a 
#   given directory. 
#   TAKEN FROM: http://tclrep.cvs.sourceforge.net/viewvc/tclrep/modules/jobexec
#
#   Overview
#
#   The command may be executed either in foreground or in background
#   with the "execute" method.
#   - If the command is executed in foreground, the "execute"
#   method returns the message on the standard output generated
#   by the last command on the pipeline. 
#   The command does not return until the command is completely 
#   finished. If the command generates
#   message on the standard error, an error is generated at the 
#   Tcl level.
#   - If the command is executed in background, the "execute" method
#   returns immediately, while the command continues to be executed.
#   The standard output and error messages can be managed in the 
#   client code by a callback method.
#
#   The waitend method allow to run a job in background and to wait for the 
#   end of the job before anything else can be done.
#   The alljobswaitend method allow to run several jobs in background
#   and to wait until all jobs are done. The alljobswaitend method 
#   is a "barrier".

#
# Copyright 2008 Michaël Baudin
#
# The jobexec package is based on the bgexec.tcl script, which contained
# the following copyright notice.
#
################################################################################
# Modul    : bgexec1.8.tcl                                                     #
# Changed  : 08.10.2007                                                        #
# Purpose  : running processes in the background, catching their output via    #
#            event handlers                                                    #
# Author   : M.Hoffmann                                                        #
# To do    : - rewrite using NAMESPACEs                                        #
# Hinweise : >&@ and 2>@stdout don't work on Windows. A work around probably   #
#            could be using a temporay file. Beginning with Tcl 8.4.7 / 8.5    #
#            there is another (yet undocumented) way of redirection: 2>@1.     #
# History  :                                                                   #
# 19.11.03 v1.0 1st version                                                    #
# 20.07.04 v1.1 callback via UPLEVEL                                           #
# 08.09.04 v1.2 using 2>@1 instead of 2>@stdout if Tcl >= 8.4.7;               #
#               timeout-feature                                                #
# 13.10.04 v1.3 bugfix in timeout, readcmd is interruptable          #
# 18.10.04 v1.4 bugfix: timeout needs to be canceled when work is done;  #
#               some optimizations                                             #
# 14.03.05 v1.4 comments translated to english                                 #
# 17.11.05 v1.5 If specidied, a user defined timeout handler `toExit` runs in  #
#               case of a timeout to give chance to kill the PIDs given as     #
#               arg. Call should be compatible (optional parameter).           #
# 23.11.05 v1.6 User can give additional argument to his readcmd.          #
# 03.07.07 v1.7 Some Simplifications (almost compatible, unless returned       #
#               string where parsed):                                          #
#               - don't catch error first then returning error to main...      #
# 08.10.07 v1.8 fixed buggy version check!                                     #
# ATTENTION: closing a pipe leads to error broken pipe if the opened process   #
#             itself is a tclsh interpreter. Currently I don't know how to     #
#             avoid this without killing the process via toExit before closing #
#             the pipeline.                                                    #
################################################################################

package require snit
# Tclx provides the "kill" command
# package require Tclx

#package provide jobexec 1.2

snit::type jobexec {
    # the name of the job
    option -name -default ""
    # the directory into which the command is to be executed
    option -directory -default .
    # the command to execute
    option -arguments -default {}
    # the arguments to pass to the command
    option -command -default ""
    # set to 1 to enable the buffering of the output
    option -internalbuffering -default 0
    # The size of the internal buffer, in number of characters
    option -internalbuffersize -default 10000
    # buffering mode of fconfigure
    option -buffering -default "full"
    # set to 1 to run the command in background
    option -inbackground -default 0
    # the name of a logger command
    option -loggercommand -default ""
    # Name of the command which receives the STDOUT and STDERR messages.
    # The current job and the data is appended to the command as an extra argument before it is executed.
    option -onoutputcmd -default ""
    # The time after which the process is considered as finished. Default is 0.
    option -timeout -default 0
    # name of the callback command which is called when there is a timeout, with pid as argument.
    option -timeoutcmd -default ""
    # name of the callback command which is called after the process is finished.
    option -jobshutdowncmd -default ""
    # name of the callback command which is called before the process is executed.
    option -jobstartupcmd -default ""
    # set to 1 to enable verbose logging
    option -verbose -default 0
    # The handle return by the "open" on pipe
    variable processhandle ""
    # Id returned by "after" for the timeout script
    variable timeoutid ""
    # List of currently existings objects
    typevariable listofjobs {}
    # The PID associated with the job
    variable processid ""
    # Flag which goes from 0 to 1 when the job is running in background
    variable running 0
    # Time when the job started in milliseconds
    variable timer_start 0
    # Time when the job ended in milliseconds
    variable timer_end 0
    # Elapsed time for the last execution
    variable timer_elapsed 0
    # Elapsed time for all executions
    variable timer_totalelapsed 0
    # Date when the job started in seconds
    variable timer_datestart ""
    # Internal buffer
    variable buffer ""
    # Counts all the job which are currently in execution in background with the system
    variable jobcounter 0
    #
    # execute --
    #   Run the command in the foreground or in background,
    #   depending on the -inbackground option.
    #   If -inbackground is 0, returns the standard output from the last
    #   commmand in the pipeline.
    #   If -inbackground is 1, returns an empty string.
    #
    method execute {} {
        $self log "execute"
        if {$running==1} then {
            error "Job $self is allready running : kill the job first."
        }
        set inbackground [$self cget -inbackground]
        switch -- $inbackground {
            0 {
                set result [$self executeinforeground]
            }
            1 {
                set result [$self executeinbackground]
            }
            default {
                error "Unknown -inbackground value $inbackground"
            }
        }
        return $result
    }
    #
    # executeinforeground --
    #   Run the command in the foreground, waiting that the 
    #   command is done before returning.
    #
    method executeinforeground {} {
        $self log "executeinforeground"
        #
        # Store the time when the job started
        #
        $self timer_start
        #
        # Call the startup command
        #
        $self startupcmdcallback
        #
        # Store the initial directory
        #
        set cwd [pwd]
        #
        # Go in the target directory
        #
        $self log "cd $options(-directory)"
        cd $options(-directory)
        #
        # Compute the command to execute
        #
        set cmd {}
        lappend cmd exec
        foreach item $options(-command) {
            lappend cmd $item
        }
        set cmd [concat $cmd $options(-arguments)]
        #
        # Execute the command in foreground with Tcl "exec"
        #
        $self log "eval $cmd"
        set errflag [catch {set result [eval $cmd]} errmsg]
        #
        # Go back to the original directory
        #
        $self log "cd $cwd"
        cd $cwd
        if {$errflag!=0} then {
            error "$errmsg"
        } else {
          # In that case, the message is not error, it is just a message.
          $self callbackreadcmd $errmsg
        }
        #
        # Call the shutdown command
        #
        $self shutdowncmdcallback
        #
        # Store the time when the job end and measure elapsed time
        #
        $self timer_stop
        return $result
    }
    #
    # constructor --
    #   Create the jobexec and register it in the list of jobs
    #
    constructor {args} {
        $self configurelist $args
        lappend listofjobs $self
    }
    #
    # destructor --
    #   Destroy the jobexec and unregister it in the list of jobs
    #
    destructor {
        set jobindex [lsearch $listofjobs $self]
        set listofjobs [lreplace $listofjobs $jobindex $jobindex]
    }
    #
    # log --
    #   Log the given message msg with the logger command of the
    #   current jobexec.
    # Arguments:
    #   msg: the message to log
    #
    method log {msg} {
        set verbose [$self cget -verbose]
        if {$verbose==1} then {
          set loggercommand [$self cget -loggercommand]
          if {$loggercommand!=""} then {
            $self evaluateCommand $loggercommand "$msg"
          } else {
            puts "$msg"
          }
        }
        return ""
    }
    #
    # evaluateCommand --
    #   This command centralizes the callback, allowing them to have arguments
    #   which are appended to the regular arguments of the evaluated command.
    #
    method evaluateCommand { commandname args } {
        set cmd {}
        foreach item $commandname {
            lappend cmd $item
        }
        lappend cmd $self
        foreach item $args {
            lappend cmd $item
        }
        set errflag [catch {set result [eval $cmd]} errmsg]
        if {$errflag!=0} then {
            error "$::errorInfo"
        }
        return $result
    }
    #
    # executeinbackground --
    #   Runs a command in background returns an empty string immediately.
    #   The method is based on opening a channel on a pipe.
    #   If the program successfully starts, whenever the channel becomes readable,
    #   which means that the command wrote on STDOUT or STDERR,
    #   the command given with -onoutputcmd option is called back (via background_readhandler) 
    #   with the current jobexec object as first argument and the 
    #   generated line as second argument.
    #   If a -timeout option was specified (as msecs) and a timeout occurs :
    #   1. the command given with -timeoutcmd option will be called with 
    #   with the current jobexec object as first argument and PID as second argument,
    #   2. the channel is closed.
    #   will be automatically closed after
    #   When a EOF file event occurs, the process ended and 
    #   1. the channel is closed,
    #   2. the command given with -jobshutdowncmd option is called with the current jobexec object
    #      as first argument.
    #   The eof callback may be used, for example, to make sure that one buffer is empty.
    #
    method executeinbackground {} {
        $self log "executeinbackground"
        #
        # Store the time when the job started
        #
        $self timer_start
        #
        # Call the startup command
        #
        $self startupcmdcallback
        #
        # Go to the target directory
        #
        set cwd [pwd]
        cd $options(-directory)
        #
        # Update the job counter
        #
        incr jobcounter
        $self log "> job counter : $jobcounter"
        #
        # Create the pipe
        #
        set levelIndex [lindex [lsort -dict [list 8.4.7 [info patchlevel]]] 0]
        $self log "> levelIndex:$levelIndex"
        set command [$self cget -command]
        if {$command==""} then {
            error "Job command is empty."
        }
        set arguments [$self cget -arguments]
        if {$levelIndex== "8.4.7"} then {
            set p "| $command $arguments 2>@1"
        } else {
            set p "| $command $arguments 2>@stdout"
        }
        $self log "> p : $p"
        set errflag [catch {set processhandle [open $p r]} errmsg]
        #
        # Return back to the original directory before an error
        # is generated.
        #
        cd $cwd
        if {$errflag!=0} then {
            #
            # The pipe cannot be opened :
            # may be the executable does not exist.
            #
            incr jobcounter -1
            error "$errmsg"
        }
        $self log "> processhandle:$processhandle"
        fconfigure $processhandle -blocking 0
        #
        # Configure buffering at the "fconfigure" level
        #
        set buffering [$self cget -buffering]
        fconfigure $processhandle -buffering $buffering
        set timeout [$self cget -timeout]
        if {$timeout==0} then {
            set timeoutid {}
        } else {
            set timeoutid [after $timeout [mymethod background_timeout]]
        }
        $self log "> timeoutid:$timeoutid"
        #
        # Configure the "readable" file event depending on the "internalbuffering" option.
        # Note :
        #   For design reasons, one may want to create only one method "background_readhandler,
        #   with an inside "if".
        #   Instead, for performances reasons, the filevent is bound directly to the right
        #   method, so that the "if" is executed once, at the begining of the job.
        #   This is not cosmetic: this file event may occur several thousand times.
        #
        if {$options(-internalbuffering)==1} then {            
            fileevent $processhandle readable [mymethod background_readhandler_buffering]
        } else {
            fileevent $processhandle readable [mymethod background_readhandler]
        }
        #
        # Update internal state
        #
        set processid [pid $processhandle]
        $self log "> processid : $processid"
        set running 1
        return ""
    }
    #
    # shutdowncmdcallback --
    #   This command is executed after the job is finished
    #
    method shutdowncmdcallback {} {
      $self log "shutdowncmdcallback"
      set cmd $options(-jobshutdowncmd)
      if {$cmd!=""} {
        lappend cmd $self
        $self log "eval : cmd : $cmd"
        set errflag [catch {uplevel $cmd} errmsg]
        if {$errflag!=0} {
          $self myerror "Error while calling eof command $cmd.\n$errmsg\n$::errorInfo"
        }
      }
    }
    #
    # startupcmdcallback --
    #   This command is executed before the job is started
    #
    method startupcmdcallback {} {
      $self log "startupcmdcallback"
      set cmd $options(-jobstartupcmd)
      if {$cmd!=""} {
        lappend cmd $self
        $self log "eval : cmd : $cmd"
        set errflag [catch {uplevel $cmd} errmsg]
        if {$errflag!=0} {
          $self myerror "Error while calling command $cmd.\n$errmsg\n$::errorInfo"
        }
      }
    }

    #
    # background_readhandler --
    #   This command is executed whenever the execution channel becomes
    #   readable.
    #   Two cases are possible : 
    #   1. The EOF occurred on the channel, which means that the 
    #      job ends and that the eof callback command is to trigger,
    #   2. The channel just wrote a data, which has to be output thanks 
    #      to the read callback command.
    #
    method background_readhandler {} {
        #$self log "background_readhandler"
        set iseof [eof $processhandle]
        if {$iseof==1} {
            $self background_closepipe
            $self shutdowncmdcallback
        } elseif {[gets $processhandle line] != -1} {
            $self callbackreadcmd $line
        }
        return ""
    }
    #
    # background_readhandler_buffering --
    #   This command is executed whenever the execution channel becomes
    #   readable and the internalbuffering option is enabled.
    #   Two cases are possible : 
    #   1. The EOF occurred on the channel, which means that the 
    #      job ends and that the eof callback command is to trigger,
    #   2. The channel just wrote a data, which has to be output thanks 
    #      to the read callback command if the buffer is full 
    #      or bufferized the data into the buffer if the buffer is not full.
    #
    method background_readhandler_buffering {} {
        #$self log "background_readhandler_buffering"
        set iseof [eof $processhandle]
        if {$iseof==1} {
            #
            # Writes out any data currently in the buffer
            #
            $self callbackreadcmd $buffer
            set buffer ""
            #
            # Close the pipeline, to clean-up "after" scripts, and delete file events.
            #
            $self background_closepipe
            #
            # Call the shutdown callback, if available
            #
            $self shutdowncmdcallback
        } elseif {[gets $processhandle line] != -1} {
            #
            # Append the new data to the buffer.
            #
            append buffer $line
            #
            # Add a newline when necessary
            #
            set lastchar [string index $line end]
            if {$lastchar!="\n"} then {
                append buffer "\n"
            }
            #
            # Compute the size of the buffer
            #
            set slength [string length $buffer]
            #
            # Select the update method and compute toupdate
            #
            set toupdate [expr {$slength> $options(-internalbuffersize)}]
            if {$toupdate==1} then {
                $self callbackreadcmd $buffer
                set buffer ""
            }
        }
        return ""
    }
    #
    # callbackreadcmd --
    #   Call the readcmd callback command and pass it the current job
    #   and the data (which has been written on the channel)
    #
    method callbackreadcmd {data} {
        set readcmd $options(-onoutputcmd)
        if {$readcmd!=""} then {
            # we are not blocked (manpage gets, Practical... page.233)
            lappend readcmd $self $data
            set errflag [catch {uplevel $readcmd} errmsg]
            if {$errflag!=0} {
                $self background_closepipe
                $self myerror "Error while calling read command $readcmd.\n$errmsg$::errorInfo"
            }
        }
    }
    #
    # myerror --
    #   Before generating an error, fill a log file.
    #
    method myerror {errmsg} {
      set handle [open "jobexec.log" a]
      puts -nonewline $handle $errmsg
      close $handle
      error $errmsg
    }
    #
    # background_closepipe --
    #   The background job just finished.
    #   Close the pipe of the background command.
    #
    method background_closepipe {} {
        $self log "background_closepipe"
        #
        # Cancel the timeout event
        # Empty timeoutid is ignored
        #
        $self log "Canceling timeoutid $timeoutid"
        after cancel $timeoutid
        #
        # Finish job
        #
        $self background_finishjob
        return ""
    }
    #
    # background_timeout --
    #   This command is executed if a timeout occurs.
    #
    method background_timeout {} {
        $self log "background_timeout"
        # Store process id (it is cleaned-up in finishjob)
        set jobpid $processid
        #
        # Finish job
        #
        $self background_finishjob
        #
        # Call back the timeout command
        #
        set timeoutcmd [$self cget -timeoutcmd]
        if {[string length $timeoutcmd]!=0} {
            set cmd $timeoutcmd 
            lappend cmd $self $jobpid
            $self log "> executing $cmd"
            set errflag [catch {uplevel $cmd} errmsg]
            if {$errflag!=0} then {
                error "Unable to call the timeout command $timeoutcmd:\n$errmsg"
            }
        }
        return ""
    }
    #
    # background_finishjob --
    #   Make all processing common to job finished, be it a normal end or a timeout.
    #
    method background_finishjob {} {
        $self log "background_finishjob"
        #
        # Close the channel which automatically deregisters the fileevent handler
        #
        $self log "Closing channel $processhandle"
        fileevent $processhandle readable ""
        catch {close $processhandle}
        #
        # Update the job counter
        #
        incr jobcounter -1
        $self log "Updated job counter : $jobcounter"
        #
        # Update internal state
        #
        set processid ""
        set running 0
        #
        # Store the time when the job end and measure elapsed time
        #
        $self timer_stop
        return ""
    }
    #
    # timer_start --
    #   Start the timer and update timers
    #
    method timer_start {} {
        $self log "timer_start"
        set timer_start [clock clicks -milliseconds]
        set timer_datestart [clock seconds]
    }
    #
    # timer_stop --
    #   Stop the timer and update timers
    #
    method timer_stop {} {
        $self log "timer_stop"
        #
        # Compute the current elapsed time and the total elapsed time
        #
        set timer_end [clock clicks -milliseconds]
        set timer_elapsed [expr {$timer_end - $timer_start}]
        incr timer_totalelapsed $timer_elapsed
        #
        # Reset intermediate timers
        #
        set timer_start ""
        set timer_datestart ""
        set timer_end ""
    }
    #
    # get --
    #   Return the value of the key.
    #   Available keys are :
    #   -processhandle : returns the handle which has been set by 
    #     Tcl when the pipeline was created.
    #   -timeoutid : returns the id associated with the "after" event.
    #   -running : returns 1 if the job is currently running or 0 if not.
    #   -pid : if the job is currently running, 
    #     returns the process id associated with the current job.
    #     If the job is not running, returns an empty string.
    #   -start : if the job is currently running, returns the number 
    #     of milliseconds when the job started (computed from Tcl "clock clicks -milliseconds"). 
    #     This value can only be used for relative time computations.
    #     If the job is not running, returns an empty string.
    #   -elapsed : if the job is running, returns the number of milliseconds
    #     since the job started.
    #     If the job is not running, returns the number of milliseconds 
    #     for the elapsed time of the previous job.
    #     If the job has never been processed, and is not running, returns 0
    #   -totalelapsed : returns the total number of milliseconds used 
    #     when processing the job : it is the sum of all elapsed times,
    #     for all runs.
    #     If the job has never been processed, returns 0.
    #   -datestart : if the job is currently running, returns the 
    #     time when the job started (computed from Tcl "clock seconds").
    #     If the job is not running, returns an empty string.
    #
    method get {key} {
        switch -- $key {
            "-processhandle" {
                set result $processhandle
            }
            "-timeoutid" {
                set result $timeoutid
            }
            "-running" {
                set result $running
            }
            "-pid" {
                set result $processid
            }
            "-start" {
                set result $timer_start
            }
            "-elapsed" {
                if {$running==0} then {
                    set result $timer_elapsed
                } else {
                    # If the process in running, compute the elapsed time
                    # from the time when the process started
                    set current [clock clicks -milliseconds]
                    set result [expr {$current - $timer_start}]
                }
            }
            "-totalelapsed" {
                set result $timer_totalelapsed
            }
            "-datestart" {
                set result $timer_datestart
            }
            default {
                error "Unknown key $key."
            }
        }
        return $result
    }
    #
    # kill --
    #   Kill the current job
    #
    method kill {} {
        if {$running==0} then {
            error "Cannot kill job : $self is not running."
        } else {
            $self background_closepipe
            # kill $processid
        }
        return ""
    }
    #
    # copy --
    #   Returns a new job which is a copy of the current job.
    #
    method copy {} {
        set newjob [jobexec create %AUTO%]
        foreach key [array names options] {
            $newjob configure $key $options($key)
        }
        return $newjob
    }
    #
    # getrunningjobs --
    #   Return the list of all currently running jobs
    #
    typemethod getrunningjobs {} {
        set runningjobs {}
        foreach job $listofjobs {
            set isrunning [$job get -running]
            if {$isrunning==1} then {
                lappend runningjobs $job
            }
        }
        return $runningjobs
    }
    #
    # destroyall --
    #   Destroys all current jobs.
    #
    typemethod destroyall {} {
        set joblist [jobexec getjobs]
        foreach job $joblist {
            $job destroy
        }
    }
    #
    # getjobs --
    #   Return the list of all registered jobs
    #
    typemethod getjobs {} {
        return $listofjobs
    }
    #
    # find --
    #   Return the job associated with the given key
    #
    typemethod find {key value} {
        set jobmatch ""
        foreach job $listofjobs {
            switch -- $key {
                "-pid" {
                    set currentvalue $processid
                }
                default {
                    set currentvalue [$job cget $key]
                }
            }
            if {$currentvalue==$value} then {
                set jobmatch $job
                break
            }
        }
        return $jobmatch
    }
    #
    # waitend --
    #   Returns an empty string when the current job is done.
    #
    method waitend {} {
      $self log "waitend"
      if {$jobcounter==0} then {
        $self log "> Allready done."
      } else {
        set cmd [list vwait [myvar jobcounter]]
        eval $cmd
        $self log "> Release !"
      }
      return ""
    }
    #
    # alljobswaitend --
    #   Returns an empty string when all jobs ends.
    #
    typemethod alljobswaitend {} {
        foreach job $listofjobs {
            $job waitend
        }
        return ""
    }
}
