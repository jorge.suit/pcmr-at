source TestHelper.tcl
package require Icons
package require msgcat
package require wtree
package require Tk

namespace import msgcat::*

frame .control
ttk::label .control.lbCurrent -text [mc "Current:"]
ttk::entry .control.entCurrent -textvariable currentSelected
grid .control.lbCurrent -row 0 -column 0 -sticky e
grid .control.entCurrent -row 0 -column 1 -sticky e

set t [wtree .t -font TkTextFont -table yes -filter no \
           -showheader yes -showbuttons no \
           -columns [list \
                         [list image -tags STATE -label [mc "Visible"]] \
                         [list text -tags NAME -label [mc "Name"]] \
                         [list image -tags DIRTY -label [mc "Modified"]] \
                         [list text -tags MINVEL -label [mc "Min. Vel."]] \
                         [list text -tags MAXVEL -label [mc "Max. Vel."]] \
                         [list text -tags FLOWPOS -label [mc "(+) Flow"]] \
                         [list text -tags FLOWNEG -label [mc "(-) Flow"]] \
                         [list text -tags FLOWRES -label [mc "Flow"]] \
                         [list text -tags ACCVOL -label [mc "Acc. Vol."]] ]]

bind .t <ButtonPress-1> [list OnButtonPress %W %x %y]

set currentPlane ""
set currentSelected ""

proc OnButtonPress { T x y } {
  set it [$T tree identify $x $y]
  puts $it
  if {[catch {array set item $it}] || ![info exists item(item)]} {
    return
  }

  set id $item(item)
  set name [$T tree item element cget $id "tag NAME" eTXT -text]
  if {$item(column) == 0} {
    set icn [Icons GetInstance]
    set imgON [$icn Get light_bulb_on -size 16]
    set imgOFF [$icn Get light_bulb_off -size 16]
    set img [$T tree item element cget \
                 $id $item(column) eIMG -image]
    if {$img eq $imgON} {
      set img $imgOFF
      set state 0
    } elseif {$img eq $imgOFF} {
      set img $imgON
      set state 1
    }
    $T tree item element configure \
        $id $item(column) eIMG -image $img
    if {$img eq $imgON} {
      OnChangePlaneState $name $state
    }
  }
  # here we obtain the object
  if {$name ne $::currentPlane} {
    OnChangePlane $name
  }
}

proc OnChangePlaneState { name state } {
}

proc OnChangePlane { name } {
  set ::currentPlane $name
  set ::currentSelected $name
  puts "OnChangePlane $name"
}

proc UpdatePlane { name args } {
  array set opts $args
  set I [list tag "--${name}--"]
  set cmdConfigure " .t tree item element configure [list $I]"
  set first 1
  foreach tag {MINVEL MAXVEL FLOWPOS FLOWNEG FLOWRES ACCVOL} {
    if {[info exists opts($tag)]} {
      if {!$first} {
        append cmdConfigure " ,"
      } else {
        set first 0
      }
      set C [list tag $tag]
      append cmdConfigure " [list $C] eTXT -text [list $opts($tag)]"
    }
  }
  {*}$cmdConfigure
}

proc InsertPlane { args } {
  array set opts {
    STATE   0
    DIRTY   0
    MINVEL  ?
    MAXVEL  ?
    FLOWPOS ?
    FLOWNEG ?
    FLOWRES ?
    ACCVOL  ?
  }
  array set opts $args
  set icn [Icons GetInstance]
  set imgON [$icn Get light_bulb_on -size 16]
  set imgOFF [$icn Get light_bulb_off -size 16]
  set imgNOR [$icn Get tick -size 16]
  set imgMOD [$icn Get exclamation_octagon -size 16]
  if {![info exists opts(NAME)]} {
    error [mc "NAME argument is required"]
  }
  set name $opts(NAME)
  set id [.t insert [list [list [expr {$opts(STATE)?$imgON:$imgOFF}]] \
                         [list $name] \
                         [list [expr {$opts(DIRTY)?$imgMOD:$imgNOR}]] \
                         [list $opts(MINVEL)] \
                         [list $opts(MAXVEL)] \
                         [list $opts(FLOWPOS)] \
                         [list $opts(FLOWNEG)] \
                         [list $opts(FLOWRES)] \
                         [list $opts(ACCVOL)]]]
  .t item tag add $id "--${name}--"
}

InsertPlane NAME Root
InsertPlane NAME Asc DIRTY 1
InsertPlane NAME "Asc 1" DIRTY 1

grid .control -row 0 -column 0 -sticky snew
grid .t -row 1 -column 0 -sticky snew
grid rowconfigure . 1 -weight 1
grid columnconfigure . 0 -weight 1

UpdatePlane Root ACCVOL 5
UpdatePlane Asc ACCVOL 4.8
UpdatePlane Asc FLOWRES 2