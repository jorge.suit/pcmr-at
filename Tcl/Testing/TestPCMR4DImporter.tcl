set trace_pcmr4d_load 1

set __cwd__ [file normalize [file dirname [info script]]]

set __testing__ [expr {[lrange [file split $__cwd__] end-3 end] eq {pcmr trunk Tcl Testing}}]

puts [lrange $__cwd__ end-3 end]

if {$tcl_platform(platform) eq "unix" && $__testing__} {
  lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules /usr/local/src/TCL/tcllib/tklib/modules
  set auto_path [linsert $auto_path 0 [file normalize ../lib]]
}

package require Pcmr4d
ImporterApplication::Run


