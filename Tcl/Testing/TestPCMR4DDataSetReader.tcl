set __cwd__ [file normalize [file dirname [info script]]]
set __testing__ [expr {[lrange [file split $__cwd__] end-3 end] eq {pcmr trunk Tcl Testing}}]

if {$tcl_platform(platform) eq "unix" && $__testing__} {
  lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules /usr/local/src/TCL/tcllib/tklib/modules
  set auto_path [linsert $auto_path 0 [file normalize ../lib]]
}

package require PCMR4DWidgets

PCMR4DDataSet ds

if {[llength $argv]} {
  ds Open [lindex $argv 0]
} else {
  ds Open [file join $env(HOME) Data/pcmr/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL]
}

puts "The study has [ds GetNumberOfTimeStep] time-steps"

set img [ds GetVelocityImage -timestep 0 -component |V| -vtk yes]

puts [$img Print]

exit
