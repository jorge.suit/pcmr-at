package require vtk

vtkPolyDataReader reader
reader SetFileName [lindex $argv 0]

vtkTriangleFilter triangulator
triangulator SetInputConnection [reader GetOutputPort]

vtkDecimatePro decimate
decimate SetInputConnection [triangulator GetOutputPort]
decimate PreserveTopologyOn
decimate SetTargetReduction 0.6
decimate SetFeatureAngle 15
decimate SetSplitAngle 60
decimate PreSplitMeshOn
decimate SplittingOn
puts [decimate Print]

set useTaubin 0

if {$useTaubin} {
  vtkWindowedSincPolyDataFilter smoother
  smoother SetNumberOfIterations 15
  smoother BoundarySmoothingOff
  smoother FeatureEdgeSmoothingOff
  smoother SetFeatureAngle 120
  #smooth SetEdgeAngle 180
  smoother SetPassBand 0.0001
  #smooth NonManifoldSmoothingOn
  smoother NormalizeCoordinatesOn
} else {
  vtkSmoothPolyDataFilter smoother
  smoother SetNumberOfIterations 400
  smoother FeatureEdgeSmoothingOff
  smoother SetRelaxationFactor 0.01
}

smoother SetInputConnection [decimate GetOutputPort]
puts [smoother Print]

vtkGenericDataObjectWriter writer
writer SetInputConnection [smoother GetOutputPort]
writer SetFileName "/tmp/octreeBoundarySmoothed.vtk"
writer Update
exit
