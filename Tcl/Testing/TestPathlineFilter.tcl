if {$argc != 2} {
  #~/Data/pcmr/Studies/WSR_test_data WSR_test_data_SeedPlane.vtk
  puts "Usage: $argv0 study_dir seed_plane.vtk"
  exit
}

set __cwd__ [file normalize [file dirname [info script]]]

set __testing__ [expr {[lrange [file split $__cwd__] end-3 end] eq {pcmr trunk Tcl Testing}}]

if {$tcl_platform(platform) eq "unix" && $__testing__} {
  lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules /usr/local/src/TCL/tcllib/tklib/modules
  set auto_path [linsert $auto_path 0 [file normalize ../lib]]
}

package require PCMR4DWidgets
package require vtkinteraction

PCMR4DDataSet ds
ds Open [lindex $argv 0]

vtkTkRenderWidget .tkrw
::vtk::bind_tk_render_widget .tkrw
wm protocol . WM_DELETE_WINDOW ::vtk::cb_exit

package require gridplus

namespace import gridplus::*

gridplus entry .options -title Options { 
   {"Resample Factor"    .resample    + 10 !int} 
   {"Reinjection Rate"   .reinjection 10 !int} 
   {"Trace Length"       .trace       10 !int}
} 

gridplus button .buttons { 
   {Apply .apply ~Apply !} {Exit .exit ~Exit}
} 

gridplus layout .left { 
   .options
   .buttons:ew
} 

proc ResampleTimeValues { timeValues rate } {
  set n [llength $timeValues]
  set t0 [lindex $timeValues 0]
  foreach t1 [lrange $timeValues 1 end] {
    set delta [expr {double($t1 - $t0) / $rate}]
    for {set i 0} {$i < $rate} {incr i} {
      lappend result [expr {$t0 + $i * $delta}]
    }
    set t0 $t1
  }
  lappend result $t0
}

foreach t [ds GetTimeValues] {
  lappend TimeValuesOriginal [expr {$t / 1000.0}]
}

scale .scTime -from 0 -to [expr {[llength $TimeValuesOriginal] - 1}] \
    -orient horizontal
set CurrentStep 0

set renWin [.tkrw GetRenderWindow]
vtkRenderer ren
ren SetBackground 0.0 0.0 0.0
$renWin AddRenderer ren
$renWin Render
gridplus layout .main { 
   .left+nw .tkrw:snew
      |      .scTime=ew
}

gpset .options,resample 2
gpset .options,trace 1
gpset .options,reinjection 0

set TimeValuesResampled [ResampleTimeValues $TimeValuesOriginal $(.options,resample)]

gridplus pack .main -resize xy

set imgMag [ds GetVelocityImage -component MAG -vtk yes -timestep 0]
vtkPassThrough passImage
passImage SetInputData $imgMag

foreach {minValue maxValue} [$imgMag GetScalarRange] break
set x0 [ expr { ($maxValue+$minValue)/5 } ]
vtkVolumeProperty volumeProperty
volumeProperty SetInterpolationTypeToLinear

set opacityTransferFunction [volumeProperty  GetScalarOpacity]
$opacityTransferFunction RemoveAllPoints
$opacityTransferFunction AddPoint  $minValue 0.0
$opacityTransferFunction AddPoint  $x0   0.0
$opacityTransferFunction AddPoint  $maxValue   0.05

set colorTransferFunction [volumeProperty GetRGBTransferFunction]
#$colorTransferFunction RemoveAllPoints
#$colorTransferFunction AddRGBPoint  $minValue 0.2 0.3 0.5
#$colorTransferFunction AddRGBPoint  $maxValue 0.0 0.5 0.8

vtkSmartVolumeMapper volumeMapper

vtkVolume volume
volume SetMapper volumeMapper
volume SetProperty volumeProperty
volumeMapper SetInputConnection [passImage GetOutputPort]
ren AddVolume volume

update

set TemporalSource [TemporalVelocitySource]
$TemporalSource SetRescaleFactor 10
$TemporalSource SetTclDataSet ds

# create pipeline for rendering
#
vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 1200
  lut SetVectorModeToMagnitude
  lut Build

vtkPolyDataReader seedReader
seedReader SetFileName [lindex $argv 1]

vtkTemporalInterpolator timeInterp
timeInterp SetInputConnection [$TemporalSource GetOutputPort]
timeInterp SetResampleFactor $(.options,resample)

vtkMaskPoints maskPoints
maskPoints SetInputConnection [seedReader GetOutputPort]
maskPoints SetMaximumNumberOfPoints 100
maskPoints RandomModeOn

if {[info command vtkPParticlePathFilter] ne "" } {
  vtkPParticlePathFilter pathFilter
  puts [[pathFilter GetController] Print]
} else {
  vtkParticlePathFilter pathFilter
}
pathFilter SetInputConnection 0 [timeInterp GetOutputPort]
pathFilter SetInputConnection 1 [maskPoints GetOutputPort]
pathFilter SetStartTime [lindex $TimeValuesOriginal 0]
pathFilter SetTerminationTime [lindex $TimeValuesOriginal end]
pathFilter SetForceReinjectionEveryNSteps $(.options,reinjection)
#pathFilter ComputeVorticityOff
pathFilter SetTerminalSpeed 50

vtkOutlineFilter outline
outline SetInputConnection [pathFilter GetOutputPort]
vtkPolyDataMapper outlineMapper
  outlineMapper SetInputConnection [outline GetOutputPort]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

vtkTriangleFilter splitFilter
splitFilter SetInputConnection [pathFilter GetOutputPort]
splitFilter PassVertsOn

vtkThreshold thresholdLines
thresholdLines SetInputConnection [splitFilter GetOutputPort]
thresholdLines SetInputArrayToProcess 0 0 0 0 "ParticleAge"

vtkThresholdPoints thresholdPoints
thresholdPoints SetInputConnection [thresholdLines GetOutputPort]
thresholdPoints ThresholdByUpper 0
thresholdPoints SetInputArrayToProcess 0 0 0 0 "ParticleAge"

vtkSphereSource sphere
sphere SetRadius 1

vtkGlyph3D particleGlyph
particleGlyph SetSourceConnection [sphere GetOutputPort]
particleGlyph SetInputConnection [thresholdPoints GetOutputPort]
particleGlyph ScalingOff
particleGlyph SetColorModeToColorByVector
particleGlyph OrientOff

vtkPolyDataMapper glyphMapper
glyphMapper SetInputConnection [particleGlyph GetOutputPort]
glyphMapper SetScalarModeToUsePointFieldData
glyphMapper UseLookupTableScalarRangeOn
glyphMapper SelectColorArray "VectorMagnitude"
glyphMapper SetLookupTable lut

vtkActor glyphActor
glyphActor SetMapper glyphMapper
[glyphActor GetProperty] SetOpacity 0.8

vtkDataSetMapper pathMapper
pathMapper SetInputConnection [thresholdLines GetOutputPort]
pathMapper SetScalarModeToUsePointFieldData
pathMapper UseLookupTableScalarRangeOn
pathMapper SelectColorArray "Velocity"
pathMapper SetLookupTable lut

vtkActor pathActor
pathActor SetMapper pathMapper
pathActor VisibilityOn
[pathActor GetProperty] SetOpacity 0.8

vtkInteractorStyleTrackballCamera style
[$renWin GetInteractor] SetInteractorStyle style

ren AddActor pathActor
ren AddActor glyphActor
#ren AddActor outlineActor
[ren GetActiveCamera] Zoom 1.5
ren ResetCamera
$renWin Render

proc Exit { } {
  ::vtk::cb_exit
}


proc Apply { } {
  set reinjection $::(.options,reinjection)
  if {$reinjection != [pathFilter GetForceReinjectionEveryNSteps]} {
    pathFilter SetForceReinjectionEveryNSteps $reinjection
  }
  set resample $::(.options,resample)
  if {$resample != [timeInterp GetResampleFactor]} {
    set ::TimeValuesResampled [ResampleTimeValues $::TimeValuesOriginal $resample]
    timeInterp SetResampleFactor $resample
  }
  $::renWin Render
}

.scTime configure  -command OnChangeStep

proc WriteFrameToVideo { videoWriter {first 0}} {
  vtkWindowToImageFilter w2i
  w2i SetInput $::renWin
  w2i Update
  $videoWriter SetInputConnection [w2i GetOutputPort]
  if {$first} {
    videoWriter Start
  }
  $videoWriter Write
  w2i Delete
  update
}

proc RecordAnimation { } {
  set resampleFactor [timeInterp GetResampleFactor]
  #vtkFFMPEGWriter videoWriter
  vtkOggTheoraWriter videoWriter
  videoWriter SetQuality 2
  videoWriter SetFileName "/tmp/particle.mpeg"
  videoWriter SetRate \
      [expr {[ds GetNeededFrameRate] * $resampleFactor}]
  foreach {_t0 _t1} $::TimeValuesResampled break
  set eps [expr {($_t1 - $_t0)*1.5}]
  set eps1 [expr {($_t1 - $_t0)*0.5}]
  set t0 [lindex $::TimeValuesResampled 0]
  set first 1
  foreach a {1} {
    set i0 0
    set _ct $::CurrentStep
    OnChangeStep 0
    set ::CurrentStep $_ct
    WriteFrameToVideo videoWriter 1
    set traceLength $::(.options,trace)
    for {set i1 1} {$i1 < $traceLength} {incr i1} {
      set t1 [lindex $::TimeValuesResampled $i1]
      #thresholdLines ThresholdBetween [expr {$t0 - $eps1}] [expr {$t1 + $eps1}]
      thresholdLines ThresholdBetween $t0 $t1
      thresholdLines Update
      update
      thresholdPoints ThresholdBetween [expr {$t1 - $eps1}] [expr {$t1 + $eps1}]
      thresholdPoints Update
      update
      if { ($i1 > $resampleFactor) && !($i1 % $resampleFactor) } {
        passImage SetInputData [ds GetVelocityImage -component MAG -vtk yes -timestep [ expr { $i1 / $resampleFactor } ] ]
      }
      $::renWin Render 
      WriteFrameToVideo videoWriter
    }
    incr i0
    while {$i1 < [llength $::TimeValuesResampled]} {
      set t0 [lindex $::TimeValuesResampled $i0]
      set t1 [lindex $::TimeValuesResampled $i1]
      thresholdLines ThresholdBetween $t0 $t1
      thresholdLines Update
      #thresholdLines ThresholdBetween [expr {$t0 - $eps1}] [expr {$t1 + $eps1}]
      thresholdPoints Update
      update
      thresholdPoints ThresholdBetween [expr {$t1 - $eps1}] [expr {$t1 + $eps1}]
      thresholdPoints Update
      update
      if { !($i1 % $resampleFactor) } {
        passImage SetInputData [ds GetVelocityImage -component MAG -vtk yes -timestep [ expr { $i1 / $resampleFactor } ] ]
      }
      $::renWin Render
      WriteFrameToVideo videoWriter
      incr i0
      incr i1
    }
  }
  videoWriter End
  videoWriter Delete
  OnChangeStep $::CurrentStep
}

proc TestChangeStep { t0 t1 } {
}

proc OnChangeStep { i } {
  set ::CurrentStep $i
  if {$i == [expr {[llength $::TimeValuesOriginal] - 1}]} {
    set ti [lindex $::TimeValuesResampled end-1]
  } else {
    set ti [lindex $::TimeValuesOriginal $i]
  }
  foreach {t0 t1} $::TimeValuesResampled break
  thresholdLines ThresholdBetween $t0 $ti
  set eps [expr {($t1 - $t0)*0.5}]
  thresholdPoints ThresholdBetween [expr {$ti - $eps}] [expr {$ti + $eps}]
  set imgMag [ds GetVelocityImage -component MAG -vtk yes -timestep $i]
  passImage SetInputData $imgMag
  $::renWin Render
}

