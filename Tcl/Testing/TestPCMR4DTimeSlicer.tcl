lappend auto_path ../lib

package require TKUtil
package require ITKUtil
package require PCMR4DWidgets

ITKUtil::LoadITKTcl

PCMR4DDataSet ds
#ds Open ../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL
#ds Open ../../../../biomedical/data/Studies/Francesc
ds Open ../../../../biomedical/data/Studies/CreuB_CONNAV

text .txt
PCMR4DTimeSlicer .tscroller -command {puts "active: t=%i, ts=%t"}
grid .txt -row 0 -column 0 -sticky snew
grid .tscroller -row 1 -column 0 -sticky ew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
.tscroller configure -dataset ds 
