lappend auto_path /usr/local/src/TCL/tcllib/tcllib/modules
set auto_path [linsert $auto_path 0 [file normalize ../lib]]

package require vtk
package require Pcmr4d

puts $argc

if {$argc != 5} {
  puts "usage: $argv0 input_mesh prefix_output sx sy sz"
  exit
}

set input_mesh [lindex $argv 0]
if {![file exists $input_mesh]} {
  puts "argument input_mesh='$input_mesh' does not exist"
  exit
}


vtkXMLUnstructuredGridReader reader
reader SetFileName $input_mesh
reader Update

SampleCFDMeshToStudy [reader GetOutput] {*}[lrange $argv 1 4] 50 8 0.1

puts BYE!
exit
