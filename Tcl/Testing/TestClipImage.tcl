lappend auto_path ../lib
package require ClipImage
package require Img

set png [lindex $argv 0]
set img [image create photo -file $png]

ClipImage .ci -image $img -background white
pack .ci -fill both
.ci configure -boxvisible 1