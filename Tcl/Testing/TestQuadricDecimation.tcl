package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

vtkQuadricDecimation reduce
  reduce SetInputConnection [reader GetOutputPort]
  reduce SetTargetReduction 0.5

if {[file extension [lindex $argv 1]] eq ".stl"} {
  vtkSTLWriter writer
  writer SetFileTypeToBinary
} else {
  vtkGenericDataObjectWriter writer
}
writer SetInputConnection [reduce GetOutputPort]
writer SetFileName [lindex $argv 1]
writer Update

puts BYE!
exit
