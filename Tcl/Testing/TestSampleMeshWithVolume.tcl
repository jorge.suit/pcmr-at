package require vtk

if {$argc != 5} {
  puts "usage: $argv0 input_mesh output_volume nx ny nz"
  exit
}

set input_mesh [lindex $argv 0]
if {![file exists $input_mesh]} {
  puts "argument input_mesh='$input_mesh' does not exist"
  exit
}

set output_volume [lindex $argv 1]
if {![file writable [file dirname $output_volume]]} {
  puts "argument output_volume='$output_volume' is not writable"
  exit
}

set dim_args {nx ny nz}
for {set i 2} {$i < 5} {incr i} {
  set n [lindex $argv $i]
  if {[string is integer $n] && $n > 0} {
    lappend dimensions $n
  } else {
    puts "argument [lindex $dim_args [expr {$i-2}]] = $n is invalid, must positive integer"
    exit
  }
}

vtkXMLUnstructuredGridReader reader

reader SetFileName [lindex $argv 0]

vtkPassArrays pass
pass SetInputConnection [reader GetOutputPort]
pass ClearArrays
pass AddPointDataArray "Velocity"

vtkTransform transformToMM
transformToMM Scale 10 10 10

vtkTransformFilter filterToMM
filterToMM SetTransform transformToMM
filterToMM SetInputConnection [pass GetOutputPort]

vtkArrayCalculator convertToCMperS
convertToCMperS SetInputConnection [filterToMM GetOutputPort]
convertToCMperS AddVectorArrayName "Velocity" 0 1 2
convertToCMperS SetFunction "Velocity*1"
convertToCMperS SetResultArrayName "Velocity"

vtkXMLUnstructuredGridWriter writerVTU
writerVTU SetInputConnection [filterToMM GetOutputPort]
writerVTU SetFileName /tmp/transformedVTU.vtu
writerVTU Update

convertToCMperS Update

set bounds [[convertToCMperS GetOutput] GetBounds]
puts "bounds = $bounds"

foreach {min max} $bounds n $dimensions {
  set d [expr {$max - $min}]
  lappend extends $d
  set x [expr {$d*0.2}]
  set new_min [expr {$min - $x}]
  lappend origin $new_min
  lappend spacing [expr {($d + 2*$x) / $n}]
  lappend new_extend $new_min [expr {$max + $x}]
}

puts "extends = $extends"

vtkImageData volume
volume SetOrigin {*}$origin
volume SetDimensions {*}$dimensions
volume SetSpacing {*}$spacing

if {0} {
  vtkProbeFilter filterSampler
  filterSampler SetInputData volume
  filterSampler SetSourceConnection [convertToCMperS GetOutputPort]
  filterSampler Update
#puts [[aa GetOutput] Print]
#exit
} elseif {0} {
  vtkGaussianSplatter filterSampler
  filterSampler SetInputConnection [convertToCMperS GetOutputPort]
  filterSampler SetRadius 0.01
  filterSampler ScalarWarpingOn
  filterSampler SetExponentFactor -5
  filterSampler SetSampleDimensions {*}$dimensions
  filterSampler SetInputArrayToProcess 0 0 0 0 "Velocity"
  puts [filterSampler Print]
} else {
  vtkExtractVectorComponents extract
  extract SetInputConnection [convertToCMperS GetOutputPort]

  vtkShepardMethod filterSampler
  filterSampler SetModelBounds {*}$bounds
  filterSampler SetInputConnection [extract GetOutputPort 2]
  filterSampler SetMaximumDistance 0.1
  filterSampler SetSampleDimensions {*}$dimensions
}

if {0} {
vtkAssignAttribute aa
aa SetInputConnection [filterSampler GetOutputPort]
aa Assign VECTORS SCALARS POINT_DATA
vtkImageExtractComponents extractVx
extractVx SetInputConnection [aa GetOutputPort]
extractVx SetComponents 0
extractVx Update
puts [[extractVx GetOutput] Print]
exit
}

vtkStructuredPointsWriter writer
writer SetInputConnection [filterSampler GetOutputPort]
writer SetFileName $output_volume
writer Update

exit
