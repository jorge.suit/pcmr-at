source TestHelper.tcl
source ../lib/PCMR4DWidgets/PropertyContainer.tcl
source ../lib/PCMR4DWidgets/StreamlineProperties.tcl

StreamlineProperties .stl

pack .stl -fill both -expand yes

hook bind .stl <PropertyApply> . TestApply

proc TestApply { args } {
  puts "TestApply $args"
  array set arg $args
  puts "TargetType = [$arg(-container) GetTargetType]"
}
