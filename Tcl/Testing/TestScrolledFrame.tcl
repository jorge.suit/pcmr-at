# ==============================
#
# demo
#
# ==============================

package require Ttk

if {[catch {package require ScrolledFrame}]} {
  source [file join [file dirname [info script]] ScrolledFrame.tcl]
  package require ScrolledFrame
}
namespace import ::ScrolledFrame::ScrolledFrame

ScrolledFrame .sf -height 150 -width 100 \
    -xscrollcommand {.hs set} -yscrollcommand {.vs set}  -fill both ;# try both, x, y or none
ttk::scrollbar .vs -command {.sf yview}
ttk::scrollbar .hs -command {.sf xview} -orient horizontal
grid .sf -row 0 -column 0 -sticky nsew
grid .vs -row 0 -column 1 -sticky ns
grid .hs -row 1 -column 0 -sticky ew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
set f [.sf getframe]
#  .sf configure -bg white

grid columnconfigure $f 0 -weight 1

foreach i {0 1 2 3 4 5 6 7 8 9} {
  label $f.l$i -text "Hi! I'm the scrolled label $i" -relief groove
  grid $f.l$i -row $i -sticky ew
  #pack $f.l$i -padx 10 -pady 2 -anchor nw -fill x
}
label $f.lf -text "Hi! I take up the slack!" -relief groove
grid $f.lf -row 10 -sticky snew
#pack  $f.lf -in $f -padx 10 -pady 2 -anchor nw -fill both -expand 1
grid rowconfigure $f 10 -weight 1