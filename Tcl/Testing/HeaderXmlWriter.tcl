package require snit
package require tdom

snit::type HeaderXmlWriter {

  typevariable GlobalProperties {
    FormatVersion
    SourceSequence
    TimeValues NumberOfTimeSteps LengthOfTimeStep
    VelocityEncoding MinimumVelocity MaximumVelocity
    ImageOrientation InvertComponentX InvertComponentY InvertComponentZ
  }

  variable Properties -array {
    FormatVersion "0.5"
    SourceSequence "unknown"
    ImageOrientation "ASL"
    InvertComponentX 0
    InvertComponentY 1
    InvertComponentZ 0
    MinimumVelocity 0
  }

  constructor { args } {
  }
  
  method SetFormatVersion { version } {
    set Properties(FormatVersion) $version
  }

  method SetSourceSequence { path } {
    set Properties(SourceSequence) $path
  }

  method SetTimeValues { values } {
    set ll [llength $values]
    if {!$ll} {
      error "empty list of time values provided"
    }
    set v0 [lindex $values 0]
    if {![string is double $v0] || $v0 < 0} {
      error "list of time-steps must be positive"
    }
    set delta 0
    foreach v [lrange $values 1 end] {
      if {![string is double $v] || $v <= $v0} {
        error "list of time-steps must be an increasing sequence of numbers"
      }
      set delta [expr {$v - $v0}]
      set v0 $v
    }
    if {$ll > 1} {
      set delta  [expr {$delta/($ll-1)}]
    }
    set Properties(TimeValues) $values
    $self SetLengthOfTimeStep $delta
    $self SetNumberOfTimeSteps $ll
  }

  method SetLengthOfTimeStep { delta } {
    if {![string is double $delta] || $delta < 0} {
      error "length of time-step must be positive number"
    }    
    set Properties(LengthOfTimeStep) $delta
  }

  method SetNumberOfTimeSteps { n } {
    if {![string is integer $n] || $n < 0} {
      error "number of time-step must be positive integer"
    }    
    set Properties(NumberOfTimeSteps) $n
  }
  

  method SetTimeInfo {n t0 delta} {
    for {set i 0}  {$i < n} {incr i} {
      lappend values [expr {$t0 + $i*$delta}]
    }
    set Properties(TimeValues) $values
    $self SetLengthOfTimeStep $delta
    $self SetNumberOfTimeSteps $n
  }

  method SetVelocityEncoding { venc } {
    if {![string is double $venc] || $venc < 0} {
      error "velocity encoding must be positive number"
    }    
    set Properties(VelocityEncoding) $venc
    if {![info exists Properties(MaximumVelocity)]} {
      set Properties(MaximumVelocity) $venc
    }
  }

  method SetMinimumVelocity { v } {
    if {![string is double $v] || $v < 0} {
      error "minimum velocity must be positive number"
    }    
    set Properties(MinimumVelocity) $v
  }

  method SetMaximumVelocity { v } {
    if {![string is double $v] || $v < 0} {
      error "maximum velocity must be positive number"
    }    
    set Properties(MaximumVelocity) $v
  }

  method createChildNode {doc p k v} {
    set child [$doc createElement $k]
    $child appendChild [$doc createTextNode $v]
    $p appendChild $child
  }

  method WriteToFile { path } {
    set doc [dom createDocument Study]
    set root [$doc documentElement]
    foreach p [array names Properties] {
      if {$p eq "TimeValues"} {
        set child [$doc createElement "TimeValues"]
        foreach t $Properties(TimeValues) {
          $self createChildNode $doc $child "Time" $t
        }
        $root appendChild $child
      } else {
        $self createChildNode $doc $root $p $Properties($p)
      }
    }
    set fd [open $path w]
    puts -nonewline $fd [$doc asXML]
    close $fd
    $doc delete
  }
}
