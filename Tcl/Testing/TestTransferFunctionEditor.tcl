package require vtk

vtkContextView view
[view GetRenderer] SetBackground 1 1 1
[view GetRenderWindow] SetSize 400 300
vtkChartXY chart
#chart SetTitle "Chart"
chart ForceAxesToBoundsOn
[view GetScene] AddItem chart

vtkLookupTable lookupTable
lookupTable Build

vtkColorTransferFunction colorTransferFunction
colorTransferFunction AddHSVSegment 0 0 1 1 0.3333 0.3333 1 1
colorTransferFunction AddHSVSegment 0.3333 0.3333 1 1 0.6666 0.6666 1 1
colorTransferFunction AddHSVSegment 0.3333 0.3333 1 1 1 0 1 1
colorTransferFunction Build

vtkPiecewiseFunction opacityFunction
opacityFunction AddPoint 0.2 0.0
opacityFunction AddPoint 0.5 0.5
opacityFunction AddPoint 1.0 1.0

vtkCompositeTransferFunctionItem item1
item1 SetColorTransferFunction colorTransferFunction
item1 SetOpacityFunction opacityFunction
chart AddPlot item1

vtkCompositeControlPointsItem item2
item2 SetOpacityFunction opacityFunction
item2 SetColorTransferFunction colorTransferFunction
chart AddPlot item2

[view GetRenderWindow] SetMultiSamples 1
[view GetInteractor] Initialize
[view GetInteractor] Start
