package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} elseif {[file extension [lindex $argv 0]] eq ".vtp"} {
  vtkXMLPolyDataReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

set dataSet [reader GetOutput]
reader Update

set cell [vtkGenericCell New]
puts "DataSet has [$dataSet GetNumberOfCells] cells"
$dataSet GetCell 818 $cell
$dataSet GetCell 819 $cell
$dataSet GetCell 820 $cell

puts [$cell Print]
