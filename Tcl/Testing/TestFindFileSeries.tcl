if {$argc != 1} {
  puts "usage: $argv0 input_dir"
  exit
}

source FindFileSeries.tcl

FindFileSeries fileSeries

set msg [fileSeries Search [lindex $argv 0] "*.vtu"]

if {$msg != ""} {
  puts $msg
  exit
}

set series [fileSeries GetSeriesNames]
puts "[llength $series] series were found"
foreach s $series {
  puts "  $s"
}

foreach f [fileSeries GetFilesInSerie [lindex $series 0]] {
  puts $f
}

exit
