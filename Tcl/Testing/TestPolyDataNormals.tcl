package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

vtkPolyDataNormals normals
normals SetInputConnection [reader GetOutputPort]
normals AutoOrientNormalsOn
normals Update

#puts [[reader GetOutput] Print]
#puts "================================================================="
#puts [[normals GetOutput] Print]

vtkPolyDataWriter writer
writer SetFileName "/tmp/normals.vtk"
writer SetInputConnection [normals GetOutputPort]
writer Update
puts "Escrito /tmp/normals.vtk"

vtkImplicitPolyDataDistance distance
distance SetInput [normals GetOutput]

#puts [[reader GetOutput] DescribeMethods]
#puts [[reader GetOutput] GetBounds]

set bounds [[reader GetOutput] GetBounds]
foreach {min max} $bounds {
  set d [expr {$max -$min}]
  lappend extends $d
  set x [expr {$d*0.2}]
  lappend new_extend [expr {$min - $x}] [expr {$max + $x}]
}

puts "new_extend = $new_extend"

vtkSampleFunction sample
    #sample SetSampleDimensions 100 100 100
    sample SetSampleDimensions 26 56 26
    sample SetImplicitFunction distance
    sample ComputeNormalsOff
    #sample SetModelBounds {*}[[reader GetOutput] GetBounds]
    sample SetModelBounds {*}$new_extend
    #sample SetModelBounds -6.5 6 -14 13.5 -6.5 6
    sample Update

vtkGenericDataObjectWriter sampleWriter
sampleWriter SetInputConnection [sample GetOutputPort]
sampleWriter SetFileName "/tmp/sample.vtk"
sampleWriter Update

exit
