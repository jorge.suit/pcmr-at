package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} elseif {[file extension [lindex $argv 0]] eq ".vtp"} {
  vtkXMLPolyDataReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

vtkFillHolesFilter filler
  filler SetInputConnection [reader GetOutputPort]
  filler SetHoleSize 10000

if {[file extension [lindex $argv 1]] eq ".stl"} {
  vtkSTLWriter writer
} else {
  vtkGenericDataObjectWriter writer
}
writer SetInputConnection [filler GetOutputPort]
writer SetFileName [lindex $argv 1]
writer Update

puts BYE!
exit
