lappend auto_path ../lib

package require HeaderXml

set headerXML [HeaderXmlWriter %AUTO%]
puts "headerXML = $headerXML"

#$headerXML SetTimeValues {0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0}
$headerXML SetTimeInfo 10 0 0.1

$headerXML SetVelocityEncoding 150.0

$headerXML WriteToFile /tmp/header.xml

$headerXML destroy
