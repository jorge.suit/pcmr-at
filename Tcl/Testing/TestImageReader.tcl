lappend auto_path ../lib

package require ITKUtil

ITKUtil::LoadITKTcl

set reader [Image3DReader]
$reader SetFileName ~/Data/pcmr/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL
$reader Update
