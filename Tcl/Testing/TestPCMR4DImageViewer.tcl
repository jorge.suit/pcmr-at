lappend auto_path ../lib

package require TKUtil
package require ITKUtil
package require PCMR4DWidgets

ITKUtil::LoadITKTcl

PCMR4DImageViewer .iv

grid .iv -row 0 -column 0 -sticky snew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1

update idletask
wm geometry . [winfo geometry .]

PCMR4DDataSet ds
ds Open [file normalize ~/Data/pcmr/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL]
.iv configure -dataset ds
