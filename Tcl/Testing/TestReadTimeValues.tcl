package require tdom

set path "sample_header_01.xml"

set fd [open $path]
set xmlBuffer [string trim [read $fd]]
close $fd

set doc [dom parse $xmlBuffer]
set root [$doc documentElement]

set nodeTimeValues [$root selectNodes TimeValues]

foreach tnode [$nodeTimeValues childNodes] {
    set v [$tnode selectNodes {string()}]
    puts "  TimeValue = $v"
}