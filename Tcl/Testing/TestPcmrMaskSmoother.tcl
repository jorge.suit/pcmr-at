package require vtkPcmrExtTCL
package require vtk

if { $argc != 2 } {
  puts "usage: $argv0 mask_file.vtk poly_output.vtk"
  exit
}

set ext [file extension [lindex $argv 0]]
if { $ext eq ".vtk" } {
  vtkStructuredPointsReader reader
} elseif { $ext eq ".vti" } {
  vtkXMLImageDataReader reader
} else {
  puts "Unrecognized file $ext"
  exit
}
reader SetFileName [lindex $argv 0]
reader Update

vtkPcmrMaskSmoother smoother

smoother SetInputMask [reader GetOutput]
smoother SetPadding 8 8 8

vtkPolyDataWriter writer
writer SetFileName [lindex $argv 1]
writer SetInputData [smoother GetBoundarySmoothed]
writer Update
exit
