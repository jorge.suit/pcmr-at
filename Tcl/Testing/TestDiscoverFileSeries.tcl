if {$argc != 1} {
  puts "usage: $argv0 input_dir"
  exit
}

set input_dir [lindex $argv 0]
if {![file exists $input_dir] || ![file isdir $input_dir]} {
  puts "argument input_dir='$input_dir' must be a directory"
  exit
}

foreach f [glob -nocomplain -dir $input_dir *.vtu] {
  set name [string range [file tail $f] 0 end-4]
  if {[regexp {(.+?)(\d+)$} $name ==> k n]} {
    lappend mapSeries($k) [list $f $n]
  }
}


exit
