package require snit

snit::type FindFileSeries {

  variable Series -array {}

  constructor {args} {
  }

  method GetSeriesNames { } {
    return [array names Series]
  }

  method GetFilesInSerie { serie } {
    if {[info exists Series($serie)]} {
      return $Series($serie)
    }
    return ""
  }

  method Search { dir pattern } {
    if {![file exists $dir] || ![file isdir $dir]} {
      return "argument dir='$dir' must be a directory"
    }
    foreach f [glob -nocomplain -dir $dir {*}$pattern] {
      set ext [file extension $f]
      set name [string range [file tail $f] 0 end-4]
      if {[regexp {(.+?)(\d+)$} $name ==> k n]} {
        set i [string trimleft $n 0]
        lappend mapSeries(${k}*${ext}) [list $f [expr {$i eq "" ? 0 : $i}]]
      }
    }
    foreach s [array names mapSeries] {
      foreach e [lsort -integer -index 1 $mapSeries($s)] {
        lappend Series($s) [lindex $e 0]
      }
    }
  }
}
