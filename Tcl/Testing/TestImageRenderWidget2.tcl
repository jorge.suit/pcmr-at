lappend auto_path ../lib

package require ITKUtil
package require VolumeOrienter
package require PCMR4DWidgets

set infile [lindex $argv 0]
puts "reading image file $infile"

vtkStructuredPointsReader reader
  reader SetFileName $infile
  reader Update

set img [reader GetOutput]
foreach {minValue maxValue} [$img GetScalarRange] break
#set minValue [ $minmax GetMinimum ]
#set maxValue [ $minmax GetMaximum ]
puts "minValue = $minValue"
puts "maxValue = $maxValue"

MedicalVolumeOrienter volOrienter
volOrienter SetOrientation "RSA"
ImageRenderWidget .irw -volumeorienter volOrienter

# set img [ds GetVelocityImage -component MAG -timestep 3 -vtk yes]

.irw SetImageData $img -type MAG
#.irw SetImageData [ds GetVelocityImage -component FLOW -timestep 3 -vtk yes] -type velocity
.irw AlignToView Coronal
grid .irw -row 0 -column 0 -sticky snew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
