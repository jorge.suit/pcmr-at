#  envivtk_6.1 TestQuantifyPlane.tcl ../../../../biomedical/data/Studies/PMI/PMI_4DFLOW_1 ../../../../biomedical/data/Studies/PMI/PMI_4DFLOW_1/WSR/Antialiased_d0.stl 50.5710741965025 26.5460878155726 170.147472957907 0 -1 0

# envivtk_6.1 TestQuantifyPlane.tcl ../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL 3 /tmp/boundary_siemens.ply 118.0 31.58 207.22 0 -1 0

# envivtk_6.1 TestQuantifyPlane.tcl ../../../../biomedical/data/Studies/4DFLOW_SampleDataset_Aorta_3D_24SL 3  AntiAliasedDistanceField_0_FromGiD.ply 118.0 31.58 207.22 0 -1 0

# para el caso 3 SAo
# IN_AAo envivtk_6.1 TestQuantifyPlane.tcl ../../../../biomedical/data/Studies/PMI/PMI_4DFLOW_1 ../../../../biomedical/data/Studies/PMI/PMI_4DFLOW_1/WSR/Antialiased_d0_3SAo_SwapedGID.stl 55.3597574343459 57.6823785470428 165.878683099601 0 -1 0

if {$argc != 9} {
  puts "Usage:"
  puts "    $argv0 <StudyPath> <t> <BoundaryFile> <px py pz> <nx ny nz>"
  exit -1
}

source "TestHelper.tcl"

package require PCMR4DWidgets

PCMR4DDataSet ds

ds Open [lindex $argv 0]
puts "data-set opened"

# load the boundary
if {[file ext [lindex $argv 2]] eq ".stl"} {
  vtkSTLReader readerBoundary
} else {
  vtkGenericDataObjectReader readerBoundary
}
#vtkPLYReader readerBoundary
readerBoundary SetFileName [lindex $argv 2]
# REVIEW: Check here that only 2D boundary are read

# define the cut plane
set normalPlane [lrange $argv 6 8]
vtkPlane planeCut
planeCut SetOrigin {*}[lrange $argv 3 5]
planeCut SetNormal {*}$normalPlane

# define the cutter
vtkCutter cutter
cutter SetInputConnection [readerBoundary GetOutputPort]
cutter SetCutFunction planeCut
cutter GenerateValues 1 0 0

# define the streaper
vtkStripper stripper
stripper SetInputConnection [cutter GetOutputPort]

stripper Update

vtkPolyDataWriter writerPoly
writerPoly SetFileName /tmp/cut.vtk
writerPoly SetInputConnection [stripper GetOutputPort]
writerPoly Update

# need to build a planeSource to sample the data, this plane source
# should cover the bounding box of the polyline
vtkPlaneSource planeSource
planeSource SetNormal {*}$normalPlane

set contours [stripper GetOutput]

proc ScalePointInPlane {point center d} {
  set newp [list]
  foreach xp $point xc $center {
    lappend newp [expr {$xc + $d*($xp-$xc)}]
  }
  return $newp
}

set flowData [ds GetVelocityImage -component FLOW -vtk yes -timestep 3]
vtkProbeFilter probePlane
probePlane SetInputConnection [planeSource GetOutputPort]
probePlane SetSourceData $flowData

for {set i 0} {$i < [$contours GetNumberOfCells]} {incr i} {
  set cell [$contours GetCell $i]
  if {[$cell GetClassName] eq "vtkPolyLine"} {
    puts [$cell GetBounds]
    set l [expr {sqrt([$cell GetLength2])}]
    set center [list]
    foreach {c0 c1} [$cell GetBounds] {
      lappend center [expr {($c0+$c1)/2.0}]
    }
    planeSource SetCenter {*}$center
    set _p1 [planeSource GetPoint1]
    set p1 [ScalePointInPlane $_p1 $center $l]
    set _p2 [planeSource GetPoint2]
    set p2 [ScalePointInPlane $_p2 $center $l]
    set _o [planeSource GetOrigin]
    set o [ScalePointInPlane $_o $center $l]
    planeSource SetPoint1 {*}$p1
    planeSource SetPoint2 {*}$p2
    planeSource SetOrigin {*}$o
    VTKUtil::SetPlaneElementSize planeSource 1
    writerPoly SetFileName /tmp/plane${i}.vtk
    writerPoly SetInputConnection [probePlane GetOutputPort]
    writerPoly Update
    planeSource SetPoint1 {*}$_p1
    planeSource SetPoint2 {*}$_p2
    planeSource SetOrigin {*}$_o
    
  }
}


# line GetPoints return the list of points

# need to build a closed parametric spline with points of the polyline


#proc QuantifyAtContour {pline normal

puts BYE!
exit 0
