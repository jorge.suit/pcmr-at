package require vtk

if {[file extension [lindex $argv 0]] eq ".vtp"} {
  vtkXMLPolyDataReader reader
} else {
  vtkGenericDataObjectReader reader
}

reader SetFileName [lindex $argv 0]
reader Update

set pdInput [reader GetOutput]

puts "The input has [$pdInput GetNumberOfLines]"

vtkCleanPolyData clean
clean SetInputConnection [reader GetOutputPort]

vtkStripper strip
strip SetInputConnection [clean GetOutputPort]
strip Update

puts "The input cleaned/strip has [[strip GetOutput] GetNumberOfLines]"

set pdStripped [strip GetOutput]
set linesSource [$pdStripped GetLines]
set maxSize 0
set idAtMax 0
for {set i 0} {$i < [$pdStripped GetNumberOfCells]} {incr i} {
  set cell [$pdStripped GetCell $i]
  if {$maxSize < [$cell GetNumberOfPoints]} {
    set idAtMax $i
    set maxSize [$cell GetNumberOfPoints]
  }
}
vtkGenericCell cellMax
$pdStripped GetCell $idAtMax cellMax

vtkPolyData pdOutput
vtkPoints pointsOutput
vtkPolyLine polyLine
pointsOutput SetNumberOfPoints [cellMax GetNumberOfPoints]
[polyLine GetPointIds] SetNumberOfIds [cellMax GetNumberOfPoints];
for {set i 0} {$i < [cellMax GetNumberOfPoints]} {incr i} {
  set id [ cellMax GetPointId $i ]
  set pt [ $pdStripped GetPoint $id ]
  pointsOutput SetPoint $i {*}$pt
  [polyLine GetPointIds] SetId $i $i
}
vtkCellArray cells
cells InsertNextCell polyLine

pdOutput SetPoints pointsOutput
pdOutput SetLines cells

puts [pdOutput Print]

vtkPolyDataWriter writer
writer SetFileName [lindex $argv 1]
writer SetInputData pdOutput
writer Update
exit
