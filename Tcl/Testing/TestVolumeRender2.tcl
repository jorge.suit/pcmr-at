package require vtk
package require vtkinteraction
package require itktcl

vtkTkRenderWidget .tkrw
::vtk::bind_tk_render_widget .tkrw

set renWin [.tkrw GetRenderWindow]
vtkRenderer ren
ren SetBackground 0.0 0.4 0.5
$renWin AddRenderer ren

set infile [lindex $argv 0]
puts "reading image file $infile"

vtkStructuredPointsReader reader 
  reader SetFileName $infile
  reader Update
set exporter [ImageToVTKImageFilter_Float]

set img [reader GetOutput]
foreach {minValue maxValue} [$img GetScalarRange] break
#set minValue [ $minmax GetMinimum ]
#set maxValue [ $minmax GetMaximum ]

puts "minValue = $minValue"
puts "maxValue = $maxValue"

set x0 [ expr { ($maxValue+$minValue)/32.0 } ]
# Create transfer mapping scalar value to opacity
vtkPiecewiseFunction opacityTransferFunction

# Create transfer mapping scalar value to color
vtkColorTransferFunction colorTransferFunction

# The property describes how the data will look
vtkVolumeProperty volumeProperty
    volumeProperty SetColor colorTransferFunction
    volumeProperty SetScalarOpacity opacityTransferFunction
    volumeProperty ShadeOn
    volumeProperty SetInterpolationTypeToLinear

    #opacityTransferFunction AddPoint  $x0   0.0
if { 1 } {
    opacityTransferFunction AddPoint  $minValue   0.0
    opacityTransferFunction AddPoint  [expr {$maxValue*0.5}]   0.7
    opacityTransferFunction AddPoint  [expr {$maxValue*0.6}]   0.6
    opacityTransferFunction AddPoint  $maxValue   0.2
} else {
    opacityTransferFunction AddPoint  $minValue 0.0
    opacityTransferFunction AddPoint  $maxValue 1.0
}
    #opacityTransferFunction AddPoint  $maxValue 0.7

    #colorTransferFunction AddRGBPoint  $minValue 0.2 0.3 0.5
    #colorTransferFunction AddRGBPoint  $maxValue 0.0 0.5 0.8

# The mapper / ray cast function know how to render the data
#vtkVolumeRayCastCompositeFunction  compositeFunction
#vtkVolumeRayCastMapper volumeMapper
#vtkFixedPointVolumeRayCastMapper volumeMapper
vtkSmartVolumeMapper volumeMapper
    volumeMapper SetInputData [reader GetOutput]
    volumeMapper SetBlendModeToMaximumIntensity
    #volumeMapper AutoAdjustSampleDistancesOn 	
    #volumeMapper SetImageSampleDistance 1.0
    #volumeMapper SetSampleDistance 0.1

# The volume holds the mapper and the property and
# can be used to position/orient the volume
vtkVolume volume
    volume SetMapper volumeMapper
    volume SetProperty volumeProperty

# Create a composite orientation marker using
# vtkAnnotatedCubeActor and vtkAxesActor.
#
vtkAnnotatedCubeActor cube
  cube SetXPlusFaceText  "R"
  cube SetXMinusFaceText "L"
  cube SetYPlusFaceText  "P"
  cube SetYMinusFaceText "A"
  cube SetZPlusFaceText  "I"
  cube SetZMinusFaceText "S"
  cube SetXFaceTextRotation 180
  cube SetYFaceTextRotation 180
  cube SetZFaceTextRotation 90
  cube SetFaceTextScale 0.65
  set property [ cube GetCubeProperty ]
  $property SetColor 0.5 1 1
  set property [ cube GetTextEdgesProperty ]
  $property SetLineWidth 1
  $property SetDiffuse 0
  $property SetAmbient 1
  $property SetColor 0.18 0.28 0.23
  set property [ cube GetXPlusFaceProperty ]
  $property SetColor 0 0 1
  $property SetInterpolationToFlat
  set property [ cube GetXMinusFaceProperty ]
  $property SetColor 0 0 1
  $property SetInterpolationToFlat
  set property [ cube GetYPlusFaceProperty ]
  $property SetColor 0 1 0
  $property SetInterpolationToFlat
  set property [ cube GetYMinusFaceProperty ]
  $property SetColor 0 1 0
  $property SetInterpolationToFlat
  set property [ cube GetZPlusFaceProperty ]
  $property SetColor 1 0 0
  $property SetInterpolationToFlat
  set property [ cube GetZMinusFaceProperty ]
  $property SetColor 1 0 0
  $property SetInterpolationToFlat

vtkAxesActor axes
  axes SetShaftTypeToCylinder
  axes SetXAxisLabelText "x"
  axes SetYAxisLabelText "y"
  axes SetZAxisLabelText "z"
  axes SetTotalLength 1.5 1.5 1.5
  vtkTextProperty tprop
  #tprop ItalicOn
  #tprop ShadowOn
  #tprop SetFontFamilyToTimes
  [ axes GetXAxisCaptionActor2D ] SetCaptionTextProperty tprop
  vtkTextProperty tprop2
  #tprop2 ShallowCopy tprop
  [ axes GetYAxisCaptionActor2D ] SetCaptionTextProperty tprop2
  vtkTextProperty tprop3
  #tprop3 ShallowCopy tprop
  [ axes GetZAxisCaptionActor2D ] SetCaptionTextProperty tprop3

# Combine the two actors into one with vtkPropAssembly ...
#
  vtkPropAssembly assembly
  assembly AddPart axes
  assembly AddPart cube

  vtkOrientationMarkerWidget marker
  marker SetOutlineColor 0.93 0.57 0.13
  marker SetOrientationMarker assembly
  marker SetViewport 0.0 0.0 0.15 0.3

ren AddVolume volume
ren SetBackground 1 1 1

marker SetInteractor [$renWin GetInteractor]
marker SetEnabled 1
marker InteractiveOff

grid .tkrw -row 0 -column 0 -sticky snew
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
