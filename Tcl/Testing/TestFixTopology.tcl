package require vtk

if {[file extension [lindex $argv 0]] eq ".stl"} {
  vtkSTLReader reader
} else {
  vtkGenericDataObjectReader reader
}
reader SetFileName [lindex $argv 0]

vtkTriangleFilter triangulate
triangulate SetInputConnection [reader GetOutputPort]

vtkFeatureEdges featureEdges

featureEdges SetInputConnection [triangulate GetOutputPort]
featureEdges FeatureEdgesOff
featureEdges BoundaryEdgesOff
featureEdges NonManifoldEdgesOn
featureEdges ColoringOff
featureEdges Update

set numberOfNMFEdges [[featureEdges GetOutput] GetNumberOfCells]
puts "There are $numberOfNMFEdges non-manifold edges"

vtkOctreePointLocator locator
locator SetDataSet [reader GetOutput]
locator BuildLocator

vtkIdList idsCell
set points [[featureEdges GetOutput] GetPoints]
for {set i 0} {$i < [$points GetNumberOfPoints]} {incr i} {
  set pt [$points GetPoint $i]
  set id [locator FindClosestPoint {*}$pt]
  #puts "pt  = $pt"
  #puts "pt' = [[[cleanPoly GetOutput] GetPoints] GetPoint $id]"
  #puts "========================================================="
  idsCell Reset
  [triangulate GetOutput] GetPointCells $id idsCell
  #puts -nonewline "NeighordCells($id) ="
  for {set j 0} {$j < [idsCell GetNumberOfIds]} {incr j} {
    #puts -nonewline " [idsCell GetId $j]"
    set cellsToRemove([idsCell GetId $j]) 1
  }
  #puts ""
}

puts "There are [llength [array names cellsToRemove]] cells to remove"

vtkPolyData pd
pd ShallowCopy [triangulate GetOutput]

foreach id [array names cellsToRemove] {
  pd DeleteCell $id
}
pd RemoveDeletedCells

# ===========

vtkPolyDataNormals normals0
normals0 SetInputData pd
normals0 ConsistencyOn
normals0 SplittingOff
normals0 Update

#REVIEW: disconnected
vtkCleanPolyData cleanPoly
cleanPoly SetInputData pd

vtkFillHolesFilter filler0
filler0 SetInputConnection [normals0 GetOutputPort]
#filler0 SetInputData pd
filler0 SetHoleSize 1000
filler0 Update

vtkFillHolesFilter filler1
filler1 SetInputConnection [filler0 GetOutputPort]
filler1 SetHoleSize 1000
filler1 Update

vtkFillHolesFilter filler2
filler2 SetInputConnection [filler1 GetOutputPort]
filler2 SetHoleSize 1000
filler2 Update

vtkFillHolesFilter filler3
filler3 SetInputConnection [filler2 GetOutputPort]
filler3 SetHoleSize 1000
filler3 Update
vtkPolyDataNormals normals
normals SetInputConnection [filler3 GetOutputPort]
normals ConsistencyOn
normals SplittingOn

vtkFillHolesFilter filler4
filler4 SetInputConnection [normals GetOutputPort]
filler4 SetHoleSize 1000
filler4 Update

if {0} {
vtkPolyDataNormals normals
normals SetInputConnection [filler1 GetOutputPort]
normals ConsistencyOn
normals SplittingOff
#normals AutoOrientNormalsOn
}

if {[file extension [lindex $argv 1]] eq ".stl"} {
  vtkSTLWriter writer
  writer SetFileTypeToBinary
} else {
  vtkGenericDataObjectWriter writer
}

writer SetInputConnection [filler1 GetOutputPort]
#writer SetInputData pd
writer SetFileName [lindex $argv 1]
writer Update

puts BYE!
exit
