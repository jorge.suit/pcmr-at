#include "vtkSplineRepresentationAddon.h"
#include "vtkTclUtil.h"
#include "vtkSplineRepresentation.h"
#include "vtkParametricSpline.h"
#include "vtkPoints.h"
#include "vtkMath.h"

int vtkSplineRepresentationAddon_Proc(ClientData clientData,
                Tcl_Interp *interp,
                int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);

  if (objc < 3)
    {
    Tcl_AppendResult(interp,
                     "vtkSplineRepresentationAddon ERROR --\n",
                     "wrong # of arguments, should be\n",
                     "    ", cmd, " SplineRepresentation ChangeHandlePosition idx x y z",
                     NULL);
    return TCL_ERROR;
    }  
  int error = 0;
  const char* argSpline = Tcl_GetString(objv[1]);
  vtkSplineRepresentation *spline = static_cast<vtkSplineRepresentation *>(
    vtkTclGetPointerFromObject(argSpline,"vtkSplineRepresentation",
                               interp, error));
  if (error || !spline)
    {
    Tcl_AppendResult(interp,
                     "vtkSplineRepresentationAddon ERROR --\n",
                     "could not convert '", argSpline, 
                     "' to vtkSplineRepresentation", NULL);
    return TCL_ERROR;
    }
  const char* subcmd = Tcl_GetString(objv[2]);
  if (!strcmp(subcmd, "ChangeHandlePosition"))
    {
    if (objc != 7)
      {
      Tcl_AppendResult(interp,
                       "vtkSplineRepresentationAddon ERROR --\n",
                       "wrong # of arguments, should be\n",
                       "    ", cmd, " SplineRepresentation ChangeHandlePosition idx x y z",
                       NULL);
      return TCL_ERROR;
      }
    int status;
    int idx;
    status = Tcl_GetIntFromObj(interp, objv[3], &idx);
    if (status != TCL_OK)
      {
      return status;
      }
    double *center = spline->GetHandlePosition(idx);
    if (!center)
      {
      Tcl_AppendResult(interp,
                       "vtkSplineRepresentationAddon ERROR --\n",
                       "could not access handle position '",
                       Tcl_GetString(objv[3]), "'", NULL);
      return TCL_ERROR;
      }
    double xyz[3];
    for (int i = 0; i < 3; i++)
      {
      status = Tcl_GetDoubleFromObj(interp, objv[4+i], xyz+i);
      if (status != TCL_OK)
        {
        return status;
        }
      }
    for (int i = 0; i < 3; i++)
      {
      center[i] = xyz[i];
      }
    return TCL_OK;
    }
  Tcl_AppendResult(interp,
                   "vtkSplineRepresentationAddon ERROR --\n",
                   "wrong subcommand '", subcmd, "', should be\n",
                   "    ", cmd, " SplineRepresentation ChangeHandlePosition idx x y z",
                   NULL);
  return TCL_ERROR;
}

int Tcl_GetPointFromObj(Tcl_Interp *interp, Tcl_Obj *obj, double p[])
{
  Tcl_Obj **objv;
  int objc;
  
  if (Tcl_ListObjGetElements(interp, obj, &objc, &objv) != TCL_OK) 
    {
    return TCL_ERROR;
    }
  if (objc != 3)
    {
    Tcl_AppendResult(interp,
                     "Wrong number of coordinates, must be a list of 3 numbers:",
                     Tcl_GetString(obj),
                     NULL);
    return TCL_ERROR;
    }
  for (int i = 0; i < 3; ++i)
    {
    if(Tcl_GetDoubleFromObj(interp, objv[i], p+i) != TCL_OK)
      {
      return TCL_ERROR;
      }
    }
  return TCL_OK;
}

int vtkSplineProjectOnPlane_Proc(ClientData clientData,
                                 Tcl_Interp *interp,
                                 int objc, Tcl_Obj *const objv[])
{
  const char* cmd = Tcl_GetString(objv[0]);

  if (objc != 4)
    {
    Tcl_AppendResult(interp,
                     "vtkSplineProjectOnPlane ERROR --\n",
                     "wrong # of arguments, should be\n",
                     "    ", cmd, " ParametricSpline Origin Normal",
                     NULL);
    return TCL_ERROR;
    }  
  int error = 0;
  const char* argSpline = Tcl_GetString(objv[1]);
  vtkParametricSpline *spline = static_cast<vtkParametricSpline *>(
    vtkTclGetPointerFromObject(argSpline,"vtkParametricSpline",
                               interp, error));
  if (error || !spline)
    {
    Tcl_AppendResult(interp,
                     "ERROR vtkSplineProjectOnPlane--\n",
                     "could not convert '", argSpline, 
                     "' to vtkParametricSpline", NULL);
    return TCL_ERROR;
    }
  double origin[3];
  double normal[3];
  if (Tcl_GetPointFromObj(interp, objv[2], origin) == TCL_ERROR)
    {
    return TCL_ERROR;
    }
  if (Tcl_GetPointFromObj(interp, objv[3], normal) == TCL_ERROR)
    {
    return TCL_ERROR;
    }
  vtkPoints *points = spline->GetPoints();
  double p[3];
  double po[3];
  double proj[3];
  for(int i = 0; i < points->GetNumberOfPoints(); i++)
    {
    points->GetPoint(i, p);
    po[0] = p[0] - origin[0];
    po[1] = p[1] - origin[1];
    po[2] = p[2] - origin[2];

    double t = vtkMath::Dot(normal,po);

    proj[0] = p[0] - t * normal[0];
    proj[1] = p[1] - t * normal[1];
    proj[2] = p[2] - t * normal[2];
    points->SetPoint(i, proj);
    }
  spline->Modified();
  return TCL_OK;
}

int vtkSplineRepresentationAddon_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "vtkSplineRepresentationAddon",
                      vtkSplineRepresentationAddon_Proc, NULL, NULL);
  Tcl_CreateObjCommand(interp, "vtkSplineProjectOnPlane",
                      vtkSplineProjectOnPlane_Proc, NULL, NULL);
  return TCL_OK;
}
