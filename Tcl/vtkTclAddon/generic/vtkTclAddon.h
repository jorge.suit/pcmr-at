#ifndef __vtkTclAddon_h
#define __vtkTclAddon_h

#include <tcl.h>
#include "vtkTclAddon_Export.h"

extern "C" {
  int VTKTCLADDON_EXPORT Vtktcladdon_Init(Tcl_Interp *interp);
}

#endif

