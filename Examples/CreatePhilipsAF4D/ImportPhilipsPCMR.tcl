package require snit
package require BWidget
package require msgcat

namespace import -force msgcat::*

namespace eval ImportPhilipsPCMR {
  variable win
  variable options
  variable inputDir 
  variable outputDir
  variable factorX 0
  variable factorY 0
  variable data
  variable widgets
  variable mkPath
  
  variable cwd [ file normalize [ file dirname [ info script ] ] ]

  set data(progress) 0
  
  if { [ lsearch [ image names ] folder_yellow_16 ] == -1 } {
    image create photo folder_yellow_16 -data {
      R0lGODlhEAAQAPYAAK+KGLGMGLOOGbKNGrKMG7OMG7OOG7SOGbSPGbWPH7SQGaOFI8GLNMmJN8Se
      MMSXOcSfO9eQOuOePN6xLcOhN8WgNMahNcehNcmjNMijN8mmNcukN8ulN8umNsumN8KgOMeiOMei
      OcemPMekPselPsemPtGnN9uqNeG9K+K4LeO/LOS8LeW/LeytO/G5OeTALPTHN/XMNvTBOffQNcun
      QtGtQtCuSd6+Q922Sum8S/TCTvjETvjITvjOTe/USO7USfrTTfzaS/veTPzgTPzkTfzmUfvpXd3A
      ZurTg/rrhvLgl/nrqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5
      BAEAAEwALAAAAAAQABAAAAeSgEyCg4SFhoeGBwgICgGIggYTKSssAgqXlwiEAC4yMDEzKi8vKioo
      A4MALScYGxwesLAdFRcgTAASJj9GSUu+v0tKFkwEERlCREW8wEtIIUwFDTVAQUPJy75HggUMODw9
      1NbKvTbbDzk7O9/h1w7bEDrp6evVPhqCCTTy++s3gwsfSJQoQWIEiYIjRFB4xLAhoUAAOw==
    }
  }

  proc Show { args } {
    variable win
    variable widgets
    variable options
    variable data
    
    set w .dlgImport
    
    set options(-parent) "."
    array set options $args
    if { ![ winfo exists $w ] } {
      set wparent $options(-parent)
      set bg [ $wparent cget -background ]
      Dialog $w -title [ mc "Import Philips Phase Contrast Image" ]  \
          -parent $wparent -modal local -cancel 1 -separator 1 \
          -background $bg -transient false
      $w add -text [ mc "Ok" ] -command [ namespace code OnClickOK ] \
          -background $bg
      $w add -text [ mc "Cancel" ]  \
          -background $bg
      set f [ $w getframe ]
      label $f.lbInput -text [ mc "Input directory" ]:
      set varInput "[ namespace current ]::inputDir"
      entry $f.entInput -textvariable $varInput
      button $f.btnInput -image folder_yellow_16 \
          -command [ namespace code "ChooseDir inputDir" ]
      grid $f.lbInput -row 0 -column 0 -sticky "ne"
      grid $f.entInput -row 0 -column 1 -sticky "ew"
      grid $f.btnInput -row 0 -column 2 -sticky "w"
      label $f.lbOutput -text [ mc "Output directory" ]:
      set varOutput "[ namespace current ]::outputDir"
      entry $f.entOutput -textvariable $varOutput
      button $f.btnOutput -image folder_yellow_16 \
          -command [ namespace code "ChooseDir outputDir 0" ]
      grid $f.lbOutput -row 1 -column 0 -sticky "ne"
      grid $f.entOutput -row 1 -column 1 -sticky "new"
      grid $f.btnOutput -row 1 -column 2 -sticky "nw"
      labelframe $f.fadv -text [ mc "Advanced" ]
      grid $f.fadv -row 2 -column 0 -sticky "snew" -columnspan 3
      label $f.fadv.lfactX -text [ mc "Factor X:" ]
      label $f.fadv.lfactY -text [ mc "Factor Y:" ]
      label $f.fadv.lmkpath -text [ mc "Importer Path:" ] -state normal
      grid $f.fadv.lfactX -row 0 -column 0 -sticky "ne"
      grid $f.fadv.lfactY -row 1 -column 0 -sticky "ne"
      grid $f.fadv.lmkpath -row 2 -column 0 -sticky "ne"
      set varFactorX "[ namespace current ]::factorX"
      entry $f.fadv.entX -textvariable $varFactorX
      set varFactorY "[ namespace current ]::factorY"
      entry $f.fadv.entY -textvariable $varFactorY
      set varMkPath "[ namespace current ]::mkPath"
      entry $f.fadv.entMkPath -textvariable $varMkPath -state normal
      grid $f.fadv.entX -row 0 -column 1 -sticky "new"
      grid $f.fadv.entY -row 1 -column 1 -sticky "new"
      grid $f.fadv.entMkPath -row 2 -column 1 -sticky "new"
      grid rowconfigure $f.fadv 3 -weight 1
      grid columnconfigure $f.fadv 1 -weight 1
      grid columnconfigure $f.fadv 2 -weight 1
      labelframe $f.fout -text [ mc "Trace" ]
      grid $f.fout -row 3 -column 0 -sticky "snew" -columnspan 3
      set widgets(txtTrace) [ text $f.fout.txt ]
      grid $f.fout.txt -row 0 -column 0 -sticky "snew"
      grid rowconfigure $f.fout.txt 0 -weight 1
      grid columnconfigure $f.fout.txt 0 -weight 1
      ProgressBar $f.pbar -maximum 1 \
          -variable  "[ namespace current ]::data(progress)" -relief flat \
          -troughcolor $bg -foreground #c3c3ff
      set widgets(pbar) $f.pbar
      grid $f.pbar -row 4 -column 1 -columnspan 2 -sticky sew
      grid columnconfigure $f 1 -weight 1
      grid rowconfigure $f 3 -weight 1
      set win $w
      wm protocol $win WM_DELETE_WINDOW [ namespace code WM_DELETE_WINDOW ]
    } else {
    }
    set data(progress) 0
    $widgets(txtTrace) delete 0.0 end
    $win draw
  }

  proc GetMkPath { } {
    variable mkPath

    return $mkPath
  }
  
  proc ChooseDir { varname {mustexist 1} } {
    variable win
    
    upvar \#0 "[ namespace current ]::$varname" var
    
    if { $var eq "" } {
      set var [ pwd ]
    }
    set res [ tk_chooseDirectory -parent $win -initialdir $var \
                  -mustexist $mustexist -title [ mc "Select output directory" ] ]
    if { $res ne "" } {
      set var $res
    }
  }
  
  proc OnClickOK { } {
    variable win
    variable inputDir
    variable outputDir
    variable widgets
    
    if { ![ file exists $inputDir ] || ![ file readable $inputDir ] } {
      MessageDlg $win.ask -title [ mc "Status Directory" ] \
          -icon error \
          -type ok -aspect 75 \
          -message [ mc "Directory '%s' does not exists or is not readable" $inputDir ]
      return
    }
    if { [ file exists $outputDir ] } {
      if { ![ file writable $outputDir ] } {
        MessageDlg $win.ask -title [ mc "Status Directory" ] \
            -icon error \
            -type ok -aspect 75 \
            -message [ mc "Directory %s is not writable" $outputDir ]
        return
      } else {
        set ans [ MessageDlg $win.ask -title [ mc "Confirm overwrite" ] \
                      -icon warning \
                      -type okcancel -aspect 75 \
                      -message [ mc "Directory '%s' already exist, overwrite?" $outputDir ] ]
        if { $ans == 1 } {
          return
        }
      }
    } else {
      set ans [ MessageDlg $win.ask -title [ mc "Confirm create" ] \
                    -icon warning \
                    -type okcancel -aspect 75 \
                    -message [ mc "Directory '%s' is going to be created, proceed?" $outputDir ] ]
      if { $ans == 1 } {
        return
      }
      
      if { 0 && [ catch { file mkdir $outputDir } msgerror ] } {
        MessageDlg $win.ask -title [ mc "Status Directory" ] \
            -icon error \
            -type ok -aspect 75 \
            -message [ mc "While creating '%s':\n %s" $outputDir $msgerror ]
        return
      }
    }
    update
    doImport $inputDir $outputDir
    #Dialog::enddialog $win 1
  }
  
  proc doImport { dirInput dirOutput } {
    variable win
    variable data
    variable cwd
    variable widgets

    set exePath [ GetMkPath ]
    if { [ file pathtype $exePath ] eq "relative" } {
      set exePath [ file join $cwd $exePath ]
    }
    if { ![ file exists $exePath ] } {
      MessageDlg $win.ask -title [ mc "Executable status" ] \
          -icon error \
          -type ok -aspect 75 \
          -message [ mc "File '%s' does not exists" $exePath ]
      return
    }
    if { ![ file executable $exePath ] } {
      MessageDlg $win.ask -title [ mc "Executable status" ] \
          -icon error \
          -type ok -aspect 75 \
          -message [ mc "File '%s' is not executable" $exePath ]
      return
    }
    runProcess $exePath $dirInput $dirOutput
  }

  proc runProcess { exePath dirInput dirOutput } {
    variable widgets
    variable factorX
    variable factorY

    set txt $widgets(txtTrace)
    $txt delete 0.0 end
    set cmd [ list $exePath $dirInput $dirOutput ]
    if { $factorX > 0 && $factorY > 0 } {
      lappend cmd $factorX $factorY
    }
    $txt insert end "starting $cmd ...\n"
    set fd [ open |$cmd r ]
    Dump "$fd opened"
    fconfigure $fd -blocking false -buffering line
    fileevent $fd readable "[ namespace current ]::captureTrace $fd"
  }

  proc captureTrace { chan } {
    variable widgets
    
    if { [ eof $chan ] } {
      close $chan
      Dump "$chan closed!"
    } else {
      set line [ gets $chan ]
      set txt $widgets(txtTrace)
      $txt insert end "${line}\n"
      $txt see end
    }
  }

  proc Dump { msg } {
    variable widgets
      
    set txt $widgets(txtTrace)
    $txt insert end "${msg}\n"
    $txt see end
      
  }

  proc WM_DELETE_WINDOW { } {
    variable win
    
    Dialog::enddialog $win 1
  }
}

proc uniqkey { } {
     set key   [ expr { pow(2,31) + [ clock clicks ] } ]
     set key   [ string range $key end-8 end-3 ]
     set key   [ clock seconds ]$key
     return $key
 }

 proc sleep { ms } {
     set uniq [ uniqkey ]
     set ::__sleep__tmp__$uniq 0
     after $ms set ::__sleep__tmp__$uniq 1
     vwait ::__sleep__tmp__$uniq
     unset ::__sleep__tmp__$uniq
 }

wm geometry . 0x0+300+300
sleep 500
update

ImportPhilipsPCMR::Show 

exit
