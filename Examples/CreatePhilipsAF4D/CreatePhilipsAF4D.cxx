#include <fstream>
#include "pcmrPhilipsGDCMIO.h"
#include "pcmrPhilipsImageReader.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include "itkImageAdaptor.h"

using namespace boost::filesystem;

typedef itk::ImageFileWriter< itk::PhaseContrast3DImage >
Writer3DType;

class NegatePixelAccessor  
{
public:
  /** External typedef. It defines the external aspect
   * that this class will exhibit. */
  typedef float ExternalType;

  /** Internal typedef. It defines the internal real
   * representation of data. */
  typedef float InternalType;

  inline void Set(float & output, const float & input) 
  {
    output = -input;
  }

  inline float Get( const float & input )  const
  {
    return -input;
  }
};

class NegateImageAdaptor : public itk::ImageAdaptor<itk::PhaseContrastImage, NegatePixelAccessor >
{
public:
  /** Standard class typedefs. */
  typedef NegateImageAdaptor           Self;
  typedef itk::ImageAdaptor<itk::PhaseContrastImage, NegatePixelAccessor> Superclass;
  typedef itk::SmartPointer<Self>        Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;
  
  /** Method for creation through the object factory. */
  itkNewMacro(Self);  

  /** Run-time type information (and related methods). */
  itkTypeMacro( NegateImageAdaptor, ImageAdaptor );

protected:
  NegateImageAdaptor() {}
  virtual ~NegateImageAdaptor() {}
  
private:
  NegateImageAdaptor(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

};

boost::mutex io_mutex;
void read_dicom( const path & path, pcmr::Philips::EnhancedMRI::Pointer & emri )
{
  io_mutex.lock();
  std::cout << "NOTICE: reading dicom " << path << "" << std::endl;
  io_mutex.unlock();
  emri->SetFileName(path.string().c_str());
  emri->Read();
}

int InitAorta4DStudy( const char * studyDir,
                      const pcmr::Philips::EnhancedMRI::Pointer emri,
                      float minVelocity, float maxVelocity )
{
  create_directory( studyDir );
  path headerPath( studyDir );
  headerPath /= "header.sth";
  std::ofstream ofs( headerPath.c_str() );
  if ( ofs.good() )
    {
    // patien_orientation must be read in EnhancedMRI
    ofs << "time_step_count " << emri->GetNumberOfPhases() << " "
        << "venc " << emri->GetPCVelocity() << " "
        << "patient_orientation " << "HFS" << " "
        << "vector_mag_min " << minVelocity << " "
        << "vector_mag_max " << maxVelocity << " ";
    ofs << "trigger_time {";
    for ( unsigned int i = 0; i < emri->GetNumberOfPhases(); i++ )
      {
      const pcmr::Philips::FrameType *aFrame = emri->GetFrameInfo( i );
      if ( aFrame ) 
        {
        ofs << " " << aFrame->m_TriggerTime;
        }
      }
    ofs << " }" << std::endl;
    }
  ofs.close();
  return 0;
}

template <class TImage4D>
void Write3DVolumesToStudy( TImage4D *image4D,
                            const char * dirOutput, const char * patternName )
{
  typedef typename itk::ExtractImageFilter< TImage4D, itk::PhaseContrast3DImage >
    Extract3DType;

  typename Extract3DType::Pointer extractVOL = Extract3DType::New();
  extractVOL->SetInput( image4D );
 
  typename TImage4D::IndexType startRegion = image4D->GetLargestPossibleRegion().GetIndex();
  typename TImage4D::SizeType sizeRegion = image4D->GetLargestPossibleRegion().GetSize();
  unsigned int t0 = startRegion[3];
  unsigned int tn = t0 + sizeRegion[3];
  sizeRegion[3] = 0;
  bool firstCheck = true;
  Writer3DType::Pointer writer = Writer3DType::New();
  for ( unsigned int t = t0; t < tn; t++ )
    {
    startRegion[3] = t;
    typename TImage4D::RegionType region(startRegion, sizeRegion);
    extractVOL->SetExtractionRegion( region );
#if ITK_VERSION_MAJOR >= 4
    extractVOL->SetDirectionCollapseToIdentity(); // This is required.
#endif
    extractVOL->Update();
    itk::PhaseContrast3DImage::Pointer output = extractVOL->GetOutput();
    output->DisconnectPipeline();
    path pathOutput( dirOutput );
    pathOutput /= patternName;
    std::string outputName( pathOutput.string().c_str() );
    std::stringstream ts; ts << t;
    boost::replace_all( outputName, "%t", ts.str() );
    writer->SetInput( output );
    if ( firstCheck )
      {
      firstCheck = false;
      std::cout << "Escribiendo en \"" << outputName << "\"\n"; 
      }
    writer->SetFileName( outputName );
    writer->Update();
    }
}

int main(int argc, char *argv[])
{
  // Validate input parameters
  if( argc < 3 || (argc > 3 && argc != 5) )
    {
    std::cout << "Usage: " 
              << argv[0]
              << " Input_Directory"
              << " Output_Directory"
			  << " ?factorX factorY?"
              << std::endl;
    return EXIT_FAILURE;
    }

  path p_in (argv[1]);
  path p_out(argv[2]);
  int factorX = -1;
  int factorY = -1;
  
  if ( !exists(p_in) || !is_directory(p_in) )
    {
    std::cout << "ERROR: " << p_in << " does not exists or is not a directory" << std::endl;
    return 1;
    }
  if ( exists(p_out) )
    {
    std::cout << "ERROR: " << p_out << " output directory already exists" << std::endl;
    return 1;
    }

  if ( argc > 3 ) 
    {
    factorX = atoi( argv[3] );
	  factorY = atoi( argv[4] );
    if ( factorX <= 1 || factorY <= 1 )
      {
      std::cout << "ERROR: " << p_in << " invalid arguments (factorX,factorY) = (" << argv[3] << "," << argv[4] << "), must be > 1" << std::endl;
      return 1;      
      }
    }
  else
    {
    factorX = 0;
	  factorY = 0;
    }
  path p_IM_0001( p_in );
  p_IM_0001 /= "IM_0001";
  pcmr::Philips::EnhancedMRI::Pointer emri1 = pcmr::Philips::EnhancedMRI::New();
  path p_IM_0003( p_in );
  p_IM_0003 /= "IM_0003";
  pcmr::Philips::EnhancedMRI::Pointer emri3 = pcmr::Philips::EnhancedMRI::New();
  path p_IM_0005( p_in );
  p_IM_0005 /= "IM_0005";
  pcmr::Philips::EnhancedMRI::Pointer emri5 = pcmr::Philips::EnhancedMRI::New();
  boost::thread thread1( boost::bind( &read_dicom, boost::ref(p_IM_0001), boost::ref(emri1) ) );
  boost::thread thread3( boost::bind( &read_dicom, boost::ref(p_IM_0003), boost::ref(emri3) ) );
  boost::thread thread5( boost::bind( &read_dicom, boost::ref(p_IM_0005), boost::ref(emri5) ) );
  thread1.join();
  thread3.join();
  thread5.join();

  //read_dicom( p_IM_0001, emri1 );
  pcmr::StatusType status1 = emri1->GetStatus();
  if ( status1 != pcmr::OK )
    {
    std::cout << "ERROR: " << GetStatusDescription( status1 ) << ", while reading " << p_IM_0001 << "\n";
    return 0;
    }

  //read_dicom( p_IM_0003, emri3 );
  pcmr::StatusType status3 = emri3->GetStatus();
  if ( status3 != pcmr::OK )
    {
    std::cout << "ERROR: " << GetStatusDescription( status3 ) << ", while reading " << p_IM_0003 << "\n";
    return 0;
    }

  //read_dicom( p_IM_0005, emri5 );
  pcmr::StatusType status5 = emri5->GetStatus();
  if ( status5 != pcmr::OK )
    {
    std::cout << "ERROR: " << GetStatusDescription( status5 ) << ", while reading " << p_IM_0005 << "\n";
    return 0;
    }

  emri1->Print( std::cout );
  emri3->Print( std::cout );
  emri5->Print( std::cout );

  unsigned int totalFrames = emri1->GetNumberOfFrames();

  const pcmr::Philips::FrameType *magFrame = emri1->GetFrameInfo( 0 );
  magFrame->Print( std::cout );
  const pcmr::Philips::FrameType *flowFrame = emri1->GetFrameInfo( totalFrames/2 );
  flowFrame->Print( std::cout );

  std::cout << "NOTICE: now reading pixel data ... " << "\n";
  itk::PhaseContrastImage::Pointer img4DMagnitude;
  itk::PhaseContrastImage::Pointer img4DPhaseX;
  itk::PhaseContrastImage::Pointer img4DPhaseY;
  itk::PhaseContrastImage::Pointer img4DPhaseZ;
  pcmr::Philips::RescalerType phaseRescale( flowFrame->m_RescaleSlope/magFrame->m_RescaleSlope,
                                            flowFrame->m_RescaleIntercept );

  const unsigned int factorReduceX = static_cast<unsigned int>( factorX );
  const unsigned int factorReduceY = static_cast<unsigned int>( factorY );

  std::cout << "NOTICE: reading magnitude image from: " << p_IM_0001 << std::endl;
  img4DMagnitude = pcmr::Philips::ReadMagnitudeImage( p_IM_0001.string().c_str(), 
                                                      emri1->GetNumberOfPhases(), 
                                                      emri1->GetNumberOfSlicesMR(),
                                                      magFrame->m_SpacingBetweenSlices,
                                                      factorReduceX, factorReduceY );
  std::cout << "NOTICE: reading flow image from: " << p_IM_0001 << std::endl;
  img4DPhaseX = pcmr::Philips::ReadPhaseImage( p_IM_0001.string().c_str(), 
                                               emri1->GetNumberOfPhases(), 
                                               emri1->GetNumberOfSlicesMR(),
                                               phaseRescale,
                                               magFrame->m_SpacingBetweenSlices,
                                               factorReduceX, factorReduceY );
  std::cout << "NOTICE: reading flow image from: " << p_IM_0003 << std::endl;
  img4DPhaseY = pcmr::Philips::ReadPhaseImage( p_IM_0003.string().c_str(), 
                                               emri1->GetNumberOfPhases(), 
                                               emri1->GetNumberOfSlicesMR(),
                                               phaseRescale,
                                               magFrame->m_SpacingBetweenSlices,
                                               factorReduceX, factorReduceY );
  std::cout << "NOTICE: reading flow image from: " << p_IM_0005 << std::endl;
  img4DPhaseZ = pcmr::Philips::ReadPhaseImage( p_IM_0005.string().c_str(), 
                                               emri1->GetNumberOfPhases(), 
                                               emri1->GetNumberOfSlicesMR(),
                                               phaseRescale,
                                               magFrame->m_SpacingBetweenSlices,
                                               factorReduceX, factorReduceY );

  std::cout << "NOTICE: All images read\n";

  itk::PhaseContrastTimeAveragedImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  PCMRAFilter->SetMagnitudeImage( img4DMagnitude );
  PCMRAFilter->SetPhaseXImage( img4DPhaseX );
  PCMRAFilter->SetPhaseYImage( img4DPhaseY );
  PCMRAFilter->SetPhaseZImage( img4DPhaseZ );
  PCMRAFilter->Update();
  PCMRAFilter->Print( std::cout );

  // 0- create the study directory
  InitAorta4DStudy( p_out.string().c_str(),
                    emri1,
                    PCMRAFilter->GetMinimumVelocity(),
                    PCMRAFilter->GetMaximumVelocity() );

  // 1- save premask

  std::cout << "NOTICE: comuting premask (PC-MRA time average)\n";
  
  path p_premask( p_out );
  p_premask /= "premask.vtk";
  Writer3DType::Pointer writer = Writer3DType::New();
  writer->SetInput( PCMRAFilter->GetOutput() );
  std::string outputName( p_premask.string().c_str() );
  writer->SetFileName( outputName );
  writer->Update();

  //itk::PhaseContrast3DImage::Pointer premaskImage = PCMRAFilter->GetOutput();
  //premaskImage->DisconnectPipeline();

  std::cout << "NOTICE: PC-MRA time average done!\n";
  
  // 1- save magnitude
  Write3DVolumesToStudy( img4DMagnitude.GetPointer(), p_out.string().c_str(), "mag_%t.vtk" );

  // 2- save X-velocity
  Write3DVolumesToStudy( img4DPhaseX.GetPointer(), p_out.string().c_str(), "vct_%t_0.vtk" );

  // 3- save Y-velocity
  Write3DVolumesToStudy( img4DPhaseZ.GetPointer(), p_out.string().c_str(), "vct_%t_1.vtk" );

  // 4- save Z-velocity
  NegateImageAdaptor::Pointer negateZ = NegateImageAdaptor::New();
  NegatePixelAccessor negator;
  negateZ->SetImage( img4DPhaseY );
  negateZ->SetPixelAccessor( negator );
  Write3DVolumesToStudy( negateZ.GetPointer(), p_out.string().c_str(), "vct_%t_2.vtk" );
  std::cout << "NOTICE: Finish!\n";
  return 0;
}
