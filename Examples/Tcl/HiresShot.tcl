proc HiresShot { png args } {
    array set options {
	-mag 5
    }
    array set options $args
    vtkRenderLargeImage hires
    hires SetInput ren1
    hires SetMagnification $options(-mag)
    vtkPNGWriter writer
    writer SetFileName $png
    writer SetInputConnection [hires GetOutputPort]
    writer Write
    writer Delete
    hires Delete
    return [file normalize $png]
}
