package require vtk
package require vtkinteraction
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

proc GotoTimeStep { {ts ""} } {
  if { $ts eq "" } {
    set ts [StudyHelper::GetCurrentTimeStep]
  }
  set imgVector [ StudyHelper::GetVectorImage -timestep $ts ]
  probe SetSource $imgVector
  return $imgVector 
}

StudyHelper::AddObserver ActivateTimeStep {
  puts "Observer ActivateTimeStep [ StudyHelper::GetCurrentTimeStep ]"
  probe SetSource [ StudyHelper::GetVectorImage ]
}

StudyHelper::AddObserver ActivateTimeStep {
  renWin Render
} -last yes

set imgVector [ StudyHelper::GetVectorImage ]

vtkPlaneWidget planeWidget 
    planeWidget SetInput $imgVector
    planeWidget NormalToXAxisOn
    planeWidget SetResolution 50
    planeWidget SetRepresentationToOutline
    #planeWidget SetPlaceFactor 0.75
    planeWidget SetHandleSize 0.02
    planeWidget PlaceWidget

vtkPolyData plane
    planeWidget GetPolyData plane

vtkProbeFilter probe
    probe SetInput plane

set imgVector [ GotoTimeStep ]

# create pipeline for rendering
#
vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 [StudyHelper::GetVelocityEncoding]
  lut SetVectorModeToMagnitude
  lut Build

vtkVectorNorm magnitude
  magnitude NormalizeOff
  magnitude SetInputConnection [ probe GetOutputPort ]
  magnitude Update

vtkPolyDataMapper contourMapper
    contourMapper SetInputConnection [ magnitude GetOutputPort ]
    #contourMapper SetInput [ StudyHelper::GetOutputAA probe ]
    contourMapper UseLookupTableScalarRangeOn
    contourMapper SetLookupTable lut

vtkActor contourActor
    contourActor SetMapper contourMapper
    contourActor VisibilityOff

#set probeAA [ StudyHelper::GetOutputAA probe ]
vtkHedgeHog hhog
  #hhog SetInput [ StudyHelper::GetOutputAA probe ]
  #StudyHelper::ConnectAA hhog probe
  hhog SetInputConnection [ magnitude GetOutputPort ]
  hhog SetScaleFactor 0.05

vtkPolyDataMapper hhogMapper
  hhogMapper SetInputConnection [hhog GetOutputPort]
  #StudyHelper::ConnectAA hhogMapper hhog
  hhogMapper UseLookupTableScalarRangeOn
  hhogMapper SetLookupTable lut

vtkActor hhogActor
  hhogActor SetMapper hhogMapper

vtkImageMarchingCubes marching
  marching SetInput [ StudyHelper::GetDistanceMapFromMask ]
  marching SetValue 0 0
vtkPolyDataMapper isoMapper
  isoMapper SetInputConnection [marching GetOutputPort]
  isoMapper ScalarVisibilityOff
  isoMapper ImmediateModeRenderingOn
vtkActor isoActor
  isoActor SetMapper isoMapper
  eval [isoActor GetProperty] SetDiffuseColor 0 0.7 0
  eval [isoActor GetProperty] SetOpacity 0.1

vtkOutlineFilter outline
  outline SetInput $imgVector
    
vtkPolyDataMapper outlineMapper
  outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1

# Add the actors to the renderer, set the background and size
#
ren1 AddActor isoActor
ren1 AddActor outlineActor
ren1 AddActor contourActor
ren1 AddActor hhogActor
ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7
#iren Initialize

# render the image
#
#iren AddObserver UserEvent {wm deiconify .vtkInteract}

# prevent the tk window from showing up then start the event loop
wm withdraw .

toplevel .top
wm title .top "Test: Probe Plane"
wm protocol .top WM_DELETE_WINDOW ::vtk::cb_exit

set display_frame [frame .top.f1]
set ctrl_buttons [frame .top.btns]

grid $display_frame -row 0 -column 0 -sticky snew
grid $ctrl_buttons  -row 1 -column 0 -sticky snew
grid rowconfigure .top 0 -weight 1
grid columnconfigure .top 0 -weight 1


set labTS [ label $ctrl_buttons.lbts -text "Time step" ]
grid $labTS -row 0 -column 0 -sticky se

set currentTimeStep [ StudyHelper::GetCurrentTimeStep ]
set scaleTS [ scale $ctrl_buttons.scts \
                  -from 0 -to [expr {[StudyHelper::GetNumberOfTimeStep]-1}] \
                  -variable currentTimeStep -showvalue 1 \
                  -orient horizontal \
                  -command GotoTimeStep ]
grid $scaleTS -row 0 -column 1 -sticky sew

set quit_button [button $ctrl_buttons.btn1 -text "Quit" -command  ::vtk::cb_exit]
grid $quit_button -row 0 -column 2 -sticky s

grid columnconfigure $ctrl_buttons 1 -weight 1


set render_widget [vtkTkRenderWidget $display_frame.r \
                       -width 600 -height 600  -rw renWin]

grid $render_widget -sticky snew
grid rowconfigure $display_frame 0 -weight 1
grid columnconfigure $display_frame 0 -weight 1
::vtk::bind_tk_render_widget $render_widget

set iren [[$render_widget GetRenderWindow] GetInteractor]
#vtkRenderWindowInteractor iren
#    iren SetRenderWindow renWin

vtkInteractorStyleTrackballCamera style
$iren SetInteractorStyle style

# Associate the line widget with the interactor

planeWidget SetInteractor $iren
planeWidget AddObserver EnableEvent BeginInteraction
planeWidget AddObserver StartInteractionEvent BeginInteraction
planeWidget AddObserver InteractionEvent ProbeData

[ren1 GetActiveCamera] Zoom 1.5
ren1 ResetCamera
renWin Render


# Actually generate contour lines.
proc BeginInteraction {} {
    planeWidget GetPolyData plane
    contourActor VisibilityOn
}

proc ProbeData {} {
    planeWidget GetPolyData plane
    [probe GetOutput] Update
}

proc GotoTimeStep { ts } {
  StudyHelper::ActivateTimeStep [expr {round($ts)}]
}
