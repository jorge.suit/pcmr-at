package require snit
package require tdom

snit::type HeaderXml {
  component Document
  variable Xml

  option -xmlfile -configuremethod _confXmlFile
  option -xmltext -configuremethod _confXmlText

  constructor { args } {
    $self configurelist $args
  }

  destructor {
    $self _deleteDocument
  }

  method _deleteDocument { } {
    if {$Document ne ""} {
      $Document delete
      set Document delete
    }
  }

  method _confXmlFile { o v } {
    if {![file readable $v]} {
      error "File $v is not readable"
    }
    $self _deleteDocument
    set options(-xmltext) ""
    set options($o) $v
    set fd [open $v]
    set xmlBuffer [string trim [read $fd]]
    close $fd
    set Document [dom parse $xmlBuffer]
  }

  method _confXmlText { o v } {
    $self _deleteDocument
    set options(-xmlfile) ""
    set options($o) $v
    set xmlBuffer [string trim $v]
    set Document [dom parse $xmlBuffer]
  }

  typemethod GetGlobalProperties { } {
    return {NumberOfTimeSteps LengthOfTimeStep VelocityEncoding MinimumVelocity MaximumVelocity ImageOrientation}
  }

  method GetNumberOfTimeSteps { } {
    return [$self GetGlobalProperty NumberOfTimeSteps]
  }

  method GetLengthOfTimeStep { } {
    return [$self GetGlobalProperty LengthOfTimeStep]
  }

  method GetVelocityEncoding { } {
    return [$self GetGlobalProperty VelocityEncoding]
  }

  method GetMinimumVelocity { } {
    return [$self GetGlobalProperty MinimumVelocity]
  }

  method GetMaximumVelocity { } {
    return [$self GetGlobalProperty MaximumVelocity]
  }

  method GetImageOrientation { } {
    return [$self GetGlobalProperty ImageOrientation]
  }

  method GetGlobalProperty { key } {
    $Document selectNodes string(/Study/${key})
  }

  method GetExtendedProperties { } {
    set allKeys {}
    foreach ef [[$Document selectNodes /Study/ExtendedFields] childNodes] {
      lappend allKeys [$ef nodeName]
    }
    return $ef
  }

  method GetExtendedProperty { key } {
    set extended [$doc selectNodes /Study/ExtendedFields]
    set node [$extended selectNodes $key]
    set description [$node selectNodes {string(Description)}]
    set value [$node selectNodes {string(Value)}]
    return [list -description $description -value $value]
  }
}