source StudyHelper.tcl
StudyHelper::InitFromArgv

set imgVelocityX [StudyHelper::GetVelocityImage -component "VX"]
set imgVelocityY [StudyHelper::GetVelocityImage -component "VY"]
set imgVelocityZ [StudyHelper::GetVelocityImage -component "VZ"]

set VelMagFilter [TernaryMagnitudeFilter]
$VelMagFilter SetInput1 $imgVelocityX
$VelMagFilter SetInput2 $imgVelocityY
$VelMagFilter SetInput3 $imgVelocityZ
$VelMagFilter Update

set imgVelocity [$VelMagFilter GetOutput]
$imgVelocity Print

set filterRange [MinMaxCalc]
$filterRange SetImage $imgVelocity
$filterRange Compute

set minValue [$filterRange GetMinimum]
set maxValue [$filterRange GetMaximum]
set maxIndex [$filterRange GetIndexOfMaximum]
puts "Image Range: \[$minValue,$maxValue\]"
puts "Maximun at: $maxIndex"

proc DumpPixel {img name pos} {
  puts "${name}([join $pos ,]) = [$img GetPixelValue $pos]"
}


DumpPixel $imgVelocityX imgVelocityX $maxIndex
DumpPixel $imgVelocityY imgVelocityY $maxIndex
DumpPixel $imgVelocityZ imgVelocityZ $maxIndex

puts "STATUS: OK"

exit


