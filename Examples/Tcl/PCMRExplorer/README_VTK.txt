Aqui describire como gestionar el pipeline de VTK para reutilizarlo
sin perdida de memoria.

¿Me puede servir el garbage collector de VTK?

Hay un renderer donde voy a insertar los diferentes actores.

¿Se puede liberar el contenido de un actor hasta el origen de datos?
¿Se puede liberar todo? Sí, la clave está en la respuesta a la
pregunta anterior.

Descripción del interfaz:

0- Todos son objetos!!! La aplicación es un singleton (instancia unica
de una clase).

1- Una barra de herramientas superior con los botones de acciones.

2- Un panel izquierdo con pestañas de opciones de visualizacion y actores.

3- Un paner derecho con el Renderer y el navegador de pasos de tiempo y slices.

4- Una barra de status inferior donde insertaremos mensajes y barras
de progreso.

Describe las actividades que puede hacer un usuario con PCMRExplorer:

- abrir un estudio, desde un boton o un menu

- seleccionar el volumen que se visualiza para paso de tiempo actual

- cambiar de paso de tiempo

- mover los slices ortogonales mediante un widget scale

- cambiar a una de las 3 vistas ortogonales

- conocer el valor de intensidad de un pixel seleccionado en uno de los 3 planos de corte.

- activar/desactivar los planos de corte.

- enmascarar la visualización según una máscara precalculada.
...

- segmentar: generar una mascara para cada paso de tiempo.

- hedgehog: visualizar el campo vectorial como flechas.

- streamlines: visualizar el campo vectorial como streamlines.

- particle tracing: visualizar el campo vectorial en el tiempo.

- video: grabar un video de una secuencia de lo que está viendo.

- cuantificar el flujo

- WSR

- generar una malla de tetrahedros a partir de una mascara, poder
  almacenar el campo vectorial interpolado en los nodos de la malla
