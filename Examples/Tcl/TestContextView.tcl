package require vtk
package require vtkinteraction
package require vtkTclAddon

vtkTkRenderWidget .tkrw
::vtk::bind_tk_render_widget .tkrw

set renWin [.tkrw GetRenderWindow]

#vtkRenderer ren
#ren SetBackground 0.0 0.4 0.5

#$renWin AddRenderer ren

pack .tkrw -fill both -expand yes

vtkContextView view
view SetRenderWindow $renWin

wm protocol . WM_DELETE_WINDOW {
  view Delete
  destroy .
}

vtkTable table

set numPoints 29

vtkFloatArray arrX
arrX SetName "X Axis"
arrX SetNumberOfTuples $numPoints

vtkFloatArray arrC
arrC SetName "Cosine"
arrC SetNumberOfTuples $numPoints

vtkFloatArray arrS
arrS SetName "Sine"
arrS SetNumberOfTuples $numPoints

set inc [expr {7.0 / ($numPoints-1)}]
#table SetNumberOfRows $numPoints


for {set i 0} {$i < $numPoints} {incr i} {
  set x [expr {$i* $inc}]
  arrX SetValue $i $x
  arrC SetValue $i [expr {cos($x)}]
  arrS SetValue $i [expr {sin($x)}]
}
table AddColumn arrX 
table AddColumn arrC
table AddColumn arrS

vtkChartXY chart
[view GetScene] AddItem chart

set line [chart AddPlot 0]
$line SetInputData table 0 1
$line SetColor 255 0 0 255

set line [chart AddPlot 0]
$line SetInputData table 0 2
$line SetColor 0 255 0 255
$line SetWidth 2.0

set axisX [chart GetAxis 1]
vtkAxisAddon $axisX SetTitle "Seconds"

set axisY [chart GetAxis 0]
vtkAxisAddon $axisY SetTitle "Flow (cm/s)"
set axisProp [$axisY GetTitleProperties]

vtkTable tableMark

vtkFloatArray arrMarkX
arrMarkX SetName "X0"
arrMarkX SetNumberOfTuples 2
arrMarkX SetValue 0 4
arrMarkX SetValue 1 4

vtkFloatArray arrMarkY
arrMarkY SetName "Y0"
arrMarkY SetNumberOfTuples 2
arrMarkY SetValue 0 0
arrMarkY SetValue 1 1

tableMark SetNumberOfRows 2

tableMark AddColumn arrMarkX
tableMark AddColumn arrMarkY

set line [chart AddPlot 0]
$line SetInteractive 0
$line SetInputData tableMark 0 1
$line SetColor 255 0 0 255

if {0} {
  vtkAxis tmp
  tmp SetPosition 0
  tmp SetPoint1 70 412
  tmp SetPoint2 70 33
}


# in order to refresh chart
# $plot SetInputData ""
# $plot SetInputData $table $c1 $c2