package require vtk

puts $argv

vtkStructuredPointsReader reader
 reader SetFileName [lindex $argv 0]

set img [reader GetOutput]
$img Update
foreach {imax jmax kmax} [$img GetDimensions] break
incr imax -1
incr jmax -1
incr kmax -1
set k [expr {$kmax / 2}]

vtkExtractVOI extract
  extract SetInput $img
  extract SetVOI 0 $imax 0 $jmax $k $k
  extract SetSampleRate 1 1 1
  extract Update

vtkImageResize resize
  resize SetInputConnection [extract GetOutputPort] 
  resize SetOutputDimensions 64 64 1
  resize Update

vtkImageFlip flip
  flip SetInputConnection [resize GetOutputPort]
  flip SetFilteredAxis 1
  flip Update

foreach {rmin rmax} [[resize GetOutput] GetScalarRange] break

set w [expr {$rmax - $rmin}]
set l [expr {0.5*($rmax + $rmin)}]

puts "window = $w, level = $l"
vtkImageMapToWindowLevelColors wlMapper
  wlMapper SetWindow $w
  wlMapper SetLevel   $l
  wlMapper SetInputConnection [flip GetOutputPort]

vtkPNGWriter writer
  writer SetInputConnection [wlMapper GetOutputPort]
  writer SetFileName "thumbnail.png"
  writer SetFileDimensionality 2
  writer Write

exit