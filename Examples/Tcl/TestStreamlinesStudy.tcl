#lappend auto_path /usr/local/lib/vtk-5.5/

package require vtk
package require vtkinteraction
source "StudyHelper.tcl"
source "HiresShot.tcl"

StudyHelper::InitFromArgv

puts "REQTS = [ StudyHelper::GetCurrentTimeStep ]"
set imgVector [ StudyHelper::GetVectorImage -masked no ]

vtkPlaneWidget planeWidget 
    planeWidget SetInput $imgVector
    planeWidget NormalToXAxisOn
    planeWidget SetResolution 50
    planeWidget SetRepresentationToOutline
    #planeWidget SetPlaceFactor 0.75
    planeWidget SetHandleSize 0.02
    planeWidget PlaceWidget

vtkPlaneSource lowresPlane
  lowresPlane SetResolution 10 10
vtkPlaneSource hiresPlane
  hiresPlane SetResolution 30 30

vtkPolyData plane
    planeWidget GetPolyData plane

vtkProbeFilter probe
    probe SetInput plane
    probe SetSource $imgVector

# create pipeline for rendering
#
vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 180
  lut SetVectorModeToMagnitude
  lut Build

vtkVectorNorm magnitude
  magnitude NormalizeOff
  magnitude SetInputConnection [ probe GetOutputPort ]
  magnitude Update

vtkPolyDataMapper contourMapper
    contourMapper SetInputConnection [ magnitude GetOutputPort ]
    #contourMapper SetInput [ StudyHelper::GetOutputAA probe ]
    contourMapper UseLookupTableScalarRangeOn
    contourMapper SetLookupTable lut

vtkActor contourActor
    contourActor SetMapper contourMapper
    contourActor VisibilityOff

vtkRungeKutta4 rk4
vtkStreamLine streamer
    streamer SetInput $imgVector
    #streamer SetSource plane
    streamer SetMaximumPropagationTime 10
    streamer SetIntegrationStepLength 0.05
    streamer SetStepLength .05
    streamer SetNumberOfThreads 2
    streamer SetIntegrationDirectionToForward
    streamer SpeedScalarsOn
    #streamer VorticityOn
    streamer SetIntegrator rk4

vtkCleanPolyData cleanFilter
    cleanFilter SetInputConnection [streamer GetOutputPort]
    cleanFilter SetTolerance 0.0001
    cleanFilter PointMergingOn

vtkTubeFilter tubes
    tubes SetInputConnection [cleanFilter GetOutputPort]
    tubes SetRadius 0.2
    tubes SetNumberOfSides 4
vtkPolyDataMapper streamMapper
    streamMapper SetInputConnection [tubes GetOutputPort]
    streamMapper UseLookupTableScalarRangeOn
    streamMapper SetLookupTable lut
vtkActor streamlineActor
    streamlineActor SetMapper streamMapper
    streamlineActor VisibilityOff

vtkImageMarchingCubes marching
  marching SetInput [ StudyHelper::GetDistanceMapFromMask ]
  marching SetValue 0 0
vtkPolyDataMapper isoMapper
  isoMapper SetInputConnection [marching GetOutputPort]
  isoMapper ScalarVisibilityOff
  isoMapper ImmediateModeRenderingOn
vtkActor isoActor
  isoActor SetMapper isoMapper
  eval [isoActor GetProperty] SetDiffuseColor 0 0.7 0
  eval [isoActor GetProperty] SetOpacity 0.1

vtkOutlineFilter outline
  outline SetInput $imgVector

vtkPolyDataMapper outlineMapper
  outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin

vtkInteractorStyleTrackballCamera style
[renWin GetInteractor] SetInteractorStyle style

# Associate the line widget with the interactor

planeWidget SetInteractor iren
planeWidget AddObserver EnableEvent OnEnablePlaneWidget
planeWidget AddObserver StartInteractionEvent BeginInteraction
planeWidget AddObserver EndInteractionEvent EndInteraction
planeWidget AddObserver InteractionEvent ProbeData

# Add the actors to the renderer, set the background and size
#
ren1 AddActor isoActor
ren1 AddActor outlineActor
ren1 AddActor contourActor
ren1 AddActor streamlineActor
ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7
iren Initialize

# render the image
#
iren AddObserver UserEvent {wm deiconify .vtkInteract}

[ren1 GetActiveCamera] Zoom 1.5
ren1 ResetCamera
renWin Render

# prevent the tk window from showing up then start the event loop
wm withdraw .

proc UpdatePlaneSourceFromPlaneWidget { planeWidget planeSource } {
  $planeSource SetOrigin {*}[ $planeWidget GetOrigin ]
  $planeSource SetPoint1 {*}[ $planeWidget GetPoint1 ]
  $planeSource SetPoint2 {*}[ $planeWidget GetPoint2 ]
  $planeSource SetNormal {*}[ $planeWidget GetNormal ]
  $planeSource SetCenter {*}[ $planeWidget GetCenter ]
}

set firstTime 1
proc OnEnablePlaneWidget {} {
  if { $::firstTime } {
    UpdatePlaneSourceFromPlaneWidget planeWidget lowresPlane
    UpdatePlaneSourceFromPlaneWidget planeWidget hiresPlane
    streamer SetSource [ hiresPlane GetOutput ]
    set ::firstTime 0
  }
  contourActor VisibilityOn
  streamlineActor VisibilityOn
}

# Actually generate contour lines.
proc BeginInteraction {} {
  puts "Begin: [planeWidget GetEnabled]"
  #planeWidget SetResolution 10
  #planeWidget GetPolyData plane
  UpdatePlaneSourceFromPlaneWidget planeWidget lowresPlane
  UpdatePlaneSourceFromPlaneWidget planeWidget hiresPlane
  streamer SetSource [ lowresPlane GetOutput ]
  contourActor VisibilityOn
  streamlineActor VisibilityOn
}

proc EndInteraction {} {
  puts "Terminé"
  #planeWidget SetResolution 50
  streamer SetSource [ hiresPlane GetOutput ]
  streamer Update
  #ProbeData
}

proc ProbeData {} {
  UpdatePlaneSourceFromPlaneWidget planeWidget lowresPlane
  UpdatePlaneSourceFromPlaneWidget planeWidget hiresPlane
  streamer Update
  planeWidget GetPolyData plane
  [probe GetOutput] Update
}
