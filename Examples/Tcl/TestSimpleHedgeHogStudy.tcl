#lappend auto_path /usr/local/lib/vtk-5.5/

package require vtk
package require vtkinteraction
source "StudyHelper.tcl"

StudyHelper::InitFromArgv

foreach c {VX VY VZ} {
  set vtkImgComp($c) [StudyHelper::GetVelocityImage -component $c -vtk yes]
}

vtkImageAppendComponents iac
foreach c {VX VY VZ} {
  iac AddInput $vtkImgComp($c)
}
iac Update

set imgVector  [iac GetOutput]

set pd [$imgVector GetPointData]
set scalars [$pd GetScalars]
set vectors [$scalars New]
$vectors SetName "Velocity"
$vectors DeepCopy $scalars
$pd SetVectors $vectors
$pd RemoveArray "scalars"

vtkMaskPoints maskPoint
maskPoint SetInput $imgVector
maskPoint SetOnRatio 2
maskPoint SetMaximumNumberOfPoints [$imgVector GetNumberOfPoints]

#vtkAssignAttribute aa
#aa SetInputConnection [maskPoint GetOutputPort]
#aa Assign SCALARS VECTORS POINT_DATA

vtkVectorNorm magnitude
  magnitude NormalizeOff
  magnitude SetInputConnection [maskPoint GetOutputPort]
  magnitude Update

vtkHedgeHog hhog
  hhog SetInputConnection [magnitude GetOutputPort]
  #hhog SetInputConnection [aa GetOutputPort ]
  hhog SetScaleFactor 0.05

vtkLookupTable lut
  lut SetHueRange 0.667 0.0 
  lut SetRange 0 180
  lut SetVectorModeToMagnitude
  lut Build

vtkPolyDataMapper vectorMapper
  vectorMapper SetInputConnection [hhog GetOutputPort]
  vectorMapper UseLookupTableScalarRangeOn
  vectorMapper SetLookupTable lut

vtkActor vectorActor
  vectorActor SetMapper vectorMapper

vtkOutlineFilter outline
  outline SetInput $imgVector
vtkPolyDataMapper outlineMapper
  outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
  outlineActor SetMapper outlineMapper

# Create the RenderWindow, Renderer and both Actors
#
vtkRenderer ren1
vtkRenderWindow renWin
    renWin AddRenderer ren1

# Add the actors to the renderer, set the background and size
#
ren1 AddActor outlineActor
ren1 AddActor vectorActor
ren1 SetBackground 1 1 1
renWin SetSize 500 500
ren1 SetBackground 0.7 0.7 0.7

vtkRenderWindowInteractor iren
iren SetRenderWindow renWin
iren AddObserver UserEvent {wm deiconify .vtkInteract}

vtkInteractorStyleTrackballCamera style
iren SetInteractorStyle style

ren1 ResetCamera
renWin Render
