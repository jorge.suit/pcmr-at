package require vtk
package require vtkinteraction

vtkTkRenderWidget .tkrw
::vtk::bind_tk_render_widget .tkrw

set renWin [.tkrw GetRenderWindow]

vtkRenderer ren
ren SetBackground 0.0 0.4 0.5

$renWin AddRenderer ren

pack .tkrw -fill both -expand yes
