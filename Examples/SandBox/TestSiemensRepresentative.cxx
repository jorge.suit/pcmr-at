#include "pcmrSiemensImporter.h"

int main(int argc, const char* argv[])
{
  if( argc !=2 )
    {
    std::cout << "Usage: " 
              << argv[0]
              << " Sequence_Directory"
              << std::endl;
    return EXIT_FAILURE;
    }
  
  pcmr::Siemens::Importer::Pointer siemensImporter = pcmr::Siemens::Importer::New();
  siemensImporter->SetSequenceDirectory(argv[1]);

  pcmr::StatusType status;

  status = siemensImporter->ReadInformation();
  if (status != pcmr::OK)
    {
    std::cout << "FAIL: " << pcmr::GetStatusDescription(status) << std::endl;
    return status;
    }

  siemensImporter->WriteMagnitudeRepresentativeSlice("/tmp/mag.png");
  siemensImporter->WritePhaseRepresentativeSlice(pcmr::X, "/tmp/X.png");
  siemensImporter->WritePhaseRepresentativeSlice(pcmr::Y, "/tmp/Y.png");
  siemensImporter->WritePhaseRepresentativeSlice(pcmr::Z, "/tmp/Z.png");
  return 0;
}
