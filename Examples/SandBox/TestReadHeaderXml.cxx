#include <boost/foreach.hpp>
#include "pcmrHeaderReader.h"
#include <iostream>

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cerr << "usage: '" << argv[0] << "' header.xml\n";
    return -1;
    }
  pcmr::HeaderReader::Pointer header = pcmr::HeaderReader::New();

  header->SetFileName(argv[1]);
  std::cout << "Velocity Encoding = " << header->GetVelocityEncoding() << "\n";
  std::cout << "Number of Time Steps = " << header->GetNumberOfTimeSteps() << "\n";
  std::cout << "Length of Time Step = " << header->GetLengthOfTimeStep() << "\n";
  std::cout << "Image Orientation = " << header->GetImageOrientation() << "\n";
  std::vector<std::string> properties;
  header->GetExtendedProperties(properties);
  pcmr::HeaderReader::Property prop;
  BOOST_FOREACH(std::string &v, properties)
    {
    header->GetExtendedProperty(v, prop);
    std::cout << prop.description << " = " << prop.value << "\n";
    }
  return 0;
}
