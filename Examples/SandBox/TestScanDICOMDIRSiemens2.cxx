#include "pcmrSiemensGDCMIO.h"

#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkTileImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkFlipImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkOrientImageFilter.h"

#include "itkPhaseContrastImage.h"
#include "itkPhaseContrastTimeAveragedImageFilter.h"
#include "itkPhaseContrastTimeStdDevImageFilter.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace boost::filesystem;

template< class TInput, class TOutput >
class NegateValue
{
public:
  NegateValue() {}
  ~NegateValue() {}

  bool operator!=(const NegateValue &) const
  {
    return false;
  }

  bool operator==(const NegateValue & other) const
  {
    return !( *this != other );
  }

  inline TOutput operator()(const TInput & A) const
  { return -A; }
};

template< class TInputImage, class TOutputImage >
class NegateImageFilter:
  public itk::UnaryFunctorImageFilter< TInputImage, TOutputImage,
                                       NegateValue<
                                         typename TInputImage::PixelType,
                                         typename TOutputImage::PixelType > >
{
public:
  /** Standard class typedefs. */
  typedef NegateImageFilter Self;
  typedef itk::UnaryFunctorImageFilter<
    TInputImage, TOutputImage,
    NegateValue< typename TInputImage::PixelType,
                 typename TOutputImage::PixelType > >  Superclass;

  typedef itk::SmartPointer< Self >       Pointer;
  typedef itk::SmartPointer< const Self > ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(NegateImageFilter,
               itk::UnaryFunctorImageFilter);

protected:
  NegateImageFilter() {}
  virtual ~NegateImageFilter() {}
private:
  NegateImageFilter(const Self &); //purposely not implemented
  void operator=(const Self &); //purposely not implemented
};

#define STRING_CMP( STR, VALUE )                 \
  STR.compare( 0, sizeof(VALUE)-1, VALUE )

pcmr::StatusType InsertNewPcmrSlice( const gdcm::DataSet &ds,
                                     const char *pathDCM,
                                     std::vector<pcmr::Siemens::PCSliceInfo>& container )
{
  pcmr::Siemens::PCSliceInfo sliceInfo;
  pcmr::ComplexImageComponentType pcmrType = pcmr::CIC_UNKNOWN;
  pcmr::StatusType status;
  
  status = sliceInfo.ReadFromDataSet( ds, pcmrType );
  if ( status == pcmr::NO_ACQ3D )
    {
    return pcmr::OK;
    }
  else if ( status == pcmr::OK )
    {
    sliceInfo.m_Path = pathDCM;
    container.push_back( sliceInfo );
    return pcmr::OK;
    }
  else
    {
    std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
              << "' while reading '" << pathDCM << "'\n";
    return pcmr::DCM_NO_PM;
    }
}

pcmr::StatusType InsertNewPcmrSlice( const char *pathDCM,
                                     std::vector<pcmr::Siemens::PCSliceInfo>& container )
{
  gdcm::Reader reader;
  /*
  std::ifstream inputStream;
  inputStream.open( pathDCM, std::ios::binary );
  reader.SetStream( inputStream );*/
  reader.SetFileName( pathDCM );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << pathDCM << std::endl;
    return pcmr::ST_NOREAD;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  return InsertNewPcmrSlice( ds, pathDCM, container );
}

void PrintSlices( std::vector<pcmr::Siemens::PCSliceInfo>& container, size_t from, size_t to )
{
  for ( unsigned int i = from; i <= to; i++ )
    {
    std::cout << "PATH: " << container[i].m_Path << std::endl;
    std::cout << "InstanceNumber: " << container[i].m_InstanceNumber << std::endl;
    std::cout << "TimeStep: " << container[i].m_TimeStep << std::endl;    
    std::cout << "FlowEncodingDirection: " << container[i].m_FlowEncodingDirection << std::endl;
    std::cout << "PhaseEncodingDirectionPositive: " << container[i].m_PhaseEncodingDirectionPositive << std::endl;
    }
}

typedef itk::GDCMImageIO ImageIOType;
typedef itk::ImageSeriesReader< itk::PhaseContrast3DImage > ReaderType;
typedef itk::RescaleIntensityImageFilter <itk::PhaseContrast3DImage,itk::PhaseContrast3DImage> RescaleFilterType;

typedef itk::UnaryFunctorImageFilter< itk::PhaseContrast3DImage,
                                      itk::PhaseContrast3DImage,
                                      NegateValue<
                                        itk::PhaseContrast3DImage::PixelType,
                                        itk::PhaseContrast3DImage::PixelType > > NegateImage3DType;
typedef itk::TileImageFilter < itk::PhaseContrast3DImage,
                               itk::PhaseContrastImage> TileFilterType;
typedef itk::ImageFileWriter< itk::PhaseContrast3DImage > Writer3DType;
//typedef itk::ImageAdaptor<itk::PhaseContrast3DImage, NegatePixelAccessor> NegateImage3DType;
typedef itk::OrientImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrast3DImage> OrienterType;

itk::PhaseContrast3DImage::Pointer
ReadSeries3D( ReaderType::FileNamesContainer filenames )
{
  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  ReaderType::Pointer seriesReader = ReaderType::New();

  seriesReader->SetImageIO( gdcmIO );
  seriesReader->SetFileNames( filenames );
  try
    {
    seriesReader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while reading the series" << std::endl;
    std::cerr << excp << std::endl;
    return NULL;
    }
  OrienterType::Pointer orienter = OrienterType::New();
  orienter->UseImageDirectionOn();
  /*SetDesiredCoordinateOrientationToSagittal is equivalent to
   *  SetDesiredCoordinateOrientation(ITK_COORDINATE_ORIENTATION_ASL).
   */

  // this orientation gets the correct side labels inside VolView
  // orienter->SetDesiredCoordinateOrientationToAxial();

  // this gets the correct orientation inside Aorta4D, restore this to
  // production!!!!
  orienter->SetDesiredCoordinateOrientationToSagittal();

  // this similar a variation of sagittal = ASL
  // orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_PIL);
  orienter->SetInput( seriesReader->GetOutput() );
  orienter->Update();
  return orienter->GetOutput();
}

int WriteImage3D( itk::PhaseContrast3DImage::Pointer image,
                  const char* outputImage )
{
  std::cout << "Writing the image as a vtk image '"
            << outputImage << "' ...\n";
   
  Writer3DType::Pointer writer = Writer3DType::New();
  writer->SetInput( image );
  writer->SetFileName( outputImage );
  try
    {
    writer->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
    std::cerr << "Exception thrown while writing the image" << std::endl;
    std::cerr << excp << std::endl;
    return -1;
    }
  return 0;
}

int SelectMagnitudeVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                           const std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                           ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;
 
  for ( size_t i = sizeVolume * idxTimeStep;
        i < sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesMagnitude[i].m_Path );
    //filenames.push_back( seriesMagnitude[i].m_Path );
    }

  return 0;
}

// orthogonal to axial view, phase y comes first in order
int SelectPhaseYVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  size_t baseY = 0;

  for ( size_t i = baseY + sizeVolume * idxTimeStep;
        i < baseY + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to coronal view
int SelectPhaseXVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  // skip slices for PhaseY which comes first
  size_t baseX = seriesPhase.size()/3;

  for ( size_t i = baseX + sizeVolume * idxTimeStep;
        i < baseX + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

// orthogonal to sagittal view
int SelectPhaseZVolume( size_t idxTimeStep, size_t numberOfTimeStep,
                        const std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                        ReaderType::FileNamesContainer &filenames )
{
  size_t sizeVolume = seriesPhase.size() / (3 * numberOfTimeStep);
  size_t baseX = seriesPhase.size()/3;
  size_t baseZ = baseX*2;

  for ( size_t i = baseZ + sizeVolume * idxTimeStep;
        i < baseZ + sizeVolume * ( idxTimeStep + 1 ); i++ ) 
    {
    filenames.insert( filenames.begin(), seriesPhase[i].m_Path );
    //filenames.push_back( seriesPhase[i].m_Path );
    }
  return 0;
}

int TestReadOneTimeStep( size_t idxTimeStep, size_t numberOfTimeStep,
                         const std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                         const std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                         float velocityEncoding )
{
  ReaderType::FileNamesContainer filenamesMag, 
    filenamesX, filenamesY, filenamesZ;
  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;

  std::cout << "Reading image data for time step " <<  idxTimeStep << " ...\n";

  filenamesMag.reserve( sizeVolume );
  SelectMagnitudeVolume( idxTimeStep, numberOfTimeStep,
                         seriesMagnitude, filenamesMag );

  std::cout << "There are " << filenamesMag.size() << " selected for magnitude volume\n";
  
  for ( int i = 0; i < filenamesMag.size(); i++ )
    {
    std::cout << "cp " << filenamesMag[i] << " /tmp/mag" << std::endl;
    }

  filenamesX.reserve( sizeVolume );
  SelectPhaseXVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesX );

  for ( int i = 0; i < filenamesX.size(); i++ )
    {
    std::cout << "cp " << filenamesX[i] << " /tmp/phaseX" << std::endl;
    }

  filenamesY.reserve( sizeVolume );
  SelectPhaseYVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesY );

  for ( int i = 0; i < filenamesY.size(); i++ )
    {
    std::cout << "cp " << filenamesY[i] << " /tmp/phaseY" << std::endl;
    }

  filenamesZ.reserve( sizeVolume );
  SelectPhaseZVolume( idxTimeStep, numberOfTimeStep,
                      seriesPhase, filenamesZ );

  for ( int i = 0; i < filenamesZ.size(); i++ )
    {
    std::cout << "cp " << filenamesZ[i] << " /tmp/phaseZ" << std::endl;
    }

  typedef itk::FlipImageFilter<itk::PhaseContrast3DImage> FlipFilterType;
  FlipFilterType::Pointer flipper = FlipFilterType::New();
  bool flipAxes[3] = { true, true, true };
  flipper->SetFlipAxes(flipAxes);
  bool flipAxisY = false;

  itk::PhaseContrast3DImage::Pointer imgMag = ReadSeries3D( filenamesMag );
  if ( flipAxisY )
    {
    flipper->SetInput( imgMag );
    WriteImage3D( flipper->GetOutput(), "siem_mag.vtk" );
    }
  else
    {
    WriteImage3D( imgMag, "siem_mag.vtk" );
    }

  RescaleFilterType::Pointer rescaler = RescaleFilterType::New();
  rescaler->SetOutputMinimum( -velocityEncoding );
  rescaler->SetOutputMaximum( +velocityEncoding );

  itk::PhaseContrast3DImage::Pointer imgPhaseX = ReadSeries3D( filenamesX );
  rescaler->SetInput( imgPhaseX );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_X.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_X.vtk" );
    }

  itk::PhaseContrast3DImage::Pointer imgPhaseY = ReadSeries3D( filenamesY );
  rescaler->SetInput( imgPhaseY );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_Y.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_Y.vtk" );
    }

  itk::PhaseContrast3DImage::Pointer imgPhaseZ = ReadSeries3D( filenamesZ );
  rescaler->SetInput( imgPhaseZ );
  if ( flipAxisY )
    {
    flipper->SetInput( rescaler->GetOutput() );
    WriteImage3D( flipper->GetOutput(), "siem_Z.vtk" );
    }
  else
    {
    WriteImage3D( rescaler->GetOutput(), "siem_Z.vtk" );
    }

  return 0;
}

int InitAorta4DStudy( const char *studyDir,
                      float minVelocity, float maxVelocity,
                      float velocityEncoding,
                      size_t numberOfTimeSteps,
                      float lengthTimeStep)
{
  create_directory( studyDir );
  path headerPath( studyDir );
  headerPath /= "header.sth";
  std::ofstream ofs( headerPath.c_str() );
  if ( ofs.good() )
    {
    ofs << "time_step_count " << numberOfTimeSteps << " "
        << "venc " << velocityEncoding << " "
        << "patient_orientation " << "HFS" << " "
        << "vector_mag_min " << minVelocity << " "
        << "vector_mag_max " << maxVelocity << " ";
    ofs << "length_time_step " << lengthTimeStep;
    ofs <<  std::endl;
    } 
  ofs.close();
  return 0;
}

void Write3DImageToStudy( itk::PhaseContrast3DImage::ConstPointer image,
                          const char *dirOutput, const char *patternName,
                          size_t t = 0 )
{
    path pathOutput( dirOutput );
    pathOutput /= patternName;
    std::string outputName( pathOutput.string().c_str() );
    std::stringstream ts; ts << t;
    boost::replace_all( outputName, "%t", ts.str() );
    Writer3DType::Pointer writer = Writer3DType::New();
    writer->SetInput( image );
    writer->SetFileName( outputName );
    writer->Update();
}

struct PreprocessParameters
{
  bool m_negateX;
  bool m_negateY;
  bool m_negateZ;

  void Print( )
  {
    std::cout << "Preprocess phase directions:\n";
    std::cout << (m_negateX?"do":"don't") << " negate X\n";
    std::cout << (m_negateY?"do":"don't") << " negate Y\n";
    std::cout << (m_negateZ?"do":"don't") << " negate Z\n";
  }

  pcmr::StatusType ReadFromDICOM( const char* dcm )
  {
    pcmr::StatusType status;
    gdcm::Reader reader;

    this->m_negateX = false;
    this->m_negateY = false;
    this->m_negateZ = true;

    reader.SetFileName( dcm );
    if( !reader.Read() )
      {
      std::cerr << "Could not read: " << dcm << std::endl;
      return pcmr::ST_NOREAD;
      }
    gdcm::File &file = reader.GetFile();
    gdcm::DataSet &ds = file.GetDataSet();
  
    // This is the name of the protocol in Creu Blanca
    const char *protocolCreuBlanca = "mm_fs4DMRI";
    status = pcmr::CheckProtocolName( ds, protocolCreuBlanca );
    if ( status == pcmr::OK )
      {
      this->m_negateX = true;
      this->m_negateY = true;
      }
    else
      {
      std::cerr <<  "Error '" << pcmr::GetStatusDescription( status ) 
                << "' while reading '" << dcm << "'\n";
      }
    return status;
  }
};

int TestExportStudy( const char * dirOutput,
                     size_t numberOfTimeStep,
                     float velocityEncoding,
                     const std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                     const std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase )
{
  std::cout << "Starting TestExportStudy\n";

  PreprocessParameters preprocessP;

  std::vector<itk::PhaseContrast3DImage::Pointer> arrayMag;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseX;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseY;
  std::vector<itk::PhaseContrast3DImage::Pointer> arrayPhaseZ;

  TileFilterType::Pointer tilers[4];
  TileFilterType::LayoutArrayType layout;
  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 1;
  layout[3] = 0;
  
  for ( size_t i = 0; i < 4; i++ )
    {
    tilers[i] = TileFilterType::New();
    tilers[i]->SetLayout( layout );
    }

  size_t sizeVolume = seriesMagnitude.size() / numberOfTimeStep;
  ReaderType::FileNamesContainer filenamesMag, 
    filenamesX, filenamesY, filenamesZ;

  filenamesMag.reserve( sizeVolume );
  filenamesX.reserve( sizeVolume );
  filenamesY.reserve( sizeVolume );
  filenamesZ.reserve( sizeVolume );

  preprocessP.ReadFromDICOM( seriesPhase[0].m_Path.c_str() );
  preprocessP.Print();
  for ( size_t ts = 0; ts < numberOfTimeStep; ts++ )
    {
    std::cout << " Extracting timestep number " << ts << std::endl;

    // read magnitude for this timestep
    filenamesMag.clear();
    SelectMagnitudeVolume( ts, numberOfTimeStep,
                           seriesMagnitude, filenamesMag );
    itk::PhaseContrast3DImage::Pointer imgMag = ReadSeries3D( filenamesMag );
    imgMag->Update();
    tilers[0]->SetInput( ts, imgMag );

    // read v_x component for this timestep
    filenamesX.clear();
    SelectPhaseXVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesX );
    itk::PhaseContrast3DImage::Pointer imgPhaseX = ReadSeries3D( filenamesX );
    RescaleFilterType::Pointer rescalerX = RescaleFilterType::New();
    rescalerX->SetOutputMinimum( -velocityEncoding );
    rescalerX->SetOutputMaximum( +velocityEncoding );
    rescalerX->SetInput( imgPhaseX );
    if ( preprocessP.m_negateX )
      {
      NegateImage3DType::Pointer negateX = NegateImage3DType::New();
      negateX->SetInput( rescalerX->GetOutput() );
      negateX->Update();
      tilers[1]->SetInput( ts, negateX->GetOutput() );
      }
    else 
      {
      rescalerX->Update();
      tilers[1]->SetInput( ts, rescalerX->GetOutput() );
      }

    // read v_y component for this timestep
    filenamesY.clear();
    SelectPhaseYVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesY );
    itk::PhaseContrast3DImage::Pointer imgPhaseY = ReadSeries3D( filenamesY );
    RescaleFilterType::Pointer rescalerY = RescaleFilterType::New();
    rescalerY->SetOutputMinimum( -velocityEncoding );
    rescalerY->SetOutputMaximum( +velocityEncoding );
    rescalerY->SetInput( imgPhaseY );
    if ( preprocessP.m_negateY )
      {
      NegateImage3DType::Pointer negateY = NegateImage3DType::New();
      negateY->SetInput( rescalerY->GetOutput() );
      negateY->Update();
      tilers[2]->SetInput( ts, negateY->GetOutput() );
      }
    else
      {
      rescalerY->Update();
      tilers[2]->SetInput( ts, rescalerY->GetOutput() );
      }

    // read v_z component for this timestep
    filenamesZ.clear();
    SelectPhaseZVolume( ts, numberOfTimeStep,
                        seriesPhase, filenamesZ );
    itk::PhaseContrast3DImage::Pointer imgPhaseZ = ReadSeries3D( filenamesZ );
    RescaleFilterType::Pointer rescalerZ = RescaleFilterType::New();
    rescalerZ->SetOutputMinimum( -velocityEncoding );
    rescalerZ->SetOutputMaximum( +velocityEncoding );
    rescalerZ->SetInput( imgPhaseZ );
    if ( preprocessP.m_negateZ )
      {
      NegateImage3DType::Pointer negateZ = NegateImage3DType::New();
      negateZ->SetInput( rescalerZ->GetOutput() );
      negateZ->Update();
      tilers[3]->SetInput( ts, negateZ->GetOutput() );
      }
    else
      {
      rescalerZ->Update();
      tilers[3]->SetInput( ts, rescalerZ->GetOutput() );
      }
    }
  
  std::cout << "Computing time averaged PCMRA filter ...\n";
  itk::PhaseContrastTimeAveragedImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeAveragedImageFilter::New();
  //itk::PhaseContrastTimeStdDevImageFilter::Pointer PCMRAFilter = itk::PhaseContrastTimeStdDevImageFilter::New();
  PCMRAFilter->SetNoiseMaskThreshold( 0.01 );
  PCMRAFilter->SetMagnitudeImage( tilers[0]->GetOutput() );
  PCMRAFilter->SetPhaseXImage   ( tilers[1]->GetOutput() );
  PCMRAFilter->SetPhaseYImage   ( tilers[2]->GetOutput() );
  PCMRAFilter->SetPhaseZImage   ( tilers[3]->GetOutput() );
  PCMRAFilter->Update();
  PCMRAFilter->Print( std::cout );
  /*
  std::cout << "X=" << tilers[1]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() ) 
            << "\n";
  std::cout << "Y=" << tilers[2]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() )
            << "\n";
  std::cout << "Z=" << tilers[3]->GetOutput()->GetPixel( PCMRAFilter->GetMaximumVelocityIndex() )
            << "\n";
  */
  if ( InitAorta4DStudy( dirOutput, 0, velocityEncoding, velocityEncoding,
                         numberOfTimeStep, seriesPhase[0].m_TimeStep ) == 0 )
    {
    Write3DImageToStudy( PCMRAFilter->GetOutput(), dirOutput, "premask.vtk" );
    for ( size_t ts = 0; ts < numberOfTimeStep; ts++ )
      {
       Write3DImageToStudy( tilers[0]->GetInput( ts ), dirOutput,
                            "mag_%t.vtk", ts );
       Write3DImageToStudy( tilers[1]->GetInput( ts ), dirOutput,
                            "vct_%t_0.vtk", ts );
       Write3DImageToStudy( tilers[2]->GetInput( ts ), dirOutput,
                            "vct_%t_1.vtk", ts );
       Write3DImageToStudy( tilers[3]->GetInput( ts ), dirOutput,
                            "vct_%t_2.vtk", ts );
      }
    }
  return 0;
}

pcmr::StatusType SiemensPCMR_ScanDICOMDIR( const char * dicomDir,
                                           pcmr::Siemens::PCStudyInfo &pcmrStudyInfoFromMagnitude,
                                           pcmr::Siemens::PCStudyInfo &pcmrStudyInfoFromPhase,
                                           std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                                           std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                                           size_t &numberOfSeries,
                                           size_t &numberOfImages )
{
  numberOfSeries = 0;
  numberOfImages = 0;
  // Instanciate the reader:
  gdcm::Reader reader;
  /*
  std::ifstream inputStream;
  inputStream.open( dicomDir, std::ios::binary );
  reader.SetStream( inputStream );*/
  reader.SetFileName( dicomDir );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << dicomDir << std::endl;
    return pcmr::ST_NOREAD;
    }
  std::stringstream strStream;
  path pathDcmRoot( path(dicomDir).parent_path() );

  std::cout << "pathDcmRoot: '" << pathDcmRoot << "'\n";
  std::cout.flush();
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  gdcm::FileMetaInformation &fmi = file.GetHeader();

  gdcm::MediaStorage ms;
  ms.SetFromFile(file);
  if( ms != gdcm::MediaStorage::MediaStorageDirectoryStorage )
    {
    std::cout << "This file is not a DICOMDIR" << std::endl;
    return pcmr::INV_DICOMDIR;
    }

  if (fmi.FindDataElement( gdcm::Tag (0x0002, 0x0002)))
    {  
    strStream.str("");
    fmi.GetDataElement( gdcm::Tag (0x0002, 0x0002) ).GetValue().Print(strStream);
    std::cout << fmi.GetDataElement( gdcm::Tag (0x0002, 0x0002) ) << "\n";
    }
  else
    {
    std::cerr << " Media Storage Sop Class UID not present" << std::cout;
    }

  //TODO il faut trimer strm.str() avant la comparaison au cas ou...
  if ("1.2.840.10008.1.3.10"!=strStream.str())
    {
    std::cout << "This file is not a DICOMDIR" << std::endl;
    return pcmr::INV_DICOMDIR;
    }

  // (0004,1220) SQ (Sequence with explicit length #=1887)   # 1512498, 1 DirectoryRecordSequence
  gdcm::Tag tsq_DirectoryRecord(0x0004,0x1220);
  if( !ds.FindDataElement( tsq_DirectoryRecord ) )
    {
    std::cout << "Fatal error: DirectoryRecordSequence not found\n";
    return pcmr::NO_DRECSEQ;
    }
  const gdcm::DataElement &sq_DirectoryRecord = ds.GetDataElement( tsq_DirectoryRecord );
  const gdcm::SmartPointer<gdcm::SequenceOfItems> sqi = sq_DirectoryRecord.GetValueAsSQ();
  if ( !sqi )
    {
    std::cout << "Fatal error: DirectoryRecordSequence found but it's empty\n";
    return pcmr::DRECSEQ_EMPTY;
    }
  
  std::cout << "DirectoryRecordSequence has " << sqi->GetNumberOfItems() << " number of entries\n";

  typedef enum
  {
    RT_DONTCARE,
    RT_SERIES,
    RT_IMAGE
  } DcmDirRecordType;

  DcmDirRecordType previousItemType = RT_DONTCARE;
  pcmr::ComplexImageComponentType collectImage = pcmr::CIC_UNKNOWN;
  
  for ( size_t i = 0; i < sqi->GetNumberOfItems(); i++ )
    {
    const gdcm::Item & item = sqi->GetItem( i + 1 );
    strStream.str("");
    if (item.FindDataElement(gdcm::Tag (0x0004, 0x1430)))
      {
      item.GetDataElement(gdcm::Tag (0x0004, 0x1430)).GetValue().Print(strStream);
      }
    if ( !STRING_CMP( strStream.str(), "SERIES" ) )
      {
      ++numberOfSeries;
      previousItemType = RT_SERIES;
      collectImage = pcmr::CIC_UNKNOWN;
      }
    else if ( !STRING_CMP( strStream.str(), "IMAGE" ) )
      {
      ++numberOfImages;
      strStream.str( "" );
      if ( item.FindDataElement( gdcm::Tag(0x0004, 0x1500) ) )
        {
        item.GetDataElement(gdcm::Tag(0x0004, 0x1500)).GetValue().Print(strStream); 
        }
      else
        {
        std::cout << "Error: there is an image with a path 0x0004,0x1500 unspecified\n";
        continue;
        }
      
      path pathImage( pathDcmRoot );
      std::string pathRel( strStream.str() );
      std::replace( pathRel.begin(), pathRel.end(), '\\', '/' );
      pathImage /= pathRel;
      // process first image from this serie
      if ( previousItemType == RT_SERIES )
        {
        // check if it is a 4D flow or magnitude series
        pcmr::Siemens::PCStudyInfo studyInfo;
        pcmr::StatusType status;
        //pcmr::ComplexImageComponentType iType = TryPcmrDicom( pathImage.c_str(), studyInfo );
        status = studyInfo.ReadFromDICOM( pathImage.string().c_str() );
        if ( status == pcmr::OK )
          {
          if ( studyInfo.GetImageComponentType() == pcmr::MAGNITUDE )
            {
            collectImage = pcmr::MAGNITUDE;
            pcmrStudyInfoFromMagnitude = studyInfo;
            }
          else if ( studyInfo.GetImageComponentType() == pcmr::PHASE )
            {
            collectImage = pcmr::PHASE;
            pcmrStudyInfoFromPhase = studyInfo;
            }
          }
        }
      if ( collectImage == pcmr::MAGNITUDE )
        {
        // insert in the collection of magnitude images
        // std::cout << pathImage << std::endl;
        InsertNewPcmrSlice( pathImage.string().c_str(), seriesMagnitude );
        }
      else if ( collectImage == pcmr::PHASE )
        {
        // insert in the collection of flow images
        InsertNewPcmrSlice( pathImage.string().c_str(), seriesPhase );
        }
      previousItemType = RT_IMAGE;
      }
    else
      {
      // interrupt series
      collectImage = pcmr::CIC_UNKNOWN;
      previousItemType = RT_DONTCARE;
      }
    }
  std::cout << "There were found " << seriesMagnitude.size() 
            << " slices of magnitude\n";
  std::cout << "There were found " << seriesPhase.size() 
            << " slices of phase\n";

  if ( !seriesMagnitude.size() || !seriesPhase.size() )
    {
    std::cerr << "No magnitude or phase found during directory scan at: \"" 
              << dicomDir << "\"" << std::endl;
    return pcmr::DCM_NO_PM;
    }
  std::cout << "Sorting series ...\n";
  std::sort( seriesMagnitude.begin(), seriesMagnitude.end() );
  std::sort( seriesPhase.begin(), seriesPhase.end() );
  std::cout << "Series sorted!\n";

  return pcmr::OK;
}

// this should be a function of the directory scanner
pcmr::StatusType SiemensPCMR_TryDICOM( const char *dcm,
                                       std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                                       std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                                       size_t &numberOfSeries,
                                       size_t &numberOfImages )
{
  pcmr::StatusType status;
  gdcm::Reader reader;
  /*
  std::ifstream inputStream;
  inputStream.open( dcm, std::ios::binary );
  reader.SetStream( inputStream );*/
  reader.SetFileName( dcm );
  if( !reader.Read() )
    {
    std::cout << "Could not read: \"" << dcm << "\"" << std::endl;
    return pcmr::OK;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  if ( pcmr::IsDICOMDIR( file ) )
    {
    std::cout << "Found DICOMDIR: \"" << dcm << "\"" << std::endl; 
    return pcmr::OK;
    }

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  
  status = pcmr::CheckManufacturer( ds, "SIEMENS" );
  if ( status != pcmr::OK )
    {
    std::cerr << "Not a SIEMENS DICOM: "
              << pcmr::GetStatusDescription(status) << ", while reading \"" << dcm << "\"" << std::endl;
    return status;
    }
  
  if ( !pcmr::HasPixelData( ds ) )
    {
    return pcmr::OK;
    }
  ++numberOfImages;
  // check if it is a 4D flow or magnitude series
  pcmr::ComplexImageComponentType imageType;
  pcmr::Siemens::PCSliceInfo sliceInfo;
  status = sliceInfo.ReadFromDataSet( ds, imageType );
  if ( status == pcmr::OK )
    {
    sliceInfo.m_Path = dcm;
    if ( imageType == pcmr::MAGNITUDE )
      {
      seriesMagnitude.push_back( sliceInfo );
      }
    else if ( imageType == pcmr::PHASE )
      {
      seriesPhase.push_back( sliceInfo );
      }
    }
  return pcmr::OK;
}

pcmr::StatusType SiemensPCMR_ScanDirectory0( const char * dir,
                                         std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                                         std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                                         size_t &numberOfSeries,
                                         size_t &numberOfImages )
{
  path pathDir( dir );
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for ( directory_iterator it = directory_iterator( pathDir );
        it != directory_iterator(); it++ )
    {
    pcmr::StatusType status;
    if ( is_directory( it->status() ) )
      {
      status = SiemensPCMR_ScanDirectory0( it->path().string().c_str(),
                                           seriesMagnitude,
                                           seriesPhase,
                                           numberOfSeries,
                                           numberOfImages );                                              
      }
    else
      {
      status = SiemensPCMR_TryDICOM( it->path().string().c_str(),
                                     seriesMagnitude,
                                     seriesPhase,
                                     numberOfSeries,
                                     numberOfImages );
      }
    if ( status != pcmr::OK )
        {
        std::cout << "aborted directory scanning at file " 
                  << it->path() 
                  << ": " << pcmr::GetStatusDescription( status ) << std::endl;
        return status;
        }
    }
  
  return pcmr::OK;
}

pcmr::StatusType SiemensPCMR_ScanDirectory( const char *dir,
                                            pcmr::Siemens::PCStudyInfo &pcmrStudyInfoFromMagnitude,
                                            pcmr::Siemens::PCStudyInfo &pcmrStudyInfoFromPhase,
                                            std::vector<pcmr::Siemens::PCSliceInfo> &seriesMagnitude,
                                            std::vector<pcmr::Siemens::PCSliceInfo> &seriesPhase,
                                            size_t &numberOfSeries,
                                            size_t &numberOfImages )
{
  pcmr::StatusType status = SiemensPCMR_ScanDirectory0( dir, seriesMagnitude, seriesPhase,
                                                        numberOfSeries, numberOfImages );
  if ( status != pcmr::OK )  
    {
    return status;
    }
  std::cout << "seriesMagnitude.size() = " << seriesMagnitude.size() << "\n";
  std::cout << "seriesPhase.size() = " << seriesPhase.size() << "\n";
  if ( !seriesMagnitude.size() || !seriesPhase.size() )
    {
    std::cerr << "No magnitude or phase found during directory scan at: \"" 
              << dir << "\"" << std::endl;
    return pcmr::DCM_NO_PM;
    }
  pcmrStudyInfoFromMagnitude.ReadFromDICOM( seriesMagnitude[0].m_Path.c_str() );
  std::cout << "pcmrStudyInfoFromMagnitude:\n";
  pcmrStudyInfoFromMagnitude.Print();
  pcmrStudyInfoFromPhase.ReadFromDICOM( seriesPhase[1].m_Path.c_str() );
  std::cout << "pcmrStudyInfoFromPhase:\n";
  pcmrStudyInfoFromPhase.Print();

  std::cout << "Sorting series ...\n";
  std::sort( seriesMagnitude.begin(), seriesMagnitude.end() );
  std::sort( seriesPhase.begin(), seriesPhase.end() );
  std::cout << "Series sorted!\n";
  return pcmr::OK;
}

int main( int argc, char *argv[] )
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " InputDir ?DirectoryStudy?" << std::endl;
    return 1;
    }
  const char *inputDir = argv[1];
  
  // check if the directory exists
  path pathInputDir( inputDir );
  if ( !exists( pathInputDir ) )
    {
    std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return 1;
    }
  if ( !is_directory( pathInputDir ) ) 
    {
    std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return 1;
    }

  // check if the directory contains a DICOMDIR
  path pathDICOMDIR( pathInputDir );
  pathDICOMDIR /= "DICOMDIR";

  size_t numberOfSeries = 0;
  size_t numberOfImages = 0;

  pcmr::Siemens::PCStudyInfo pcmrStudyInfoFromMagnitude;
  pcmr::Siemens::PCStudyInfo pcmrStudyInfoFromPhase;
  std::vector<pcmr::Siemens::PCSliceInfo> seriesMagnitude;
  std::vector<pcmr::Siemens::PCSliceInfo> seriesPhase;

  if ( exists( pathDICOMDIR ) )
    {
    if ( SiemensPCMR_ScanDICOMDIR( pathDICOMDIR.string().c_str(),
                                   pcmrStudyInfoFromMagnitude,
                                   pcmrStudyInfoFromPhase,
                                   seriesMagnitude, seriesPhase,
                                   numberOfSeries, numberOfImages ) != pcmr::OK )
      {
      return -1;
      }
    }
  else
    {
    if ( SiemensPCMR_ScanDirectory( inputDir,
                                    pcmrStudyInfoFromMagnitude,
                                    pcmrStudyInfoFromPhase,
                                    seriesMagnitude, seriesPhase,
                                    numberOfSeries, numberOfImages ) != pcmr::OK )
      {
      std::cout << "Failed scanning directory of DICOM's from \"" << inputDir <<"\"" << std::endl;
      return -1;
      }
    }

  std::cout << "Info: there are " << numberOfSeries << " series\n";
  std::cout << "Info: there are " << numberOfImages << " images\n";
  std::cout << "Info: there are " << seriesMagnitude.size() << " images M\n";
  std::cout << "Info: there are " << seriesPhase.size() << " images P\n";

  // let's try to load a volume and write it back to disk in vtk
  // format
  
  //PrintSlices( seriesMagnitude, seriesMagnitude.size()-10, seriesMagnitude.size()-1 );
  PrintSlices( seriesPhase, 0, 0 );
  PrintSlices( seriesPhase, seriesPhase.size()/3+1, seriesPhase.size()/3+1 );
  PrintSlices( seriesPhase, seriesPhase.size()-1, seriesPhase.size()-1 );
  if ( pcmrStudyInfoFromPhase.m_VelocityEncoding <= 0.0 ) 
    {
    pcmrStudyInfoFromPhase.m_VelocityEncoding = 150.0;
    }
  pcmrStudyInfoFromPhase.Print();
  if ( argc > 2 ) 
    {
    TestExportStudy( argv[2], pcmrStudyInfoFromMagnitude.m_NumberOfVolumes,
                     pcmrStudyInfoFromPhase.m_VelocityEncoding,
                     seriesMagnitude, seriesPhase );
    }
  else
    {
    TestReadOneTimeStep( 3, pcmrStudyInfoFromMagnitude.m_NumberOfVolumes,
                         seriesMagnitude, seriesPhase,
                         pcmrStudyInfoFromPhase.m_VelocityEncoding );
    }

  return 0;
}
