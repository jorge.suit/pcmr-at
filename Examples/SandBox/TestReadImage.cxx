#include "itkImage.h"
#include "itkGDCMImageIO.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkPermuteAxesImageFilter.h"
#include "itkExtractImageFilter.h"

int main( int argc, char *argv[] )
{
  // Validate input parameters
  if( argc < 2 )
    {
      std::cerr << "Usage: " 
                << argv[0]
                << " DICOM_File"
                << std::endl;
      return EXIT_FAILURE;
    }

  typedef float PixelType;
  typedef itk::Image< PixelType, 3 >
    ImageType3D;
  typedef itk::Image< PixelType, 4 >
    ImageType4D;
  typedef itk::ImageFileReader< ImageType4D >
    ReaderType;
  typedef itk::ImageFileWriter< ImageType3D >
    WriterType;
  typedef itk::GDCMImageIO
    ImageIOType;
  
  ImageIOType::Pointer gdcmIO = ImageIOType::New();
  ReaderType::Pointer reader = ReaderType::New();

  std::cout << "Reading image\n";
  reader->SetImageIO( gdcmIO );
  reader->SetFileName( argv[1] );

  try
    {
      reader->Update();
    }
  catch (itk::ExceptionObject &excp)
    {
      std::cerr << "Exception thrown while reading the series" << std::endl;
      std::cerr << excp << std::endl;
      return EXIT_FAILURE;
    }
  std::cout << "Image read\n";

  ImageType4D::Pointer img = reader->GetOutput();
  ImageType4D::RegionType R0 = img->GetLargestPossibleRegion();
  ImageType4D::RegionType R1( R0 );
  // ImageType::SizeValueType s_t, s_z;

  R1.SetSize( 2, 25 );
  R1.SetSize( 3, 82 );
  img->SetRegions( R1 );
  img->Update();
  img->GetLargestPossibleRegion().Print( std::cout );

  typedef itk::ExtractImageFilter< ImageType4D, ImageType4D > Extract4DType;
  Extract4DType::Pointer splitMP = Extract4DType::New();

  ImageType4D::IndexType startComp;
  startComp.Fill(0);
  startComp[3] = 41;
 
  ImageType4D::SizeType sizeComp;
  sizeComp = img->GetLargestPossibleRegion().GetSize();
  sizeComp[3] = 41;
  
  ImageType4D::RegionType regionComp( startComp, sizeComp );

  splitMP->SetExtractionRegion( regionComp );
  splitMP->SetInput( img );
#if ITK_VERSION_MAJOR >= 4
  splitMP->SetDirectionCollapseToIdentity(); // This is required.
#endif
  splitMP->Update();
  ImageType4D::Pointer imgComp = splitMP->GetOutput();
  imgComp->DisconnectPipeline();
  imgComp->GetLargestPossibleRegion().Print( std::cout );

  typedef itk::PermuteAxesImageFilter<ImageType4D> PermuterType;
  PermuterType::PermuteOrderArrayType order;

  PermuterType::Pointer permuter = PermuterType::New();
  order[0] = 0;
  order[1] = 1;
  order[2] = 3;
  order[3] = 2;

  std::cout << "Permuting axis\n";
  
  permuter->SetOrder( order );
  permuter->SetInput( imgComp );
  permuter->Update();

  ImageType4D::Pointer imgp = permuter->GetOutput();
  imgp->DisconnectPipeline();
  imgp->GetLargestPossibleRegion().Print( std::cout );

  std::cout << "Extracting 3D volumen\n";
  
  typedef itk::ExtractImageFilter< ImageType4D, ImageType3D > Extract3DType;
  Extract3DType::Pointer extractVOL = Extract3DType::New();

  ImageType4D::IndexType desiredStart = imgp->GetLargestPossibleRegion().GetIndex();
  desiredStart[3] += 6;
 
  ImageType4D::SizeType desiredSize;
  desiredSize = imgp->GetLargestPossibleRegion().GetSize();
  desiredSize[3] = 0;
  
  ImageType4D::RegionType desiredRegion(desiredStart, desiredSize);

  extractVOL->SetExtractionRegion( desiredRegion );
  extractVOL->SetInput( imgp );
#if ITK_VERSION_MAJOR >= 4
  extractVOL->SetDirectionCollapseToIdentity(); // This is required.
#endif
  extractVOL->Update();

  std::cout << "3D volume extracted\n";
  extractVOL->GetOutput()->GetLargestPossibleRegion().Print( std::cout );

  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( extractVOL->GetOutput() );
  writer->SetFileName( "roi_sample.vtk" );
  writer->Update();

   /*
  typedef itk::RegionOfInterestImageFilter< ImageType, ImageType > ROIType;

  ImageType::SizeType inSize = reader->GetOutput()->GetLargestPossibleRegion().GetSize();

  ImageType::IndexType start = reader->GetOutput()->GetLargestPossibleRegion().GetIndex();
  start[ 2 ] = 1025 ;
  ImageType::SizeType size = inSize;
  size[ 2 ] = 25;

  ImageType::RegionType desiredRegion;
  desiredRegion.SetSize(size);
  desiredRegion.SetIndex(start);
 
  ROIType::Pointer filter = ROIType::New();
  
  filter->SetRegionOfInterest(desiredRegion);
  filter->SetInput(reader->GetOutput());
  filter->Update();
  
  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( filter->GetOutput() );
  writer->SetFileName( "roi_sample.vtk" );
  writer->Update();
  */
  return 0;
}
