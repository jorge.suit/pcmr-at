/*=========================================================================

  Program: GDCM (Grassroots DICOM). A DICOM library

  Copyright (c) 2006-2011 Mathieu Malaterre
  All rights reserved.
  See Copyright.txt or http://gdcm.sourceforge.net/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*
 * This example is ... guess what this is for :)
 */

#include "gdcmReader.h"
#include "gdcmWriter.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"

#include <iostream>


template< int TVR, int TVM >
int TryTag( uint16_t group, uint16_t element,
            gdcm::DataSet &ds,
            gdcm::Element<TVR,TVM> &el )
{
  gdcm::VR vr = gdcm::Element<TVR,TVM>::GetVR();
  const char* vrString = gdcm::VR::GetVRString( vr );
  gdcm::Tag tag( group, element );
  if( !ds.FindDataElement( tag ) ) {
    std::cout << "No aparece el tag " << tag << "\n";
    return 0;
  }
  std::cout << "El tag " << tag << " ha sido encontrado\n";
  const gdcm::DataElement& data = ds.GetDataElement( tag );
  //gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el2;
  el.SetFromDataElement( data );
  return 1;
}


int main(int argc, char *argv[])
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " input.dcm" << std::endl;
    return 1;
    }
  const char *filename = argv[1];

  /*
  // (2001,101a) FL 200\0\0                #  12, 3 PCVelocity
  const gdcm::DictEntry                                                 \
    PhilipsTagPCVelocity( 0x2001, 0x101a, "FL", "1-n", "PCVelocity" );
  */
  
  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << filename << std::endl;
    return 1;
    }

  // If we reach here, we know for sure only 1 thing:
  // It is a valid DICOM file (potentially an old ACR-NEMA 1.0/2.0 file)
  // (Maybe, it's NOT a Dicom image -could be a DICOMDIR, a RTSTRUCT, etc-)

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  // (0008,0008) CS [DERIVED\PRIMARY\FLOW_ENCODED\MIXED]     #  34, 4 ImageType
  // http://www.dabsoft.ch/dicom/6/6/
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM4> el_ImageType;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM4>( 0x0008, 0x0008, ds, el_ImageType ) ) {
    el_ImageType.Print( std::cout ); std::cout << "\n";
  }

  // (0008,0016) UI =EnhancedMRImageStorage              #  28, 1 SOPClassUID
  // http://www.dabsoft.ch/dicom/4/B.5/
  // http://www.dabsoft.ch/dicom/6/6/
  gdcm::Element<gdcm::VR::UI,gdcm::VM::VM1> el_MediaStorage;
  if ( TryTag<gdcm::VR::UI,gdcm::VM::VM1>( 0x0008, 0x0016, ds, el_MediaStorage ) ) {
    std::cout << "'"; el_MediaStorage.Print( std::cout ); std::cout << "'\n";
  }

  // (0008,0060) CS [MR]                                     #   2, 1 Modality
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0060)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_Modality;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x0060, ds, el_Modality ) ) {
    el_Modality.Print( std::cout ); std::cout << "\n";
  }

  // (0008,0070) LO [Philips Medical Systems]          #  24, 1 Manufacturer
  // http://www.dabsoft.ch/dicom/6/6/#(0008,0070)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_Manufacturer;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x0070, ds,
                                           el_Manufacturer ) ) {
    el_Manufacturer.Print( std::cout ); std::cout << "\n";
  }

  //(0008,1090) LO [Achieva]                    #   8, 1 ManufacturersModelName
  // http://www.dabsoft.ch/dicom/6/6/#(0008,1090)
  gdcm::Element<gdcm::VR::LO,gdcm::VM::VM1> el_ManufacturersModelName;
  if ( TryTag<gdcm::VR::LO,gdcm::VM::VM1>( 0x0008, 0x1090, ds,
                                           el_ManufacturersModelName ) ) {
    el_ManufacturersModelName.Print( std::cout ); std::cout << "\n";
  }
  
  //(0008,9208) CS [MIXED]                      #   6, 1 ComplexImageComponent
  // http://www.dabsoft.ch/dicom/6/6/#(0008,9208)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_ComplexImageComponent;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9208, ds,
                                           el_ComplexImageComponent ) ) {
    el_ComplexImageComponent.Print( std::cout ); std::cout << "\n";
  }

  //(0008,9209) CS [FLOW_ENCODED]               #  12, 1 AcquisitionContrast
  // http://www.dabsoft.ch/dicom/6/6/#(0008,9209)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_AcquisitionContrast;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0008, 0x9209, ds,
                                           el_AcquisitionContrast ) ) {
    el_AcquisitionContrast.Print( std::cout ); std::cout << "\n";
  }

  //(0018,0023) CS [3D]                         #   2, 1 MRAcquisitionType
  // http://www.dabsoft.ch/dicom/6/6/#(0018,0023)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_MRAcquisitionType;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x0023, ds,
                                           el_MRAcquisitionType ) ) {
    el_MRAcquisitionType.Print( std::cout ); std::cout << "\n";
  }

  //(0018,9014) CS [YES]                        #   4, 1 PhaseContrast
  // http://www.dabsoft.ch/dicom/6/6/#(0018,9014)
  gdcm::Element<gdcm::VR::CS,gdcm::VM::VM1> el_PhaseContrast;
  if ( TryTag<gdcm::VR::CS,gdcm::VM::VM1>( 0x0018, 0x9014, ds,
                                           el_PhaseContrast ) ) {
    el_PhaseContrast.Print( std::cout ); std::cout << "\n";
  }
  
  //(0028,0008) IS [2050]                       #   4, 1 NumberOfFrames
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0008)
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_NumberOfFrames;
  if ( TryTag<gdcm::VR::IS,gdcm::VM::VM1>( 0x0028, 0x0008, ds,
                                           el_NumberOfFrames ) ) {
    el_NumberOfFrames.Print( std::cout ); std::cout << "\n";
  }
  
  //(0028,0010) US 256                          #   2, 1 Rows
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0010)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Rows;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0010, ds,
                                           el_Rows ) ) {
    el_Rows.Print( std::cout ); std::cout << "\n";
  }

  //(0028,0011) US 256                          #   2, 1 Columns
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0011)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_Columns;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0011, ds,
                                           el_Columns ) ) {
    el_Columns.Print( std::cout ); std::cout << "\n";
  }

  //(0028,0100) US 16                           #   2, 1 BitsAllocated
  // http://www.dabsoft.ch/dicom/6/6/#(0028,00100)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_BitsAllocated;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0100, ds,
                                           el_BitsAllocated ) ) {
    el_BitsAllocated.Print( std::cout ); std::cout << "\n";
  }

  //(0028,0101) US 12                           #   2, 1 BitsStored
  // http://www.dabsoft.ch/dicom/6/6/#(0028,00101)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_BitsStored;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0101, ds,
                                           el_BitsStored ) ) {
    el_BitsStored.Print( std::cout ); std::cout << "\n";
  }

  //(0028,0102) US 11                           #   2, 1 HighBit
  // http://www.dabsoft.ch/dicom/6/6/#(0028,0102)
  gdcm::Element<gdcm::VR::US,gdcm::VM::VM1> el_HighBit;
  if ( TryTag<gdcm::VR::US,gdcm::VM::VM1>( 0x0028, 0x0102, ds,
                                           el_HighBit ) ) {
    el_HighBit.Print( std::cout ); std::cout << "\n";
  }

  //(2001,1017) SL 25                           #   4, 1 NumberOfPhasesMR
  // Philips private
  gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfPhasesMR;
  if ( TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1017, ds,
                                           el_NumberOfPhasesMR ) ) {
    el_NumberOfPhasesMR.Print( std::cout ); std::cout << "\n";
  }

  //(2001,1018) SL 41                           #   4, 1 NumberOfSlicesMR
  // Philips private
  gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NumberOfSlicesMR;
  if ( TryTag<gdcm::VR::SL,gdcm::VM::VM1>( 0x2001, 0x1018, ds,
                                           el_NumberOfSlicesMR ) ) {
    el_NumberOfSlicesMR.Print( std::cout ); std::cout << "\n";
  }

  //(2001,101a) FL 0\200\0                      #  12, 3 PCVelocity
  // Philips private
  gdcm::Element<gdcm::VR::FL,gdcm::VM::VM1_n> el_PCVelocity;
  if ( TryTag<gdcm::VR::FL,gdcm::VM::VM1_n>( 0x2001, 0x101a, ds,
                                           el_PCVelocity ) ) {
    el_PCVelocity.Print( std::cout ); std::cout << "\n";
    gdcm::Tag tag( 0x2001, 0x101a );
    const gdcm::DataElement& data = ds.GetDataElement( tag );
    std::cout << "a ver : " << data << "\n";
  }

  
  // Now visit the frames information

  //(5200,9230) SQ (Sequence with undefined length #=2050)  # u/l, 1 PerFrameFunctionalGroupsSequence
  // http://www.dabsoft.ch/dicom/6/6/#(5200,9230)
  gdcm::Tag tsq_PerFrameInfo(0x5200,0x9230);
  if( !ds.FindDataElement( tsq_PerFrameInfo ) )
    {
      std::cout << "No se ha encontrado PerFrameFunctionalGroupsSequence\n";
    }
  else
    {
      const gdcm::DataElement &sq_PerFrameInfo = ds.GetDataElement( tsq_PerFrameInfo );
      // std::cout << sq_PerFrameInfo << std::endl;
      const gdcm::SequenceOfItems *sqi = sq_PerFrameInfo.GetValueAsSQ();
      if( !sqi || !sqi->GetNumberOfItems() )
        {
          std::cout << "No hay SequenceOfItems o la secuencia no tiene items\n";
        }
      else
        {
          std::cout << "La secuencia tiene " << sqi->GetNumberOfItems() << " frames\n";
          // inspect the first item
          const gdcm::Item & item1 = sqi->GetItem(1);
          // std::cout << item1 << std::endl;
          const gdcm::DataSet& nestedds = item1.GetNestedDataSet();
          //std::cout << nestedds << std::endl;
         
        }
    }

  return 0;
}

/*
 * (*) static type, means that extra DICOM information VR & VM are computed at compilation time.
 * The compiler is deducing those values from the template arguments of the class.
 */
