#include "PhilipsPhaseContrastImage.h"
#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"

itk::PhaseContrast3DImage::Pointer
ExtractVolume3D( itk::PhaseContrastImage::Pointer image4D,
                 unsigned int timeStep )
{
  typedef itk::ExtractImageFilter< itk::PhaseContrastImage, itk::PhaseContrast3DImage > Extract3DType;
  Extract3DType::Pointer extractVOL = Extract3DType::New();

  std::cout << "Extracting 3D volumen\n";
  itk::PhaseContrastImage::IndexType desiredStart = image4D->GetLargestPossibleRegion().GetIndex();
  desiredStart[3] += timeStep;
 
  itk::PhaseContrastImage::SizeType desiredSize;
  desiredSize = image4D->GetLargestPossibleRegion().GetSize();
  desiredSize[3] = 0;
  
  itk::PhaseContrastImage::RegionType desiredRegion(desiredStart, desiredSize);

  extractVOL->SetExtractionRegion( desiredRegion );
  extractVOL->SetInput( image4D );
#if ITK_VERSION_MAJOR >= 4
  extractVOL->SetDirectionCollapseToIdentity(); // This is required.
#endif
  extractVOL->Update();
  itk::PhaseContrast3DImage::Pointer output = extractVOL->GetOutput();
  output->DisconnectPipeline();
  std::cout << "3D volume extracted\n";
  return output;
}

int main( int argc, char *argv[] )
{
  // Validate input parameters
  if( argc < 2 )
    {
      std::cerr << "Usage: " 
                << argv[0]
                << " DICOM_File m|p"
                << std::endl;
      return EXIT_FAILURE;
    }
  enum ComponentType { UNKNOWN, MAGNITUDE, PHASE, MIXED };
  const char* componentName[] =
    {
      "unknown", "magnitude", "phase", "mixed"
    };
  ComponentType readComponent = UNKNOWN;
  
  if ( argc > 2 )
    {
      if ( !strcmp( argv[2], "m" ) )
        {
          readComponent = MAGNITUDE;
        }
      else if ( !strcmp( argv[2], "p" ) )
        {
          readComponent = PHASE;
        }
      else if ( !strcmp( argv[2], "b" ) )
        {
          readComponent = MIXED;
        }
      else
        {
          std::cerr << "Usage: " 
                    << argv[0]
                    << " DICOM_File m|p|b"
                    << std::endl;
          return EXIT_FAILURE;
        }
    }
        
  std::cout << "Reading 4D " << componentName[readComponent] << " image ...\n";
  
  itk::PhaseContrastImage::Pointer img4DMagnitude;
  itk::PhaseContrastImage::Pointer img4DPhase;
  //philips4d::RescaleInfoType phaseRescale;
  philips4d::RescalerType phaseRescale( 0.09768009768009/1.75555555555555, -200 );
  switch ( readComponent )
    {
    case MAGNITUDE:
      img4DMagnitude = philips4d::ReadMagnitudeImage( argv[1], 25, 41, 3.0 );
      break;
    case PHASE:
      img4DPhase = philips4d::ReadPhaseImage( argv[1], 25, 41, phaseRescale );
      break;
    case MIXED:
      {
        philips4d::PhaseContrastMixedImageType bothImages = philips4d::ReadMixedImage( argv[1], 25, 41, phaseRescale );
        img4DMagnitude = bothImages.first;
        img4DPhase = bothImages.second;
        break;
      }
    default:
      std::cerr << "unknown component" << std::endl;
      return EXIT_FAILURE;
    }
  if ( readComponent == MAGNITUDE || readComponent == MIXED )
    {
      std::cout << "Magnitude Image:\n";
      std::cout << img4DMagnitude->GetSpacing(); std::cout << "\n";
      img4DMagnitude->GetLargestPossibleRegion().Print( std::cout );
    }
  if ( readComponent == PHASE || readComponent == MIXED )
    {
      std::cout << "Phase Image:\n";
      std::cout << img4DPhase->GetSpacing(); std::cout << "\n";
      img4DPhase->GetLargestPossibleRegion().Print( std::cout );
    }

  typedef itk::ImageFileWriter< itk::PhaseContrast3DImage >
    WriterType;

  WriterType::Pointer writer = WriterType::New();
  if ( readComponent == MAGNITUDE || readComponent == MIXED )
    {
      
      itk::PhaseContrast3DImage::Pointer vol3D = ExtractVolume3D( img4DMagnitude, 6 );
      std::cout << "Volume 3D for manitude image:\n";
      vol3D->GetLargestPossibleRegion().Print( std::cout );
      writer->SetInput( vol3D );
      std::string outputName( "magnitude_6.vtk" );
      writer->SetFileName( outputName );
      writer->Update();
    }
  if ( readComponent == PHASE || readComponent == MIXED )
    {
      
      itk::PhaseContrast3DImage::Pointer vol3D = ExtractVolume3D( img4DPhase, 6 );
      std::cout << "Volume 3D for phase image:\n";
      vol3D->GetLargestPossibleRegion().Print( std::cout );
      writer->SetInput( vol3D );
      std::string outputName( "phase_6.vtk" );
      writer->SetFileName( outputName );
      writer->Update();
    }

  return 0;
}
