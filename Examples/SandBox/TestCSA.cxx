#include "pcmrSiemensGDCMIO.h"

#include "gdcmReader.h"
#include "gdcmImageReader.h"
#include "gdcmImageWriter.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"

#include <boost/filesystem.hpp>
using namespace boost::filesystem;

#include <boost/unordered_map.hpp>
#include <boost/foreach.hpp>

pcmr::StatusType CSADumpElement( const char *name, gdcm::CSAHeader &csa )
{
  return pcmr::OK;
}


int TestOneFile( const char * fileName )
{
  pcmr::StatusType status;
  gdcm::Reader reader;
  reader.SetFileName( fileName );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << fileName << std::endl;
    return 1;
    }

  gdcm::CSAHeader csa;
  gdcm::DataSet& ds = reader.GetFile().GetDataSet();

  gdcm::PrivateTag t1( csa.GetCSAImageHeaderInfoTag() );
  std::cout << t1 << std::endl;

  /*
  gdcm::PrivateTag t2( csa.GetCSASeriesHeaderInfoTag() );
  std::cout << t2 << std::endl;

  gdcm::PrivateTag t3( csa.GetCSADataInfo() );
  std::cout << t3 << std::endl;
  */
  gdcm::PrivateTag &reqTag = t1;

  const char * reqTagArg = "1";
  
  /*
  if ( argc > 2 )
    {
     reqTagArg = argv[2];
    }

  if ( !strcmp( reqTagArg, "1" ) )
    {
    reqTag = t1;
    }
  else if ( !strcmp( reqTagArg, "2" ) )
    {
    reqTag = t2;
    }
  else if ( !strcmp( reqTagArg, "3" ) )
    {
    reqTag = t3;
    }
  else 
    {
    std::cout << "Unknown requested TAG " << reqTagArg << "\n";
    }

  std::cout << "tag requested " << reqTag << std::endl;
  */
  if( ds.FindDataElement( reqTag ) )
    {
    csa.LoadFromDataElement( ds.GetDataElement( reqTag ) );
    csa.Print( std::cout );
    }
  else
    {
    std::cout << "CSAHeader tag not found " << reqTag << "\n";
    return -1;
    }

  // FlowVenc
  float venc = -1.0;
  status = pcmr::Siemens::GetCSAVelocityEncoding( csa, venc );
  if ( status == pcmr::TAG_NOTFOUND )
    {
    std::cerr << "FlowVenc not found\n";
    }
  else
    {
    std::cout << "FlowVenc = ";
    if ( status == pcmr::CSA_ITEM_EMPTY ) 
      {
      std::cout << "<empty>" << std::endl;
      }
    else
      {
      std::cout << venc << std::endl;
      }
    }

  // FlowEncodingDirection
  int direction = -1;
  status = pcmr::Siemens::GetCSAEncodingDirection( csa, direction );
  if ( status == pcmr::TAG_NOTFOUND )
    {
    std::cerr << "FlowEncodingDirection not found\n";
    }
  else
    {
    std::cout << "FlowEncodingDirection = ";
    if ( status == pcmr::CSA_ITEM_EMPTY ) 
      {
      std::cout << "<empty>" << std::endl;
      }
    else
      {
      std::cout << direction << std::endl;
      }
    }

  // PhaseEncodingDirectionPositive
  int isPositive = -1;
  status = pcmr::Siemens::GetCSADirectionPositive( csa, isPositive );
  if ( status == pcmr::TAG_NOTFOUND )
    {
    std::cerr << "PhaseEncodingDirectionPositive not found\n";
    }
  else
    {
    std::cout << "PhaseEncodingDirectionPositive = ";
    if ( status == pcmr::CSA_ITEM_EMPTY ) 
      {
      std::cout << "<empty>" << std::endl;
      }
    else
      {
      std::cout << isPositive << std::endl;
      }
    }
  return 0;
}

int TestScanDirectory( const char * dirName,
                       boost::unordered_map<int,int> &dirMap,
                       size_t &countNotFound )
{
  path pathDir( dirName );
  std::cout << "Scanning directory \"" << dirName << "\"" << std::endl;
  for ( directory_iterator it = directory_iterator( pathDir );
        it != directory_iterator(); it++ )
    {
    int status;
    if ( is_directory( it->status() ) )
      {
      status = TestScanDirectory( it->path().c_str(),
                                  dirMap, countNotFound );                                              
      }
    else
      {
      status = 0;
      gdcm::Reader reader;
      reader.SetFileName( it->path().c_str() );
      if( !reader.Read() )
        {
        std::cerr << "Failed to read: " << it->path().c_str() << std::endl;
        continue;
        }
      gdcm::CSAHeader csa;
      gdcm::DataSet& ds = reader.GetFile().GetDataSet();
      
      gdcm::PrivateTag t1( csa.GetCSAImageHeaderInfoTag() );
      if( ds.FindDataElement( t1 ) )
        {
        csa.LoadFromDataElement( ds.GetDataElement( t1 ) );
        int direction = -1;
        if ( pcmr::Siemens::GetCSAEncodingDirection( csa, direction ) == pcmr::OK )
          {
          if ( dirMap.find( direction ) == dirMap.end() )
            {
            std::cout << "found direction value: " << direction << std::endl;
            dirMap[ direction ] = 0;
            }
          ++dirMap[ direction ];
          }
        else
          {
          ++countNotFound;
          }
        }
      else
        {
        std::cout << "CSAHeader tag not found " 
                  << t1 << ", in " << it->path().c_str() << std::endl;
        continue;
        }
      }
    }
  return 0;
}

int TestScanDirectory( const char * dirName )
{
  typedef boost::unordered_map<int,int> map_t;
  map_t dirMap;
  size_t countNotFound = 0;

  int status = TestScanDirectory( dirName, dirMap, countNotFound );

  if ( status == 0 )
    {
    std::cout << "There where " << countNotFound << " with no direction defined\n";

    BOOST_FOREACH( map_t::value_type &item, dirMap) 
      {
      std::cout<<item.first<<","<<item.second<<"\n";
      }
    
    } 
}

int main(int argc, path::value_type *argv [])
{
  if( argc < 2 )
    {
    std::cout << "usage:" << std::endl;
    std::cout << argv[0] << " dicom file | directory" << std::endl;
    return 1;
    }

  path pathInput( argv[1] );
  if ( !exists( pathInput ) )
    {
    std::cerr << "Input path \"" << argv[1] << "\" does not exists" << std::endl;
    return 1;
    }
  if ( is_directory( pathInput ) ) 
    {
    return TestScanDirectory( pathInput.c_str() );
    }
  else
    {
    return TestOneFile( pathInput.c_str() );
    }

  return 0;
}
