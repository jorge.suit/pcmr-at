#include "pcmrPhilipsGDCMIO.h"
#include "gdcmImageHelper.h"
#include "gdcmStreamImageReader.h"
#include "gdcmImageRegionReader.h"
#include "gdcmBoxRegion.h"

#include "itkImage.h"
#include "itkImportImageFilter.h"
#include "itkScalarToRGBColormapImageFilter.h"
#include "itkImageFileWriter.h"

void PrintFrame( const pcmr::Philips::EnhancedMRI &emri, int idx )
{
  const pcmr::Philips::FrameType *aFrame;

  aFrame = emri.GetFrameInfo( idx );
  if ( aFrame )
    {
      std::cout << "FrameInfo: " << idx + 1 << "\n";
      aFrame->Print( std::cout, 2 ); std::cout << "\n";
    }
}

template <class T>
void DumpStdVector(const std::vector<T> & v)
{
  std::cout << "(";
  for (size_t i = 0; i < v.size(); i++)
    {
    if (i > 0) 
      std::cout << ",";
    std::cout << v[i];
    }
  std::cout << ")\n";

}

template <class PixelType>
int WriteBufferImage(const char* fileName,
                     PixelType *buffer, size_t resx, size_t resy,
                     double sx, double sy)
{
  const unsigned int Dimension = 2;
  typedef itk::Image<PixelType, Dimension > ImageType;
  typedef itk::ImportImageFilter< PixelType, Dimension > ImportFilterType;
 
  typename ImportFilterType::Pointer importFilter = ImportFilterType::New();
 
  typename ImportFilterType::SizeType  size;
 
  size[0]  = resx;  // size along X
  size[1]  = resy;  // size along Y
  const unsigned int numberOfPixels = resx * resy;
 
  typename ImportFilterType::IndexType start;
  start.Fill( 0 );
 
  typename ImportFilterType::RegionType region;
  region.SetIndex( start );
  region.SetSize(  size  );
 
  importFilter->SetRegion( region );
 
  double origin[Dimension];
  origin[0] = 0.0;    // X coordinate
  origin[1] = 0.0;    // Y coordinate
 
  importFilter->SetOrigin( origin );
 
  double spacing[Dimension];
  spacing[0] = sx;    // along X direction
  spacing[1] = sy;    // along Y direction
 
  importFilter->SetSpacing(spacing);  
  const bool importImageFilterWillOwnTheBuffer = false;
  importFilter->SetImportPointer(buffer, numberOfPixels,
                                 importImageFilterWillOwnTheBuffer);

  typedef itk::RGBPixel<unsigned char>    RGBPixelType;
  typedef itk::Image<RGBPixelType, 2>  RGBImageType;
  typedef itk::ScalarToRGBColormapImageFilter<ImageType, RGBImageType> RGBFilterType;
  typename RGBFilterType::Pointer rgbFilter = RGBFilterType::New();
  rgbFilter->SetInput(importFilter->GetOutput());
  rgbFilter->SetColormap( RGBFilterType::Grey );
 
  typedef itk::ImageFileWriter<RGBImageType> WriterType;
  typename WriterType::Pointer writer = WriterType::New();
 
  writer->SetFileName(fileName);
 
  writer->SetInput(rgbFilter->GetOutput());
  writer->Update();
 
  return EXIT_SUCCESS;
}

int main( int argc, const char* argv[] )
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " dicom" << std::endl;
    return 1;
    }
  const char *filename = argv[1];
#if defined(USE_STREAM_IMAGE_READER)
  gdcm::StreamImageReader reader;
#else
  gdcm::ImageRegionReader reader;
#endif
  reader.SetFileName( filename );

#if defined(USE_STREAM_IMAGE_READER)
  if (!reader.ReadImageInformation())
#else
  if (!reader.ReadInformation())
#endif
    {
    std::cerr << "unable to read image information" << std::endl;
    return 1; //unable to read tags as expected.
    }

  // Get file infos
  const gdcm::File &file = reader.GetFile();
  
  // the dataset is the the set of element we are interested in:
  const gdcm::DataSet &ds = file.GetDataSet();

  pcmr::Philips::EnhancedMRI emri;

  pcmr::StatusType status = emri.ReadDataSet(ds);

  emri.Print( std::cout ); std::cout << "\n";
  PrintFrame( emri, 0 );

  // get some info about image
  std::vector<unsigned int> dimension = gdcm::ImageHelper::GetDimensionsValue(file);
  DumpStdVector(dimension);

  std::vector<double> spacing = gdcm::ImageHelper::GetSpacingValue(file);
  DumpStdVector(spacing);

  gdcm::PixelFormat pf = gdcm::ImageHelper::GetPixelFormatValue(file);
  int pixelsize = pf.GetPixelSize();
  std::cout << "ScalarType = " << pf.GetScalarTypeAsString() << std::endl,
  std::cout << "pixel size = " << pixelsize << "\n";

  char *buffer = NULL;
  std::cout << "Computed buffer size: " << (dimension[0] * dimension[1] * pixelsize) << std::endl;

#if defined(USE_STREAM_IMAGE_READER)
#else
  gdcm::BoxRegion box;
  size_t idealSlice = emri.GetNumberOfSlicesMR()/2;
  size_t idealTimeStep = 3;
  size_t idealFrameIndex = idealSlice * emri.GetNumberOfPhases() + idealTimeStep;
  std::cout << "Going to read frame at index " << idealFrameIndex << std::endl;
  box.SetDomain(0, emri.GetNumberOfColumns()-1, 0, emri.GetNumberOfRows()-1,
                idealFrameIndex, idealFrameIndex);
  reader.SetRegion(box);
  size_t buffer_len = reader.ComputeBufferLength();
  buffer = new char[buffer_len];
  std::cout << "Requested buffer size: " << buffer_len << std::endl;
  if (reader.ReadIntoBuffer(buffer, buffer_len))
    {
    std::cout << "OK\n";
    }
  else
    {
    std::cout << "FAIL\n";
    }
#endif

  switch (pf.GetScalarType())
    {
    case gdcm::PixelFormat::UINT16:
      WriteBufferImage<uint16_t>("/tmp/test.png",
                                 reinterpret_cast<uint16_t*>(buffer),
                                 dimension[0], dimension[1],
                                 spacing[0], spacing[1]);
        break;
    default:
      std::cout << "Pixel format not implemented\n";
    }

  return 0;
}
