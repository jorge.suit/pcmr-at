#include "pcmrSiemensImporter.h"

int main(int argc, const char* argv[])
{
  std::cout << "Format Version = " <<  pcmr::BaseImporter::GetFormatVersion() << std::endl;

  if( argc < 3 || (argc > 3 && argc != 7) )
    {
    std::cout << "Usage: " 
              << argv[0]
              << " Sequence_Directory"
              << " Output_Directory"
			  << " ?minX maxX minY maxY?"
              << std::endl;
    return EXIT_FAILURE;
    }
  
  pcmr::Siemens::Importer::Pointer siemensImporter = pcmr::Siemens::Importer::New();
  siemensImporter->SetSequenceDirectory(argv[1]);
  int _minX = 0;
  int _maxX = 0;
  int _minY = 0;
  int _maxY = 0;

  if ( argc > 3 ) 
    {
    _minX = atoi( argv[3] );
    _maxX = atoi( argv[4] );
    _minY = atoi( argv[5] );
    _maxY = atoi( argv[6] );
    if (_minX<0 || _maxX < _minX || _minY<0 || _maxY < _minY)
      {
      std::cout << "invalid XY ROI" << std::endl;
      return EXIT_FAILURE;;
      }
    }
  const unsigned int minX = static_cast<unsigned int>(_minX);
  const unsigned int maxX = static_cast<unsigned int>(_maxX);
  const unsigned int minY = static_cast<unsigned int>(_minY);
  const unsigned int maxY = static_cast<unsigned int>(_maxY);
  siemensImporter->SetOutputROI(minX, maxX, minY, maxY);

  pcmr::StatusType status;

  status = siemensImporter->ReadInformation();
  if (status != pcmr::OK)
    {
    std::cout << "FAIL: " << pcmr::GetStatusDescription(status) << std::endl;
    return status;
    }
  status = siemensImporter->WriteStudy(argv[2]);
  if (status != pcmr::OK)
    {
    std::cout << "FAIL: " << pcmr::GetStatusDescription(status) << std::endl;
    return status;
    }

  return 0;
}
