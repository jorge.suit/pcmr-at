#include <iostream>
#include "pcmrFlow4DReader.h"
#include "itkVTKImageToImageFilter.h"
#include "itkComposeImageFilter.h"
#include "itkVectorMagnitudeImageFilter.h"
#include "itkImageFileWriter.h"

int main( int argc, char *argv[] )
{
  if( argc != 4 )
    {
    std::cerr << "Usage: " << argv[0] << " <InputStudy> <TimeStep> <OutputImage>" << std::endl;
    return EXIT_FAILURE;
    }
  const char * inputStudy = argv[1];
  pcmr::Flow4DReader::Pointer dataSet = pcmr::Flow4DReader::New();
  pcmr::StatusType status = dataSet->SetDirectory(inputStudy);
  if (status != pcmr::OK)
    {
    std::cerr << "Could not open study: " 
              << pcmr::GetStatusDescription(status) << std::endl;
    return EXIT_FAILURE;
    }
  typedef itk::VTKImageToImageFilter<itk::PhaseContrast3DImage> ImporterType;
  ImporterType::Pointer importerX = ImporterType::New();
  ImporterType::Pointer importerY = ImporterType::New();
  ImporterType::Pointer importerZ = ImporterType::New();

  int ts = atoi(argv[2]);
  if (ts < 0 || ts >= dataSet->GetNumberOfTimeSteps())
    {
    std::cerr << "Invalid time-step " << ts 
              << ". Must be in the range [0.." 
              << dataSet->GetNumberOfTimeSteps() << "]\n";
    return EXIT_FAILURE;
    }

  vtkImageData *vtkImgX = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowX,
                                                ts);
  vtkImageData *vtkImgY = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowY,
                                                ts);
  vtkImageData *vtkImgZ = dataSet->GetFlowImage(pcmr::Flow4DReader::FlowZ,
                                                ts);
  importerX->SetInput(vtkImgX);
  importerY->SetInput(vtkImgY);
  importerZ->SetInput(vtkImgZ);

  typedef itk::ComposeImageFilter<itk::PhaseContrast3DImage,itk::PhaseContrastVector3DImage> ComposeFilter;
  ComposeFilter::Pointer composer = ComposeFilter::New();
  composer->SetInput(0, importerX->GetOutput());
  composer->SetInput(1, importerY->GetOutput());
  composer->SetInput(2, importerZ->GetOutput());

  typedef itk::VectorMagnitudeImageFilter<itk::PhaseContrastVector3DImage,itk::PhaseContrast3DImage> MagnitudeFilterType;
  MagnitudeFilterType::Pointer magnitudeFilter = MagnitudeFilterType::New();
  magnitudeFilter->SetInput(composer->GetOutput());

  const char * outputImage = argv[3];
  typedef itk::ImageFileWriter<itk::PhaseContrast3DImage> WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetInput(magnitudeFilter->GetOutput() );
  writer->SetFileName( outputImage );

  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & error )
    {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
