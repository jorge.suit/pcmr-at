#include "pcmrGDCMIO.h"
#include "pcmrPhilipsGDCMIO.h"
#include <boost/filesystem.hpp>
#include <iostream>

using namespace boost::filesystem;

pcmr::StatusType PhilipsPCMR_TryEnhancedDICOM(const char *filename,
                                              std::string &pathFlowX,
                                              std::string &pathFlowY,
                                              std::string &pathFlowZ)
{
  pcmr::StatusType status;
  gdcm::Reader reader;
  reader.SetFileName(filename);
  if(!reader.Read())
    {
    std::cout << "Could not read: \"" << filename << "\"" << std::endl;
    return pcmr::OK;
    }
  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  if (pcmr::IsDICOMDIR(file))
    {
    std::cout << "Found DICOMDIR: \"" << filename << "\"" << std::endl; 
    return pcmr::OK;
    }
  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();
  if ((status=pcmr::Philips::EnhancedMRI::CheckManufacturer(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckManufacturer: " 
              << pcmr::GetStatusDescription(status) << std::endl;
    return pcmr::OK;
    }
  if ((status=pcmr::Philips::EnhancedMRI::CheckEnhancedMRImageType(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckEnhancedMRImageType: " 
              << pcmr::GetStatusDescription(status) << std::endl;
    return pcmr::OK;
    }
  if ((status=pcmr::Philips::EnhancedMRI::CheckEnhancedMRSOPClassUID(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckEnhancedMRSOPClassUID: " 
              << pcmr::GetStatusDescription(status) << std::endl;
    return pcmr::OK;
    }
  if ( (status=pcmr::Philips::EnhancedMRI::CheckModalityMR(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckModalityMR: " 
              << pcmr::GetStatusDescription(status) << std::endl;
      return pcmr::OK;
    }
  if ( (status=pcmr::Philips::EnhancedMRI::CheckFlowEncodeAcquisition(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckFlowEncodeAcquisition: " 
              << pcmr::GetStatusDescription(status) << std::endl;
      return pcmr::OK;
    }
  if ( (status=pcmr::Philips::EnhancedMRI::CheckMRAcquisitionType3D(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckMRAcquisitionType3D: " 
              << pcmr::GetStatusDescription(status) << std::endl;
      return pcmr::OK;
    }
  if ( (status=pcmr::Philips::EnhancedMRI::CheckPhaseContrast(ds)) != pcmr::OK )
    {
    std::cout << "EnhancedMRI::CheckPhaseContrast: " 
              << pcmr::GetStatusDescription(status) << std::endl;
      return pcmr::OK;
    }
  float pcv[3];
  if ((status=pcmr::Philips::EnhancedMRI::ReadPCVelocity(ds,pcv)) != pcmr::OK )
    {
    return status;
    }
  int i;
  for (i = 0; i < 3; i++)
    {
    if (pcv[i] != 0.0)
      {
      break;
      }
    }
  if (i == 3)
    {
    return pcmr::NULL_PCV;
    }
  // REVIEW: this index mapping does not correspond to the actual flow
  // component. 
  // FH:     0     0    150     (this is phase Y sequence "IM_0005")
  // AP:     0     150  0       (this is phase X sequence "IM_0001")
  // RL:     150  0     0       (this is phase Z sequence "IM_0003")
  switch (i)
    {
    case 0:
      if (pathFlowZ == "")
        { 
        pathFlowZ = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: Z component already found at file " << pathFlowZ << ", while processing file " << filename << std::endl;
        }
      break;
    case 1:
      if (pathFlowX == "")
        { 
        pathFlowX = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: X component already found at file " << pathFlowX << ", while processing file " << filename << std::endl;
        }
      break;
    default:
      if (pathFlowY == "")
        { 
        pathFlowY = filename;
        }
      else
        {
        std::cout << "PhilipsPCMR_TryEnhancedDICOM: Y component already found at file " << pathFlowY << ", while processing file " << filename << std::endl;
        }
      break;
    }
  return pcmr::OK;
}

pcmr::StatusType PhilipsPCMR_ScanDirectory0(const char *dir,
                                            std::string &pathFlowX,
                                            std::string &pathFlowY,
                                            std::string &pathFlowZ)
{
  path pathDir(dir);
  std::cout << "Scanning directory \"" << dir << "\"" << std::endl;
  for (directory_iterator it = directory_iterator(pathDir);
       it != directory_iterator(); it++)
    {
    pcmr::StatusType status;
    if (is_directory(it->status()))
      {
      status = PhilipsPCMR_ScanDirectory0(it->path().string().c_str(),
                                          pathFlowX,
                                          pathFlowY,
                                          pathFlowZ);
      }
    else
      {
      status = PhilipsPCMR_TryEnhancedDICOM(it->path().string().c_str(),
                                            pathFlowX,
                                            pathFlowY,
                                            pathFlowZ);
      }
    if (status != pcmr::OK)
      {
      std::cout << "aborted directory scanning at file " 
                << it->path() 
                << ": " << pcmr::GetStatusDescription(status) << std::endl;
      return status;
      }
    }
  return pcmr::OK;
}

pcmr::StatusType PhilipsPCMR_ScanDirectory(const char *dir,
                                           std::string &pathFlowX,
                                           std::string &pathFlowY,
                                           std::string &pathFlowZ)
{
  pathFlowX = "";
  pathFlowY = "";
  pathFlowZ = "";
  pcmr::StatusType status = PhilipsPCMR_ScanDirectory0(dir,
                                                       pathFlowX,
                                                       pathFlowY,
                                                       pathFlowZ);
  if (pathFlowX == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: X component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  if (pathFlowY == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: Y component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  if (pathFlowZ == "")
    {
    std::cout << "PhilipsPCMR_ScanDirectory: Z component not found\n";
    return pcmr::NO_ENHANCEDMRI;
    }
  return pcmr::OK;
}

int main( int argc, char *argv[] )
{
  if( argc != 2 )
    {
    std::cerr << argv[0] << " InputDir" << std::endl;
    return 1;
    }
  const char *inputDir = argv[1];
  
  // check if the directory exists
  path pathInputDir(inputDir);
  if (!exists(pathInputDir))
    {
    std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return 1;
    }
  if (!is_directory(pathInputDir)) 
    {
    std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return 1;
    }
  std::string pathFlowX, pathFlowY, pathFlowZ;

  pcmr::StatusType status = PhilipsPCMR_ScanDirectory(pathInputDir.string().c_str(),
                                                      pathFlowX,
                                                      pathFlowY,
                                                      pathFlowZ);
  if (status == pcmr::OK)
    {
    std::cout << "X component path: " << pathFlowX << std::endl
              << "Y component path: " << pathFlowY << std::endl
              << "Z component path: " << pathFlowZ << std::endl;
    }
  else
    {
    std::cout << "PhilipsPCMR_ScanDirectory: "
              << pcmr::GetStatusDescription(status) << std::endl;
    }
  return 0;
}
