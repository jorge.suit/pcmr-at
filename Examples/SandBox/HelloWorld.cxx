/*=========================================================================

  Program: GDCM (Grassroots DICOM). A DICOM library

  Copyright (c) 2006-2011 Mathieu Malaterre
  All rights reserved.
  See Copyright.txt or http://gdcm.sourceforge.net/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*
 * This example is ... guess what this is for :)
 */

#include "gdcmReader.h"
#include "gdcmWriter.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"


#include <iostream>

/*
template< int TVR, int TVM >
int TryTag( uint16_t group, uint16_t element,
            gdcm::Element<TVR,TVM> &el )
{
  gdcm::VR vr = gdcm::Element<TVR,TVM>::GetVR();
  const char* vrString = gdcm::VR::GetVRString( vr );
  gdcm::Tag tag( group, element );
  if( !ds.FindDataElement( tag ) ) {
    std::cout << "No aparece el tag " << tag_NOF << "\n";
    return 0;
  }
  std::cout << "El tag " << tag_NOF << " ha sido encontrado\n";
  const gdcm::DataElement& data_NOF = ds.GetDataElement( tag_NOF );
  //gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el2;
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_NOF;
  el_NOF.SetFromDataElement( data_NOF );
  el_NOF.Print( std::cout ); std::cout << "\n";
  */

int main(int argc, char *argv[])
{
  if( argc < 2 )
    {
    std::cerr << argv[0] << " input.dcm" << std::endl;
    return 1;
    }
  const char *filename = argv[1];

  /*
  // (2001,101a) FL 200\0\0                #  12, 3 PCVelocity
  const gdcm::DictEntry                                                 \
    PhilipsTagPCVelocity( 0x2001, 0x101a, "FL", "1-n", "PCVelocity" );
  */
  
  // Instanciate the reader:
  gdcm::Reader reader;
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
    std::cerr << "Could not read: " << filename << std::endl;
    return 1;
    }

  // If we reach here, we know for sure only 1 thing:
  // It is a valid DICOM file (potentially an old ACR-NEMA 1.0/2.0 file)
  // (Maybe, it's NOT a Dicom image -could be a DICOMDIR, a RTSTRUCT, etc-)

  // The output of gdcm::Reader is a gdcm::File
  gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  gdcm::DataSet &ds = file.GetDataSet();

  // ds.Print( std::cout );

  /*
  gdcm::DataSet::ConstIterator iter = ds.Begin();
  int i = 0;
  for( ; iter != ds.End(); iter++ ) {
    ++i;
    std::cout << *iter << "\n";
    const gdcm::ByteValue *bvalue = iter->GetByteValue();
    if ( bvalue ) {
      std::cout << bvalue->GetPointer() << "\n";
    }
    std::cout << iter->GetVL() << "," << iter->GetVR() << "\n";
    
  }

  std::cout << "Hay " << i << " elements\n";
  */
  
  //gdcm::Attribute<0x2001,0x101a,gdcm::VR::FL,gdcm::VM::VM1_n> at;
  gdcm::Tag tagPCVelocity( 0x2001, 0x101a );

  std::cout << tagPCVelocity << "\n" ;
  if( !ds.FindDataElement( tagPCVelocity ) ) {
    std::cout << "No aparece el tag de velocidad\n";
    return 1;
  }
  std::cout << "El tag de velocidad ha sido encontrado\n";
  const gdcm::DataElement& data = ds.GetDataElement( tagPCVelocity );
  gdcm::Element<gdcm::VR::FL,gdcm::VM::VM1_n> el;
  el.SetFromDataElement( data );
  std::cout << el.GetLength() << " -- " << el.GetValue() << std::endl;
  for ( int i = 0; i < el.GetLength(); i++ ) {
    const gdcm::VRToType<gdcm::VR::FL>::Type &vc = el.GetValue( i );
    if ( i ) std::cout << "\\";
    std::cout << vc;
  }
  std::cout << "\n";

  //gdcm::Attribute<0x2001,0x101a,gdcm::VR::FL,gdcm::VM::VM1_n> at;
  // gdcm::Tag tagFrame( 0x2001, 0x1017 );
  gdcm::Tag tag_NOF( 0x0028, 0x0008 );
  if( !ds.FindDataElement( tag_NOF ) ) {
    std::cout << "No aparece el tag " << tag_NOF << "\n";
    return 1;
  }
  std::cout << "El tag " << tag_NOF << " ha sido encontrado\n";
  const gdcm::DataElement& data_NOF = ds.GetDataElement( tag_NOF );
  //gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el2;
  gdcm::Element<gdcm::VR::IS,gdcm::VM::VM1> el_NOF;
  el_NOF.SetFromDataElement( data_NOF );
  el_NOF.Print( std::cout ); std::cout << "\n";

  // gdcm::Tag tagFrame( 0x2001, 0x1017 );
  gdcm::Tag tag_NOPC( 0x2001, 0x1017 );
  if( !ds.FindDataElement( tag_NOPC ) ) {
    std::cout << "No aparece el tag " << tag_NOPC << "\n";
    return 1;
  }
  std::cout << "El tag " << tag_NOPC << " ha sido encontrado\n";
  const gdcm::DataElement& data_NOPC = ds.GetDataElement( tag_NOPC );
  gdcm::Element<gdcm::VR::SL,gdcm::VM::VM1> el_NOPC;
  el_NOPC.SetFromDataElement( data_NOPC );
  el_NOPC.Print( std::cout ); std::cout << "\n";

  /*
           (0020,000e) UI [1.3.46.670589.11.35047.5.0.6940.2012032612014407158] #  52, 1 SeriesInstanceUID
    (0020,000d) UI [1.3.51.0.1.1.193.146.12.142.1652827.1652797] #  44, 1 StudyInstanceUID
        (0020,000e) UI [1.3.46.670589.11.35047.5.0.6940.2012032611512165888] #  52, 1 SeriesInstanceUID
    (0020,000d) UI [1.3.51.0.1.1.193.146.12.142.1652827.1652797] #  44, 1 StudyInstanceUID
        (0020,000e) UI [1.3.46.670589.11.35047.5.0.6940.2012032611585451055] #  52, 1 SeriesInstanceUID
    (0020,000d) UI [1.3.51.0.1.1.193.146.12.142.1652827.1652797] #  44, 1 StudyInstanceUID
(0020,000d) UI [1.3.51.0.1.1.193.146.12.142.1652827.1652797] #  44, 1 StudyInstanceUID
(0020,000e) UI [0.0.0.0.3.14427.8103.1338279680.12319.8323329] #  46, 1 SeriesInstanceUID
(0020,0010) SH [1652827]                                #   8, 1 StudyID
(0020,0011) IS [8103]                                   #   4, 1 SeriesNumber
(0020,0012) IS [11]                                     #   2, 1 AcquisitionNumber
(0020,0013) IS [1532]                                   #   4, 1 InstanceNumber
(0020,0032) DS [-32.391847610473\-163.05447387695\160]  #  38, 3 ImagePositionPatient
(0020,0037) DS [0.34354436397552\0.93913644552230\0\0\0\-1] #  42, 6 ImageOrientationPatient
(0020,0052) UI [1.3.46.670589.11.35047.5.0.9896.2012032611501638004] #  52, 1 FrameOfReferenceUID
(0020,0060) CS (no value available)                     #   0, 0 Laterality
(0020,0100) IS [1]                                      #   2, 1 TemporalPositionIdentifier
(0020,0105) IS [1]                                      #   2, 1 NumberOfTemporalPositions
(0020,0242) UI [1.3.46.670589.11.35047.5.20.1.1.6940.2012032612534476385] #  56, 1 Unknown Tag & Data
(0020,1040) LO (no value available)                     #   0, 0 PositionReferenceIndicator
(0020,4000) LT [THE OUTPUT OF THIS SOFTWARE IS FOR INVESTIGATIONAL USE ONLY - NOT ... # 110, 1 ImageComments
(0020,9056) SH [1]                                      #   2, 1 StackID
(0020,9057) UL 21                                       #   4, 1 InStackPositionNumber
(0020,9072) CS [U]                                      #   2, 1 FrameLaterality
(0020,9128) UL 1                                        #   4, 1 TemporalPositionIndex
(0020,9153) FD 290                                      #   8, 1 TriggerDelayTime
(0020,9157) UL 1\21\4\3\7                               #  20, 5 DimensionIndexValues
(0020,9161) UI [0.0.0.0.7.14427.8103.1.1338279680.12319.8323329] #  48, 1 ConcatenationUID
(0020,9162) US 1532                                     #   2, 1 InConcatenationNumber
(0020,9163) US 2050                                     #   2, 1 InConcatenationTotalNumber
(0020,9221) SQ (Sequence with undefined length #=1)     # u/l, 1 DimensionOrganizationSequence
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
(0020,9222) SQ (Sequence with undefined length #=5)     # u/l, 1 DimensionIndexSequence
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
    (0020,9165) AT (0020,9056)                              #   4, 1 DimensionIndexPointer
    (0020,9167) AT (0020,9111)                              #   4, 1 FunctionalGroupPointer
    (0020,9421) LO [Stack ID]                               #   8, 1 DimensionDescriptionLabel
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
    (0020,9165) AT (0020,9057)                              #   4, 1 DimensionIndexPointer
    (0020,9167) AT (0020,9111)                              #   4, 1 FunctionalGroupPointer
    (0020,9421) LO [In-Stack Position Number]               #  24, 1 DimensionDescriptionLabel
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
    (0020,9165) AT (2005,106e)                              #   4, 1 DimensionIndexPointer
    (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
    (0020,9213) LO [Philips MR Imaging DD 001]              #  26, 1 DimensionIndexPrivateCreator
    (0020,9238) LO [Philips MR Imaging DD 005]              #  26, 1 FunctionalGroupPrivateCreator
    (0020,9421) LO [Private Scanning Sequence]              #  26, 1 DimensionDescriptionLabel
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
    (0020,9165) AT (2005,1011)                              #   4, 1 DimensionIndexPointer
    (0020,9167) AT (2005,140f)                              #   4, 1 FunctionalGroupPointer
    (0020,9213) LO [Philips MR Imaging DD 001]              #  26, 1 DimensionIndexPrivateCreator
    (0020,9238) LO [Philips MR Imaging DD 005]              #  26, 1 FunctionalGroupPrivateCreator
    (0020,9421) LO [Private ImageTypeMR]                    #  20, 1 DimensionDescriptionLabel
    (0020,9164) UI [1.3.46.670589.11.35047.5.0.2592.2012032613032760000] #  52, 1 DimensionOrganizationUID
    (0020,9165) AT (0020,9153)                              #   4, 1 DimensionIndexPointer
    (0020,9167) AT (0018,9118)                              #   4, 1 FunctionalGroupPointer
    (0020,9421) LO [Trigger Delay Time]                     #  18, 1 DimensionDescriptionLabel
(0020,9228) UL 1531                                     #   4, 1 ConcatenationFrameOffsetNumber
(0020,9251) FD 1200                                     #   8, 1 Unknown Tag & Data
(0020,9254) FD 0                                        #   8, 1 Unknown Tag & Data
(0020,9255) FD 0                                        #   8, 1 Unknown Tag & Data
 */

  
  /*
  at.Set( ds );
  gdcm::Attribute<0x2001,0x101a,gdcm::VR::FL,gdcm::VM::VM1_n>::ArrayType v = at.GetValue();
  std::cout << "PC Velocity=" << v << std::endl;
  */
  return 0;
}

/*
 * (*) static type, means that extra DICOM information VR & VM are computed at compilation time.
 * The compiler is deducing those values from the template arguments of the class.
 */
