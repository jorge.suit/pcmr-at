#include "pcmrPhilipsGDCMIO.h"
#include <boost/filesystem.hpp>
#include <iostream>

using namespace boost::filesystem;

int main( int argc, char *argv[] )
{
  if( argc != 2 )
    {
    std::cerr << argv[0] << " InputDir" << std::endl;
    return 1;
    }
  const char *inputDir = argv[1];
  
  // check if the directory exists
  path pathInputDir(inputDir);
  if (!exists(pathInputDir))
    {
    std::cerr << "Input path \"" << inputDir << "\" does not exists" << std::endl;
    return 1;
    }
  if (!is_directory(pathInputDir)) 
    {
    std::cerr << "Input path \"" << inputDir << "\" is not a directory" << std::endl;
    return 1;
    }
  std::string pathFlowX, pathFlowY, pathFlowZ;

  pcmr::StatusType status = pcmr::Philips::EnhancedMRI::ScanDirectory(pathInputDir.string().c_str(),
                                                                      pathFlowX,
                                                                      pathFlowY,
                                                                      pathFlowZ);
  if (status == pcmr::OK)
    {
    std::cout << "X component path: " << pathFlowX << std::endl
              << "Y component path: " << pathFlowY << std::endl
              << "Z component path: " << pathFlowZ << std::endl;
    }
  else
    {
    std::cout << "EnhancedMRI::ScanDirectory: "
              << pcmr::GetStatusDescription(status) << std::endl;
    }
  return 0;
}
