#include "gdcmDirectoryHelper.h"

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
    std::cout << "Usage: " << argv[0] << " directory\n";
    return 0;
    }
  gdcm::Directory::FilenamesType fn = gdcm::DirectoryHelper::GetMRImageSeriesUIDs(argv[1]);

  std::cout << "There are " << fn.size() << " series UID\n";
  for(gdcm::Directory::FilenamesType::iterator it = fn.begin();
      it != fn.end(); it++)
    {
    std::cout << *it << std::endl;
    }
  return 0;
}
