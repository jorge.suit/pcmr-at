#include "itkObject.h"
#include "pcmrFlow4DReader.h"
#include <iostream>

itk::Object* Flow4Reader_New()
{
  itk::Object::Pointer reader = pcmr::Flow4DReader::New().GetPointer();
  reader->Register();
  reader->Print(std::cout);
  return reader;
}

itk::Object::Pointer Flow4Reader_New1()
{
  itk::Object::Pointer reader = pcmr::Flow4DReader::New().GetPointer();
  return reader;
}

int main(int argc, char *argv[])
{
  itk::Object* (*p1)() = Flow4Reader_New;
  itk::Object::Pointer (*p2)() = Flow4Reader_New1;

  //itk::Object::Pointer reader = cmr::Flow4DReader::New().GetPointer();
  itk::Object::Pointer reader = pcmr::Flow4DReader::New().GetPointer();

  //itk::Object* ptr1 = new pcmr::Flow4DReader;
  itk::Object *ptr2 = (*p1)();

  ptr2->Print(std::cout);
  ptr2->Delete();
  //ptr1->Delete();
 
  itk::Object::Pointer ptr3 = (*p2)();
  pcmr::Flow4DReader *ptr4 = dynamic_cast<pcmr::Flow4DReader*>(ptr3.GetPointer());
  ptr4->Print(std::cout);


  return 0;
}
